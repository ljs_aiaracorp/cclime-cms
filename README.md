## cclime_adminWeb

> Creator: ljs@aiaracorp.com
> Date: 2021/9/24

## Description

- 관리자 페이지입니다.

## Install & Build

- 콘솔(mac, 리눅스는 터미널)에서 명령어를 입력해 설치 및 빌드를 합니다.
- 다음 명령어로 모듈을 설치합니다.

```
npm install
```

- 모듈 설치 후 개발환경으로 실행하기 위해서는 다음 명령어를 실행하면 됩니다.

```
npm run start
```

- 모듈 설치 후 배포를 위한 빌드를 하기 위해서는 다음 명령어를 실행하면 되고, build 폴더에 빌드 결과가 저장됩니다. build 폴더 내의 파일들을 서버에 업로드하면 배포가 완료됩니다.

```
npm run build
```
