import { Form, Col, Row } from "react-bootstrap";

export const text = (txt, size) => {
  return (
    <Col sm={size ? size : 2} style={st.font}>
      {txt}
    </Col>
  );
};

export const fileUpload = (uploadedFile) => {
  return (
    <Form.Group as={Row} className="mb-2">
      {text(uploadedFile.name)}
      <Col sm={2}>
        <Form.File id={uploadedFile.id} label={uploadedFile.label} accept={uploadedFile.accept} custom onChange={uploadedFile.onUpload} />
      </Col>
    </Form.Group>
  );
};

export const textInput = (name, size) => {
  return (
    <Form.Group as={Row}>
      {text(name)}
      <Col sm={size}>
        <Form.Control type="text" placeholder={name} />
      </Col>
    </Form.Group>
  );
};

export const st = {
  leftArrow: {
    width: "30px",
    marginRight: "20px",
    marginBottom: "10px",
    cursor: "pointer",
  },
  userInfo: {
    backgroundColor: "#eaeaea",
    padding: "5px 0px 5px 10px",
    marginBottom: "15px",
  },
  formGroupCtrl: {
    marginBottom: "-15px",
  },
  font: {
    fontSize: "16px",
    marginTop: "6px",
    textAlign: "center",
  },
  btn: {
    width: "100%",
    height: "42px",
  },
  moreBtn: {
    width: "200px",
  },
  participHistory: {
    border: "1px solid gray",
    maxWidth: "25rem",
    padding: "20px",
  },
  time: {
    marginRight: "30px",
  },
  count: {
    fontSize: "27px",
  },
  pt: {
    marginRight: "30px",
  },
  textArea: {
    height: "180px",
    whiteSpace: "pre-line",
  },

  // Conference List
  confList: {
    border: "1px solid gray",
    padding: "10px",
  },
  imgSizing: {
    width: "100px",
    verticalAlign: "middle",
    margin: "0px auto",
  },
  conferenceTitle: {
    marginBottom: "10px",
    fontWeight: "bold",
  },
  title: {
    marginBottom: "10px",
  },
};

export const grayLine = <hr style={{ width: "100%" }} />;
