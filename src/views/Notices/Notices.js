import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import Pagination from "components/Pagination";
import Loading from "components/Loading";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";
import { CheckboxBtnPopup } from "../../components/Popup/CheckboxBtnPopup";

export const Notices = (props) => {
  const user = axiosUrlFunction("");
  const isUser = user.accessPath === "/admin" ? true : false;

  const [selected, setSelected] = useState(1);
  const [pagenum, setPagenum] = useState("");
  const [list, setList] = useState(null);
  const [keyword, setKeyword] = useState("");
  const [count, setCount] = useState(0);
  const [deleteId, setDeleteId] = useState("");
  const [deleteButtonModal, setDeleteButtonModal] = useState(false); // Button Delete Modal
  const [deleteCheckModal, setDeleteCheckModal] = useState(false); // Button Delete Ok bt Check
  useEffect(() => {
    getList();
  }, []);

  const getList = async (params) => {
    setList(null);
    const noticeUrl = axiosUrlFunction("storeNoticeList");

    if (!params) {
      params = {};
    }
    if (params.keyword === undefined) {
      params.keyword = keyword;
    }
    if (params.page === undefined) {
      params.page = selected - 1;
    }
    setPagenum(params.page);

    const queries = [];
    if (params.keyword !== "") {
      queries.push(`word=${params.keyword}`);
    }
    queries.push(`page=${params.page}`);
    queries.push(`size=10`);

    const queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";

    axios
      .get(`${noticeUrl.apiUrl}${queryStr}`, {
        headers: {
          Authorization: `Bearer ${noticeUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        setCount(res.data.data.totalElements);
        setList(res.data.data.content);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getList(params);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // Delete ButtonModal open & close
  const deleteOpenButtonModal = () => {
    setDeleteButtonModal(true);
  };
  const deleteCloseButtonModal = () => {
    setDeleteButtonModal(false);
  };

  // Delete Modal 2
  const deleteCheckButtonModal = () => {
    setDeleteCheckModal(false);
    window.location.replace("/admin/notices/list");
  };

  // 공지사항 삭제
  const deleteData = async () => {
    const deleteUrl = axiosUrlFunction("notice/delete", "검색");
    const data = {
      notice_id: parseInt(deleteId),
    };
    axios
      .put(`${deleteUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${deleteUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then((rse) => {
        deleteCloseButtonModal();
        setDeleteCheckModal(true);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else deleteData();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">공지/안내 관리 - 공지사항</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <Table className="table-hover search-box">
                <tbody>
                  <tr>
                    <th className="select-search-target">
                      <Form.Control as="select">
                        <option value="name">제목</option>
                      </Form.Control>
                    </th>
                    <th>
                      <Form.Control
                        type="text"
                        placeholder="검색"
                        value={keyword}
                        onChange={(e) => setKeyword(e.target.value)}
                        onKeyUp={(e) => {
                          if (e.keyCode === 13) {
                            setSelected(1);
                            getList({
                              page: 0,
                            });
                          }
                        }}
                        maxLength={50}
                        style={{
                          display: "inline-block",
                          width: "500px",
                          verticalAlign: "middle",
                        }}
                      />
                      <Button
                        variant="light"
                        onClick={(e) => {
                          setSelected(1);
                          getList({
                            page: 0,
                          });
                        }}
                        style={{
                          display: "inline-block",
                          border: "solid 1px #E3E3E3",
                          color: "#565656",
                          verticalAlign: "middle",
                        }}
                        className="search-btn"
                      >
                        <FontAwesomeIcon icon={faSearch} />
                      </Button>
                    </th>
                  </tr>
                </tbody>
              </Table>
              {isUser && (
                <div className="text-right">
                  <Button
                    onClick={(e) => {
                      window.location.replace(`/admin/notices/add`);
                    }}
                  >
                    공지 작성
                  </Button>
                </div>
              )}
              <Table className="table-hover mt-0">
                <thead>
                  <tr>
                    <th className="border-0">NO.</th>
                    <th className="border-0">제목</th>
                    <th className="border-0">작성일</th>
                    <th className="border-0"></th>
                  </tr>
                </thead>
                <tbody>
                  {list !== undefined && list !== null ? (
                    list.map((notices, i) => {
                      return (
                        <tr key={i} className="cursor">
                          <td
                            onClick={() => {
                              props.history.push(
                                `${user.accessPath}/notices/view/${notices.id}`
                              );
                            }}
                          >
                            {i + 1 + 10 * pagenum}
                          </td>
                          <td
                            onClick={() => {
                              props.history.push(
                                `${user.accessPath}/notices/view/${notices.id}`
                              );
                            }}
                          >
                            {notices.title}
                          </td>
                          <td
                            onClick={() => {
                              props.history.push(
                                `${user.accessPath}/notices/view/${notices.id}`
                              );
                            }}
                          >
                            {notices.date.slice(0, 10)}
                          </td>
                          {isUser === true ? (
                            <td>
                              <Button
                                className="mr-3"
                                onClick={() => {
                                  props.history.push(
                                    `${user.accessPath}/notices/edit/${notices.id}`
                                  );
                                }}
                              >
                                변경
                              </Button>
                              <Button
                                onClick={() => {
                                  setDeleteId(notices.id);
                                  setDeleteButtonModal(true);
                                }}
                              >
                                삭제
                              </Button>
                            </td>
                          ) : (
                            <td></td>
                          )}
                        </tr>
                      );
                    })
                  ) : (
                    <tr>
                      <td colSpan={4} style={{ textAlign: "center" }}>
                        <Loading />
                      </td>
                    </tr>
                  )}
                </tbody>
              </Table>
              <Pagination
                count={count}
                forcePage={selected - 1}
                selected={(pageSelect) => {
                  setSelected(pageSelect);
                  getList({ page: pageSelect - 1 });
                }}
              />
            </Card.Body>
          </Card>
        </Col>
      </Row>
      {deleteButtonModal && (
        <CheckboxBtnPopup
          onClose={deleteCloseButtonModal}
          onConfirm={true}
          onOk={deleteData}
        >
          정말로 해당 공지를 삭제하시겠습니까?
        </CheckboxBtnPopup>
      )}
      {deleteCheckModal && (
        <CheckboxBtnPopup onClose={deleteCheckButtonModal}>
          해당 정보가 삭제되었습니다.
        </CheckboxBtnPopup>
      )}
    </Container>
  );
};
