import React, { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import Pagination from "components/Pagination";
import Loading from "components/Loading";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch, faLock } from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";

export const QNA = (props) => {
  const user = axiosUrlFunction("");
  const isUser = user.accessPath === "/admin" ? true : false;

  const [selected, setSelected] = useState(1);
  const [pagenum, setPagenum] = useState("");
  const [list, setList] = useState(null);
  const [keyword, setKeyword] = useState("");
  const [count, setCount] = useState(0);
  const [deleteId, setDeleteId] = useState("");
  const [deleteButtonModal, setDeleteButtonModal] = useState(false); // Button Delete Modal
  const [deleteCheckModal, setDeleteCheckModal] = useState(false); // Button Delete Ok bt Check

  const [isChain, setIsChain] = useState(true);

  useEffect(() => {
    getList();
  }, []);

  // 리스트 불러오기
  const getList = async (params) => {
    setList(null);
    const qnaUrl = axiosUrlFunction("qnaList");

    if (!params) {
      params = {};
    }
    if (params.keyword === undefined) {
      params.keyword = keyword;
    }
    if (params.page === undefined) {
      params.page = selected - 1;
    }
    setPagenum(params.page);

    const queries = [];
    if (params.keyword !== "") {
      queries.push(`word=${params.keyword}`);
    }
    queries.push(`page=${params.page}`);
    queries.push(`size=10`);

    const queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";

    axios
      .get(`${qnaUrl.apiUrl}${queryStr}`, {
        headers: {
          Authorization: `Bearer ${qnaUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        setCount(res.data.data.totalElements);
        setList(res.data.data.content);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getList(params);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 답변 삭제
  const deleteAnswer = async (id) => {
    const deleteUrl = axiosUrlFunction("question/answer/delete", "검색");

    const data = {
      answer_id: id,
    };
    axios
      .put(`${deleteUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${deleteUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then((res) => {
        alert("답변 삭제를 완료했습니다.");
        window.location.replace("/admin/notices/qna/list");
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else deleteAnswer(id);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // qna 질문 삭제
  const deleteQuestion = async (id) => {
    const deleteUrl = axiosUrlFunction("question/delete");
    const data = {
      question_id: id,
    };
    axios
      .put(`${deleteUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${deleteUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then((res) => {
        alert("질문 삭제를 완료했습니다.");
        list.map((value, i) => {
          if (value.question_id === id) {
            list.splice(i, 1);
          }
        });
        setList([...list]);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else deleteQuestion(id);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else if (err.response.status === 409) {
          alert("답변이 존재하여 삭제 할 수 없습니다.");
          return;
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };
  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">
                공지/안내 관리 - Q&A({isUser ? "본사" : "지점"})
              </Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <Table className="table-hover search-box">
                <tbody>
                  <tr>
                    <th className="select-search-target">
                      <Form.Control as="select">
                        <option value="name">제목</option>
                      </Form.Control>
                    </th>
                    <th>
                      <Form.Control
                        type="text"
                        placeholder="검색"
                        value={keyword}
                        onChange={(e) => setKeyword(e.target.value)}
                        onKeyUp={(e) => {
                          if (e.keyCode === 13) {
                            setSelected(1);
                            getList({
                              page: 0,
                            });
                          }
                        }}
                        maxLength={50}
                        style={{
                          display: "inline-block",
                          width: "400px",
                          verticalAlign: "middle",
                        }}
                      />
                      <Button
                        variant="light"
                        onClick={(e) => {
                          setSelected(1);
                          getList({
                            page: 0,
                          });
                        }}
                        style={{
                          display: "inline-block",
                          border: "solid 1px #E3E3E3",
                          color: "#565656",
                          verticalAlign: "middle",
                        }}
                        className="search-btn"
                      >
                        <FontAwesomeIcon icon={faSearch} />
                      </Button>
                    </th>
                  </tr>
                </tbody>
              </Table>
              {!isUser && (
                <div className="text-right">
                  <Button
                    onClick={(e) => {
                      window.location.replace(`/staff/notices/qna/add`);
                    }}
                  >
                    질문 작성
                  </Button>
                </div>
              )}
              <table className="table-hover mt-0 table-center">
                <thead>
                  <tr>
                    <th className="border-0">NO.</th>
                    <th className="border-0">제목</th>
                    <th className="border-0">작성일</th>
                    <th className="border-0">작성자</th>
                    <th className="border-0"></th>
                  </tr>
                </thead>
                <tbody>
                  {list !== undefined && list !== null ? (
                    list.map((list, i) => {
                      return (
                        <React.Fragment key={list.question_id}>
                          <tr key={i} className="cursor">
                            {list.is_public === "Y" ? (
                              <>
                                <td
                                  onClick={() => {
                                    props.history.push(
                                      `${user.accessPath}/notices/qna/view/${list.question_id}`
                                    );
                                  }}
                                >
                                  {i + 1 + 10 * pagenum}
                                </td>
                                <td
                                  onClick={() => {
                                    props.history.push(
                                      `${user.accessPath}/notices/qna/view/${list.question_id}`
                                    );
                                  }}
                                >
                                  {list.title.substring(0, 7)}......
                                </td>
                                <td
                                  onClick={() => {
                                    props.history.push(
                                      `${user.accessPath}/notices/qna/view/${list.question_id}`
                                    );
                                  }}
                                >
                                  {list.question_date.replace("T", " ")}
                                </td>
                                <td
                                  onClick={() => {
                                    props.history.push(
                                      `${user.accessPath}/notices/qna/view/${list.question_id}`
                                    );
                                  }}
                                >
                                  {list.id}
                                </td>

                                {isUser === false &&
                                user.user_id === list.id ? (
                                  <td>
                                    <Button
                                      className="mr-3"
                                      onClick={() => {
                                        props.history.push(
                                          `/staff/notices/qna/edit/${list.question_id}`
                                        );
                                      }}
                                    >
                                      수정
                                    </Button>
                                    <Button
                                      onClick={() =>
                                        deleteQuestion(list.question_id)
                                      }
                                    >
                                      삭제
                                    </Button>
                                  </td>
                                ) : list.state === "답변완료" ? (
                                  <td></td>
                                ) : user.accessor === "main" ? (
                                  <td>
                                    <Button
                                      onClick={() => {
                                        props.history.push(
                                          `/admin/notices/qna/answer/${list.question_id}`
                                        );
                                      }}
                                    >
                                      답변
                                    </Button>
                                  </td>
                                ) : (
                                  <td></td>
                                )}
                              </>
                            ) : (
                              <>
                                <td
                                  onClick={() => {
                                    if (
                                      user.user_id === list.id ||
                                      user.accessor === "main"
                                    ) {
                                      props.history.push(
                                        `${user.accessPath}/notices/qna/view/${list.question_id}`
                                      );
                                    }
                                  }}
                                >
                                  {i + 1 + 10 * pagenum}
                                </td>
                                <td
                                  onClick={() => {
                                    if (
                                      user.user_id === list.id ||
                                      user.accessor === "main"
                                    ) {
                                      props.history.push(
                                        `${user.accessPath}/notices/qna/view/${list.question_id}`
                                      );
                                    }
                                  }}
                                >
                                  {list.title.substring(0, 7)}......{" "}
                                  <FontAwesomeIcon icon={faLock} />
                                </td>
                                <td
                                  onClick={() => {
                                    if (
                                      user.user_id === list.id ||
                                      user.accessor === "main"
                                    ) {
                                      props.history.push(
                                        `${user.accessPath}/notices/qna/view/${list.question_id}`
                                      );
                                    }
                                  }}
                                >
                                  {list.question_date.replace("T", " ")}
                                </td>
                                <td
                                  onClick={() => {
                                    if (
                                      user.user_id === list.id ||
                                      user.accessor === "main"
                                    ) {
                                      props.history.push(
                                        `${user.accessPath}/notices/qna/view/${list.question_id}`
                                      );
                                    }
                                  }}
                                >
                                  {list.id}
                                </td>

                                {isUser === false &&
                                user.user_id === list.id ? (
                                  <td>
                                    <Button
                                      className="mr-3"
                                      onClick={() => {
                                        props.history.push(
                                          `/staff/notices/qna/edit/${list.question_id}`
                                        );
                                      }}
                                    >
                                      수정
                                    </Button>
                                    <Button
                                      onClick={() =>
                                        deleteQuestion(list.question_id)
                                      }
                                    >
                                      삭제
                                    </Button>
                                  </td>
                                ) : list.state === "답변완료" ? (
                                  <td></td>
                                ) : user.accessor === "main" ? (
                                  <td>
                                    <Button
                                      onClick={() => {
                                        props.history.push(
                                          `/admin/notices/qna/answer/${list.question_id}`
                                        );
                                      }}
                                    >
                                      답변
                                    </Button>
                                  </td>
                                ) : (
                                  <td></td>
                                )}
                              </>
                            )}
                          </tr>
                          {list.state === "답변완료" ? (
                            <tr className="cursor">
                              <td
                                onClick={() => {
                                  if (list.is_public === "N") {
                                    if (
                                      user.user_id === list.id ||
                                      user.accessor === "main"
                                    ) {
                                      props.history.push(
                                        `${user.accessPath}/notices/qna/view/${list.question_id}`
                                      );
                                    }
                                  } else {
                                    props.history.push(
                                      `${user.accessPath}/notices/qna/view/${list.question_id}`
                                    );
                                  }
                                }}
                              ></td>
                              <td
                                onClick={() => {
                                  if (list.is_public === "N") {
                                    if (
                                      user.user_id === list.id ||
                                      user.accessor === "main"
                                    ) {
                                      props.history.push(
                                        `${user.accessPath}/notices/qna/view/${list.question_id}`
                                      );
                                    }
                                  } else {
                                    props.history.push(
                                      `${user.accessPath}/notices/qna/view/${list.question_id}`
                                    );
                                  }
                                }}
                              >
                                └&nbsp; RE : 관리자 답변
                              </td>
                              <td
                                onClick={() => {
                                  if (list.is_public === "N") {
                                    if (
                                      user.user_id === list.id ||
                                      user.accessor === "main"
                                    ) {
                                      props.history.push(
                                        `${user.accessPath}/notices/qna/view/${list.question_id}`
                                      );
                                    }
                                  } else {
                                    props.history.push(
                                      `${user.accessPath}/notices/qna/view/${list.question_id}`
                                    );
                                  }
                                }}
                              >
                                {list.answer_date.replace("T", " ")}
                              </td>
                              <td
                                onClick={() => {
                                  if (list.is_public === "N") {
                                    if (
                                      user.user_id === list.id ||
                                      user.accessor === "main"
                                    ) {
                                      props.history.push(
                                        `${user.accessPath}/notices/qna/view/${list.question_id}`
                                      );
                                    }
                                  } else {
                                    props.history.push(
                                      `${user.accessPath}/notices/qna/view/${list.question_id}`
                                    );
                                  }
                                }}
                              >
                                관리자
                              </td>

                              {isUser && (
                                <td>
                                  <Button
                                    className="mr-3"
                                    onClick={() => {
                                      props.history.push(
                                        `/admin/notices/qna/answerEdit/${list.question_id}`
                                      );
                                    }}
                                  >
                                    수정
                                  </Button>
                                  <Button
                                    onClick={() => deleteAnswer(list.answer_id)}
                                  >
                                    삭제
                                  </Button>
                                </td>
                              )}
                            </tr>
                          ) : (
                            <tr></tr>
                          )}
                        </React.Fragment>
                      );
                    })
                  ) : (
                    <tr>
                      <td colSpan={4} style={{ textAlign: "center" }}>
                        <Loading />
                      </td>
                    </tr>
                  )}
                </tbody>
              </table>
              <Pagination
                count={count}
                forcePage={selected - 1}
                selected={(pageSelect) => {
                  setSelected(pageSelect);
                  getList({ page: pageSelect - 1 });
                }}
              />
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
