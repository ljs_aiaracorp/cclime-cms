import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";
import { TextEditer } from "utils/TextEditer";

export const EditQNA = (props) => {
  const [title, setTitle] = useState(""); // 제목
  const [description, setDescription] = useState(""); // 내용
  const [isPublic, setIsPublic] = useState(""); // 공개여부
  useEffect(() => {
    getList();
  }, []);

  // get List
  const getList = async () => {
    const getUrl = axiosUrlFunction("qna");

    axios
      .get(`${getUrl.apiUrl}?question_id=${props.match.params.id}`, {
        headers: {
          Authorization: `Bearer ${getUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        setTitle(res.data.data.title);
        setDescription(res.data.data.content);
        setIsPublic(res.data.data.is_public);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getList();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // send Q&A Data
  const sendData = async () => {
    const sendUrl = axiosUrlFunction("question", "검색");

    const data = {
      question_id: props.match.params.id,
      title,
      content: description,
      is_public: isPublic,
    };

    axios
      .put(`${sendUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${sendUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        props.history.push(`${sendUrl.accessPath}/notices/qna/list`);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            if (!isCheck) return;
            else sendData();
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };
  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">공지/안내 관리 - Q&A - 질문 수정</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <Table className="table-hover">
                <colgroup>
                  <col style={{ width: "10%" }}></col>
                  <col style={{ width: "90%" }}></col>
                </colgroup>
                <tbody>
                  <tr>
                    <th>제목</th>
                    <td>
                      <input
                        className="form-control"
                        type="text"
                        required
                        placeholder="제목"
                        defaultValue={title}
                        maxLength={50}
                        onChange={(e) => setTitle(e.target.value)}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>공개범위</th>
                    <td>
                      <Form.Check
                        type="radio"
                        name="isOpen"
                        label="공개"
                        value="Y"
                        checked={
                          isPublic === "Y" || isPublic === "y" ? true : false
                        }
                        onChange={(e) => setIsPublic(e.target.value)}
                      />
                      <Form.Check
                        type="radio"
                        name="isOpen"
                        label="비공개"
                        value="N"
                        checked={
                          isPublic === "N" || isPublic === "n" ? true : false
                        }
                        onChange={(e) => setIsPublic(e.target.value)}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>내용</th>
                    <td style={{ height: "750px", verticalAlign: "top" }}>
                      <TextEditer
                        value={description || ""}
                        onChange={setDescription}
                      />
                    </td>
                  </tr>
                </tbody>
              </Table>
              <div className="text-center">
                <Button onClick={sendData}>저장</Button>
                <Button
                  className="ml-2"
                  onClick={() => {
                    props.history.push("/staff/notices/qna/list");
                  }}
                >
                  취소
                </Button>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
