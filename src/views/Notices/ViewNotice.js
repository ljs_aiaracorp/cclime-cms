import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";
import "../css/noticesview.scss";

export const ViewNotice = (props) => {
  const user = axiosUrlFunction("");
  const [title, setTitle] = useState(""); // 공지사항 제목
  const [date, SetDate] = useState(""); // 작성일
  const [description, setDescription] = useState(""); // 공지사항 내용

  useEffect(() => {
    getList();
  }, []);

  // 공지수정 할 자료
  const getList = async () => {
    const getUrl = axiosUrlFunction("notice");
    axios
      .get(`${getUrl.apiUrl}?notice_id=${props.match.params.id}`, {
        headers: {
          Authorization: `Bearer ${getUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then((res) => {
        setTitle(res.data.data.title);
        SetDate(res.data.data.createdAt.slice(0, 10));
        stringTohtml(res.data.data.content);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getList();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // html 변환
  const stringTohtml = (html) => {
    const array = html.split("</p>");
    for (let i = 0; i < array.length; i++) {
      if (array[i] !== "") {
        const template = document.createElement("template");
        template.innerHTML = array[i] + "</p>";
        document
          .getElementById("descriptions")
          .appendChild(template.content.firstChild);
      }
    }
  };

  const pageback = () => {
    props.history.push(`${user.accessPath}/notices/list`);
  };
  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">
                <span className="cursor" onClick={pageback}>
                  &lt;{" "}
                </span>
                공지/안내 관리 - 공지사항 - 공지 확인
              </Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0 noticesView">
              <div className="noticesView-column">
                <h3 className="noticesView-column__title text-center">
                  {title}
                </h3>
                <div>
                  <span className="float-right">작성일: {date}</span>
                </div>
              </div>
            </Card.Body>
            <div style={{ minHeight: "600px" }} id="descriptions"></div>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
