import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";
import { TextEditer } from "utils/TextEditer";

export const EditAnswersQNA = (props) => {
  const [title, setTitle] = useState(""); //제목
  const [description, setDescription] = useState(""); // 내용
  const [answerDes, setAnswerDes] = useState(""); // 답글 내용
  const [answerId, setAnswerId] = useState(""); // 답글 아이디

  useEffect(() => {
    getList();
  }, []);

  // get List
  const getList = async () => {
    const getUrl = axiosUrlFunction("qna");

    axios
      .get(`${getUrl.apiUrl}?question_id=${props.match.params.id}`, {
        headers: {
          Authorization: `Bearer ${getUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        setTitle(res.data.data.title);
        stringTohtml(res.data.data.content);
        setAnswerDes(res.data.data.answer);
        setAnswerId(res.data.data.answer_id);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getList();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // html 변환
  const stringTohtml = (html) => {
    const array = html.split("</p>");
    for (let i = 0; i < array.length; i++) {
      if (array[i] !== "") {
        const template = document.createElement("template");
        template.innerHTML = array[i] + "</p>";
        document
          .getElementById("descriptions")
          .appendChild(template.content.firstChild);
      }
    }
  };

  // Send Answer Data
  const sendData = async () => {
    const sendUrl = axiosUrlFunction("question/answer", "검색");

    const data = {
      answer_id: answerId,
      answer: answerDes,
    };

    axios
      .put(`${sendUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${sendUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(() => {
        props.history.push(`/admin/notices/qna/list`);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else sendData();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };
  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">공지/안내 관리 - Q&A - 답변수정</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <Table className="table-hover">
                <colgroup>
                  <col style={{ width: "10%" }}></col>
                  <col style={{ width: "90%" }}></col>
                </colgroup>
                <tbody>
                  <tr>
                    <th>제목</th>
                    <td>
                      <input
                        className="form-control"
                        type="text"
                        required
                        placeholder="제목"
                        defaultValue={title}
                        readOnly={true}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th colSpan={2}>
                      내용
                      <div
                        id="descriptions"
                        style={{
                          minHeight: "300px",
                        }}
                      ></div>
                    </th>
                  </tr>
                  <tr>
                    <th colSpan={2} style={{ height: "750px" }}>
                      답변
                      <TextEditer
                        value={answerDes || ""}
                        onChange={setAnswerDes}
                      />
                    </th>
                  </tr>
                </tbody>
              </Table>
              <div className="text-center">
                <Button onClick={sendData}>저장</Button>
                <Button
                  className="ml-2"
                  onClick={() => {
                    props.history.push("/admin/notices/qna/list");
                  }}
                >
                  취소
                </Button>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
