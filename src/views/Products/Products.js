import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import Pagination from "components/Pagination";
import Loading from "components/Loading";
import moment from "moment";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";

export const Products = (props) => {
  const [categoryList, setCategoryList] = useState([]);
  const [list, setList] = useState([]);
  const [selected, setSelected] = useState(1);
  const [count, setCount] = useState(0);
  const [pagenum, setPagenum] = useState(0);

  const [selectedCategoryIndex, setSelectedCategoryIndex] = useState(0);

  useEffect(() => {
    getCategory();
  }, []);

  // 제품 카테고리 리스트
  const getCategory = async () => {
    const categoryUrl = axiosUrlFunction("categoryList/tab", "검색");

    axios
      .get(`/api/categoryList/tab?type=product`, {
        headers: {
          Authorization: `Bearer ${categoryUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        if (res.data.data.length > 0) {
          setSelectedCategoryIndex(res.data.data[0].categoryId);
          getProduct({ page: 0, id: res.data.data[0].categoryId });
        }
        setCategoryList(res.data.data);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getCategory();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 제품 리스트
  const getProduct = async (params) => {
    setList(null);
    if (!params) {
      params = {};
    }

    const productUrl = axiosUrlFunction("productList", "검색");

    if (props.location.search !== "") {
      params.id = props.location.search.slice(1);
      setSelectedCategoryIndex(parseInt(props.location.search.slice(1)));
      props.location.search = "";
    }

    if (params.page === undefined) {
      params.page = selected - 1;
    }

    setPagenum(params.page);

    const queries = [];
    queries.push(`page=${params.page}`);
    queries.push(`size=10`);
    queries.push(`category_id=${params.id}`);
    const queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";

    axios
      .get(`${productUrl.apiUrl}${queryStr}`, {
        headers: {
          Authorization: `Bearer ${productUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        setList(res.data.data.content);
        setCount(res.data.data.totalElements);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getProduct(params);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 상품 삭제
  const deleteData = async (id) => {
    const deleteUrl = axiosUrlFunction("product/delete", "검색");
    const data = {
      product_id: id,
    };
    axios
      .put(`${deleteUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${deleteUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async () => {
        alert("해당상품 삭제가 완료되었습니다.");
        getProduct({
          page: 0,
          id: selectedCategoryIndex,
        });
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else deleteData(id);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };
  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">제품관리 - 상품관리</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              {categoryList.length > 0 ? (
                categoryList.map((categoryName, i) => {
                  return (
                    <Button
                      key={i}
                      style={{
                        width: "150px",
                        height: "50px",
                      }}
                      className={
                        selectedCategoryIndex === categoryName.categoryId
                          ? "select-btn selected mt-1 mr-1"
                          : "select-btn mt-3 mr-1"
                      }
                      onClick={(e) => {
                        setSelectedCategoryIndex(categoryName.categoryId);
                        getProduct({
                          page: 0,
                          id: categoryName.categoryId,
                        });
                      }}
                    >
                      {categoryName.categoryTitle}
                    </Button>
                  );
                })
              ) : (
                <div></div>
              )}
              <div className="text-right">
                <Button
                  onClick={(e) => {
                    props.history.push(
                      `/admin/products/add/${selectedCategoryIndex}`
                    );
                  }}
                >
                  상품 추가
                </Button>
              </div>
              <Table className="table-hover mt-0">
                <thead>
                  <tr>
                    <th className="border-0">NO.</th>
                    <th className="border-0">제품명</th>
                    <th className="border-0">제품 이미지</th>
                    <th className="border-0">제품 설명</th>
                    <th className="border-0">가격</th>
                    <th className="border-0"></th>
                  </tr>
                </thead>
                <tbody>
                  {list !== undefined && list !== null ? (
                    list.map((product, i) => {
                      return (
                        <tr key={i} className="cursor">
                          <td>{i + 1 + 10 * pagenum}</td>
                          <td>{product.product_name}</td>
                          <td>
                            {" "}
                            <img
                              style={{
                                width: "65px",
                                height: "145px",
                              }}
                              src={product.product_img}
                              alt="이미지가 없습니다."
                            />
                          </td>
                          <td>
                            {product.product_explanation.length > 40
                              ? product.product_explanation.substring(0, 40) +
                                "..."
                              : product.product_explanation}
                          </td>
                          <td>
                            {product.product_price.toLocaleString("ko-KO") +
                              "원"}
                          </td>
                          <td>
                            <Button
                              className="mr-3"
                              onClick={() => {
                                props.history.push(
                                  `/admin/products/edit/${product.product_id}?${selectedCategoryIndex}`
                                );
                              }}
                            >
                              보기/수정
                            </Button>
                            <Button
                              onClick={() => deleteData(product.product_id)}
                            >
                              삭제
                            </Button>
                          </td>
                        </tr>
                      );
                    })
                  ) : (
                    <tr>
                      <td colSpan={6} style={{ textAlign: "center" }}>
                        <Loading />
                      </td>
                    </tr>
                  )}
                </tbody>
              </Table>
              <Pagination
                count={count}
                forcePage={selected - 1}
                selected={(pageSelect) => {
                  setSelected(pageSelect);
                  getProduct({
                    page: pageSelect - 1,
                    id: selectedCategoryIndex,
                  });
                }}
              />
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
