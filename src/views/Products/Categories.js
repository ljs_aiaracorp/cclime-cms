import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import Pagination from "components/Pagination";
import Loading from "components/Loading";
import { AddCategoryPopup } from "./AddCategoryPopup";
import { EditCategoryPopup } from "./EditCategoryPopup";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";

export const Categories = (props) => {
  const [selected, setSelected] = useState(1);
  const [list, setList] = useState([]);
  const [count, setCount] = useState(0);
  const [pagenum, setPagenum] = useState();
  const [modifyData, setModifyData] = useState({
    id: "",
    title: "",
    use: "",
    img: "",
  });

  const [showAddCategoryPopup, setShowAddCategoryPopup] = useState(false);
  const [showEditCategoryPopup, setShowEditCategoryPopup] = useState(false);
  useEffect(() => {
    getList();
  }, []);

  // 카테고리 List
  const getList = async (params) => {
    setList(null);
    const getUrl = axiosUrlFunction("categoryList", "검색");

    if (!params) {
      params = {};
    }
    if (params.page === undefined) {
      params.page = selected - 1;
    }
    setPagenum(params.page);

    const queries = [];
    queries.push(`page=${params.page}`);
    queries.push(`size=10`);
    queries.push(`type=product`);
    const queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";

    axios
      .get(`${getUrl.apiUrl}${queryStr}`, {
        headers: {
          Authorization: `Bearer ${getUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then((res) => {
        setCount(res.data.data.totalElements);
        setList(res.data.data.content);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getList(params);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 카테고리 삭제
  const deleteData = async (id) => {
    const deleteUrl = axiosUrlFunction("category/delete", "검색");

    const data = {
      category_id: id,
    };

    axios
      .put(`${deleteUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${deleteUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(() => {
        window.location.replace("/admin/products/categories/list");
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else deleteData(id);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };
  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">제품관리 - 카테고리 관리</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <div className="text-right">
                <Button onClick={(e) => setShowAddCategoryPopup(true)}>
                  카테고리 추가
                </Button>
              </div>
              <table className="table-hover mt-0 table-center">
                <thead>
                  <tr>
                    <th className="border-0">NO.</th>
                    <th className="border-0">카테고리명</th>
                    <th className="border-0">활성화여부</th>
                    <th className="border-0">카테고리 이미지</th>
                    <th className="border-0"></th>
                  </tr>
                </thead>
                <tbody>
                  {list !== undefined && list !== null ? (
                    list.map((value, i) => {
                      const use = value.use === "Y" ? "ON" : "OFF";
                      return (
                        <tr key={i} className="cursor">
                          <td>{i + 1 + 10 * pagenum}</td>
                          <td>{value.category_title}</td>
                          <td>{use}</td>
                          <td>
                            <img
                              style={{
                                width: "65px",
                                height: "145px",
                              }}
                              src={value.category_img}
                              alt="이미지가 없습니다."
                            />
                          </td>
                          <td>
                            <Button
                              className="mr-3"
                              onClick={() => {
                                setShowEditCategoryPopup(true);
                                setModifyData({
                                  id: value.category_id,
                                  title: value.category_title,
                                  use: value.use,
                                  img: value.category_img,
                                });
                              }}
                            >
                              수정
                            </Button>
                            <Button
                              onClick={() => {
                                deleteData(value.category_id);
                              }}
                            >
                              삭제
                            </Button>
                          </td>
                        </tr>
                      );
                    })
                  ) : (
                    <tr>
                      <td colSpan={5} style={{ textAlign: "center" }}>
                        <Loading />
                      </td>
                    </tr>
                  )}
                </tbody>
              </table>
              <Pagination
                count={count}
                forcePage={selected - 1}
                selected={(pageSelect) => {
                  setSelected(pageSelect);
                  getList({ page: pageSelect - 1 });
                }}
              />
            </Card.Body>
          </Card>
        </Col>
      </Row>
      {showAddCategoryPopup && (
        <AddCategoryPopup
          onClickClose={(e) => setShowAddCategoryPopup(false)}
        />
      )}
      {showEditCategoryPopup && (
        <EditCategoryPopup
          listItem={modifyData}
          onClickClose={(e) => setShowEditCategoryPopup(false)}
        />
      )}
    </Container>
  );
};
