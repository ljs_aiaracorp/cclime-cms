import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import Pagination from "components/Pagination";
import Loading from "components/Loading";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";

export const Banners = (props) => {
  const [list, setList] = useState([]);
  const [selected, setSelected] = useState(1);
  const [count, setCount] = useState(0);
  const [pagenum, setPagenum] = useState(0);
  const [keyword, setKeyword] = useState("");
  useEffect(() => {
    getList();
  }, []);

  const getList = async (params) => {
    setList(null);
    if (!params) {
      params = {};
    }
    if (params.page === undefined) {
      params.page = selected - 1;
    }
    setPagenum(params.page);

    const getUrl = axiosUrlFunction("bannerList", "검색");

    const queries = [];
    queries.push(`page=${params.page}`);
    queries.push(`size=10`);
    if (keyword !== "") queries.push(`word=${keyword}`);
    const queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";
    axios
      .get(`${getUrl.apiUrl}${queryStr}`, {
        headers: {
          Authorization: `Bearer ${getUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        setCount(res.data.data.totalElements);
        setList(res.data.data.content);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getList(params);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 배너 삭제
  const deleteData = async (id) => {
    const deleteUrl = axiosUrlFunction("banner/delete", "검색");
    const data = {
      banner_id: id,
    };

    axios
      .put(`${deleteUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${deleteUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(() => {
        window.location.replace("/admin/products/banners/list");
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else deleteData(id);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };
  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">제품관리 - 배너관리</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <Table className="table-hover search-box">
                <tbody>
                  <tr>
                    <th className="select-search-target">
                      <Form.Control as="select">
                        <option value="name">제목</option>
                      </Form.Control>
                    </th>
                    <th>
                      <Form.Control
                        type="text"
                        placeholder="검색"
                        value={keyword}
                        onChange={(e) => setKeyword(e.target.value)}
                        onKeyUp={(e) => {
                          if (e.keyCode === 13) {
                            setSelected(1);
                            getList({
                              page: 0,
                            });
                          }
                        }}
                        maxLength={50}
                        style={{
                          display: "inline-block",
                          width: "400px",
                          verticalAlign: "middle",
                        }}
                      />
                      <Button
                        variant="light"
                        onClick={(e) => {
                          setSelected(1);
                          getList({
                            page: 0,
                          });
                        }}
                        style={{
                          display: "inline-block",
                          border: "solid 1px #E3E3E3",
                          color: "#565656",
                          verticalAlign: "middle",
                        }}
                        className="search-btn"
                      >
                        <FontAwesomeIcon icon={faSearch} />
                      </Button>
                    </th>
                  </tr>
                </tbody>
              </Table>
              <div className="text-right">
                <Button
                  onClick={(e) => {
                    props.history.push("/admin/products/banners/add");
                  }}
                >
                  배너생성
                </Button>
              </div>
              <table className="table-hover mt-0 table-center">
                <thead>
                  <tr>
                    <th className="border-0">NO.</th>
                    <th className="border-0">제목</th>
                    <th className="border-0">작성일</th>
                    <th className="border-0">활성화 여부</th>
                    <th className="border-0"></th>
                  </tr>
                </thead>
                <tbody>
                  {list !== undefined && list !== null ? (
                    list.map((banner, i) => {
                      const use =
                        banner.use === "Y" || banner.use === "y"
                          ? "활성화"
                          : "비활성화";
                      const style = {
                        color: use === "활성화" ? "blue" : "red",
                      };
                      return (
                        <tr key={i} className="cursor">
                          <td>{i + 1 + 10 * pagenum}</td>
                          <td>{banner.title}</td>
                          <td>{banner.createdAt.slice(0, 10)}</td>
                          <td style={style}>{use}</td>
                          <td>
                            <Button
                              className="mr-3"
                              onClick={() =>
                                props.history.push(
                                  `/admin/products/banners/edit/${banner.bannerId}`
                                )
                              }
                            >
                              변경
                            </Button>
                            <Button
                              onClick={() => {
                                deleteData(banner.bannerId);
                              }}
                            >
                              삭제
                            </Button>
                          </td>
                        </tr>
                      );
                    })
                  ) : (
                    <tr>
                      <td colSpan={5} style={{ textAlign: "center" }}>
                        <Loading />
                      </td>
                    </tr>
                  )}
                </tbody>
              </table>
              <Pagination
                count={count}
                forcePage={selected - 1}
                selected={(pageSelect) => {
                  setSelected(pageSelect);
                  getList({ page: pageSelect - 1 });
                }}
              />
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
