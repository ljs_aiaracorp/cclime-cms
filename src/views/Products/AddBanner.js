import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import { FileBtn } from "components/FileUpload";
import { axiosUrlFunction } from "utils/AxiosUrl";
import axios from "axios";

export const AddBanner = (props) => {
  const [bannerInfo, setBannerInfo] = useState({
    title: "",
    img: "",
    use: "y",
  });
  const [isEnabled, setIsEnabled] = useState(true);

  // 배너 등록
  let sw = 0;
  const sendData = async () => {
    if (sw === 1) return;
    if (bannerInfo.title === "") {
      alert("제목을 입력해 주세요.");
      return;
    } else if (bannerInfo.img === "") {
      alert("이미지를 등록해 주세요.");
      return;
    }

    const sendUrl = axiosUrlFunction("banner", "검색");
    const formData = new FormData();
    formData.append("banner_title", bannerInfo.title);
    formData.append("images", bannerInfo.img[0]);
    formData.append("use", bannerInfo.use);

    sw = 1;
    axios
      .post(`${sendUrl.apiUrl}`, formData, {
        headers: {
          Authorization: `Bearer ${sendUrl.token}`,
          "Content-Type": `multipart/form-data`,
        },
      })
      .then((res) => {
        sw = 0;
        alert("배너등록을 완료했습니다.");
        props.history.push("/admin/products/banners/list");
      })
      .catch((err) => {
        sw = 0;
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else sendData();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">제품관리 - 배너관리 - 배너생성</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <Table className="table-hover">
                <colgroup>
                  <col style={{ width: "10%" }}></col>
                  <col style={{ width: "90%" }}></col>
                </colgroup>
                <tbody>
                  <tr>
                    <th colSpan={2}>배너정보</th>
                  </tr>
                  <tr>
                    <th>제목</th>
                    <td>
                      <input
                        className="form-control mb-3"
                        type="text"
                        required
                        placeholder="제목"
                        value={bannerInfo.title}
                        maxLength={15}
                        onChange={(e) =>
                          setBannerInfo((value) => ({
                            ...value,
                            title: e.target.value,
                          }))
                        }
                      />
                      <span>※ 배너 제목은 관리차원에서만 사용됩니다.</span>
                    </td>
                  </tr>
                  <tr>
                    <th>배너이미지</th>
                    <td>
                      <FileBtn
                        name="업로드"
                        fileData={(data) => {
                          setBannerInfo((value) => ({
                            ...value,
                            img: data,
                          }));
                        }}
                        accept="image/*"
                        id="picture"
                        imageUrl={""}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>활성화 여부</th>
                    <td>
                      <Button
                        className={
                          isEnabled
                            ? "select-btn selected mb-3"
                            : "select-btn mb-3"
                        }
                        onClick={(e) => {
                          setIsEnabled(true);
                          setBannerInfo((value) => ({
                            ...value,
                            use: "y",
                          }));
                        }}
                      >
                        ON
                      </Button>
                      <Button
                        className={
                          isEnabled
                            ? "select-btn mb-3"
                            : "select-btn selected mb-3"
                        }
                        onClick={(e) => {
                          setIsEnabled(false);
                          setBannerInfo((value) => ({
                            ...value,
                            use: "n",
                          }));
                        }}
                      >
                        OFF
                      </Button>
                      <br />※ 활성화 여부가 ON일 경우 앱에 해당 배너가
                      표시됩니다!
                    </td>
                  </tr>
                </tbody>
              </Table>
              <div className="text-center">
                <Button onClick={sendData}>저장</Button>
                <Button
                  className="ml-2"
                  onClick={() => {
                    props.history.push("/admin/products/banners/list");
                  }}
                >
                  취소
                </Button>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
