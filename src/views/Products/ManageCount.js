import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import Pagination from "components/Pagination";
import Loading from "components/Loading";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronUp, faChevronDown } from "@fortawesome/free-solid-svg-icons";
import "../css/sliderButton/sliderButton.scss";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";

export const ManageCount = (props) => {
  const [categoryList, setCategoryList] = useState([]);
  const [list, setList] = useState(null);
  const [selected, setSelected] = useState(1);
  const [count, setCount] = useState(0);
  const [pagenum, setPagenum] = useState(0);
  const [selectedCategoryIndex, setSelectedCategoryIndex] = useState(0);
  const [productInfoCheck, setProductInfoCheck] = useState(false);
  useEffect(() => {
    getCategory();
  }, []);

  // 제품 카테고리 리스트
  const getCategory = async () => {
    const categoryUrl = axiosUrlFunction("categoryList");

    axios
      .get(`${categoryUrl.apiUrl}`, {
        headers: {
          Authorization: `Bearer ${categoryUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        if (res.data.data.length > 0) {
          setSelectedCategoryIndex(res.data.data[0].category_id);
          getProduct({ page: 0, id: res.data.data[0].category_id });
          if (res.data.data[0].sold_out === "N") {
            setProductInfoCheck(false);
          } else if (res.data.data[0].sold_out === "Y") {
            setProductInfoCheck(true);
          }
        }
        setCategoryList(res.data.data);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getCategory();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 제품 리스트
  const getProduct = async (params) => {
    setList(null);
    if (!params) {
      params = {};
    }
    const productUrl = axiosUrlFunction("productStockList");

    if (params.page === undefined) {
      params.page = selected - 1;
    }
    setPagenum(params.page);

    const queries = [];
    queries.push(`page=${params.page}`);
    queries.push(`size=10`);
    queries.push(`category_id=${params.id}`);
    const queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";

    axios
      .get(`${productUrl.apiUrl}${queryStr}`, {
        headers: {
          Authorization: `Bearer ${productUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        setList(res.data.data.content);
        setCount(res.data.data.totalElements);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getProduct(params);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 개수 이벤트
  const stockEventHandler = (index, control) => {
    if (control === "up") list[index].product_stock += 1;
    else if (control === "down") {
      if (list[index].product_stock === 0) return;
      else list[index].product_stock -= 1;
    }
    stockChange(index);
  };

  // 개수 저장
  const stockChange = async (index) => {
    const stockUrl = axiosUrlFunction("product/stock");

    const data = {
      store_product_id: list[index].product_store_id,
      product_stock: list[index].product_stock,
    };

    axios
      .put(`${stockUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${stockUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async () => {
        setList([...list]);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else stockChange(index);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 품절여부
  const soldOutCheck = async () => {
    const soldOutUrl = axiosUrlFunction("soldOut");

    const data = {
      sold_out: !productInfoCheck ? "Y" : "N",
    };

    axios
      .put(`${soldOutUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${soldOutUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else soldOutCheck();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };
  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">제품관리 - 지점별 재고관리</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              {categoryList.length > 0 ? (
                categoryList.map((categoryName, i) => {
                  return (
                    <Button
                      key={i}
                      style={{
                        width: "150px",
                        height: "50px",
                      }}
                      className={
                        selectedCategoryIndex === categoryName.category_id
                          ? "select-btn selected mt-1 mr-1"
                          : "select-btn mt-3 mr-1"
                      }
                      onClick={(e) => {
                        setSelectedCategoryIndex(categoryName.category_id);
                        getProduct({
                          page: 0,
                          id: categoryName.category_id,
                        });
                      }}
                    >
                      {categoryName.category_title}
                    </Button>
                  );
                })
              ) : (
                <div></div>
              )}

              <div style={{ display: "flex", justifyContent: "flex-end" }}>
                <span
                  className="mr-3"
                  style={{ fontSize: "18px", fontWeight: "bold" }}
                >
                  제품 품절 표시
                </span>
                <input
                  type="checkbox"
                  id="checkbox-input"
                  checked={productInfoCheck}
                  onChange={(e) => {
                    setProductInfoCheck(e.target.checked);
                    soldOutCheck();
                  }}
                />
                <label
                  htmlFor="checkbox-input"
                  className="round-slider-container mr-3"
                >
                  <div className="round-slider-container__title">On</div>
                  <div className="round-slider-container__title">Off</div>
                  <div className="round-slider"></div>
                </label>
              </div>
              <table className="table-hover mt-4 table-center">
                <thead>
                  <tr>
                    <th className="border-0">NO.</th>
                    <th className="border-0">제품명</th>
                    <th className="border-0">제품 이미지</th>
                    <th className="border-0">재고</th>
                  </tr>
                </thead>
                <tbody>
                  {list !== undefined && list !== null ? (
                    list.map((product, i) => {
                      return (
                        <tr key={i}>
                          <td>{i + 1 + 10 * pagenum}</td>
                          <td>{product.product_name}</td>
                          <td>
                            <img
                              style={{
                                width: "65px",
                                height: "145px",
                              }}
                              src={product.product_img}
                              alt="이미지가 없습니다."
                            />
                          </td>
                          <td>
                            <div
                              style={{
                                display: "flex",
                                alignItems: "center",
                                justifyContent: "center",
                              }}
                            >
                              <FontAwesomeIcon
                                icon={faChevronDown}
                                onClick={() => {
                                  stockEventHandler(i, "down");
                                }}
                                className="mr-3 cursor"
                              />
                              <input
                                className="input-appearance"
                                style={{
                                  border: "1px solid rgba(0,0,0, .3)",
                                  width: "90px",
                                  height: "40px",
                                  fontSize: "20px",
                                  overflow: "hidden",
                                  textAlign: "center",
                                }}
                                type="number"
                                value={product.product_stock}
                                onChange={(e) => {
                                  const val =
                                    0 > parseInt(e.target.value)
                                      ? 0
                                      : parseInt(e.target.value);
                                  {
                                    product.product_stock = val;
                                    setList([...list]);
                                    if (e.target.value !== "") stockChange(i);
                                  }
                                }}
                              />

                              <FontAwesomeIcon
                                icon={faChevronUp}
                                onClick={() => {
                                  stockEventHandler(i, "up");
                                }}
                                className="ml-3 cursor"
                              />
                            </div>
                          </td>
                        </tr>
                      );
                    })
                  ) : (
                    <tr>
                      <td colSpan={4} style={{ textAlign: "center" }}>
                        <Loading />
                      </td>
                    </tr>
                  )}
                </tbody>
              </table>
              <Pagination
                count={count}
                forcePage={selected - 1}
                selected={(pageSelect) => {
                  setSelected(pageSelect);
                  getProduct({
                    page: pageSelect - 1,
                    id: selectedCategoryIndex,
                  });
                }}
              />
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
