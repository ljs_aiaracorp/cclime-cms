import React, { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import { FileBtn } from "components/FileUpload";
import "../css/sliderButton/sliderButton.scss";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";

export const AddProduct = (props) => {
  const [productData, setProductData] = useState({
    id: props.match.params.id,
    categoryName: "",
    name: "",
    explanation: "",
    images: "",
    price: "",
  });
  const [productInfo, setProductInfo] = useState({
    product_info_name: "",
    volumn: "",
    product_info_explanation: "",
    periodUse: "",
    ingredients: "",
    manufacturer: "",
    manufacturerCountry: "",
    audit: "",
    precautions: "",
    assurance: "",
    tel: "",
    howUse: "",
  });

  const [productInfoCheck, setProductInfoCheck] = useState(false);
  useEffect(() => {
    getCategoryInfo();
  }, []);

  // 카테고리 정보
  const getCategoryInfo = async () => {
    const infoUrl = axiosUrlFunction("category", "검색");
    axios
      .get(`${infoUrl.apiUrl}?category_id=${props.match.params.id}`, {
        headers: {
          Authorization: `Bearer ${infoUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        setProductData((value) => ({
          ...value,
          categoryName: res.data.data.categoryTitle,
        }));
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getCategoryInfo();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 제품 저장
  let sw = 0;
  const sendData = async (event) => {
    event.preventDefault();
    if (sw === 1) return;
    if (productData.images === "") {
      alert("제품이미지를 등록해 주세요.");
      return;
    } else if (isNaN(productData.price)) {
      alert("가격을 입력해 주세요.");
      return;
    }

    let sendUrl = "";
    const formData = new FormData();
    if (!productInfoCheck) {
      sendUrl = axiosUrlFunction("product", "검색");
      formData.append("category_id", productData.id);
      formData.append("name", productData.name);
      formData.append("explanation", productData.explanation);
      formData.append("images", productData.images[0]);
      formData.append("price", productData.price);
      formData.append("product_info", "n");
    } else {
      sendUrl = axiosUrlFunction("productInfo", "검색");
      formData.append("category_id", productData.id);
      formData.append("product_name", productData.name);
      formData.append("product_explanation", productData.explanation);
      formData.append("images", productData.images[0]);
      formData.append("price", productData.price);
      formData.append("product_info", "y");
      formData.append("product_info_name", productInfo.product_info_name);
      formData.append("volume", productInfo.volumn);
      formData.append(
        "product_info_explanation",
        productInfo.product_info_explanation
      );
      formData.append("periodUse", productInfo.periodUse);
      formData.append("ingredients", productInfo.ingredients);
      formData.append("manufacturer", productInfo.manufacturer);
      formData.append("manufacturerCountry", productInfo.manufacturerCountry);
      formData.append("audit", productInfo.audit);
      formData.append("precautions", productInfo.precautions);
      formData.append("assurance", productInfo.assurance);
      formData.append("tel", productInfo.tel);
      formData.append("howUse", productInfo.howUse);
    }

    sw = 1;
    axios
      .post(`${sendUrl.apiUrl}`, formData, {
        headers: {
          Authorization: `Bearer ${sendUrl.token}`,
          "Content-Type": `multipart/form-data`,
        },
      })
      .then(async () => {
        sw = 0;
        alert("상품추가를 완료했습니다.");
        props.history.push(`/admin/products/list?${props.match.params.id}`);
      })
      .catch((err) => {
        sw = 0;
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else sendData();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  return (
    <Container fluid>
      <form onSubmit={sendData}>
        <Row>
          <Col md="12">
            <Card className="card-plain table-plain-bg">
              <Card.Header>
                <Card.Title as="h4">
                  제품관리 - 상품관리 - {productData.categoryName} 상품추가
                </Card.Title>
              </Card.Header>
              <Card.Body className="table-full-width table-responsive px-0">
                <Table className="table-hover">
                  <colgroup>
                    <col style={{ width: "15%" }}></col>
                    <col style={{ width: "85%" }}></col>
                  </colgroup>
                  <tbody>
                    <tr>
                      <th colSpan={2}>상품정보</th>
                    </tr>
                    <tr>
                      <th>카테고리</th>
                      <td>
                        <input
                          className="form-control"
                          type="text"
                          required
                          placeholder="카테고리"
                          value={productData.categoryName}
                          readOnly={true}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>상품명</th>
                      <td>
                        <input
                          className="form-control"
                          type="text"
                          required
                          placeholder="상품명"
                          value={productData.name}
                          maxLength={30}
                          onChange={(e) =>
                            setProductData((value) => ({
                              ...value,
                              name: e.target.value,
                            }))
                          }
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>제품설명</th>
                      <td>
                        <textarea
                          className="form-control"
                          value={productData.explanation}
                          onChange={(e) =>
                            setProductData((value) => ({
                              ...value,
                              explanation: e.target.value,
                            }))
                          }
                          style={{
                            minHeight: "200px",
                            resize: "none",
                          }}
                        ></textarea>
                      </td>
                    </tr>
                    <tr>
                      <th>제품이미지</th>
                      <td>
                        <FileBtn
                          name="업로드"
                          fileData={(data) => {
                            setProductData((value) => ({
                              ...value,
                              images: data,
                            }));
                          }}
                          accept="image/*"
                          id="picture"
                          imageUrl={""}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>가격</th>
                      <td>
                        <input
                          className="form-control"
                          type="number"
                          required
                          placeholder="가격"
                          value={productData.price}
                          onChange={(e) => {
                            const val =
                              0 > parseInt(e.target.value)
                                ? 0
                                : parseInt(e.target.value);
                            setProductData((value) => ({
                              ...value,
                              price: val,
                            }));
                          }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>
                        상품정보
                        <br />
                        제공고시
                      </th>
                      <td>
                        <input
                          type="checkbox"
                          id="checkbox-input"
                          onChange={(e) =>
                            setProductInfoCheck(e.target.checked)
                          }
                        />
                        <label
                          htmlFor="checkbox-input"
                          className="round-slider-container"
                        >
                          <div className="round-slider-container__title">
                            On
                          </div>
                          <div className="round-slider-container__title">
                            Off
                          </div>
                          <div className="round-slider"></div>
                        </label>
                      </td>
                    </tr>
                    {productInfoCheck && (
                      <React.Fragment>
                        <tr>
                          <th>제품명</th>
                          <td>
                            <textarea
                              className="form-control productInfo-column__description"
                              value={productInfo.product_info_name}
                              required
                              onChange={(e) =>
                                setProductInfo((value) => ({
                                  ...value,
                                  product_info_name: e.target.value,
                                }))
                              }
                            ></textarea>
                          </td>
                        </tr>
                        <tr>
                          <th>용량 또는 중량</th>
                          <td>
                            <textarea
                              className="form-control productInfo-column__description"
                              value={productInfo.volumn}
                              required
                              onChange={(e) =>
                                setProductInfo((value) => ({
                                  ...value,
                                  volumn: e.target.value,
                                }))
                              }
                            ></textarea>
                          </td>
                        </tr>
                        <tr>
                          <th>상품간략설명</th>
                          <td>
                            <textarea
                              className="form-control productInfo-column__description"
                              value={productInfo.product_info_explanation}
                              required
                              onChange={(e) =>
                                setProductInfo((value) => ({
                                  ...value,
                                  product_info_explanation: e.target.value,
                                }))
                              }
                            ></textarea>
                          </td>
                        </tr>
                        <tr>
                          <th>사용방법</th>
                          <td>
                            <textarea
                              className="form-control productInfo-column__description"
                              value={productInfo.howUse}
                              required
                              onChange={(e) =>
                                setProductInfo((value) => ({
                                  ...value,
                                  howUse: e.target.value,
                                }))
                              }
                            ></textarea>
                          </td>
                        </tr>
                        <tr>
                          <th>
                            사용기한 또는
                            <br /> 개봉 후 사용기간
                          </th>
                          <td>
                            <textarea
                              className="form-control productInfo-column__description"
                              value={productInfo.periodUse}
                              required
                              onChange={(e) =>
                                setProductInfo((value) => ({
                                  ...value,
                                  periodUse: e.target.value,
                                }))
                              }
                            ></textarea>
                          </td>
                        </tr>
                        <tr>
                          <th>전성분</th>
                          <td>
                            <textarea
                              className="form-control productInfo-column__description"
                              value={productInfo.ingredients}
                              required
                              onChange={(e) =>
                                setProductInfo((value) => ({
                                  ...value,
                                  ingredients: e.target.value,
                                }))
                              }
                            ></textarea>
                          </td>
                        </tr>
                        <tr>
                          <th>
                            제조자 및 <br /> 제조판매업자
                          </th>
                          <td>
                            <textarea
                              className="form-control productInfo-column__description"
                              value={productInfo.manufacturer}
                              required
                              onChange={(e) =>
                                setProductInfo((value) => ({
                                  ...value,
                                  manufacturer: e.target.value,
                                }))
                              }
                            ></textarea>
                          </td>
                        </tr>
                        <tr>
                          <th>제조국</th>
                          <td>
                            <textarea
                              className="form-control productInfo-column__description"
                              value={productInfo.manufacturerCountry}
                              required
                              onChange={(e) =>
                                setProductInfo((value) => ({
                                  ...value,
                                  manufacturerCountry: e.target.value,
                                }))
                              }
                            ></textarea>
                          </td>
                        </tr>
                        <tr>
                          <th>
                            식품의약품안전처리심사
                            <br />필 유무
                          </th>
                          <td>
                            <textarea
                              className="form-control productInfo-column__description"
                              value={productInfo.audit}
                              required
                              onChange={(e) =>
                                setProductInfo((value) => ({
                                  ...value,
                                  audit: e.target.value,
                                }))
                              }
                            ></textarea>
                          </td>
                        </tr>
                        <tr>
                          <th>사용시 주의사항</th>
                          <td>
                            <textarea
                              className="form-control productInfo-column__description"
                              value={productInfo.precautions}
                              required
                              onChange={(e) =>
                                setProductInfo((value) => ({
                                  ...value,
                                  precautions: e.target.value,
                                }))
                              }
                            ></textarea>
                          </td>
                        </tr>
                        <tr>
                          <th>품질보증 기준</th>
                          <td>
                            <textarea
                              className="form-control productInfo-column__description"
                              value={productInfo.assurance}
                              required
                              onChange={(e) =>
                                setProductInfo((value) => ({
                                  ...value,
                                  assurance: e.target.value,
                                }))
                              }
                            ></textarea>
                          </td>
                        </tr>
                        <tr>
                          <th>
                            소비자상담 관련
                            <br />
                            전화번호
                          </th>
                          <td>
                            <textarea
                              className="form-control productInfo-column__description"
                              value={productInfo.tel}
                              required
                              onChange={(e) =>
                                setProductInfo((value) => ({
                                  ...value,
                                  tel: e.target.value,
                                }))
                              }
                            ></textarea>
                          </td>
                        </tr>
                      </React.Fragment>
                    )}
                  </tbody>
                </Table>
                <div className="text-center">
                  <Button type="submit">저장</Button>
                  <Button
                    className="ml-2"
                    onClick={() => props.history.push("/admin/products/list")}
                  >
                    취소
                  </Button>
                </div>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </form>
    </Container>
  );
};
