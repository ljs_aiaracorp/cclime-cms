import { useState } from "react";
import "../css/Login.scss";
import { Modal, Button, Row, Col, Form, Card, Table } from "react-bootstrap";
import { FileBtn } from "components/FileUpload";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";

export const AddCategoryPopup = (props) => {
  const [isEnabled, setIsEnabled] = useState(true);
  const [sendData, setSendData] = useState({
    title: "",
    use: "y",
    images: "",
  });

  // 카테고리 등록
  const sendList = async () => {
    const sendUrl = axiosUrlFunction("categoryImg", "검색");

    if (sendData.title === "") {
      alert("카테고리명을 입력해 주세요.");
      return;
    } else if (sendData.images === "") {
      alert("이미지를 업로드해 주세요.");
      return;
    }

    const formData = new FormData();
    formData.append("title", sendData.title);
    formData.append("use", sendData.use);
    formData.append("images", sendData.images[0]);
    formData.append("type", "product");

    axios
      .post(`${sendUrl.apiUrl}`, formData, {
        headers: {
          Authorization: `Bearer ${sendUrl.token}`,
          "Content-Type": `multipart/form-data`,
        },
      })
      .then((res) => {
        window.location.replace("/admin/products/categories/list");
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else sendList();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };
  return (
    <Modal show={true} onHide={props.onClickClose} size="lg">
      <Modal.Header closeButton>
        <Modal.Title>
          <b>카테고리 추가</b>
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Row>
          <Col md="12">
            <Card className="card-plain table-plain-bg">
              <Card.Body className="table-full-width table-responsive px-0">
                <Table className="table-hover">
                  <tbody>
                    <tr>
                      <th>카테고리명</th>
                      <td>
                        <input
                          className="form-control"
                          type="text"
                          required
                          placeholder="카테고리명"
                          value={sendData.title}
                          maxLength={15}
                          onChange={(e) => {
                            setSendData((value) => ({
                              ...value,
                              title: e.target.value,
                            }));
                          }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>활성화 여부</th>
                      <td>
                        <Button
                          className={
                            isEnabled ? "select-btn selected" : "select-btn"
                          }
                          onClick={(e) => {
                            setIsEnabled(true);
                            setSendData((value) => ({
                              ...value,
                              use: "y",
                            }));
                          }}
                        >
                          ON
                        </Button>
                        <Button
                          className={
                            isEnabled ? "select-btn" : "select-btn selected"
                          }
                          onClick={(e) => {
                            setIsEnabled(false);
                            setSendData((value) => ({
                              ...value,
                              use: "n",
                            }));
                          }}
                        >
                          OFF
                        </Button>
                      </td>
                    </tr>
                    <tr>
                      <th>카테고리 사진</th>
                      <td>
                        <FileBtn
                          name="업로드"
                          fileData={(data) => {
                            setSendData((value) => ({
                              ...value,
                              images: data,
                            }));
                          }}
                          accept="image/*"
                          id="picture"
                          imageUrl={""}
                        />
                      </td>
                    </tr>
                  </tbody>
                </Table>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Modal.Body>

      <Modal.Footer className="text-center" style={{ display: "block" }}>
        <Button variant="primary" onClick={sendList}>
          저장
        </Button>
        <Button variant="primary" onClick={props.onClickClose} className="ml-2">
          취소
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
