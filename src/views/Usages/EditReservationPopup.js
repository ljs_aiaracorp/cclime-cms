import { useState, useEffect } from "react";
import "../css/Login.scss";
import { Modal, Button, Row, Col, Form, Card, Table } from "react-bootstrap";
import { BeautistListPopup } from "components/Popup/BeautistListPopup";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";
import moment from "moment";
import { BeautystPopup } from "components/Popup/BeautystPopup";

export const EditReservationPopup = (props) => {
  const [selectedDate, setSelectedDate] = useState(
    moment(props.reservation.reservation_start).format("YYYY-MM-DD")
  );
  const [time, setTime] = useState(
    parseInt(moment(props.reservation.reservation_start).format("HHmm"))
  );
  const [timeCount, setTimeCount] = useState(null); // 예약시간 List
  const [beautistInfo, setBeautistInfo] = useState({
    // 선택한 뷰티스트 정보
    name: "",
    beautyst_id: "",
  });
  const [beautistPopup, setBeautistPopup] = useState(false); // 지점별 뷰티스트 리스트 팝업
  const [listprogram, setListProgram] = useState([]);
  useEffect(() => {
    getTimeCount();
  }, []);

  // 예약시간
  const getTimeCount = async (time) => {
    const timeUrl = axiosUrlFunction("reservation/timeCheck");
    const isAdmin = timeUrl.accessor === "main" ? true : false;
    const day = props.reservation.reservation_start.split("T");
    const queries = [];
    if (isAdmin) queries.push(`store_id=${props.reservation.store_id}`);
    if (time === undefined) queries.push(`ymd=${day[0].replace(/\-/g, "")}`);
    else queries.push(`ymd=${time.replace(/\-/g, "")}`);
    const queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";
    setListProgram(props.reservation.program_name.split(","));

    const res = await axios
      .get(`${timeUrl.apiUrl}${queryStr}`, {
        headers: {
          Authorization: `Bearer ${timeUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getTimeCount();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
    setTimeCount(res.data.data);
  };

  // 에약시간 View 출력
  const viewTime = (time, index) => {
    const result = [];
    let strPmAm = "";
    if (parseInt(time.substring(0, 2)) < 12) strPmAm = "오전";
    else strPmAm = "오후";

    if (index !== timeCount.length - 1) {
      result.push(
        <option
          key={index}
          value={parseInt(time.substring(0, 5).replace(/\:/g, ""))}
        >
          {strPmAm} {time.substring(0, 5)} ({timeCount[index].count} /{" "}
          {timeCount[index].bed})
        </option>
      );
    } else {
      return;
    }
    return result;
  };

  // 선택한 뷰티스트 정보
  const addBeautistEvent = (list) => {
    setBeautistInfo(list);
    props.reservation.beautyst_name = list.name;
    setBeautistPopup(false);
  };

  // 예약수정
  const sendData = async () => {
    const sendUrl = axiosUrlFunction("reservation/change");
    const timeModify = time < 1000 ? "0" + time : time;
    let data = {
      reservation_id: props.reservation.reservation_id,
      reservation_time: selectedDate.replace(/\-/g, "") + timeModify,
      beautyst_id: null,
    };

    if (beautistInfo.beautyst_id !== "") {
      data.beautyst_id = beautistInfo.beautyst_id;
    } else if (props.reservation.beautyst_id !== null) {
      data.beautyst_id = props.reservation.beautyst_id;
    }

    if (sendUrl.accessor === "main") {
      data = {
        ...data,
        store_id: props.reservation.store_id,
      };
    }

    axios
      .put(`${sendUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${sendUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        if (res.data.data === 0) {
          alert(res.data.message);
          return;
        }
        props.onClickSave();
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else sendData();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };
  return (
    <Modal show={true} onHide={props.onClickClose} size="lg">
      <Modal.Header closeButton>
        <Modal.Title>
          <b>
            예약 조회/수정({props.reservation.isProgram ? "프로그램" : "선불권"}
            )
          </b>
        </Modal.Title>
      </Modal.Header>

      <Modal.Body className="pt-0">
        <Row>
          <Col md="12">
            <Card className="card-plain table-plain-bg">
              <Card.Body
                className="table-full-width table-responsive px-0 pt-0"
                style={{ fontSize: "18px" }}
              >
                <div
                  className="title-background"
                  style={{
                    fontSize: "20px",
                    paddingLeft: "10px",
                    width: "100%",
                  }}
                >
                  고객정보
                </div>
                <div className="row">
                  <div className="col-12 ml-2">
                    <span style={{ fontWeight: "bold" }}>고객명</span> :{" "}
                    {props.reservation.user_name}
                    &nbsp;&nbsp;&nbsp;{" "}
                    <span style={{ fontWeight: "bold" }}>고객번호</span> :{" "}
                    {props.reservation.user_id}
                    &nbsp;&nbsp;&nbsp;{" "}
                    <span style={{ fontWeight: "bold" }}>
                      핸드폰 번호
                    </span> : {props.reservation.phone}
                  </div>
                </div>
                <div
                  className="title-background"
                  style={{
                    fontSize: "20px",
                    paddingLeft: "10px",
                    width: "100%",
                  }}
                >
                  예약정보
                </div>
                <div className="row">
                  <div className="col-6">
                    <div className="mb-4">
                      <span style={{ fontWeight: "bold" }}>예약지점</span> :{" "}
                      {props.reservation.store_name}
                    </div>
                    {!props.reservation.isProgram && (
                      <>
                        <div className="mb-2">
                          <span style={{ fontWeight: "bold" }}>
                            선불권 선택 프로그램
                          </span>
                          {listprogram.map((value, i) => {
                            return (
                              <span
                                className="ml-2 small"
                                style={{
                                  backgroundColor: "white",
                                  border: "1px solid black",
                                  padding: "8px",
                                }}
                                key={i}
                              >
                                {value}
                              </span>
                            );
                          })}
                        </div>
                      </>
                    )}
                    <div className="mb-4">
                      <span style={{ fontWeight: "bold" }}>예약 프로그램</span>{" "}
                      : {props.reservation.program_name}
                    </div>
                    <div className="mb-0">
                      <span style={{ fontWeight: "bold" }}>담당 뷰티스트</span>{" "}
                      : {props.reservation.beautyst_name || "미배정"}
                      <button
                        style={{ backgroundColor: "white" }}
                        className="ml-2"
                        onClick={() => setBeautistPopup(true)}
                      >
                        선택
                      </button>
                    </div>
                  </div>
                  <div>
                    <span style={{ fontWeight: "bold" }}>예약날짜</span>
                    <input
                      type="date"
                      className="form-control mb-3"
                      style={{
                        display: "inline-block",
                        width: "80%",
                      }}
                      value={selectedDate}
                      onChange={(e) => {
                        setSelectedDate(e.target.value);
                        getTimeCount(e.target.value);
                      }}
                    />
                    <br />
                    <span style={{ fontWeight: "bold" }}>예약시간</span>
                    <select
                      className="form-control"
                      style={{
                        display: "inline-block",
                        width: "80%",
                      }}
                      value={time}
                      onChange={(e) => {
                        setTime(parseInt(e.target.value));
                      }}
                    >
                      {timeCount !== null && timeCount !== undefined ? (
                        timeCount.map((time, index) => {
                          return viewTime(time.time, index);
                        })
                      ) : (
                        <option></option>
                      )}
                    </select>
                  </div>
                </div>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Modal.Body>

      <Modal.Footer className="text-center" style={{ display: "block" }}>
        <Button variant="primary" onClick={(e) => sendData()}>
          확인
        </Button>
      </Modal.Footer>

      {beautistPopup && (
        <BeautystPopup
          onClickClose={() => setBeautistPopup(false)}
          onStoreId={props.reservation.store_id}
          onSaveEvent={addBeautistEvent}
        />
      )}
    </Modal>
  );
};
