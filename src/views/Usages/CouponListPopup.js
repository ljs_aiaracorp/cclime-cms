import React from "react";
import { Modal, Button, Row, Col, Form, Card, Table } from "react-bootstrap";

export const CouponListPopup = ({ onclose, children }) => {
  return (
    <Modal show={true} onHide={onclose} size="sm">
      <Modal.Header closeButton>
        <Modal.Title>
          <b>쿠폰선택</b>
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <div className="addUserPopup__Modal">
          <div
            className="addUserPouup__Modal-content"
            style={{
              width: "100%",
              height: "300px",
              overflow: "auto",
            }}
          >
            <br />
            {children}
          </div>
          <div className="text-center modal-footer">
            <br />
            <button className="btn btn-primary" onClick={onclose}>
              닫기
            </button>
          </div>
        </div>
      </Modal.Body>

      <Modal.Footer></Modal.Footer>
    </Modal>
  );
};
