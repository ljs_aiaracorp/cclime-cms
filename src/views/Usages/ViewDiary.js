import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import { useSelector } from "react-redux";
import moment from "moment";

export const ViewDiary = (props) => {
  const isLoggedIn = useSelector((state) => state.login.isLoggedIn);
  // if (!isLoggedIn) {
  //   window.location.href = "/login";
  // }

  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">예약/사용 처리 - 관리차트 - 다이어리 보기</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <div>"김샛별"님의 다이어리</div>
              <div className="row">
                <div className="col-8">
                  <h4>다이어리1</h4>
                  <div className="row">
                    <div className="col-3 diary">
                      <div className="diary-img"></div>
                      <div className="diary-title">작은얼굴관리 <span style={{ color: '#e20346' }}>1</span>회차</div>
                      <div className="diary-date">(2021.02.03)</div>
                      <div className="diary-status">(비공개)</div>
                    </div>
                    <div className="col-3 diary">
                      <div className="diary-img"></div>
                      <div className="diary-title">이목구비 <span style={{ color: '#e20346' }}>1</span>회차</div>
                      <div className="diary-date">(2021.02.15)</div>
                      <div className="diary-status">(비공개)</div>
                    </div>
                    <div className="col-3 diary">
                      <div className="diary-img"></div>
                      <div className="diary-title">이목구비 <span style={{ color: '#e20346' }}>2</span>회차</div>
                      <div className="diary-date">(2021.02.16)</div>
                      <div className="diary-status">(비공개)</div>
                    </div>
                    <div className="col-3 diary">
                      <div className="diary-img"></div>
                      <div className="diary-title">작은얼굴관리 <span style={{ color: '#e20346' }}>2</span>회차</div>
                      <div className="diary-date">(2021.03.01)</div>
                      <div className="diary-status">(공개)</div>
                    </div>
                  </div>
                  <h4>다이어리2</h4>
                  <div className="row">
                    <div className="col-3 diary">
                      <div className="diary-img"></div>
                      <div className="diary-title">작은얼굴관리 <span style={{ color: '#e20346' }}>1</span>회차</div>
                      <div className="diary-date">(2021.02.03)</div>
                      <div className="diary-status">(비공개)</div>
                    </div>
                    <div className="col-3 diary">
                      <div className="diary-img"></div>
                      <div className="diary-title">이목구비 <span style={{ color: '#e20346' }}>1</span>회차</div>
                      <div className="diary-date">(2021.02.15)</div>
                      <div className="diary-status">(비공개)</div>
                    </div>
                    <div className="col-3 diary">
                      <div className="diary-img"></div>
                      <div className="diary-title">이목구비 <span style={{ color: '#e20346' }}>2</span>회차</div>
                      <div className="diary-date">(2021.02.16)</div>
                      <div className="diary-status">(비공개)</div>
                    </div>
                    <div className="col-3 diary">
                      <div className="diary-img"></div>
                      <div className="diary-title">작은얼굴관리 <span style={{ color: '#e20346' }}>2</span>회차</div>
                      <div className="diary-date">(2021.03.01)</div>
                      <div className="diary-status">(공개)</div>
                    </div>
                  </div>
                </div>
                <div className="col-4">
                  <h4>관리메모</h4>
                  <textarea className="form-control" style={{ minHeight: '500px' }}></textarea>
                  <div className="text-right mt-2">
                    <Button>저장</Button>
                    <Button className="ml-2">초기화</Button>
                  </div>
                </div>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
