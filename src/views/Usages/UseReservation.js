import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import moment from "moment";
import { EditReservationPopup } from "./EditReservationPopup";
import { StorePopup2 } from "components/Popup/StorePopup2";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";
import { NoshowPopup } from "./NoshowPopup";
import { UserListPopup } from "components/Popup/UserListPopup";

export const UseReservation = (props) => {
  const loginUser = JSON.parse(localStorage.getItem("admin")); // Login User Check
  const accessUser = loginUser ? loginUser.data.role : null; // Login User role
  const isAdmin = accessUser === "main";

  const [selectedTab, setSelectedTab] = useState(0);

  const [searchUserName, setSearchUserName] = useState("");
  const [searchPhone, setSearchPhone] = useState("");
  const [searchUserId, setSearchUserId] = useState("");
  const [searchStartDate, setSearchStartDate] = useState(
    moment().format("YYYY-MM-DD")
  );
  const [searchEndDate, setSearchEndDate] = useState(
    moment().format("YYYY-MM-DD")
  );
  const [searchHour, setSearchHour] = useState(-1);

  const [sort, setSort] = useState("asc");

  const [reservationList, setReservationList] = useState([]);
  const [programList, setProgramList] = useState([]);
  const [productList, setProductList] = useState([]);
  const [usedProductList, setUsedProductList] = useState([]);

  const [isProgram, setIsProgram] = useState(true);

  const [selectedReservation, setSelectedReservation] = useState(null);
  const [type, setType] = useState("program");
  const [store, setStore] = useState({
    // store Information
    storeId: "",
    storeName: "",
  });
  const [productSearch, setProductSearch] = useState({
    userName: "",
    userPhone: "",
    userNumber: "",
  });

  const [storeList, setStoreList] = useState([]); // 지점
  const [storeModal, setStoreModal] = useState(false); // 지점선택 모달
  const [noShowPopup, setNoShowPopup] = useState({
    view: false,
    id: "",
  });
  const [userPopup, setUserPopup] = useState(false);
  const [timeCount, setTimeCount] = useState(null); // 예약시간 List
  const [time, setTime] = useState(-1);
  // StoreModal setter
  const addStoreEvent = (store) => {
    if (store.storeId !== "") {
      setStore({
        storeId: store.storeId,
        storeName: store.value,
      });
    } else if (store.storeId === "") {
      setStoreModal(false);
      return;
    }
    getTimeCount(searchStartDate.replace(/\-/g, ""), store.storeId);
    setStoreModal(false);
  };

  // 검색
  const getList = async (params) => {
    let reservationApi = "";
    if (selectedTab === 0) {
      reservationApi = axiosUrlFunction("reservation/searchList/wait");
    } else if (selectedTab === 1) {
      reservationApi = axiosUrlFunction("reservation/searchList/complete");
    }

    const axiosconfig = {
      headers: {
        Authorization: `Bearer ${reservationApi.token}`,
        "Content-Type": `application/json`,
      },
    };

    if (
      parseInt(searchStartDate.replace(/\-/g, "")) >
      parseInt(searchEndDate.replace(/\-/g, ""))
    ) {
      alert("시작기간을 확인해 주세요.");
      return;
    }

    const queries = [];
    if (searchUserName !== "") queries.push(`user_name=${searchUserName}`);
    if (searchPhone !== "") queries.push(`phone=${searchPhone}`);
    if (searchUserId !== "") queries.push(`user_id=${searchUserId}`);
    if (searchStartDate !== "") {
      if (searchEndDate === "") {
        alert("종료일자를 선택해 주세요.");
        return;
      } else {
        queries.push(`start=${searchStartDate.replace(/\-/g, "")}`);
        queries.push(`end=${searchEndDate.replace(/\-/g, "")}`);
      }
    }

    if (time > -1) {
      queries.push(`time=${time}`);
    }

    if (isAdmin) {
      if (store.storeId === "") {
        alert("지점을 선택해 주세요.");
      } else {
        queries.push(`store_id=${store.storeId}`);
      }
    }
    queries.push(`type=${type}`);

    if (params === undefined) {
      queries.push(`order_by=${sort}`);
    } else {
      queries.push(`order_by=${params.sort}`);
    }

    const queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";

    const response = await axios
      .get(`${reservationApi.apiUrl}${queryStr}`, axiosconfig)
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getList(params);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else if (err.response.status === 500) {
          return;
        } else if (err.response.status === 400) {
          alert("올바르게 입력해 주세요.");
          return;
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
    if (response !== undefined) {
      if (selectedTab === 0) setReservationList(response.data.data);
      if (selectedTab === 1) setProgramList(response.data.data);
    }
  };

  // 제품리스트 검색
  const getProductList = async () => {
    const productUrl = axiosUrlFunction("reservation/productList");
    const queries = [];

    queries.push(`user_id=${productSearch.userNumber}`);
    if (isAdmin) queries.push(`store_id=${store.storeId}`);
    const queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";

    await axios
      .get(`${productUrl.apiUrl}${queryStr}`, {
        headers: {
          Authorization: `Bearer ${productUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        setProductList(res.data.data.retention_product_list);
        setUsedProductList(res.data.data.use_product_list);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getProductList();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else if (err.response.status === 500) {
          alert("지점을 선택해주세요.");
          return;
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  const numberWithCommas = (x) => {
    if (!x) return x;
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };

  // 사용처리 Handler
  const completeReservations = async (reservations) => {
    if (reservations.length === 0) {
      return alert("사용처리할 예약을 선택해주세요.");
    }
    const reservationApi = axiosUrlFunction("reservation/complete", "예약");
    const axiosconfig = {
      headers: {
        Authorization: `Bearer ${reservationApi.token}`,
        "Content-Type": `application/json`,
      },
    };
    await axios
      .put(
        reservationApi.apiUrl,
        {
          store_id: isAdmin ? store.storeId : null,
          reservation_ids: reservations.map(
            (reservation) => reservation.reservation_id
          ),
        },
        axiosconfig
      )
      .then((res) => {
        alert("사용처리 되었습니다.");
        getList();
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else completeReservations(reservations);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 미방문 처리
  const completeNoShowList = async (reservations) => {
    if (reservations.length === 0) {
      return;
    }
    if (reservations.length > 1) {
      alert("하나만 체크해 주세요.");
      return;
    } else {
      const noShowUrl = axiosUrlFunction("reservation/noShowDetail");
      const res = await axios
        .get(
          `${noShowUrl.apiUrl}?reservation_id=${reservations[0].reservation_id}`,
          {
            headers: {
              Authorization: `Bearer ${noShowUrl.token}`,
              "Content-Type": `application/json`,
            },
          }
        )
        .catch((err) => {
          if (err.response.status === 401) {
            if (err.response.data.error === "Unauthorized") {
              // 토큰 재발급
              const isCheck = tokenCheck();
              isCheck.then((res) => {
                if (!res) return;
                else completeNoShowList(reservations);
              });
            } else {
              alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
            }
          } else if (err.response.status === 500) {
            return;
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        });
      res.data.data = {
        ...res.data.data,
        store_id: store.storeId,
      };
      setNoShowPopup({
        view: true,
        id: res.data.data,
      });
    }
  };

  let sw = 0;
  const useProduct = async (product) => {
    if (sw === 1) {
      return;
    }
    const reservationApi = axiosUrlFunction(
      "reservation/product/complete",
      "예약"
    );

    const data = {
      retention_id: product.retention_id,
      store_id: isAdmin ? store.storeId : null,
    };

    const axiosconfig = {
      headers: {
        Authorization: `Bearer ${reservationApi.token}`,
        "Content-Type": `application/json`,
      },
    };
    sw = 1;
    await axios
      .put(reservationApi.apiUrl, data, axiosconfig)
      .then((res) => {
        sw = 0;
        alert("사용처리 되었습니다.");
        getProductList();
      })
      .catch((err) => {
        sw = 0;
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else useProduct(product);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 미방문처리 Popup Event Handler
  const buttonEventend = () => {
    getList();
    setNoShowPopup({
      view: false,
      id: "",
    });
  };

  const getUserInfo = (user) => {
    setProductSearch({
      userName: user.user_name,
      userPhone: user.phone,
      userNumber: user.user_id,
    });
    setUserPopup(false);
  };

  // 예약시간
  const getTimeCount = async (time, storeId) => {
    const timeUrl = axiosUrlFunction("reservation/timeCheck");
    const isAdmin = timeUrl.accessor === "main" ? true : false;
    const queries = [];
    if (isAdmin) {
      if (storeId === "") {
        alert("지점을 먼저 선택해 주세요.");
        return;
      } else {
        queries.push(`store_id=${storeId}`);
      }
    }
    if (time === undefined)
      queries.push(`ymd=${searchStartDate.replace(/\-/g, "")}`);
    else queries.push(`ymd=${time.replace(/\-/g, "")}`);
    const queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";

    const res = await axios
      .get(`${timeUrl.apiUrl}${queryStr}`, {
        headers: {
          Authorization: `Bearer ${timeUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getTimeCount();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });

    const times = [];
    times.push({
      time: "전체시간",
    });
    res.data.data.map((value) => {
      times.push(value);
    });
    setTimeCount(times);
  };

  // 에약시간 View 출력
  const viewTime = (time, index) => {
    const result = [];
    let strPmAm = "";
    if (parseInt(time.substring(0, 2)) < 12) strPmAm = "오전";
    else strPmAm = "오후";
    if (index === 0) {
      result.push(
        <option key={index} value={-1}>
          {time}
        </option>
      );
    } else {
      result.push(
        <option
          key={index}
          value={parseInt(time.substring(0, 5).replace(/\:/g, ""))}
        >
          {strPmAm} {time.substring(0, 5)}
        </option>
      );
    }

    return result;
  };

  useEffect(() => {
    if (!isAdmin) getTimeCount(moment().format("YYYY-MM-DD"));
  }, []);

  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">
                예약/사용 처리 - {selectedTab < 2 ? "사용처리" : "제품리스트"}
              </Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <div className="row">
                <div
                  className={`col-2 tab-btn ${
                    selectedTab === 0 ? "selected-tab" : ""
                  }`}
                  onClick={(e) => {
                    setSelectedTab(0);
                    setSearchUserName("");
                    setSearchPhone("");
                    setSearchUserId("");
                    setSearchStartDate(moment().format("YYYY-MM-DD"));
                    setSearchEndDate(moment().format("YYYY-MM-DD"));
                    setSearchHour(-1);
                    setIsProgram(true);
                    setType("program");
                    setStore({
                      storeId: "",
                      storeName: "",
                    });
                    if (!isAdmin) {
                      getTimeCount(moment().format("YYYY-MM-DD"));
                    } else {
                      setTimeCount(null);
                    }
                    setReservationList([]);
                  }}
                >
                  예약리스트
                </div>
                <div
                  className={`col-2 tab-btn ${
                    selectedTab === 1 ? "selected-tab" : ""
                  }`}
                  onClick={(e) => {
                    {
                      setSelectedTab(1);
                      setSearchUserName("");
                      setSearchPhone("");
                      setSearchUserId("");
                      setSearchStartDate(moment().format("YYYY-MM-DD"));
                      setSearchEndDate(moment().format("YYYY-MM-DD"));
                      setSearchHour(-1);
                      setIsProgram(true);
                      setType("program");
                      setStore({
                        storeId: "",
                        storeName: "",
                      });
                      if (!isAdmin) {
                        getTimeCount(moment().format("YYYY-MM-DD"));
                      } else {
                        setTimeCount(null);
                      }
                      setProgramList([]);
                    }
                  }}
                >
                  사용처리리스트
                </div>
                <div
                  className={`col-2 tab-btn ${
                    selectedTab === 2 ? "selected-tab" : ""
                  }`}
                  onClick={(e) => {
                    setSelectedTab(2);
                    setStore({
                      storeId: "",
                      storeName: "",
                    });
                    setProductSearch({
                      userName: "",
                      userPhone: "",
                      userNumber: "",
                    });
                  }}
                >
                  제품리스트
                </div>
              </div>
              {selectedTab === 0 && (
                <>
                  <div className="title-background">검색</div>
                  <Table className="table-hover search-box">
                    <tbody>
                      <tr>
                        <th
                          className="title text-center"
                          style={{ width: "100px" }}
                        >
                          고객명
                        </th>
                        <th className="select-search-target" colSpan={3}>
                          <Form.Control
                            type="text"
                            maxLength={50}
                            value={searchUserName}
                            onChange={(e) => setSearchUserName(e.target.value)}
                            onKeyUp={(e) => {
                              if (e.keyCode === 13) {
                                getList();
                              }
                            }}
                          />
                        </th>
                        <th
                          className="title text-center"
                          style={{ width: "100px" }}
                        >
                          핸드폰번호
                        </th>
                        <th className="select-search-target" colSpan={3}>
                          <Form.Control
                            type="text"
                            maxLength={50}
                            value={searchPhone}
                            onChange={(e) => setSearchPhone(e.target.value)}
                            onKeyUp={(e) => {
                              if (e.keyCode === 13) {
                                getList();
                              }
                            }}
                          />
                        </th>
                        <th
                          className="title text-center"
                          style={{ width: "100px" }}
                        >
                          고객번호
                        </th>
                        <th className="select-search-target" colSpan={3}>
                          <Form.Control
                            type="text"
                            maxLength={50}
                            value={searchUserId}
                            onChange={(e) => setSearchUserId(e.target.value)}
                            onKeyUp={(e) => {
                              if (e.keyCode === 13) {
                                getList();
                              }
                            }}
                          />
                        </th>
                      </tr>
                      <tr>
                        <th
                          className="title text-center"
                          style={{ width: "100px" }}
                        >
                          지점선택
                        </th>
                        <th className="select-search-target" colSpan={3}>
                          {isAdmin && (
                            <Form.Control
                              type="text"
                              readOnly={true}
                              value={store.storeName}
                            />
                          )}
                          {!isAdmin && (
                            <Form.Control
                              type="text"
                              maxLength={50}
                              value={loginUser.data.store_name}
                              readOnly={true}
                            />
                          )}
                        </th>
                        <th className="select-search-target" colSpan={3}>
                          {isAdmin && (
                            <Button onClick={() => setStoreModal(true)}>
                              지점검색
                            </Button>
                          )}
                        </th>
                        <th
                          className="title text-center"
                          style={{ width: "100px" }}
                        >
                          시간
                        </th>
                        <th className="select-search-target" colSpan={3}>
                          <select
                            className="form-control"
                            style={{
                              display: "inline-block",
                              width: "80%",
                            }}
                            value={time}
                            onChange={(e) => {
                              setTime(parseInt(e.target.value));
                            }}
                          >
                            {timeCount !== null && timeCount !== undefined ? (
                              timeCount.map((time, index) => {
                                return viewTime(time.time, index);
                              })
                            ) : (
                              <option>시간을 선택해주세요.</option>
                            )}
                          </select>
                        </th>
                      </tr>
                      <tr>
                        <th
                          className="title text-center"
                          style={{ width: "100px" }}
                        >
                          구분
                        </th>
                        <th className="select-search-target" colSpan={3}>
                          <Button
                            className={
                              isProgram ? "select-btn selected" : "select-btn"
                            }
                            onClick={(e) => {
                              setIsProgram(true);
                              setType("program");
                            }}
                          >
                            프로그램
                          </Button>
                          <Button
                            className={
                              isProgram ? "select-btn" : "select-btn selected"
                            }
                            onClick={(e) => {
                              setIsProgram(false);
                              setType("prepaid");
                            }}
                          >
                            선불권
                          </Button>
                        </th>
                        <th
                          className="title text-center"
                          style={{ width: "100px" }}
                        >
                          기간
                        </th>
                        <th className="select-search-target" colSpan={5}>
                          <Form.Group as={Row} className="mb-0">
                            <Col sm={5} className="pr-0">
                              <Form.Control
                                className="mb-0"
                                type="date"
                                placeholder="시작 날짜"
                                value={searchStartDate}
                                onChange={(e) => {
                                  setSearchStartDate(e.target.value);
                                  getTimeCount(e.target.value, store.storeId);
                                }}
                              />
                            </Col>
                            <Col
                              sm={1}
                              style={{ fontSize: "24px", textAlign: "center" }}
                            >
                              ~
                            </Col>
                            <Col sm={5} className="pl-0">
                              <Form.Control
                                className="mb-0"
                                type="date"
                                placeholder="종료 날짜"
                                value={searchEndDate}
                                onChange={(e) =>
                                  setSearchEndDate(e.target.value)
                                }
                              />
                            </Col>
                          </Form.Group>
                        </th>
                      </tr>
                    </tbody>
                  </Table>
                  <div className="text-center">
                    <Button
                      onClick={() => {
                        getList();
                      }}
                    >
                      검색
                    </Button>
                    <Button
                      onClick={() => {
                        if (!window.confirm("초기화 하시겠습니까?")) return;
                        setSearchUserName("");
                        setSearchPhone("");
                        setSearchUserId("");
                        setSearchStartDate(moment().format("YYYY-MM-DD"));
                        setSearchEndDate(moment().format("YYYY-MM-DD"));
                        if (!isAdmin) {
                          getTimeCount(moment().format("YYYY-MM-DD"));
                        } else {
                          setTimeCount(null);
                        }

                        setSearchHour(-1);
                        setIsProgram(true);
                        setType("program");
                        setStore({
                          storeId: "",
                          storeName: "",
                        });
                        setReservationList([]);
                      }}
                      className="ml-3 cancel"
                    >
                      초기화
                    </Button>
                  </div>
                  <div
                    className="title-background"
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      alignItems: "center",
                    }}
                  >
                    예약리스트
                    <div
                      style={{
                        display: "flex",
                        width: "200px",
                        alignItems: "center",
                      }}
                    >
                      <span className="mr-3 text">필터</span>
                      <select
                        className="form-control"
                        style={{
                          display: "flex",
                          width: "130px",
                        }}
                        value={sort}
                        onChange={(e) => {
                          setSort(e.target.value);
                          getList({
                            sort: e.target.value,
                          });
                        }}
                      >
                        <option value="asc">최신 순</option>
                        <option value="desc">오래된 순</option>
                      </select>
                    </div>
                  </div>
                  <div className="row mt-5 mb-4">
                    <div className="col-3">
                      <Form.Check
                        type="checkbox"
                        label="전체선택"
                        className="checkBox-label"
                        style={{ fontSize: "20px", color: "inherit" }}
                        checked={
                          reservationList.filter(
                            (reservation) => reservation.checked
                          ).length === reservationList.length
                        }
                        onChange={(e) => {
                          setReservationList(
                            reservationList.concat().map((reservation) => {
                              reservation.checked = e.target.checked;
                              return reservation;
                            })
                          );
                        }}
                      />
                    </div>
                    <div
                      className="col-6 text-center"
                      style={{
                        fontSize: "20px",
                        fontWeight: "bold",
                      }}
                    >
                      총{" "}
                      <span style={{ color: "#e20346" }}>
                        {reservationList.length}
                      </span>
                      개의 예약이 있습니다.
                    </div>
                    <div className="col-3 text-right">
                      <button
                        className="mr-3"
                        style={{
                          padding: "7px",
                          width: "120px",
                          backgroundColor: "#ff9f31",
                          color: "white",
                          fontWeight: "bold",
                        }}
                        onClick={(e) => {
                          completeNoShowList(
                            reservationList.filter(
                              (reservation) => reservation.checked
                            )
                          );
                        }}
                      >
                        미방문처리
                      </button>
                      <button
                        style={{
                          padding: "7px",
                          width: "120px",
                          backgroundColor: "#ea0000",
                          color: "white",
                          fontWeight: "bold",
                        }}
                        onClick={(e) =>
                          completeReservations(
                            reservationList.filter(
                              (reservation) => reservation.checked
                            )
                          )
                        }
                      >
                        사용처리
                      </button>
                    </div>
                  </div>
                  {reservationList.map((reservation, idx) => {
                    return (
                      <div className="reservate text-center" key={idx}>
                        <Form.Check
                          type="checkbox"
                          label=""
                          checked={reservation.checked}
                          onChange={() => {
                            const list = reservationList.concat();
                            list[idx].checked = !list[idx].checked;
                            setReservationList(list);
                          }}
                          style={{
                            position: "absolute",
                            top: "10px",
                            left: "0px",
                          }}
                        />
                        <span className="reservate-type">고객명</span>
                        <span className="reservate-value">
                          {reservation.user_name}
                        </span>
                        <span className="reservate-type">예약 프로그램</span>
                        <span className="reservate-value">
                          {reservation.title}
                        </span>
                        <span className="reservate-type">예약일시</span>
                        <span className="reservate-value">
                          {moment(reservation.start).format("YYYY.MM.DD HH:mm")}{" "}
                          ~ {moment(reservation.end).format("HH:mm")}
                        </span>
                        <span className="reservate-type">담당자</span>
                        <span className="reservate-value">
                          {reservation.beautyst_name || "미배정"}
                        </span>
                      </div>
                    );
                  })}
                </>
              )}
              {selectedTab === 1 && (
                <>
                  <div className="title-background">검색</div>
                  <Table className="table-hover search-box">
                    <tbody>
                      <tr>
                        <th
                          className="title text-center"
                          style={{ width: "100px" }}
                        >
                          고객명
                        </th>
                        <th className="select-search-target" colSpan={3}>
                          <Form.Control
                            type="text"
                            maxLength={50}
                            value={searchUserName}
                            onChange={(e) => setSearchUserName(e.target.value)}
                            onKeyUp={(e) => {
                              if (e.keyCode === 13) {
                                getList();
                              }
                            }}
                          />
                        </th>
                        <th
                          className="title text-center"
                          style={{ width: "100px" }}
                        >
                          핸드폰번호
                        </th>
                        <th className="select-search-target" colSpan={3}>
                          <Form.Control
                            type="text"
                            maxLength={50}
                            value={searchPhone}
                            onChange={(e) => setSearchPhone(e.target.value)}
                            onKeyUp={(e) => {
                              if (e.keyCode === 13) {
                                getList();
                              }
                            }}
                          />
                        </th>
                        <th
                          className="title text-center"
                          style={{ width: "100px" }}
                        >
                          고객번호
                        </th>
                        <th className="select-search-target" colSpan={3}>
                          <Form.Control
                            type="text"
                            maxLength={50}
                            value={searchUserId}
                            onChange={(e) => setSearchUserId(e.target.value)}
                            onKeyUp={(e) => {
                              if (e.keyCode === 13) {
                                getList();
                              }
                            }}
                          />
                        </th>
                      </tr>
                      <tr>
                        <th
                          className="title text-center"
                          style={{ width: "100px" }}
                        >
                          지점선택
                        </th>
                        <th className="select-search-target" colSpan={3}>
                          {isAdmin && (
                            <Form.Control
                              type="text"
                              readOnly={true}
                              value={store.storeName}
                            />
                          )}
                          {!isAdmin && (
                            <Form.Control
                              type="text"
                              maxLength={50}
                              value={loginUser.data.store_name}
                              readOnly="true"
                            />
                          )}
                        </th>
                        <th className="select-search-target" colSpan={3}>
                          {isAdmin && (
                            <Button onClick={() => setStoreModal(true)}>
                              지점검색
                            </Button>
                          )}
                        </th>
                        <th
                          className="title text-center"
                          style={{ width: "100px" }}
                        >
                          시간
                        </th>
                        <th className="select-search-target" colSpan={3}>
                          <select
                            className="form-control"
                            style={{
                              display: "inline-block",
                              width: "80%",
                            }}
                            value={time}
                            onChange={(e) => {
                              setTime(parseInt(e.target.value));
                            }}
                          >
                            {timeCount !== null && timeCount !== undefined ? (
                              timeCount.map((time, index) => {
                                return viewTime(time.time, index);
                              })
                            ) : (
                              <option>시간을 선택해주세요.</option>
                            )}
                          </select>
                        </th>
                      </tr>
                      <tr>
                        <th
                          className="title text-center"
                          style={{ width: "100px" }}
                        >
                          구분
                        </th>
                        <th className="select-search-target" colSpan={3}>
                          <Button
                            className={
                              isProgram ? "select-btn selected" : "select-btn"
                            }
                            onClick={(e) => {
                              setIsProgram(true);
                              setType("program");
                            }}
                          >
                            프로그램
                          </Button>
                          <Button
                            className={
                              isProgram ? "select-btn" : "select-btn selected"
                            }
                            onClick={(e) => {
                              setIsProgram(false);
                              setType("prepaid");
                            }}
                          >
                            선불권
                          </Button>
                        </th>
                        <th
                          className="title text-center"
                          style={{ width: "100px" }}
                        >
                          기간
                        </th>
                        <th className="select-search-target" colSpan={5}>
                          <Form.Group as={Row} className="mb-0">
                            <Col sm={5} className="pr-0">
                              <Form.Control
                                className="mb-0"
                                type="date"
                                placeholder="시작 날짜"
                                value={searchStartDate}
                                onChange={(e) => {
                                  setSearchStartDate(e.target.value);
                                  getTimeCount(e.target.value, store.storeId);
                                }}
                              />
                            </Col>
                            <Col
                              sm={1}
                              style={{ fontSize: "24px", textAlign: "center" }}
                            >
                              ~
                            </Col>
                            <Col sm={5} className="pl-0">
                              <Form.Control
                                className="mb-0"
                                type="date"
                                placeholder="종료 날짜"
                                value={searchEndDate}
                                onChange={(e) =>
                                  setSearchEndDate(e.target.value)
                                }
                              />
                            </Col>
                          </Form.Group>
                        </th>
                      </tr>
                    </tbody>
                  </Table>
                  <div className="text-center">
                    <Button
                      onClick={() => {
                        getList();
                      }}
                    >
                      검색
                    </Button>
                    <Button
                      onClick={() => {
                        if (!window.confirm("초기화 하시겠습니까?")) return;
                        setSearchUserName("");
                        setSearchPhone("");
                        setSearchUserId("");
                        setSearchStartDate(moment().format("YYYY-MM-DD"));
                        setSearchEndDate(moment().format("YYYY-MM-DD"));
                        setSearchHour(-1);
                        setIsProgram(true);
                        setType("program");
                        setStore({
                          storeId: "",
                          storeName: "",
                        });
                        setProgramList([]);
                        if (!isAdmin) {
                          getTimeCount(moment().format("YYYY-MM-DD"));
                        } else {
                          setTimeCount(null);
                        }
                      }}
                      className="ml-3 cancel"
                    >
                      초기화
                    </Button>
                  </div>
                  <div
                    className="title-background"
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      alignItems: "center",
                    }}
                  >
                    사용처리리스트
                    <div
                      style={{
                        display: "flex",
                        width: "200px",
                        alignItems: "center",
                      }}
                    >
                      <span className="mr-3 text">필터</span>
                      <select
                        className="form-control"
                        style={{
                          display: "flex",
                          width: "130px",
                        }}
                        value={sort}
                        onChange={(e) => {
                          setSort(e.target.value);
                          setTimeCount(null);
                          getList({
                            sort: e.target.value,
                          });
                        }}
                      >
                        <option value="asc">최신 순</option>
                        <option value="desc">오래된 순</option>
                      </select>
                    </div>
                  </div>
                  <div className="row mt-5 mb-4">
                    <div
                      className="col-12 text-center"
                      style={{
                        fontSize: "20px",
                        fontWeight: "bold",
                      }}
                    >
                      총{" "}
                      <span style={{ color: "#e20346" }}>
                        {programList.length}
                      </span>
                      개의 프로그램/선불권 사용 처리가 있습니다.
                    </div>
                  </div>
                  {programList.map((program, idx) => {
                    return (
                      <div className="reservate text-center" key={idx}>
                        <span
                          className="text-left"
                          style={{
                            display: "inline-block",
                          }}
                        >
                          <span className="reservate-type">고객명</span>
                          <span className="reservate-value">
                            {program.user_name}
                          </span>
                          <span className="reservate-type">예약 프로그램</span>
                          <span className="reservate-value">
                            {program.title}
                          </span>
                          <span className="reservate-type">예약일시</span>
                          <span className="reservate-value">
                            {moment(program.start).format("YYYY.MM.DD HH:mm")} ~{" "}
                            {moment(program.end).format("HH:mm")}
                          </span>
                          <span className="reservate-type">담당자</span>
                          <span className="reservate-value">
                            {program.beautyst_name || "미배정"}
                          </span>
                          {program.is_prepaid && (
                            <>
                              <br />
                              <br />
                              <span className="reservate-type">
                                남은 선불권 잔액
                              </span>
                              <span className="reservate-value">
                                {numberWithCommas(program.remind_amount || 0)}원
                              </span>
                            </>
                          )}
                        </span>
                      </div>
                    );
                  })}
                </>
              )}
              {selectedTab === 2 && (
                <>
                  <div className="title-background">검색</div>
                  <Table className="table-hover search-box">
                    <tbody>
                      <tr>
                        <th
                          className="title text-center"
                          style={{ width: "100px" }}
                        >
                          지점선택
                        </th>
                        <th className="select-search-target" colSpan={3}>
                          {isAdmin && (
                            <Form.Control
                              type="text"
                              readOnly={true}
                              value={store.storeName}
                            />
                          )}
                          {!isAdmin && (
                            <Form.Control
                              type="text"
                              maxLength={50}
                              value={loginUser.data.store_name}
                              readOnly="true"
                            />
                          )}
                        </th>
                        <th className="select-search-target">
                          {isAdmin && (
                            <Button
                              onClick={() => {
                                setStoreModal(true);
                              }}
                            >
                              지점검색
                            </Button>
                          )}
                        </th>
                        <th
                          className="title text-center"
                          style={{ width: "150px" }}
                        >
                          고객 정보 검색
                        </th>
                        <th colSpan={3}>
                          <Button
                            onClick={() => setUserPopup(true)}
                            // disabled={store.storeId !== "" ? false : true}
                          >
                            고객검색
                          </Button>
                        </th>
                      </tr>
                      <tr>
                        <th
                          className="title text-center"
                          style={{ width: "100px" }}
                        >
                          고객명
                        </th>
                        <th className="select-search-target" colSpan={3}>
                          <Form.Control
                            type="text"
                            readOnly
                            maxLength={50}
                            defaultValue={productSearch.userName}
                          />
                        </th>
                        <th
                          className="title text-center"
                          style={{ width: "100px" }}
                        >
                          핸드폰번호
                        </th>
                        <th className="select-search-target" colSpan={3}>
                          <Form.Control
                            type="text"
                            readOnly
                            maxLength={50}
                            defaultValue={productSearch.userPhone}
                          />
                        </th>
                        <th
                          className="title text-center"
                          style={{ width: "100px" }}
                        >
                          고객번호
                        </th>
                        <th className="select-search-target" colSpan={3}>
                          <Form.Control
                            type="text"
                            readOnly
                            maxLength={50}
                            defaultValue={productSearch.userNumber}
                          />
                        </th>
                      </tr>
                    </tbody>
                  </Table>
                  <div className="text-center">
                    <Button
                      disabled={productSearch.userNumber === "" ? true : false}
                      onClick={() => {
                        getProductList();
                      }}
                    >
                      검색
                    </Button>
                    <Button
                      onClick={() => {
                        if (!window.confirm("초기화 하시겠습니까?")) return;
                        setStore({
                          storeId: "",
                          storeName: "",
                        });
                        setProductSearch({
                          userName: "",
                          userPhone: "",
                          userNumber: "",
                        });
                      }}
                      className="ml-3 cancel"
                    >
                      초기화
                    </Button>
                  </div>
                  <div className="row mt-3" style={{ minHeight: "300px" }}>
                    <div className="col-6">
                      <div className="title-background">주문한 제품 리스트</div>
                      {productList.map((product, idx) => {
                        return (
                          <div className="row mb-1" key={idx}>
                            <div className="col-3">
                              <div
                                className="product-thumbnail"
                                style={{
                                  backgroundImage: `url(${product.img})`,
                                  backgroundRepeat: "round",
                                  backgroundSize: "cover",
                                  width: "125px",
                                  height: "195px",
                                }}
                              ></div>
                            </div>
                            <div className="col-6">
                              <div className="product">
                                <div>제품명: {product.product_name}</div>
                                <div>제품분류: {product.category_title}</div>
                                <div>
                                  제품가격:{" "}
                                  {numberWithCommas(product.product_price)}원
                                </div>
                                <div>
                                  수량:{" "}
                                  <span style={{ color: "#e20346" }}>
                                    {product.retention_total}
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div className="col-3 pt-4">
                              <Button onClick={(e) => useProduct(product)}>
                                사용
                              </Button>
                            </div>
                          </div>
                        );
                      })}
                    </div>
                    <div className="col-6">
                      <div className="title-background">
                        판매(수령)완료 리스트
                      </div>
                      {usedProductList.map((product, idx) => {
                        return (
                          <div className="row mb-1" key={idx}>
                            <div className="col-1 pl-0 pr-0 pt-1">
                              NO.{idx + 1}
                            </div>
                            <div className="col-3">
                              <div
                                className="product-thumbnail"
                                style={{
                                  backgroundImage: `url(${product.img})`,
                                  backgroundRepeat: "round",
                                  backgroundSize: "cover",
                                  width: "125px",
                                  height: "195px",
                                }}
                              ></div>
                            </div>
                            <div className="col-5">
                              <div className="product">
                                <div>제품명: {product.product_name}</div>
                                <div>제품분류: {product.category_title}</div>
                                <div>
                                  제품가격:{" "}
                                  {numberWithCommas(product.product_price)}원
                                </div>
                                <div>
                                  사용수량:{" "}
                                  <span style={{ color: "#e20346" }}>
                                    {product.retention_total}
                                  </span>
                                </div>
                                <div>
                                  사용지점:{" "}
                                  <span style={{ color: "#e20346" }}>
                                    {product.store_name}
                                  </span>
                                </div>
                                <div>
                                  사용일시:{" "}
                                  <span style={{ color: "#e20346" }}>
                                    {product.use_date.substring(0, 10)}
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div className="col-3 pt-4"></div>
                          </div>
                        );
                      })}
                    </div>
                  </div>
                </>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
      {selectedReservation && (
        <EditReservationPopup
          reservation={selectedReservation}
          onClickSave={async (reservation) => {
            const reservationApi = axiosUrlFunction(
              "reservation/change",
              "예약"
            );
            const axiosconfig = {
              headers: {
                Authorization: `Bearer ${reservationApi.token}`,
                "Content-Type": `application/json`,
              },
            };
            const data = {
              reservation_id: reservation.reservation_id,
              store_id: store ? store.id : 3,
              reservation_time:
                reservation.selectedDate.replace(/-/g, "") +
                (reservation.selectedHour < 10
                  ? "0" + reservation.selectedHour
                  : reservation.selectedHour) +
                "00",
            };
            if (reservation.beautyst_id) {
              data.beautyst_id = reservation.beautyst_id;
            }
            await axios
              .put(
                `${reservationApi.apiUrl}?${paramsArr.join("&")}`,
                data,
                axiosconfig
              )
              .then((res) => {
                alert("삭제되었습니다.");
                getList();
              })
              .catch((err) => console.error(err));
          }}
          onClickClose={(e) => {
            setSelectedReservation(null);
          }}
        />
      )}
      {storeModal && (
        <StorePopup2
          onClickClose={() => setStoreModal(false)}
          onSaveEvent={addStoreEvent}
        />
      )}
      {noShowPopup.view && (
        <NoshowPopup
          reservation={noShowPopup.id}
          onEndevent={() => {
            buttonEventend();
          }}
          onClickClose={(e) => {
            setNoShowPopup({
              view: false,
              id: "",
            });
          }}
        />
      )}
      {userPopup && (
        <UserListPopup
          onclose={() => setUserPopup(false)}
          onClickUser={getUserInfo}
        />
      )}
    </Container>
  );
};
