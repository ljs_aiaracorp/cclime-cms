import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";
import moment from "moment";

export const AddCoupon = (props) => {
  const userApiData = axiosUrlFunction("coupon", "검색");

  const [couponName, setCouponName] = useState(""); // 쿠폰명
  const [type, setType] = useState(""); // 쿠폰분류
  const [dcMethod, setDcMethod] = useState(""); // 할인방법
  const [discount, setDiscount] = useState(""); // 할인율
  const [minDiscount, setMinDiscount] = useState(""); // 최소금액
  const [maxDiscount, setMaxDiscount] = useState(""); // 최대금액
  const [downloadDay, setDownloadDay] = useState(""); // 다운로드 기한
  const [periodType, setPeriodType] = useState("issued"); // 사용타입
  const [periodDay, setPeriodDay] = useState(""); // 발급 사용기한
  const [explanation, setExplanation] = useState(""); // 참고사항

  useEffect(() => {}, []);

  const sendData = async (event) => {
    event.preventDefault();
    if (type === "") {
      alert("쿠폰분류를 선택해 주세요.");
      return;
    } else if (dcMethod === "") {
      alert("할인방법을 선택해 주세요.");
      return;
    }
    // data setter
    const data = {
      name: couponName,
      coupon_type: type,
      discount_type: dcMethod,
      discount: parseInt(discount),
      download_period: parseInt(downloadDay),
      period_type: periodType,
      coupon_period: parseInt(periodDay),
      explanation: explanation,
      min_discount_price: parseInt(minDiscount),
      max_discount_price: parseInt(maxDiscount),
    };

    if (data.min_discount_price > data.max_discount_price) {
      alert("최대금액보다 최소금액가격이 더 큽니다.\n다시입력해주세요.");
      return;
    }
    axios
      .post(`${userApiData.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${userApiData.token}`,
          "content-type": "application/json;charset=UTF-8",
        },
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else sendData();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });

    props.history.push(`${userApiData.accessPath}/usages/coupon/list`);
  };

  return (
    <Container fluid>
      <form onSubmit={sendData}>
        <Row>
          <Col md="12">
            <Card className="card-plain table-plain-bg">
              <Card.Header>
                <Card.Title as="h4">
                  예약/사용 처리 - 쿠폰관리 - 쿠폰생성
                </Card.Title>
              </Card.Header>
              <Card.Body className="table-full-width table-responsive px-0">
                <div className="row">
                  <div
                    className={`col-2 tab-btn selected-tab`}
                    onClick={(e) => {}}
                  >
                    쿠폰리스트
                  </div>
                  <div className={`col-2 tab-btn`} onClick={(e) => {}}>
                    등급별 쿠폰지급 관리
                  </div>
                </div>
                <Table className="table-hover">
                  <colgroup>
                    <col style={{ width: "15%" }}></col>
                    <col style={{ width: "35%" }}></col>
                    <col style={{ width: "15%" }}></col>
                    <col style={{ width: "35%" }}></col>
                  </colgroup>
                  <tbody>
                    <tr>
                      <th colSpan={4} className="sub-title">
                        쿠폰정보
                      </th>
                    </tr>
                    <tr>
                      <th>쿠폰명</th>
                      <td>
                        <input
                          className="form-control"
                          type="text"
                          required
                          placeholder="쿠폰명"
                          onChange={(e) => setCouponName(e.target.value)}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>쿠폰분류</th>
                      <td>
                        <Button
                          className={
                            type === "program"
                              ? "select-btn selected"
                              : "select-btn"
                          }
                          onClick={(e) => setType("program")}
                        >
                          프로그램
                        </Button>
                        <Button
                          className={
                            type === "product"
                              ? "select-btn selected"
                              : "select-btn"
                          }
                          onClick={(e) => setType("product")}
                        >
                          제품
                        </Button>
                      </td>
                    </tr>
                    <tr>
                      <th>할인방법</th>
                      <td>
                        <Button
                          className={
                            dcMethod === "percent"
                              ? "select-btn selected"
                              : "select-btn"
                          }
                          onClick={(e) => setDcMethod("percent")}
                        >
                          비율할인
                        </Button>
                        <Button
                          className={
                            dcMethod === "money"
                              ? "select-btn selected"
                              : "select-btn"
                          }
                          onClick={(e) => setDcMethod("money")}
                        >
                          금액할인
                        </Button>
                      </td>
                    </tr>
                    {dcMethod === "percent" && (
                      <>
                        <tr>
                          <th>할인율</th>
                          <td>
                            <input
                              className="form-control"
                              type="text"
                              required
                              placeholder="할인율"
                              onChange={(e) => setDiscount(e.target.value)}
                            />
                          </td>
                        </tr>
                        <tr>
                          <th>사용 최소금액</th>
                          <td>
                            <input
                              className="form-control"
                              type="number"
                              required
                              placeholder="최소금액"
                              onChange={(e) => {
                                const val =
                                  0 > parseInt(e.target.value)
                                    ? 0
                                    : parseInt(e.target.value);
                                setMinDiscount(val);
                              }}
                            />
                          </td>
                        </tr>
                        <tr>
                          <th>사용 최대금액</th>
                          <td>
                            <input
                              className="form-control"
                              type="number"
                              required
                              placeholder="최대금액"
                              onChange={(e) => {
                                const val =
                                  0 > parseInt(e.target.value)
                                    ? 0
                                    : parseInt(e.target.value);
                                setMaxDiscount(val);
                              }}
                            />
                          </td>
                        </tr>
                      </>
                    )}
                    {dcMethod === "money" && (
                      <>
                        <tr>
                          <th>할인금액</th>
                          <td>
                            <input
                              className="form-control"
                              type="text"
                              required
                              placeholder="할인금액"
                              onChange={(e) => setDiscount(e.target.value)}
                            />
                          </td>
                        </tr>
                        <tr>
                          <th>사용 최소금액</th>
                          <td>
                            <input
                              className="form-control"
                              type="number"
                              required
                              placeholder="최소금액"
                              onChange={(e) => {
                                const val =
                                  0 > parseInt(e.target.value)
                                    ? 0
                                    : parseInt(e.target.value);
                                setMinDiscount(val);
                              }}
                            />
                          </td>
                        </tr>
                        <tr>
                          <th>사용 최대금액</th>
                          <td>
                            <input
                              className="form-control"
                              type="number"
                              required
                              placeholder="최대금액"
                              onChange={(e) => {
                                const val =
                                  0 > parseInt(e.target.value)
                                    ? 0
                                    : parseInt(e.target.value);
                                setMaxDiscount(val);
                              }}
                            />
                          </td>
                        </tr>
                      </>
                    )}
                    <tr>
                      <th>다운로드 기한</th>
                      <td>
                        <input
                          className="form-control"
                          type="text"
                          required
                          placeholder="n"
                          defaultValue={periodDay}
                          onChange={(e) => setDownloadDay(e.target.value)}
                          style={{
                            display: "inline-block",
                            width: "50px",
                          }}
                        />{" "}
                        지급 후 n일 이내 다운로드 가능
                      </td>
                    </tr>
                    <tr>
                      <th>사용기한</th>
                      <td>
                        <Button
                          className={
                            periodType === "issued"
                              ? "select-btn selected"
                              : "select-btn"
                          }
                          onClick={(e) => setPeriodType("issued")}
                        >
                          발급후
                        </Button>
                        <Button
                          className={
                            periodType === "download"
                              ? "select-btn selected"
                              : "select-btn"
                          }
                          onClick={(e) => setPeriodType("download")}
                        >
                          다운로드후
                        </Button>
                      </td>
                    </tr>
                    {periodType === "issued" && (
                      <tr>
                        <th>발급후</th>
                        <td>
                          <input
                            className="form-control"
                            type="text"
                            required
                            placeholder="n"
                            defaultValue={periodDay}
                            onChange={(e) => setPeriodDay(e.target.value)}
                            style={{
                              display: "inline-block",
                              width: "50px",
                            }}
                          />{" "}
                          일 이내 사용가능
                        </td>
                      </tr>
                    )}
                    {periodType === "download" && (
                      <tr>
                        <th>다운로드후</th>
                        <td>
                          <input
                            className="form-control"
                            type="text"
                            required
                            placeholder="n"
                            onChange={(e) => setPeriodDay(e.target.value)}
                            style={{
                              display: "inline-block",
                              width: "50px",
                            }}
                          />{" "}
                          일 이내 사용가능
                        </td>
                      </tr>
                    )}
                    <tr>
                      <th>
                        쿠폰 유의사항
                        <br />
                        (뒷면)
                      </th>
                      <td>
                        <textarea
                          className="form-control"
                          required
                          style={{ minHeight: "200px" }}
                          onChange={(e) => setExplanation(e.target.value)}
                        ></textarea>
                      </td>
                    </tr>
                  </tbody>
                </Table>
                <div className="text-center">
                  <Button type="submit">저장</Button>
                  <Button onClick={sendData} className="ml-2">
                    취소
                  </Button>
                </div>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </form>
    </Container>
  );
};
