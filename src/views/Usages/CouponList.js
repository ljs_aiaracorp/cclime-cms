import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import axios from "axios";
import Pagination from "components/Pagination";
import Loading from "components/Loading";
import uuid from "react-uuid";
import { CouponByGradePopup } from "./CouponByGradePopup";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";

export const CouponList = (props) => {
  const userApiData = axiosUrlFunction("couponList", "검색"); // axios url & user Inform

  const [titles, setTitles] = useState([
    { label: "NO." },
    { label: "분류" },
    { label: "할인방식" },
    { label: "할인율" },
    { label: "쿠폰명" },
    { label: "사용기한" },
    { label: "" },
  ]);

  const [selectedTab, setSelectedTab] = useState(0);

  const [selected, setSelected] = useState(1);
  const [list, setList] = useState(null);
  const [count, setCount] = useState(0);
  const [pagenum, setPagenum] = useState();

  const [showCouponByGradePopup, setShowCouponByGradePopup] = useState(false);
  const [gold, setGold] = useState("");
  const [platinum, setPlatinum] = useState("");
  const [emerald, setEmerald] = useState("");
  const [sapphire, setSapphire] = useState("");
  const [diamond, setDimond] = useState("");
  const [pushingList, setPushingList] = useState([
    {
      grade_name: "",
      coupon_id: "",
      coupon_name: "",
      discount_type: "",
      discount: "",
      quantity: "",
    },
  ]);
  const [couponNameList, setCouponNameList] = useState([]);
  useEffect(() => {
    getList();
    getGrade();
    getCoupon();
  }, []);

  // Get Coupon
  const getList = async (params) => {
    setList(null);
    if (!params) {
      params = {};
    }

    if (params.page === undefined) {
      params.page = selected - 1;
    }

    setPagenum(params.page);

    const queries = [];
    queries.push(`page=${params.page}`);
    queries.push(`size=10`);
    const queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";

    axios
      .get(`${userApiData.apiUrl}${queryStr}`, {
        headers: {
          Authorization: `Bearer ${userApiData.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        setCount(res.data.data.totalElements);

        setList(
          res.data.data.content.map((contents) => {
            const sapCodeArr = [];
            sapCodeArr.push(contents);
            return sapCodeArr;
          })
        );
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getList(params);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // Delete Coupon
  const deleteCoupon = (id) => {
    const userApiData2 = axiosUrlFunction("coupon/delete", "검색");
    const dataDelete = {
      coupon_id: id,
    };

    axios
      .put(`${userApiData2.apiUrl}`, dataDelete, {
        headers: {
          Authorization: `Bearer ${userApiData2.token}`,
          "content-type": "application/json;charset=UTF-8",
        },
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else deleteCoupon(id);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });

    window.location.replace(`${userApiData2.accessPath}/usages/coupon/list`);
  };

  // ************* 등급별 쿠폰지급 관리 *************
  const setGrade = (value) => {
    setGold(value[1].grade_condition);
    setPlatinum(value[2].grade_condition);
    setEmerald(value[3].grade_condition);
    setSapphire(value[4].grade_condition);
    setDimond(value[5].grade_condition);
  };
  const getGrade = async () => {
    axios
      .get(`/api/grade/benefit`, {
        headers: {
          Authorization: `Bearer ${userApiData.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        setGrade(res.data.data);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getGrade();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 등급조건 체크
  const gradeConditionCheck = () => {
    const check = {
      description: "",
      checking: "",
    };
    if (gold >= platinum) {
      check.description =
        "Platinum 조건이 Gold 보다 낮게 책정 되있습니다.\n다시 확인해주세요.";
      check.checking = false;
      return check;
    } else if (platinum >= emerald) {
      check.description =
        "Emerald 조건이 Platinum 보다 낮게 책정 되있습니다.\n다시 확인해주세요.";
      check.checking = false;
      return check;
    } else if (emerald >= sapphire) {
      check.description =
        "Sapphire 조건이 Emerald 보다 낮게 책정 되있습니다.\n다시 확인해주세요.";
      check.checking = false;
      return check;
    } else if (sapphire >= diamond) {
      check.description =
        "Diamond 조건이 Sapphire 보다 낮게 책정 되있습니다.\n다시 확인해주세요.";
      check.checking = false;
      return check;
    }

    check.checking = true;
    return check;
  };

  const sendGrade = async () => {
    const gradeApi = axiosUrlFunction("grade", "검색");
    const check = gradeConditionCheck();

    if (!check.checking) {
      alert(check.description);
      return null;
    }
    const data = [
      {
        grade_name: "gold",
        grade_condition: gold,
      },
      {
        grade_name: "platinum",
        grade_condition: platinum,
      },
      {
        grade_name: "emerald",
        grade_condition: emerald,
      },
      {
        grade_name: "sapphire",
        grade_condition: sapphire,
      },
      {
        grade_name: "diamond",
        grade_condition: diamond,
      },
    ];

    axios
      .put(`${gradeApi.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${userApiData.token}`,
          "content-type": "application/json;charset=UTF-8",
        },
      })
      .then((res) => {
        alert("수정이 완료되었습니다.");
        props.history.push(`${userApiData.accessPath}/usages/coupon/list`);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else sendGrade();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 등급별 쿠폰정보
  const getCoupon = async () => {
    const getCouponUrl = axiosUrlFunction("grade/coupon", "검색");

    axios
      .get(`${getCouponUrl.apiUrl}`, {
        headers: {
          Authorization: `Bearer ${getCouponUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        const array = [];
        const couponName = [];
        res.data.data.map((coupon) => {
          array.push(coupon);
        });
        for (let i = 0; i < array.length; i++) {
          couponName.push(array[i].grade_name);
          if (array[i].discount_type === "percent")
            array[i].discount = array[i].discount + "%";
          else
            array[i].discount =
              array[i].discount.toLocaleString("ko-KO") + "원";
        }

        const set = new Set(couponName);
        const newCouponname = [...set];

        setCouponNameList(newCouponname);
        setPushingList(array);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getCoupon();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  const viewCoupon = (name) => {
    const result = [];

    for (let i = 0; i < pushingList.length; i++) {
      let str = "";
      if (i !== pushingList.length - 1) {
        str = ",";
      } else {
        str = " ";
      }
      if (name === pushingList[i].grade_name) {
        result.push(
          <p key={uuid()}>
            {pushingList[i].coupon_name}({pushingList[i].discount}){" "}
            {pushingList[i].quantity}장{str} &nbsp;
          </p>
        );
      }
    }
    return result;
  };

  // 등급별 쿠폰지급 수정
  const popupCouponList = (list) => {
    getCoupon();
    setShowCouponByGradePopup(false);
  };
  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">
                예약/사용 처리 - 쿠폰관리 - 쿠폰리스트
              </Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <div className="row">
                <div
                  className={`col-2 tab-btn ${
                    selectedTab === 0 ? "selected-tab" : ""
                  }`}
                  onClick={(e) => setSelectedTab(0)}
                >
                  쿠폰리스트
                </div>
                <div
                  className={`col-2 tab-btn ${
                    selectedTab === 1 ? "selected-tab" : ""
                  }`}
                  onClick={(e) => setSelectedTab(1)}
                >
                  등급별 쿠폰지급 관리
                </div>
              </div>
              {selectedTab === 0 && (
                <>
                  <div className="text-right">
                    <Button
                      onClick={(e) => {
                        props.history.push("/admin/usages/coupon/add");
                      }}
                    >
                      쿠폰생성
                    </Button>
                  </div>
                  <Table className="table-hover">
                    <thead>
                      <tr>
                        {titles.map((v, i) => {
                          return (
                            <th key={i} className="border-0">
                              {v.label}
                            </th>
                          );
                        })}
                      </tr>
                    </thead>
                    <tbody>
                      {list !== undefined && list !== null ? (
                        list.map((coupon, i) => {
                          const type =
                            coupon[0]["period_type"] === "download"
                              ? "다운로드부터"
                              : "발급일부터";
                          const conversion =
                            coupon[0]["discount_type"] === "비율할인"
                              ? coupon[0]["discount"] + "%"
                              : coupon[0]["discount"].toLocaleString("ko-KO");
                          return (
                            <tr key={i} className="cursor">
                              <td>{i + 1 + 10 * pagenum}</td>
                              <td>{coupon[0]["coupon_type"]}</td>
                              <td>{coupon[0]["discount_type"]}</td>
                              <td>{conversion}</td>
                              <td>{coupon[0]["name"]}</td>
                              <td>
                                {type} {coupon[0]["coupon_period"]}일
                              </td>
                              <td>
                                <Button
                                  onClick={(e) => {
                                    window.location.replace(
                                      `${userApiData.accessPath}/usages/coupon/pay/${coupon[0]["coupon_id"]}`
                                    );
                                  }}
                                  className="mr-2"
                                >
                                  지급
                                </Button>
                                <Button
                                  onClick={(e) => {
                                    window.location.replace(
                                      `${userApiData.accessPath}/usages/coupon/edit/${coupon[0]["coupon_id"]}`
                                    );
                                  }}
                                  className="mr-2"
                                >
                                  보기/수정
                                </Button>
                                <Button
                                  onClick={(e) => {
                                    deleteCoupon(coupon[0]["coupon_id"]);
                                  }}
                                  className="mr-2"
                                >
                                  삭제
                                </Button>
                              </td>
                            </tr>
                          );
                        })
                      ) : (
                        <tr>
                          <td
                            colSpan={titles.length}
                            style={{ textAlign: "center" }}
                          >
                            <Loading />
                          </td>
                        </tr>
                      )}
                    </tbody>
                  </Table>
                  <Pagination
                    count={count}
                    forcePage={selected - 1}
                    selected={(pageSelect) => {
                      setSelected(pageSelect);
                      getList({ page: pageSelect - 1 });
                    }}
                  />
                </>
              )}
              {selectedTab === 1 && (
                <>
                  <h4>등급별 승급조건 관리</h4>
                  <div>등급기준(최근 1년간 결제 금액 기준)</div>
                  <Table>
                    <tbody>
                      <tr>
                        <td>BASIC</td>
                        <td>
                          <input
                            className="form-control"
                            type="text"
                            defaultValue="신규 회원 가입"
                            readOnly
                          />
                        </td>
                      </tr>
                      <tr>
                        <td>GOLD</td>
                        <td>
                          <input
                            className="form-control"
                            type="number"
                            defaultValue={gold}
                            onChange={(e) => {
                              const val =
                                0 > parseInt(e.target.value)
                                  ? 0
                                  : parseInt(e.target.value);
                              setGold(val);
                            }}
                          />
                        </td>
                      </tr>
                      <tr>
                        <td>PLATINUM</td>
                        <td>
                          <input
                            className="form-control"
                            type="number"
                            defaultValue={platinum}
                            onChange={(e) => {
                              const val =
                                0 > parseInt(e.target.value)
                                  ? 0
                                  : parseInt(e.target.value);
                              setPlatinum(val);
                            }}
                          />
                        </td>
                      </tr>
                      <tr>
                        <td>EMERALD</td>
                        <td>
                          <input
                            className="form-control"
                            type="number"
                            defaultValue={emerald}
                            onChange={(e) => {
                              const val =
                                0 > parseInt(e.target.value)
                                  ? 0
                                  : parseInt(e.target.value);
                              setEmerald(val);
                            }}
                          />
                        </td>
                      </tr>
                      <tr>
                        <td>SAPPHIRE</td>
                        <td>
                          <input
                            className="form-control"
                            type="number"
                            defaultValue={sapphire}
                            onChange={(e) => {
                              const val =
                                0 > parseInt(e.target.value)
                                  ? 0
                                  : parseInt(e.target.value);
                              setSapphire(val);
                            }}
                          />
                        </td>
                      </tr>
                      <tr>
                        <td>DIAMOND</td>
                        <td>
                          <input
                            className="form-control"
                            type="number"
                            defaultValue={diamond}
                            onChange={(e) => {
                              const val =
                                0 > parseInt(e.target.value)
                                  ? 0
                                  : parseInt(e.target.value);
                              setDimond(val);
                            }}
                          />
                        </td>
                      </tr>
                    </tbody>
                  </Table>
                  <h4 style={{ display: "inline" }}>등급별 쿠폰지급 관리</h4>
                  <Button
                    className="ml-3"
                    onClick={(e) => setShowCouponByGradePopup(true)}
                  >
                    수정
                  </Button>
                  <br />
                  {couponNameList.map((name, i) => {
                    return (
                      <div
                        key={uuid()}
                        style={{
                          display: "flex",
                          width: "100%",
                          flexWrap: "wrap",
                        }}
                      >
                        <p>{name.toUpperCase()} : &nbsp;</p>
                        {viewCoupon(name)}
                      </div>
                    );
                  })}
                  <div
                    className="text-center"
                    style={{ borderTop: "3px solid", marginTop: "1rem" }}
                  >
                    <Button style={{ marginTop: "1rem" }} onClick={sendGrade}>
                      저장
                    </Button>
                  </div>
                </>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
      {showCouponByGradePopup && (
        <CouponByGradePopup
          onSendList={popupCouponList}
          onClickClose={(e) => {
            setShowCouponByGradePopup(false);
          }}
        />
      )}
    </Container>
  );
};
