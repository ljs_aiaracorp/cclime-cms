import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import { useSelector } from "react-redux";
import moment from "moment";
import { EditReservationPopup } from "./EditReservationPopup";
import { StorePopup2 } from "components/Popup/StorePopup2";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";

export const EditReservation = (props) => {
  const loginUser = JSON.parse(localStorage.getItem("admin")); // Login User Check
  const accessUser = loginUser ? loginUser.data.role : null; // Login User role
  const isAdmin = accessUser === "main";

  const [searchUserName, setSearchUserName] = useState("");
  const [searchPhone, setSearchPhone] = useState("");
  const [searchUserId, setSearchUserId] = useState("");
  const [searchStartDate, setSearchStartDate] = useState(
    moment().format("YYYY-MM-DD")
  );
  const [searchEndDate, setSearchEndDate] = useState(
    moment().format("YYYY-MM-DD")
  );
  const [searchHour, setSearchHour] = useState(-1);
  const [list, setList] = useState([]);
  const [isProgram, setIsProgram] = useState(true);
  const [type, setType] = useState("program");
  const [selectedReservation, setSelectedReservation] = useState(null);
  const [sort, setSort] = useState("asc");
  const [store, setStore] = useState({
    // store Information
    storeId: "",
    storeName: "",
  });
  const [storeModal, setStoreModal] = useState(false); // 지점선택 모달
  const [timeCount, setTimeCount] = useState(null); // 예약시간 List
  const [time, setTime] = useState(-1);

  // StoreModal setter
  const addStoreEvent = (store) => {
    if (store.storeId !== "") {
      setStore({
        storeId: store.storeId,
        storeName: store.value,
      });
    }
    getTimeCount(searchStartDate.replace(/\-/g, ""), store.storeId);
    setStoreModal(false);
  };

  // 검색
  const getList = async (params) => {
    const reservationApi = axiosUrlFunction(
      "reservation/searchList/wait",
      "예약"
    );
    const axiosconfig = {
      headers: {
        Authorization: `Bearer ${reservationApi.token}`,
        "Content-Type": `application/json`,
      },
    };

    if (
      parseInt(searchStartDate.replace(/\-/g, "")) >
      parseInt(searchEndDate.replace(/\-/g, ""))
    ) {
      alert("시작기간을 확인해 주세요.");
      return;
    }

    const queries = [];
    if (searchUserName !== "") queries.push(`user_name=${searchUserName}`);
    if (searchPhone !== "") queries.push(`phone=${searchPhone}`);
    if (searchUserId !== "") queries.push(`user_id=${searchUserId}`);
    if (searchStartDate !== "") {
      if (searchEndDate === "") {
        alert("종료일자를 선택해 주세요.");
        return;
      } else {
        queries.push(`start=${searchStartDate.replace(/\-/g, "")}`);
        queries.push(`end=${searchEndDate.replace(/\-/g, "")}`);
      }
    }
    if (time > -1) {
      queries.push(`time=${time}`);
    }

    if (isAdmin) {
      if (store.storeId === "") {
        alert("지점을 선택해 주세요.");
        return;
      } else {
        queries.push(`store_id=${store.storeId}`);
      }
    }
    queries.push(`type=${type}`);

    if (params === undefined) {
      queries.push(`order_by=${sort}`);
    } else {
      queries.push(`order_by=${params.sort}`);
    }

    const queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";

    const response = await axios
      .get(`${reservationApi.apiUrl}${queryStr}`, axiosconfig)
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getList(params);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else if (err.response.status === 500) {
          return;
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });

    if (response !== undefined) setList(response.data.data);
  };

  const sendReservation = () => {
    alert("예약 수정을 완료했습니다.");
    getList();
    setSelectedReservation(null);
  };

  // 예약시간
  const getTimeCount = async (time, storeId) => {
    const timeUrl = axiosUrlFunction("reservation/timeCheck");
    const isAdmin = timeUrl.accessor === "main" ? true : false;
    const queries = [];
    if (isAdmin) {
      if (storeId === "") {
        alert("지점을 먼저 선택해 주세요.");
        return;
      } else {
        queries.push(`store_id=${storeId}`);
      }
    }
    if (time === undefined)
      queries.push(`ymd=${searchStartDate.replace(/\-/g, "")}`);
    else queries.push(`ymd=${time.replace(/\-/g, "")}`);
    const queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";

    const res = await axios
      .get(`${timeUrl.apiUrl}${queryStr}`, {
        headers: {
          Authorization: `Bearer ${timeUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getTimeCount();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });

    const times = [];
    times.push({
      time: "전체시간",
    });
    res.data.data.map((value) => {
      times.push(value);
    });
    setTimeCount(times);
  };

  // 에약시간 View 출력
  const viewTime = (time, index) => {
    const result = [];
    let strPmAm = "";
    if (parseInt(time.substring(0, 2)) < 12) strPmAm = "오전";
    else strPmAm = "오후";
    if (index === 0) {
      result.push(
        <option key={index} value={-1}>
          {time}
        </option>
      );
    } else {
      result.push(
        <option
          key={index}
          value={parseInt(time.substring(0, 5).replace(/\:/g, ""))}
        >
          {strPmAm} {time.substring(0, 5)}
        </option>
      );
    }

    return result;
  };

  useEffect(() => {
    if (!isAdmin) getTimeCount(moment().format("YYYY-MM-DD"));
  }, []);

  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">예약/사용 처리 - 예약조회/수정</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <div className="title-background">검색</div>
              <Table className="table-hover search-box">
                <tbody>
                  <tr>
                    <th
                      className="title text-center"
                      style={{ width: "100px" }}
                    >
                      고객명
                    </th>
                    <th className="select-search-target" colSpan={3}>
                      <Form.Control
                        type="text"
                        maxLength={50}
                        value={searchUserName}
                        onChange={(e) => setSearchUserName(e.target.value)}
                        onKeyUp={(e) => {
                          if (e.keyCode === 13) {
                            getList();
                          }
                        }}
                      />
                    </th>
                    <th
                      className="title text-center"
                      style={{ width: "100px" }}
                    >
                      핸드폰번호
                    </th>
                    <th className="select-search-target" colSpan={3}>
                      <Form.Control
                        type="text"
                        maxLength={13}
                        value={searchPhone}
                        onChange={(e) => setSearchPhone(e.target.value)}
                        onKeyUp={(e) => {
                          if (e.keyCode === 13) {
                            getList();
                          }
                        }}
                      />
                    </th>
                    <th
                      className="title text-center"
                      style={{ width: "100px" }}
                    >
                      고객번호
                    </th>
                    <th className="select-search-target" colSpan={3}>
                      <Form.Control
                        type="text"
                        maxLength={50}
                        value={searchUserId}
                        onChange={(e) => setSearchUserId(e.target.value)}
                        onKeyUp={(e) => {
                          if (e.keyCode === 13) {
                            getList();
                          }
                        }}
                      />
                    </th>
                  </tr>
                  <tr>
                    <th
                      className="title text-center"
                      style={{ width: "100px" }}
                    >
                      지점선택
                    </th>
                    <th className="select-search-target" colSpan={3}>
                      {isAdmin && (
                        <Form.Control
                          type="text"
                          readOnly={true}
                          value={store.storeName}
                        />
                      )}
                      {!isAdmin && (
                        <Form.Control
                          type="text"
                          maxLength={50}
                          value={loginUser.data.store_name}
                          readOnly={true}
                        />
                      )}
                    </th>
                    <th className="select-search-target" colSpan={3}>
                      {isAdmin && (
                        <Button onClick={() => setStoreModal(true)}>
                          지점검색
                        </Button>
                      )}
                    </th>
                    <th
                      className="title text-center"
                      style={{ width: "100px" }}
                    >
                      시간
                    </th>
                    <th className="select-search-target" colSpan={3}>
                      <select
                        className="form-control"
                        style={{
                          display: "inline-block",
                          width: "80%",
                        }}
                        value={time}
                        onChange={(e) => {
                          setTime(parseInt(e.target.value));
                        }}
                      >
                        {timeCount !== null && timeCount !== undefined ? (
                          timeCount.map((time, index) => {
                            return viewTime(time.time, index);
                          })
                        ) : (
                          <option>시간을 선택해주세요.</option>
                        )}
                      </select>
                    </th>
                  </tr>
                  <tr>
                    <th
                      className="title text-center"
                      style={{ width: "100px" }}
                    >
                      구분
                    </th>
                    <th className="select-search-target" colSpan={3}>
                      <Button
                        className={
                          isProgram ? "select-btn selected" : "select-btn"
                        }
                        onClick={(e) => {
                          setIsProgram(true);
                          setType("program");
                        }}
                      >
                        프로그램
                      </Button>
                      <Button
                        className={
                          isProgram ? "select-btn" : "select-btn selected"
                        }
                        onClick={(e) => {
                          setIsProgram(false);
                          setType("prepaid");
                        }}
                      >
                        선불권
                      </Button>
                    </th>
                    <th
                      className="title text-center"
                      style={{ width: "100px" }}
                    >
                      기간
                    </th>
                    <th className="select-search-target" colSpan={5}>
                      <Form.Group as={Row} className="mb-0">
                        <Col sm={5} className="pr-0">
                          <Form.Control
                            className="mb-0"
                            type="date"
                            placeholder="시작 날짜"
                            value={searchStartDate}
                            onChange={(e) => {
                              setSearchStartDate(e.target.value);
                              getTimeCount(e.target.value, store.storeId);
                            }}
                          />
                        </Col>
                        <Col
                          sm={1}
                          style={{ fontSize: "24px", textAlign: "center" }}
                        >
                          ~
                        </Col>
                        <Col sm={5} className="pl-0">
                          <Form.Control
                            className="mb-0"
                            type="date"
                            placeholder="종료 날짜"
                            value={searchEndDate}
                            onChange={(e) => setSearchEndDate(e.target.value)}
                          />
                        </Col>
                      </Form.Group>
                    </th>
                  </tr>
                </tbody>
              </Table>
              <div className="text-center">
                <Button
                  onClick={() => {
                    getList();
                  }}
                >
                  검색
                </Button>
                <Button
                  onClick={() => {
                    if (!window.confirm("초기화 하시겠습니까?")) return;
                    setSearchUserName("");
                    setSearchPhone("");
                    setSearchUserId("");
                    setSearchStartDate(moment().format("YYYY-MM-DD"));
                    setSearchEndDate(moment().format("YYYY-MM-DD"));
                    setSearchHour(-1);
                    setIsProgram(true);
                    setType("program");
                    setStore({
                      storeId: "",
                      storeName: "",
                    });
                    setTimeCount(null);
                    setList([]);
                  }}
                  className="ml-3 cancel"
                >
                  초기화
                </Button>
              </div>
              <div
                className="title-background"
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                예약리스트
                <div
                  style={{
                    display: "flex",
                    width: "200px",
                    alignItems: "center",
                  }}
                >
                  <span className="mr-3 text">필터</span>
                  <select
                    className="form-control"
                    style={{
                      display: "flex",
                      width: "130px",
                    }}
                    value={sort}
                    onChange={(e) => {
                      setSort(e.target.value);
                      getList({
                        sort: e.target.value,
                      });
                    }}
                  >
                    <option value="asc">최신 순</option>
                    <option value="desc">오래된 순</option>
                  </select>
                </div>
              </div>
              <div className="row mt-5 mb-4">
                <div
                  className="col-12 text-center"
                  style={{
                    fontSize: "20px",
                    fontWeight: "bold",
                  }}
                >
                  총{" "}
                  <span style={{ color: "#e20346" }}>
                    {list ? list.length : 0}
                  </span>
                  개의 예약이 있습니다.
                </div>
              </div>
              {list.length > 0 ? (
                list.map((reservate, index) => {
                  return (
                    <div className="reservate text-center" key={index}>
                      <span className="reservate-type">고객명</span>
                      <span className="reservate-value">
                        {reservate.user_name}
                      </span>
                      <span className="reservate-type">예약 프로그램</span>
                      <span className="reservate-value">{reservate.title}</span>
                      <span className="reservate-type">예약일시</span>
                      <span className="reservate-value">
                        {moment(reservate.start).format("YYYY.MM.DD HH:mm")} ~{" "}
                        {moment(reservate.end).format("HH:mm")}
                      </span>
                      <span className="reservate-type">담당자</span>
                      <span className="reservate-value">
                        {reservate.beautyst_name || "미배정"}
                      </span>
                      <br />
                      <br />
                      <Button
                        onClick={async (e) => {
                          const reservationApi = axiosUrlFunction(
                            "reservation/detailCount"
                          );
                          const axiosconfig = {
                            headers: {
                              Authorization: `Bearer ${reservationApi.token}`,
                              "Content-Type": `application/json`,
                            },
                          };
                          const paramsArr = [
                            `reservation_id=${reservate.reservation_id}`,
                            `ymd=${moment(reservate.start).format("YYYYMMDD")}`,
                          ];
                          if (isAdmin)
                            paramsArr.push(`store_id=${store.storeId}`);

                          const response = await axios.get(
                            `${reservationApi.apiUrl}?${paramsArr.join("&")}`,
                            axiosconfig
                          );
                          response.data.data.store_id = store.storeId;
                          response.data.data.reservation_id =
                            reservate.reservation_id;
                          setSelectedReservation({
                            isProgram,
                            ...response.data.data,
                          });
                        }}
                      >
                        조회/수정
                      </Button>
                      <Button
                        className="ml-2"
                        onClick={async (e) => {
                          if (!confirm("예약을 삭제하시겠습니까?")) return;
                          const reservationApi = axiosUrlFunction(
                            "reservation/cancel",
                            "예약"
                          );
                          const axiosconfig = {
                            headers: {
                              Authorization: `Bearer ${reservationApi.token}`,
                              "Content-Type": `application/json`,
                            },
                          };

                          await axios
                            .delete(
                              `${reservationApi.apiUrl}?reservation_id=${reservate.reservation_id}`,
                              axiosconfig
                            )
                            .then((res) => {
                              alert("삭제되었습니다.");
                              getList();
                            })
                            .catch((err) => {
                              console.error(err);
                            });
                        }}
                      >
                        예약삭제
                      </Button>
                    </div>
                  );
                })
              ) : (
                <div className="reservate text-center">
                  <span className="reservate-type">
                    검색된 예약이 없습니다.
                  </span>
                </div>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
      {selectedReservation && (
        <EditReservationPopup
          reservation={selectedReservation}
          onClickSave={sendReservation}
          // onClickSave={async (reservation) => {
          //   const reservationApi = axiosUrlFunction(
          //     "reservation/change",
          //     "예약"
          //   );
          //   const axiosconfig = {
          //     headers: {
          //       Authorization: `Bearer ${reservationApi.token}`,
          //       "Content-Type": `application/json`,
          //     },
          //   };
          //   let data = {
          //     reservation_id: reservation.reservation_id,
          //     reservation_time:
          //       reservation.selectedDate.replace(/-/g, "") +
          //       (reservation.selectedHour < 10
          //         ? "0" + reservation.selectedHour
          //         : reservation.selectedHour) +
          //       "00",
          //   };
          //   if (isAdmin) {
          //     data = {
          //       ...data,
          //       store_id: store.storeId,
          //     };
          //   }

          //   if (reservation.beautyst_id) {
          //     data.beautyst_id = reservation.beautyst_id;
          //   }
          //   await axios
          //     .put(
          //       `${reservationApi.apiUrl}?${paramsArr.join("&")}`,
          //       data,
          //       axiosconfig
          //     )
          //     .then((res) => {
          //       alert("삭제되었습니다.");
          //       getList();
          //     })
          //     .catch((err) => console.error(err));
          // }}
          onClickClose={(e) => {
            setSelectedReservation(null);
          }}
        />
      )}
      {storeModal && (
        <StorePopup2
          onClickClose={() => setStoreModal(false)}
          onSaveEvent={addStoreEvent}
        />
      )}
    </Container>
  );
};
