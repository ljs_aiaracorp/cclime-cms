import { useState, useEffect } from "react";
import "../css/Login.scss";
import { Modal, Button, Row, Col, Form, Card, Table } from "react-bootstrap";
import axios from "axios";
import { CouponListPopup } from "./CouponListPopup";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";

export const CouponByGradePopup = (props) => {
  const [modal, setModal] = useState(false);
  const [grade, setGrade] = useState("basic");
  const [selectCoupon, setSelectCoupon] = useState({
    name: "",
    coupon_id: "",
    discount_type: "",
    discount: "",
  });
  const [couponList, setCouponList] = useState([]);
  const [couponCount, setCouponCount] = useState(0);
  const [pushingList, setPushingList] = useState([
    {
      grade_name: "",
      coupon_id: "",
      coupon_name: "",
      discount_type: "",
      discount: "",
      quantity: "",
    },
  ]);
  const openModal = () => {
    setModal(true);
  };
  const closeModal = () => {
    setModal(false);
  };

  const getCouponhandler = async () => {
    const couponApi = axiosUrlFunction("couponList2", "검색");
    axios
      .get(`${couponApi.apiUrl}`, {
        headers: {
          Authorization: `Bearer ${couponApi.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then((res) => {
        setCoupon(res.data.data);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getCouponhandler();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  const setCoupon = (value) => {
    const array = [];
    value.map((coupon) => {
      array.push(coupon);
    });
    setCouponList(array);
  };

  const setCouponListHandler = (id, type, discount, event) => {
    const { checked, value } = event.target;
    if (checked) {
      setSelectCoupon({
        name: value,
        coupon_id: id,
        discount_type: type,
        discount: discount,
      });
      closeModal();
    }
  };
  // 추가목록
  const pushingCoupon = () => {
    if (isNaN(couponCount)) {
      alert("쿠폰 장수를 입력해 주세요.");
      return;
    } else if (couponCount === 0) {
      alert("쿠폰 장수를 입력해 주세요.");
      return;
    }

    const sale =
      selectCoupon.discount_type === "비율할인"
        ? selectCoupon.discount + "%"
        : selectCoupon.discount.toLocaleString("ko-KO") + "원";

    setPushingList([
      ...pushingList,
      {
        grade_name: grade,
        coupon_id: selectCoupon.coupon_id,
        coupon_name: selectCoupon.name,
        discount: sale,
        quantity: couponCount,
      },
    ]);
  };

  // 추가목록 삭제
  const deleteCoupon = (index) => {
    pushingList.splice(index, 1);
    setPushingList([...pushingList]);
  };

  // 쿠폰저장
  const sendCoupon = async () => {
    const sendApi = axiosUrlFunction("grade/coupon", "검색");

    if (setCouponCount)
      if (pushingList[0].grade_name === "")
        // 빈 배열 삭제
        pushingList.splice(0, 1);

    // 필요없는 요소 삭제
    for (let i = 0; i < pushingList.length; i++) {
      pushingList[i].coupon_name = null;
      pushingList[i].discount = null;
      pushingList[i].discount_type = null;
      delete pushingList[i].coupon_name;
      delete pushingList[i].discount;
      delete pushingList[i].discount_type;
    }

    axios
      .post(`${sendApi.apiUrl}`, pushingList, {
        headers: {
          Authorization: `Bearer ${sendApi.token}`,
          "Content-Type": `application/json`,
        },
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else sendCoupon();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
    alert("저장을 완료했습니다.");
    props.onSendList();
  };

  const getCoupon = async () => {
    const getCouponUrl = axiosUrlFunction("grade/coupon", "검색");

    axios
      .get(`${getCouponUrl.apiUrl}`, {
        headers: {
          Authorization: `Bearer ${getCouponUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        const array = [];

        res.data.data.map((coupon) => {
          array.push(coupon);
        });
        for (let i = 0; i < array.length; i++) {
          if (array[i].discount_type === "percent")
            array[i].discount = array[i].discount + "%";
          else
            array[i].discount =
              array[i].discount.toLocaleString("ko-KO") + "원";
        }
        setPushingList(array);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getCoupon();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  useEffect(() => {
    getCoupon();
  }, []);

  return (
    <Modal show={true} onHide={props.onClickClose} size="xl">
      <Modal.Header closeButton>
        <Modal.Title>
          <b>등급별 쿠폰지급 수정</b>
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Row>
          <Col md="12">
            <Card className="card-plain table-plain-bg">
              <Card.Body className="table-full-width table-responsive px-0">
                <div className="title-background">쿠폰추가</div>
                <Table className="table-hover search-box">
                  <tbody>
                    <tr>
                      <th
                        className="title text-center"
                        style={{ width: "100px" }}
                      >
                        등급
                      </th>
                      <th className="select-search-target" colSpan={1}>
                        <select
                          className="form-control"
                          onChange={(e) => setGrade(e.target.value)}
                          value={grade}
                        >
                          <option value="basic">BASIC</option>
                          <option value="gold">GOLD</option>
                          <option value="platinum">PLATINUM</option>
                          <option value="emerald">EMERALD</option>
                          <option value="sapphire">SAPPHIRE</option>
                          <option value="diamond">DIAMOND</option>
                        </select>
                      </th>
                      <th className="title text-center" colSpan={1}>
                        쿠폰
                      </th>
                      <th className="select-search-target" colSpan={3}>
                        <Form.Control
                          type="text"
                          style={{ width: "270px" }}
                          defaultValue={selectCoupon.name}
                          maxLength={50}
                          readOnly
                        />
                      </th>
                      <th className="select-search-target" colSpan={4}>
                        <Button
                          onClick={(e) => {
                            getCouponhandler();
                            openModal();
                          }}
                        >
                          선택
                        </Button>
                        {modal && (
                          <CouponListPopup onclose={closeModal}>
                            {couponList.map((coupon) => {
                              return (
                                <div key={coupon.coupon_id}>
                                  <label>
                                    <input
                                      type="checkbox"
                                      name="couponCheckBox"
                                      value={coupon.name}
                                      onChange={(event) =>
                                        setCouponListHandler(
                                          coupon.coupon_id,
                                          coupon.discount_type,
                                          coupon.discount,
                                          event
                                        )
                                      }
                                    />
                                    　{coupon.name}
                                  </label>
                                </div>
                              );
                            })}
                          </CouponListPopup>
                        )}
                      </th>
                      <th
                        className="title text-center"
                        style={{ width: "70px" }}
                      >
                        장수
                      </th>
                      <th className="select-search-target" colSpan={3}>
                        <Form.Control
                          type="number"
                          onChange={(e) => {
                            const val =
                              0 > parseInt(e.target.value)
                                ? 0
                                : parseInt(e.target.value);
                            setCouponCount(val);
                          }}
                          min={1}
                          value={couponCount}
                          style={{
                            display: "inline-block",
                            width: "70%",
                          }}
                        />{" "}
                        장
                      </th>
                    </tr>
                  </tbody>
                </Table>
                <div className="text-center">
                  <Button onClick={pushingCoupon}>추가</Button>
                  <Button
                    onClick={() => {
                      if (window.confirm("초기화 하시겠습니까?")) {
                        setGrade("basic");
                        setSelectCoupon({
                          name: "",
                          coupon_id: "",
                          discount_type: "",
                          discount: "",
                        });
                        setCouponCount(0);
                        return null;
                      }
                    }}
                    className="ml-3 cancel"
                  >
                    초기화
                  </Button>
                </div>
                <div className="title-background">쿠폰제거</div>
                <div style={{ fontSize: "23px" }}>등급</div>
                <br />
                <div style={{ height: "500px", overflow: "auto" }}>
                  {pushingList.map((coupon, index) => {
                    if (coupon.coupon_name !== "") {
                      return (
                        <div key={index}>
                          <p>
                            {coupon.grade_name.toUpperCase()} :{" "}
                            {coupon.coupon_name} ({coupon.discount}){" "}
                            {coupon.quantity}장{" "}
                            <FontAwesomeIcon
                              icon={faTimes}
                              style={{ cursor: "pointer" }}
                              onClick={() => deleteCoupon(index)}
                            />
                          </p>
                        </div>
                      );
                    }
                  })}
                </div>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Modal.Body>

      <Modal.Footer className="text-center" style={{ display: "block" }}>
        <Button variant="primary" onClick={sendCoupon}>
          저장
        </Button>
        <Button
          variant="primary"
          onClick={() => {
            if (window.confirm("초기화 하시겠습니까?")) {
              getCoupon();
            }
          }}
          className="ml-3"
        >
          초기화
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
