import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import moment from "moment";
import Calendar from "react-calendar";
import "react-calendar/dist/Calendar.css";
import "moment/locale/ko";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch, faCalendar } from "@fortawesome/free-solid-svg-icons";
import { EditReservationPopup } from "./EditReservationPopup";
import { StorePopup2 } from "components/Popup/StorePopup2";
import { EditReservatePopup } from "./EditReservatePopup";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";
import axios from "axios";

export const Reservations = (props) => {
  const [selectedYear, setSelectedYear] = useState(moment().format("yyyy")); // 달력 년
  const [selectedMonth, setSelectedMonth] = useState(moment().format("MMMM")); // 달력 월
  const [selectedDay, setSelectedDay] = useState(moment().format("DD") + "일"); // 달력 일
  const [selectedDayWeek, setSelectedDayWeek] = useState(
    // 달력 요일
    moment().format("dddd")
  );
  const [date, setDate] = useState(new Date()); // default 달력일
  const [store, setStore] = useState({
    // store Information
    storeId: "",
    sotreName: "",
  });

  const [isCalendar, setisCalendar] = useState(false);
  const [list, setList] = useState(null);
  const [selectedReservation, setSelectedReservation] = useState(null);
  const [storeModal, setStoreModal] = useState(false); // StoreModal
  const [editPopup, setEditPopup] = useState({
    isView: false,
    value: "",
  });
  const [time, setTime] = useState([
    {
      time: "",
      label: "",
    },
  ]);

  // Calendar setter
  const getDate = (event) => {
    setDate(event);
    setSelectedYear(moment(event).format("YYYY"));
    setSelectedMonth(moment(event).format("MMMM"));
    setSelectedDay(moment(event).format("DD") + "일");
    setSelectedDayWeek(moment(event).format("dddd"));
    if (store.storeId !== "") {
      getList(
        store.storeId,
        moment(event).format("YYYY"),
        moment(event).format("MM"),
        moment(event).format("DD")
      );
    } else {
      alert("지점을 선택해주세요.");
      setisCalendar(false);
      return;
    }
    getTimeCount(moment(event).format("YYYY-MM-DD"), "");
    setisCalendar(false);
  };

  const loginUser = JSON.parse(localStorage.getItem("admin")); // Login User Check
  const accessUser = loginUser ? loginUser.data.role : null; // Login User role
  const isAdmin = accessUser === "main";

  // 예약현황 리스트
  const getList = async (id, year, month, day) => {
    const reservationApi = axiosUrlFunction("reservation/list");
    const axiosconfig = {
      headers: {
        Authorization: `Bearer ${reservationApi.token}`,
        "Content-Type": `application/json`,
      },
    };
    const monthCustom =
      parseInt(selectedMonth.replace("월", "")) < 10
        ? "0" + selectedMonth.replace("월", "")
        : selectedMonth.replace("월", "");

    const adminYmd =
      year === undefined
        ? `ymd=${selectedYear + monthCustom + selectedDay.replace("일", "")}`
        : `ymd=${year + month + day}`;

    const paramsArr = isAdmin ? [adminYmd, `store_id=${id}`] : [adminYmd];

    const response = await axios
      .get(`${reservationApi.apiUrl}?${paramsArr.join("&")}`, axiosconfig)
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getList(id, year, month, day);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
            return;
          }
        } else if (err.response.status === 500) {
          alert("지점을 선택해 주세요.");
          return null;
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          return;
        }
      });

    if (response !== null) {
      response.data.data.beautystList.map((beau, index) => {
        beau.row = 0;
      });
      setList(response.data.data);
    }
  };

  // 뷰티스트 출력 메소드
  const getReservationsBeautystNameArray = () => {
    const arr = [];
    if (list !== null) {
      list.beautystList.map((beautyst, i) => {
        if (!arr.includes(beautyst.name)) {
          arr.push({
            name: beautyst.name,
            id: beautyst.beautyst_id,
          });
        }
      });
    }
    if (arr.length < 9) {
      while (arr.length < 9) {
        arr.push({
          name: "-",
          id: "",
        });
      }
    }
    return arr;
  };

  // 예약현황 스케쥴 출력 메소드
  const findReservations = (beautyst, time, timeIndex, beautyIndex) => {
    const arr = [];
    let color = "";
    if (list !== null) {
      for (let i = 0; i < list.reservationPageList.length; i++) {
        let j = 0;
        const startHour = parseInt(
          list.reservationPageList[i].reservation_start
            .substring(11, 16)
            .replace(/\:/g, "")
        );
        const endHour = parseInt(
          list.reservationPageList[i].reservation_end
            .substring(11, 16)
            .replace(/\:/g, "")
        );
        const span = Math.round((endHour - startHour) / 50);
        if (
          list.reservationPageList[i].beautyst_id === beautyst.beautyst_id &&
          startHour === time
        ) {
          if (
            list.reservationPageList[i].counseling === "N" ||
            list.reservationPageList[i].counseling === "n"
          )
            color = "rgb(251, 119, 121)";
          else color = "#f89b00";
          if (span - 1 > 0) {
            beautyst.row = span - 1;
          } else if (span - 1 === 0) {
            beautyst.row = span - 1;
          }
          arr.push({
            reservation: list.reservationPageList[i],
            color: color,
            rowSpan: span,
            prepaidCheck: list.reservationPageList[i].prepaid_check,
            delete: 2,
          });
        }
      }
    }
    if (arr.length === 0) {
      if (beautyst.row > 0) {
        beautyst.row -= 1;
        arr.push({
          delete: 1,
        });
      } else {
        arr.push({
          delete: 0,
        });
      }
    }
    return arr;
  };

  // StoreModal setter
  const addStoreEvent = (store) => {
    if (store.storeId !== "") {
      setStore({
        storeId: store.storeId,
        storeName: store.value,
      });
    } else if (store.storeId === "") {
      setStoreModal(false);
      return;
    }
    getTimeCount(moment(date).format("YYYY-MM-DD"), store.storeId);
    getList(store.storeId);
    setStoreModal(false);
  };

  // 수정 팝업
  const modifyList = (id) => {
    alert("예약수정이 완료했습니다.");
    getList(id);
    setEditPopup({
      isView: false,
      vlaue: "",
    });
  };

  // 예약시간
  const getTimeCount = async (time, storeId) => {
    const timeUrl = axiosUrlFunction("reservation/timeCheck");
    const isAdmin = timeUrl.accessor === "main" ? true : false;
    const queries = [];
    if (isAdmin) {
      if (storeId === "") queries.push(`store_id=${store.storeId}`);
      else queries.push(`store_id=${storeId}`);
    }
    queries.push(`ymd=${time.replace(/\-/g, "")}`);
    const queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";

    const res = await axios
      .get(`${timeUrl.apiUrl}${queryStr}`, {
        headers: {
          Authorization: `Bearer ${timeUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getTimeCount();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
    const timeArr = [];
    res.data.data.map((time) => {
      let amPm = parseInt(time.time.substring(0, 2)) < 12 ? "오전" : "오후";
      timeArr.push({
        time: parseInt(time.time.substring(0, 5).replace(/\:/g, "")),
        label:
          amPm + " " + time.time.substring(0, 5).replace(/\:/g, "시 ") + "분",
      });
    });
    setTime(timeArr);
  };

  useEffect(() => {
    if (!isAdmin) {
      setStore({
        storeId: "0",
        storeName: "",
      });
      getList();
      getTimeCount(moment(date).format("YYYY-MM-DD"));
    }
  }, []);
  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">예약/사용 처리 - 예약현황</Card.Title>
            </Card.Header>
            <Card.Body
              className="table-full-width table-responsive px-0"
              style={{ textAlign: "center" }}
            >
              <Table
                className="table-hover search-box"
                style={{ width: "auto", display: "inline-block" }}
              >
                <tbody>
                  <tr>
                    {isAdmin && (
                      <th className="select-search-target">
                        <input
                          type="button"
                          defaultValue="지점선택"
                          onClick={() => setStoreModal(true)}
                          className="cursor"
                          style={{
                            textAlign: "center",
                            backgroundColor: "white",
                            width: "170px",
                            height: "50px",
                          }}
                        />
                      </th>
                    )}
                    <th>
                      {isAdmin ? (
                        <Form.Control
                          type="text"
                          readOnly={true}
                          defaultValue={store.storeName}
                          style={{
                            display: "inline-block",
                            width: "400px",
                            verticalAlign: "middle",
                          }}
                        />
                      ) : (
                        <Form.Control
                          type="text"
                          placeholder="검색"
                          value={loginUser.data.store_name}
                          maxLength={50}
                          readOnly={true}
                          style={{
                            display: "inline-block",
                            width: "600px",
                            verticalAlign: "middle",
                          }}
                        />
                      )}
                    </th>
                  </tr>
                </tbody>
              </Table>
              <div className="row">
                <div className="col-4"></div>
                <div
                  className="col-4"
                  style={{
                    fontSize: "20px",
                  }}
                >
                  {selectedYear}년 {selectedMonth} {selectedDay}{" "}
                  {selectedDayWeek}{" "}
                  <FontAwesomeIcon
                    icon={faCalendar}
                    style={{
                      cursor: "pointer",
                    }}
                    onClick={() => setisCalendar(true)}
                  />
                  {isCalendar && (
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        // width: "255px",
                      }}
                    >
                      <Calendar
                        className="react-calendar-custom"
                        onChange={(e) => {
                          getDate(e);
                        }}
                        calendarType="US"
                        formatDay={(locale, date) => moment(date).format("D")}
                        value={date}
                      />
                    </div>
                  )}
                </div>
                <div className="col-4">
                  <Button
                    disabled={store.storeId ? false : true}
                    onClick={(e) => {
                      props.history.push(
                        `${isAdmin ? "/admin" : "/staff"}/usages/reservate/${
                          store.storeId
                        }`
                      );
                    }}
                  >
                    예약 추가
                  </Button>
                </div>
              </div>
              <div
                style={{
                  width: "100%",
                }}
              >
                <Table>
                  <colgroup>
                    <col style={{ width: "10%" }}></col>
                    {/* {getReservationsBeautystNameArray().map((name, index) => {
                      return <col style={{ width: "10%" }} key={index}></col>;
                    })} */}
                  </colgroup>
                  <tbody>
                    <tr>
                      <th>담당자</th>
                      {getReservationsBeautystNameArray().map((name, index) => {
                        return <th key={index}>{name.name}</th>;
                      })}
                    </tr>
                    {time.map((time, index) => {
                      return (
                        <tr key={index}>
                          <th style={{ minWidth: "129px" }}>{time.label}</th>
                          {list &&
                            list.beautystList.map((name, index2) => {
                              const reservations = findReservations(
                                name,
                                time.time,
                                index,
                                index2
                              );

                              const reservation = reservations[0];

                              if (reservation.delete === 0) {
                                return <td key={index2}></td>;
                              } else if (reservation.delete === 1) {
                                return;
                              }

                              return (
                                <td
                                  className="cursor"
                                  rowSpan={reservation.rowSpan}
                                  style={{
                                    color: "white",
                                    backgroundColor: reservation.color,
                                    textAlign: "left",
                                    verticalAlign: "top",
                                  }}
                                  key={index2}
                                  onClick={() => {
                                    setEditPopup({
                                      isView: true,
                                      value: reservation,
                                    });
                                  }}
                                >
                                  고객명: {reservation.reservation.user_name}
                                  <br />
                                  {reservation.reservation.program_ko}
                                  {reservation.reservation.counseling === "y" ||
                                  reservation.reservation.counseling === "Y" ? (
                                    <div>고객 상담</div>
                                  ) : (
                                    ""
                                  )}
                                </td>
                              );
                            })}
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
      {selectedReservation && (
        <EditReservationPopup
          reservation={selectedReservation}
          onClickSave={() => {}}
          onClickClose={(e) => {
            setSelectedReservation(null);
          }}
        />
      )}
      {storeModal && (
        <StorePopup2
          onClickClose={() => setStoreModal(false)}
          onSaveEvent={addStoreEvent}
        />
      )}
      {editPopup.isView && (
        <EditReservatePopup
          onReservation={editPopup.value}
          onStore={store}
          onClickClose2={() =>
            setEditPopup({
              isView: false,
              vlaue: "",
            })
          }
          onClickSave={modifyList}
        />
      )}
    </Container>
  );
};
