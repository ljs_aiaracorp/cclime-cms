import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import "moment/locale/ko";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendar } from "@fortawesome/free-solid-svg-icons";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";
import axios from "axios";
import moment from "moment";
import Calendar from "react-calendar";
import "react-calendar/dist/Calendar.css";
import { ProgramListPopup } from "../../components/Popup/ProgramListPopup";
import { BeautistListPopup } from "../../components/Popup/BeautistListPopup";
import { UserNameSearchPopup } from "components/Popup/UserNameSearchPopup";
import { SelectProgramOnCheckPopup } from "../Users/SelectProgramOnCheckPopup";

export const Reservate = (props) => {
  const [userName, setUserName] = useState({
    user_name: "",
    user_id: "",
  });
  const [isProgram, setIsProgram] = useState(true);
  const [programList, setProgramList] = useState({
    value: "",
    programId: "",
    programTime: "",
  }); // 프로그램/선불권 선택 리스트
  const [beautyModalList, setBeautyModalList] = useState(false);
  const [userListModal, setUserListModal] = useState(false);
  const [date, setDate] = useState(new Date()); // default 달력일
  const [selectedDate, setSelectedDate] = useState(""); // 선택 날짜
  const [selectedTime, setSelectedTime] = useState(0); // 선택 시간
  const [isCalendar, setisCalendar] = useState(false);

  const [program, setProgram] = useState(null);
  const [beautist, setBeautist] = useState(null);
  const [showSelectProgramPopup, setShowSelectProgramPopup] = useState(false);
  const [timeCount, setTimeCount] = useState(null); // 예약시간 List
  const [time, setTime] = useState("");

  // Calendar setter
  const getDate = (event) => {
    setDate(event);
    setSelectedDate(moment(event).format("YYYY-MM-DD"));
    getTimeCount(moment(event).format("YYYY-MM-DD"));
    setisCalendar(false);
  };

  const loginUser = JSON.parse(localStorage.getItem("admin")); // Login User Check
  const accessUser = loginUser ? loginUser.data.role : null; // Login User role
  const isAdmin = accessUser === "main";

  let sw = 0;
  const create = async (event) => {
    event.preventDefault();
    if (sw === 1) return;
    const reservateApi = axiosUrlFunction("reservation");
    const axiosconfig = {
      headers: {
        Authorization: `Bearer ${reservateApi.token}`,
        "Content-Type": `application/json`,
      },
    };

    if (userName.user_name === "") {
      alert("고객을 검색해 주세요.");
      return;
    }

    // const time = selectedTime < 10 ? `0${selectedTime}` : selectedTime;
    const data = {
      program_id: programList.programId === "" ? null : programList.programId,
      user_id: userName.user_id,
      reservation_time: moment(date).format("YYYYMMDD") + time,
      type: isProgram ? "program" : "counseling",
      beautyst_id: beautist === null ? null : beautist.beautyst_id,
      program_time: isProgram ? programList.programTime : 30,
    };

    if (isAdmin) {
      data["store_id"] = props.match.params.id;
    }

    sw = 1;
    await axios
      .post(reservateApi.apiUrl, data, axiosconfig)
      .then((res) => {
        if (res.data.data === 0) {
          alert(res.data.message);
          return;
        }
        sw = 0;
        alert("예약이 추가되었습니다.");
        history.back();
      })
      .catch((err) => {
        sw = 0;
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else create(event);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 프로그램/선불권 선택 리스트
  const programCheckedList = (list) => {
    setProgramList(list);
    setShowSelectProgramPopup(false);
  };

  // 예약시간
  const getTimeCount = async (time) => {
    const timeUrl = axiosUrlFunction("reservation/timeCheck");
    const isAdmin = timeUrl.accessor === "main" ? true : false;
    const queries = [];
    if (isAdmin) queries.push(`store_id=${props.match.params.id}`);
    queries.push(`ymd=${time.replace(/\-/g, "")}`);
    const queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";

    const res = await axios
      .get(`${timeUrl.apiUrl}${queryStr}`, {
        headers: {
          Authorization: `Bearer ${timeUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getTimeCount();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
    setTimeCount(res.data.data);
    setTime(res.data.data[0].time.substring(0, 5).replace(/\:/g, ""));
  };

  // 에약시간 View 출력
  const viewTime = (time, index) => {
    const result = [];
    let strPmAm = "";
    if (parseInt(time.substring(0, 2)) < 12) strPmAm = "오전";
    else strPmAm = "오후";

    result.push(
      <option
        key={index}
        value={parseInt(time.substring(0, 5).replace(/\:/g, ""))}
      >
        {strPmAm} {time.substring(0, 5)} ({timeCount[index].count} /{" "}
        {timeCount[index].bed})
      </option>
    );

    return result;
  };

  return (
    <Container fluid>
      <form onSubmit={create}>
        <Row>
          <Col md="12">
            <Card className="card-plain table-plain-bg">
              <Card.Header>
                <Card.Title as="h4">
                  예약/사용 처리 - 예약현황 - 예약 추가
                </Card.Title>
              </Card.Header>
              <Card.Body className="table-full-width table-responsive px-0">
                <div
                  style={{
                    fontSize: "25px",
                    fontWeight: "bold",
                    marginTop: "20px",
                  }}
                >
                  예약정보
                </div>
                <Table className="table-hover">
                  <colgroup>
                    <col style={{ width: "15%" }}></col>
                    <col style={{ width: "35%" }}></col>
                    <col style={{ width: "15%" }}></col>
                    <col style={{ width: "35%" }}></col>
                  </colgroup>
                  <tbody>
                    <tr>
                      <th>고객</th>
                      <td>
                        <input
                          className="form-control"
                          type="text"
                          required
                          readOnly={true}
                          placeholder="고객을 검색해주세요"
                          defaultValue={userName.user_name}
                          style={{
                            width: "76%",
                            display: "inline-block",
                            marginRight: "10px",
                          }}
                        />
                        <Button
                          onClick={() => {
                            setUserListModal(true);
                          }}
                        >
                          고객검색
                        </Button>
                      </td>
                    </tr>
                    <tr>
                      <th>예약날짜</th>
                      <td>
                        <input
                          className="form-control"
                          type="text"
                          required
                          placeholder="예약 날짜를 선택해주세요"
                          onChange={(e) => {
                            setSelectedDate(e.target.value);
                          }}
                          value={selectedDate}
                          style={{
                            width: "90%",
                            display: "inline-block",
                            marginRight: "10px",
                          }}
                        />
                        <FontAwesomeIcon
                          icon={faCalendar}
                          style={{
                            cursor: "pointer",
                          }}
                          onClick={() => setisCalendar(true)}
                        />
                        {isCalendar && (
                          <div>
                            <Calendar
                              className="react-calendar-custom"
                              calendarType="US"
                              formatDay={(locale, date) =>
                                moment(date).format("D")
                              }
                              onChange={(e) => getDate(e)}
                              value={date}
                            />
                          </div>
                        )}
                      </td>
                    </tr>
                    <tr>
                      <th>예약시간</th>
                      <td>
                        <select
                          className="form-control"
                          value={time}
                          onChange={(e) => {
                            setSelectedTime(e.target.value);
                            setTime(parseInt(e.target.value));
                          }}
                        >
                          {selectedDate !== "" &&
                          timeCount !== null &&
                          timeCount !== undefined ? (
                            timeCount.map((time, index) => {
                              return viewTime(time.time, index);
                            })
                          ) : (
                            <option>날짜를 먼저 선택해주세요</option>
                          )}
                        </select>
                      </td>
                    </tr>
                    <tr>
                      <th>예약종류</th>
                      <td>
                        <Button
                          className={
                            isProgram ? "select-btn selected" : "select-btn"
                          }
                          onClick={(e) => setIsProgram(true)}
                        >
                          프로그램
                        </Button>
                        <Button
                          className={
                            isProgram ? "select-btn" : "select-btn selected"
                          }
                          onClick={(e) => setIsProgram(false)}
                        >
                          상담
                        </Button>
                      </td>
                    </tr>
                    {isProgram === true ? (
                      <tr>
                        <th>
                          &nbsp;&nbsp;&nbsp;예약
                          <br />
                          프로그램
                        </th>
                        <td>
                          <input
                            className="form-control"
                            type="text"
                            placeholder="예약할 프로그램을 선택해주세요"
                            defaultValue={programList ? programList.value : ""}
                            style={{
                              width: "76%",
                              display: "inline-block",
                              marginRight: "10px",
                            }}
                          />
                          <Button
                            onClick={() => setShowSelectProgramPopup(true)}
                          >
                            프로그램
                            <br />
                            검색
                          </Button>
                        </td>
                      </tr>
                    ) : (
                      <tr></tr>
                    )}
                    <tr>
                      <th>
                        &nbsp;&nbsp;&nbsp;담당
                        <br />
                        뷰티스트
                      </th>
                      <td>
                        <input
                          className="form-control"
                          type="text"
                          placeholder="담당 뷰티스트를 지정해주세요"
                          defaultValue={beautist ? beautist.name : ""}
                          style={{
                            width: "76%",
                            display: "inline-block",
                            marginRight: "10px",
                          }}
                        />
                        <Button onClick={() => setBeautyModalList(true)}>
                          뷰티스트
                          <br />
                          검색
                        </Button>
                      </td>
                    </tr>
                  </tbody>
                </Table>
                <div className="text-center">
                  <Button type="submit">저장</Button>
                  <Button
                    onClick={() => window.history.back()}
                    className="ml-2"
                  >
                    취소
                  </Button>
                </div>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </form>
      {showSelectProgramPopup && (
        <SelectProgramOnCheckPopup
          onCompleteSelection={programCheckedList}
          onClickClose={(e) => {
            setShowSelectProgramPopup(false);
          }}
        />
      )}
      {/* {programModalList && (
        <ProgramListPopup
          onclose={() => setProgramModalList(false)}
          onClickProgram={(program) => {
            setProgram(program);
            setProgramModalList(false);
          }}
        />
      )} */}
      {beautyModalList && (
        <BeautistListPopup
          onclose={() => setBeautyModalList(false)}
          onClickBeautist={(beautist) => {
            setBeautist(beautist);
            setBeautyModalList(false);
          }}
          onStoreId={props.match.params.id}
        />
      )}
      {userListModal && (
        <UserNameSearchPopup
          onclose={() => setUserListModal(false)}
          onClickUser={(user) => {
            setUserName(user);
            setUserListModal(false);
          }}
        />
      )}
    </Container>
  );
};
