import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import Pagination from "components/Pagination";
import Loading from "components/Loading";
import { useSelector } from "react-redux";
import moment from "moment";

export const ManageChart = (props) => {
  const isLoggedIn = useSelector((state) => state.login.isLoggedIn);
  // if (!isLoggedIn) {
  //   window.location.href = "/login";
  // }

  const [titles, setTitles] = useState([
    { label: "NO." },
    { label: "고객성명" },
    { label: "고객번호" },
    { label: "이메일" },
    { label: "핸드폰번호" },
    { label: "고객등급" },
    { label: "" },
  ]);

  const [selectedTab, setSelectedTab] = useState(0);

  const [selectedYear, setSelectedYear] = useState(
    Number(moment().format("yyyy"))
  );
  const [selectedMonth, setSelectedMonth] = useState(
    Number(moment().format("MM"))
  );

  const [selected, setSelected] = useState(1);

  const [list, setList] = useState(null);

  const [searchTarget, setSearchTarget] = useState("name");
  const [keyword, setKeyword] = useState("");
  const [count, setCount] = useState(0);

  const [searchParam, setSearchParam] = useState({});

  const [isProgram, setIsProgram] = useState(true);

  useEffect(() => {
    getList();
  }, []);

  const getList = async (params) => {
    // setList(null);
    // if (!params) {
    //   params = {};
    // }
    // if (params.searchTarget === undefined) {
    //   params.searchTarget = searchTarget;
    // }
    // if (params.keyword === undefined) {
    //   params.keyword = keyword;
    // }
    // if (params.page === undefined) {
    //   params.page = selected;
    // }
    // setSearchParam(params);
    // const queries = [];
    // if (params.searchTarget !== "" && params.keyword !== "") {
    //   queries.push(`${params.searchTarget}=${params.keyword}`);
    // }
    // queries.push(`page=${params.page}`);
    // queries.push(`size=10`);
    // const queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";
    // axios
    //   .get(`${process.env.REACT_APP_API_URL}/assets${queryStr}`)
    //   .then(async (res) => {
    //     setCount(res.data.total);
    //     setList(res.data.data.map(v => {
    //       const sapCodeArr = [];
    //       if (v.styles) {
    //         v.styles.map(v2 => v2.sapItems && v2.sapItems.map(v3 => {
    //           if (sapCodeArr.includes(v3.sapCode)) return;
    //           sapCodeArr.push(v3.sapCode);
    //         }));
    //       }
    //       v.sapCode = sapCodeArr.join(',');
    //       return v;
    //     }));
    //   })
    //   .catch(err => {
    //     if (err.message.indexOf("401") > -1) {
    //       alert(err.response.data.detail);
    //     } else {
    //       alert('시스템오류가 발생하였습니다. \n관리자에게 문의하세요.');
    //     }
    //   });
  };

  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">예약/사용 처리 - 관리차트</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <div>고객검색</div>
              <Table className="table-hover search-box">
                <tbody>
                  <tr>
                    <th
                      className="title text-center"
                      style={{ width: "100px" }}
                    >
                      고객명
                    </th>
                    <th className="select-search-target" colSpan={3}>
                      <Form.Control type="text" maxLength={50} />
                    </th>
                    <th
                      className="title text-center"
                      style={{ width: "100px" }}
                    >
                      핸드폰번호
                    </th>
                    <th className="select-search-target" colSpan={3}>
                      <Form.Control type="text" maxLength={50} />
                    </th>
                    <th
                      className="title text-center"
                      style={{ width: "100px" }}
                    >
                      고객번호
                    </th>
                    <th className="select-search-target" colSpan={3}>
                      <Form.Control type="text" maxLength={50} />
                    </th>
                  </tr>
                </tbody>
              </Table>
              <div className="text-center">
                <Button
                  onClick={() => {
                    setSelected(1);
                    getList({
                      page: 1,
                    });
                  }}
                >
                  검색
                </Button>
                <Button
                  onClick={() => {
                    if (!window.confirm("초기화 하시겠습니까?")) return;
                    setSelected(1);
                    setSearchTarget("name");
                    setKeyword("");
                    getList({
                      page: 1,
                      searchTarget: "",
                      keyword: "",
                    });
                  }}
                  className="ml-3 cancel"
                >
                  초기화
                </Button>
              </div>
              <Table className="table-hover">
                <thead>
                  <tr>
                    {titles.map((v, i) => {
                      return (
                        <th key={i} className="border-0">
                          {v.label}
                        </th>
                      );
                    })}
                  </tr>
                </thead>
                <tbody>
                  {list !== undefined && list !== null ? (
                    list.map((user, i) => {
                      return (
                        <tr key={i} className="cursor">
                          <td>{count - (selected - 1) * 10 - i}</td>
                          <td>이름</td>
                          <td>번호</td>
                          <td>이메일</td>
                          <td>핸드폰번호</td>
                          <td>등급</td>
                          <td>
                            <Button
                              onClick={(e) => {
                                window.location.replace(
                                  `/admin/usages/view-diary/${user.userId}`
                                );
                              }}
                              className="mr-2"
                            >
                              수정
                            </Button>
                          </td>
                        </tr>
                      );
                    })
                  ) : (
                    <tr>
                      <td
                        colSpan={titles.length}
                        style={{ textAlign: "center" }}
                      >
                        <Loading />
                      </td>
                    </tr>
                  )}
                </tbody>
              </Table>
              <Pagination
                count={count}
                forcePage={selected - 1}
                selected={(pageSelect) => {
                  setSelected(pageSelect);
                  getList({ page: pageSelect });
                }}
              />
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
