import { useState, useEffect } from "react";
import "../css/Login.scss";
import { Modal, Button, Row, Col, Form, Card, Table } from "react-bootstrap";
import { BeautistListPopup } from "components/Popup/BeautistListPopup";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";
import moment from "moment";
import { BeautystPopup } from "components/Popup/BeautystPopup";

export const NoshowPopup = (props) => {
  const [date, setDate] = useState(
    moment(props.reservation.reservation_start).format("YYYY-MM-DD")
  );
  const [time, setTime] = useState(
    parseInt(moment(props.reservation.reservation_start).format("HH")) < 12
      ? "오전 " + moment(props.reservation.reservation_start).format("HH:mm")
      : "오후 " + moment(props.reservation.reservation_start).format("HH:mm")
  );

  // 미방문처리 event handler
  const deletenoShow = async () => {
    const deleteUrl = axiosUrlFunction("reservation/noShow");

    const queries = [];
    queries.push(`reservation_id=${props.reservation.reservation_id}`);
    if (props.reservation.store_id !== "")
      queries.pish(`store_id=${props.reservation.store_id}`);
    const queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";

    axios
      .delete(`${deleteUrl.apiUrl}${queryStr}`, {
        headers: {
          Authorization: `Bearer ${deleteUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then((res) => {
        alert("미방문처리가 완료되었습니다.");
        props.onEndevent();
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else deletenoShow();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else if (err.response.status === 500) {
          return;
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  //미방문사용처리 Event handler
  const putnoShow = async () => {
    const putUrl = axiosUrlFunction("reservation/noShowComplete");

    const data = {
      reservation_id: props.reservation.reservation_id,
      store_id:
        props.reservation.store_id !== "" ? props.reservation.store_id : null,
    };

    axios
      .put(`${putUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${putUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then((res) => {
        alert("미방문사용처리가 완료되었습니다.");
        props.onEndevent();
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else putnoShow();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else if (err.response.status === 500) {
          return;
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };
  return (
    <Modal show={true} onHide={props.onClickClose} size="xl">
      <Modal.Header closeButton>
        <Modal.Title>
          <b>미방문사용처리</b>
        </Modal.Title>
      </Modal.Header>

      <Modal.Body className="pt-0">
        <Row>
          <Col md="12">
            <Card className="card-plain table-plain-bg">
              <Card.Body
                className="table-full-width table-responsive px-0 pt-0"
                style={{ fontSize: "18px" }}
              >
                <div
                  className="title-background"
                  style={{
                    fontSize: "20px",
                    paddingLeft: "10px",
                    width: "100%",
                  }}
                >
                  고객정보
                </div>
                <div className="row">
                  <div className="col-12 ml-2">
                    <span style={{ fontWeight: "bold" }}>고객명</span> :{" "}
                    {props.reservation.user_name}
                    &nbsp;&nbsp;&nbsp;{" "}
                    <span style={{ fontWeight: "bold" }}>고객번호</span> :{" "}
                    {props.reservation.user_id}
                    &nbsp;&nbsp;&nbsp;{" "}
                    <span style={{ fontWeight: "bold" }}>
                      핸드폰 번호
                    </span> : {props.reservation.phone}
                  </div>
                </div>
                <div
                  className="title-background"
                  style={{
                    fontSize: "20px",
                    paddingLeft: "10px",
                    width: "100%",
                  }}
                >
                  예약정보
                </div>
                <div className="row">
                  <div className="col-5">
                    <div className="mb-4">
                      <span style={{ fontWeight: "bold" }}>예약지점</span> :{" "}
                      {props.reservation.store_name}
                    </div>
                    <div className="mb-4">
                      <span style={{ fontWeight: "bold" }}>예약 프로그램</span>{" "}
                      : {props.reservation.program_name}
                    </div>
                    <div className="mb-0">
                      <span style={{ fontWeight: "bold" }}>담당 뷰티스트</span>{" "}
                      : {props.reservation.beautyst_name || "미배정"}
                    </div>
                  </div>
                  <div className="col-6">
                    <div>
                      <span style={{ fontWeight: "bold" }} className="mr-3">
                        예약날짜
                      </span>
                      <input
                        type="text"
                        className="form-control mb-3"
                        style={{
                          display: "inline-block",
                          width: "80%",
                        }}
                        defaultValue={date}
                        readOnly={true}
                      />
                    </div>
                    <div>
                      <span style={{ fontWeight: "bold" }} className="mr-3">
                        예약시간
                      </span>
                      <input
                        className="form-control"
                        style={{
                          display: "inline-block",
                          width: "80%",
                        }}
                        defaultValue={time}
                        readOnly={true}
                      />
                    </div>
                    <div className="mt-4">
                      <span style={{ fontWeight: "bold" }}>
                        지점 미방문횟수 :{" "}
                        <span style={{ color: "red" }}>
                          {props.reservation.noShow}
                        </span>
                      </span>
                    </div>
                  </div>
                </div>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Modal.Body>

      <Modal.Footer className="text-center" style={{ display: "block" }}>
        <div>
          <p>
            해당 고객을 미방문으로 처리합니다. 미방문 사용처리의 경우 해당
            예약은 사용처리 되며 미방문횟수가 초기화됩니다.
          </p>
        </div>
        <button
          className="mr-3"
          style={{
            padding: "8px",
            backgroundColor: "white",
            borderRadius: "10%",
            width: "150px",
          }}
          variant="primary"
          onClick={deletenoShow}
        >
          미방문처리
        </button>
        <button
          style={{
            padding: "8px",
            backgroundColor: "white",
            borderRadius: "10%",
            width: "150px",
          }}
          variant="primary"
          onClick={putnoShow}
        >
          미방문사용처리
        </button>
      </Modal.Footer>
    </Modal>
  );
};
