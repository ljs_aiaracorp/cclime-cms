import { useSelector } from "react-redux";
import "../css/Login.scss";
import routes, { headerMenu } from "routes.js";

export const Main = (props) => {
  const loginUser = useSelector((state) => state.login.admin); // Login User Check
  const accessUser = loginUser ? loginUser.data.role : null; // Login User role

  let logoTitle = null; // Main LogoTitle
  let accessPathName = null; // access rights

  if (accessUser === "main") {
    logoTitle = "끌리메 본사용";
    accessPathName = "/admin";
  } else if (accessUser === "store") {
    logoTitle = "끌리메 지점용(원장)";
    accessPathName = "/staff";
  } else if (accessUser === "staff") {
    logoTitle = "끌리메 지점용(일반)";
    accessPathName = "/staff";
  } else if (accessUser === null) {
    props.history.push("/login");
  }

  return (
    <div className="bg-img">
      <div className="content" style={{ width: "50%" }}>
        <header>
          <img src="/logo.png" />
        </header>
        <div>&lt;{logoTitle}&gt;</div>
        <div className="text-left">
          {headerMenu.map((menu, idx) => {
            const key = menu.path.split("/")[1];
            const route = routes[key][0];
            if (accessUser === "main") {
              return (
                <a
                  className="main-route-button"
                  href={`${accessPathName}${route.sub[0].path}`}
                  key={idx}
                >
                  {route.name}
                </a>
              );
            } else {
              if (!route.onlyHeadquater) {
                return (
                  <a
                    className="main-route-button"
                    href={`${accessPathName}${route.sub[0].path}`}
                    key={idx}
                  >
                    {route.name}
                  </a>
                );
              }
            }
            // }
          })}
        </div>
      </div>
    </div>
  );
};
