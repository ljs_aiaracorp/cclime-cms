import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import "../css/menu/Tickets.scss";
import Loading from "components/Loading";
import { TicketsProgramPopup } from "./TicketsProgramPopup";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";

export const Tickets = (props) => {
  const [list, setList] = useState(null);
  const [programList, setProgramList] = useState(null);
  const [programYlist, setProgramYlist] = useState(null);
  const [programPopup, setProgramPopup] = useState(false);

  useEffect(() => {
    getPrepaid();
  }, []);

  // 선불권 리스트
  const getPrepaid = async () => {
    const prepaidUrl = axiosUrlFunction("prepaidList", "검색");

    axios
      .get(`${prepaidUrl.apiUrl}`, {
        headers: {
          Authorization: `Bearer ${prepaidUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        setList(res.data.data);
        getProgramList();
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getPrepaid();
            });
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 프로그램 관리
  const getProgramList = async () => {
    setProgramList(null);
    const programUrl = axiosUrlFunction("program/prepaid", "검색");

    axios
      .get(`${programUrl.apiUrl}`, {
        headers: {
          Authorization: `Bearer ${programUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        if (res.data.status) return res;
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getProgramList();
            });
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      })
      .then(async (res) => {
        const data = [];
        setProgramList(res.data.data);
        res.data.data.map((value) => {
          if (value.use === "Y" || value.use === "y") {
            data.push({
              name: value.program_ko,
            });
          }
        });
        setProgramYlist(data);
      });
  };

  // 선불권 사용가능 프로그램 편집
  const checkedList = async (value) => {
    const useUrl = axiosUrlFunction("prepaid/useProgram", "검색");

    axios
      .post(`${useUrl.apiUrl}`, value, {
        headers: {
          Authorization: `Bearer ${useUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(() => {
        getProgramList();
        setProgramPopup(false);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else checkedList(value);
            });
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 선불권 삭제
  const deleteData = async (id, index) => {
    const deleteUrl = axiosUrlFunction("prepaid/delete", "검색");
    const data = {
      prepaid_id: id,
    };

    axios
      .put(`${deleteUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${deleteUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then((res) => {
        if (res.data.data === 0) {
          alert("사용중인 고객이있어 삭제가 불가능합니다.");
          return;
        } else if (res.data.data === 1) {
          alert("삭제가 완료되었습니다.");
          list.splice(index, 1);
          setList([...list]);
        }
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else deleteData(id, index);
            });
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 활성화 여부 변경
  const changeUse = async (id) => {
    const changeUrl = axiosUrlFunction("prepaid/use", "검색");

    const data = {
      prepaid_id: id,
    };
    await axios
      .put(`${changeUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${changeUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        await getPrepaid();
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else changeUse(id);
            });
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };
  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">메뉴관리 - 선불권</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <h4>선불권 리스트</h4>
              <div className="text-right mb-3">
                <Button
                  onClick={(e) => {
                    window.location.replace("/admin/menus/tickets/add");
                  }}
                >
                  선불권 추가
                </Button>
              </div>
              <div className="ticket-tableScroll">
                <table className="table-hover mt-0 table-center ticket-tableScroll-Table">
                  <thead>
                    <tr>
                      <th className="border-0">NO.</th>
                      <th className="border-0">선불권명</th>
                      <th className="border-0">선택 가능 프로그램 수</th>
                      <th className="border-0">활성화여부</th>
                      <th className="border-0"></th>
                    </tr>
                  </thead>
                  <tbody>
                    {list !== undefined && list !== null ? (
                      list.map((value, i) => {
                        const use =
                          value.use === "Y" || value.use === "y" ? "ON" : "OFF";
                        return (
                          <tr key={i} className="cursor">
                            <td>{i + 1}</td>
                            <td>{value.name}</td>
                            <td>{value.count}</td>
                            <td>
                              <Button
                                id={value.prepaidId}
                                onClick={() => changeUse(value.prepaidId)}
                              >
                                {use}
                              </Button>
                            </td>
                            <td>
                              <Button
                                className="mr-3"
                                onClick={() =>
                                  props.history.push(
                                    `/admin/menus/tickets/edit/${value.prepaidId}`
                                  )
                                }
                              >
                                조회/수정
                              </Button>
                              <Button
                                onClick={() => deleteData(value.prepaidId, i)}
                              >
                                삭제
                              </Button>
                            </td>
                          </tr>
                        );
                      })
                    ) : (
                      <tr>
                        <td colSpan={4} style={{ textAlign: "center" }}>
                          <Loading />
                        </td>
                      </tr>
                    )}
                  </tbody>
                </table>
              </div>
              <h4>프로그램 관리</h4>
              <div className="text-right">
                <Button
                  className="mb-3"
                  onClick={(e) => {
                    setProgramPopup(true);
                  }}
                >
                  프로그램 관리
                </Button>
              </div>
              <div className="program-prepaidList">
                <div className="program-prepaidList__title">
                  선불권 프로그램
                </div>
                {programYlist !== null && programYlist !== undefined ? (
                  programYlist.map((value, i) => {
                    return <div key={i}>{value.name}</div>;
                  })
                ) : (
                  <div></div>
                )}
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
      {programPopup && (
        <TicketsProgramPopup
          list={programList}
          onCompleteSelection={checkedList}
          onClickClose={() => {
            setProgramPopup(false);
          }}
        />
      )}
    </Container>
  );
};
