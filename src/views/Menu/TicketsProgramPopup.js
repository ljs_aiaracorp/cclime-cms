import { useState, useEffect } from "react";
import "../css/Login.scss";
import { Modal, Button, Row, Col, Form, Card, Table } from "react-bootstrap";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";

export const TicketsProgramPopup = (props) => {
  const [list, setList] = useState([
    {
      id: "",
      value: "",
      checked: false,
    },
  ]);
  const [allChecked, setAllChecked] = useState(false);

  useEffect(() => {
    getList();
  }, []);

  // List 저장
  const getList = () => {
    const data = [];
    props.list.map((value) => {
      const checked = value.use === "y" || value.use === "Y" ? true : false;
      data.push({
        id: value.program_id,
        value: value.program_ko,
        checked,
      });
    });
    setList(data);
  };

  const checkboxEventHandler = (event) => {
    const { value, checked } = event.target;
    const data = [];
    list.map((check) => {
      data.push(check);
    });

    if (checked) {
      for (let i = 0; i < data.length; i++) {
        if (data[i].value === value) {
          data[i].checked = true;
          setList(data);
          return;
        }
      }
    } else {
      for (let i = 0; i < data.length; i++) {
        if (data[i].value === value) {
          data[i].checked = false;
          setList(data);
          return;
        }
      }
    }
  };

  const allCheckedList = (checked) => {
    if (checked) {
      for (let i = 0; i < list.length; i++) {
        list[i].checked = true;
      }
      setAllChecked(true);
    } else {
      for (let i = 0; i < list.length; i++) {
        list[i].checked = false;
      }
      setAllChecked(false);
    }
  };

  const sendList = () => {
    const data = [];
    list.map((v) => {
      if (v.checked) {
        data.push(v.id);
      }
    });
    return data;
  };

  return (
    <Modal show={true} onHide={props.onClickClose} size="lg">
      <Modal.Header closeButton>
        <Modal.Title>
          <b>프로그램 선택</b>
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Row>
          <Col md="12">
            <Card className="card-plain table-plain-bg">
              <Card.Body className="table-full-width table-responsive px-0">
                <div style={{ height: "600px" }}>
                  <Table className="table-hover mt-0">
                    <thead>
                      <tr>
                        <td>
                          <input
                            className="cursor"
                            type="checkbox"
                            checked={allChecked}
                            onChange={(e) => {
                              allCheckedList(e.target.checked);
                            }}
                          />
                        </td>
                        <td>NO</td>
                        <td>프로그램/선불권 명</td>
                      </tr>
                    </thead>
                    <tbody>
                      {list &&
                        list.map((v, i) => {
                          return (
                            <tr key={i}>
                              <td>
                                <input
                                  className="cursor"
                                  type="checkbox"
                                  value={v.value}
                                  checked={v.checked}
                                  onChange={(e) => {
                                    checkboxEventHandler(e);
                                  }}
                                />
                              </td>
                              <td>{i + 1}</td>
                              <td>{v.value}</td>
                            </tr>
                          );
                        })}
                    </tbody>
                  </Table>
                </div>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Modal.Body>
      <Modal.Footer className="text-center" style={{ display: "block" }}>
        <Button
          variant="primary"
          onClick={() => props.onCompleteSelection(sendList())}
        >
          선택완료
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
