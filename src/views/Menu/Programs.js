import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import "../css/menu/menu.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes, faArrowRight } from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";
import React from "react";

export const Programs = (props) => {
  const [programNameList, setProgramNameList] = useState([]);
  const [programDetail, setProgramDetail] = useState({
    name: "",
    nameEng: "",
    time: "",
    description: "",
    order: null,
    priceInfo: [
      {
        programCount: "",
        programPrice: "",
      },
    ],
    programId: "",
  });
  const [categoryMenu, setCategoryMenu] = useState([
    {
      name: "",
      categoryId: "",
      isEnabled: true,
      selected: false,
    },
  ]);

  useEffect(() => {
    getCategoryMenu();
  }, []);

  // 카테고리 메뉴
  const getCategoryMenu = async () => {
    const getcategoryUrl = axiosUrlFunction("categoryList", "검색");
    await axios
      .get(`${getcategoryUrl.apiUrl}?type=program`, {
        headers: {
          Authorization: `Bearer ${getcategoryUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        const data = [];
        res.data.data.content.map((cate, i) => {
          const selected = i === 0 ? true : false;
          data.push({
            name: cate.categoryTitle,
            categoryId: cate.categoryId,
            isEnabled: true,
            selected,
          });
        });
        getCategoryList(data[0].categoryId);
        setCategoryMenu(data);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getCategoryMenu();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 프로그램 메뉴
  const getCategoryList = async (id) => {
    const categoryUrl = axiosUrlFunction("programList", "검색");

    await axios
      .get(`${categoryUrl.apiUrl}?category_id=${id}`, {
        headers: {
          Authorization: `Bearer ${categoryUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        const data = [];
        if (res.data.data.length > 0) {
          res.data.data.map((program, i) => {
            const selected = i === 0 ? true : false;
            data.push({
              name: program.programKo,
              programId: program.programId,
              categoryId: id,
              selected,
            });
          });
          getProgramDetail(data[0].programId);
        } else {
          const program = {
            name: "",
            nameEng: "",
            time: "",
            description: "",
            order: [],
            priceInfo: [
              {
                programCount: "",
                programPrice: "",
              },
            ],
            programId: "",
            sw: 0,
          };
          setProgramDetail(program);
        }
        setProgramNameList(data);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getCategoryList(id);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 카테고리 선택 이벤트
  const clickEventHandler = (index, name) => {
    const data = [];
    categoryMenu.map((value) => {
      data.push(value);
    });

    for (let i = 0; i < data.length; i++) {
      if (i === index) {
        data[i].selected = true;
      } else {
        data[i].selected = false;
      }
    }
    getCategoryList(data[index].categoryId);
    setCategoryMenu(data);
  };

  // 프로그램 선택 이벤트
  const programClickHandler = (name) => {
    let j = 0;
    const data = [];
    programNameList.map((value) => {
      data.push(value);
    });

    for (let i = 0; i < data.length; i++) {
      if (data[i].name === name) {
        data[i].selected = true;
        j = i;
      } else {
        data[i].selected = false;
      }
    }
    getProgramDetail(data[j].programId);
    setProgramNameList(data);
  };

  // 프로그램 내용
  const getProgramDetail = async (id) => {
    const detailUrl = axiosUrlFunction("");

    await axios
      .get(`/api/program?program_id=${id}`, {
        headers: {
          Authorization: `Bearer ${detailUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then((res) => {
        const data = {
          name: res.data.data.program_ko,
          nameEng: res.data.data.program_en,
          time: res.data.data.program_time,
          description: res.data.data.program_content,
          order: res.data.data.program_manage,
          priceInfo: res.data.data.program_amount,
          programId: id,
          sw: 1,
        };
        setProgramDetail(data);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getProgramDetail(id);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  //  순서추가
  const addProgramOrder = () => {
    setProgramDetail((value) => ({
      ...value,
      order: [
        ...value.order,
        {
          programManageContent: "",
          programManageSeq: value.order.length + 1,
          updatedAt: null,
        },
      ],
    }));
  };

  // 가격추가
  const addProgramPrice = () => {
    setProgramDetail((value) => ({
      ...value,
      priceInfo: [
        ...value.priceInfo,
        {
          programCount: "",
          programPrice: "",
        },
      ],
    }));
  };

  let sendSw = 0;
  // 프로그램 저장 & 수정
  const sendData = async () => {
    if (sendSw === 1) return;
    let sendUrl = "";
    if (programDetail.sw === 1) {
      // 프로그램 수정
      sendUrl = axiosUrlFunction("program/update", "검색");
    } else if (programDetail.sw === 0) {
      // 프로그램 등록
      sendUrl = axiosUrlFunction("program", "검색");
    }

    if (categoryMenu.length === 0) {
      alert("카테고리를 추가해 주세요.");
      return;
    } else if (programDetail.order.length === 0) {
      alert("관리순서를 추가해 주세요.");
      return;
    } else if (programDetail.priceInfo.length === 0) {
      alert("가격을 추가해 주세요.");
      return;
    } else if (programDetail.name.length === 0) {
      alert("프로그램명을 입력해 주세요.");
      return;
    } else if (programDetail.nameEng.length === 0) {
      alert("프로그램 영문명을 입력해 주세요.");
      return;
    } else if (programDetail.time === "") {
      alert("관리시간을 입력해 주세요.");
      return;
    } else if (programDetail.description === "") {
      alert("프로그램 설명을 입력해 주세요.");
      return;
    } else if (isNaN(programDetail.time)) {
      alert("관리시간을 입력해 주세요.");
      return;
    }

    if (programDetail.time < 30 || programDetail.time >= 120) {
      alert("관리시간은 최소 30분 ~ 최대 120분 입니다.");
      return;
    }

    const program_manage = [];
    for (let i = 0; i < programDetail.order.length; i++) {
      if (programDetail.order[i].programManageContent === "") {
        alert("관리순서 내용을 입력해 주세요.");
        return;
      } else
        program_manage.push({
          seq: programDetail.order[i].programManageSeq,
          content: programDetail.order[i].programManageContent,
        });
    }

    const program_amount = [];
    for (let i = 0; i < programDetail.priceInfo.length; i++) {
      if (isNaN(programDetail.priceInfo[i].programCount)) {
        alert("구분을 입력해주세요.");
        return;
      }
      if (
        programDetail.priceInfo[i].programCount === "" ||
        programDetail.priceInfo[i].programCount === 0
      ) {
        alert("1회 이상 입력해 주세요.");
        return;
      } else if (programDetail.priceInfo[i].programPrice === "") {
        alert("가격을 입력해주세요.");
        return;
      } else {
        if (programDetail.priceInfo[i].toString().indexOf(",") > -1) {
          programDetail.priceInfo[i] = programDetail.priceInfo[i].replace(
            /\,/g,
            ""
          );
        }
        program_amount.push({
          count: parseInt(programDetail.priceInfo[i].programCount),
          price: parseInt(programDetail.priceInfo[i].programPrice),
        });
      }
    }

    let categoryId = "";
    for (let i = 0; i < categoryMenu.length; i++) {
      if (categoryMenu[i].isEnabled && categoryMenu[i].selected) {
        categoryId = categoryMenu[i].categoryId;
        break;
      }
    }

    const data = {
      program_id: programDetail.programId,
      category_id: categoryId,
      program_ko: programDetail.name,
      program_en: programDetail.nameEng,
      program_time: programDetail.time,
      program_content: programDetail.description,
      program_manage,
      program_amount,
    };

    const formData = new FormData();
    formData.append(
      "dto",
      new Blob([JSON.stringify(data)], { type: "application/json" })
    );

    sendSw = 1;
    await axios
      .post(`${sendUrl.apiUrl}`, formData, {
        headers: {
          Authorization: `Bearer ${sendUrl.token}`,
          "Content-Type": "multipart/form-data",
        },
      })
      .then((res) => {
        sendSw = 0;
        alert("프로그램 저장을 완료했습니다.");

        categoryMenu.map((value) => {
          if (value.selected) {
            getCategoryList(value.categoryId);
          }
        });
      })
      .catch((err) => {
        sendSw = 0;
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else sendData();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 프로그램 삭제
  const deleteData = async () => {
    const deleteUrl = axiosUrlFunction("program/delete", "검색");
    const data = {
      program_id: programDetail.programId,
    };

    axios
      .put(`${deleteUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${deleteUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then((res) => {
        alert("프로그램 삭제를 완료했습니다.");
        categoryMenu.map((value) => {
          if (value.selected) {
            getCategoryList(value.categoryId);
          }
        });
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else deleteData();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };
  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">메뉴관리 - 프로그램</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <Table>
                <thead>
                  <tr>
                    <th colSpan={2}>
                      {categoryMenu.map((category, i) => {
                        return (
                          <div
                            key={i}
                            onClick={() => {
                              clickEventHandler(i, category.name);
                            }}
                            className={`category ${
                              category.selected ? "selected" : ""
                            } cursor`}
                          >
                            {category.name}
                          </div>
                        );
                      })}
                      <Button
                        className="float-right"
                        onClick={(e) => {
                          props.history.push("/admin/menus/categories");
                        }}
                      >
                        + 카테고리 관리
                      </Button>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td
                      style={{
                        width: "20%",
                        verticalAlign: "baseline",
                        borderRight: "1px solid black",
                      }}
                    >
                      {programNameList.map((program, i) => {
                        return (
                          <div
                            key={i}
                            onClick={() => {
                              programClickHandler(program.name);
                            }}
                            className={`program ${
                              program.selected ? "selected" : ""
                            } cursor`}
                          >
                            {program.name}
                          </div>
                        );
                      })}
                      <Button
                        style={{ width: "100%" }}
                        onClick={(e) => {
                          const program = {
                            name: "",
                            nameEng: "",
                            time: "",
                            description: "",
                            order: [],
                            priceInfo: [
                              {
                                programCount: "",
                                programPrice: "",
                              },
                            ],
                            programId: "",
                            sw: 0,
                          };
                          setProgramDetail(program);
                        }}
                      >
                        + 프로그램 추가
                      </Button>
                    </td>
                    <td style={{ verticalAlign: "baseline" }}>
                      <div className="row mb-3">
                        <div className="col-3">프로그램명</div>
                        <div className="col-4">
                          <input
                            className="form-control"
                            type="text"
                            required
                            value={programDetail.name || ""}
                            onChange={(e) => {
                              setProgramDetail((value) => ({
                                ...value,
                                name: e.target.value,
                              }));
                            }}
                          />
                        </div>
                      </div>
                      <div className="row mb-3">
                        <div className="col-3">프로그램 영문명</div>
                        <div className="col-4">
                          <input
                            className="form-control"
                            type="text"
                            required
                            value={programDetail.nameEng || ""}
                            onChange={(e) => {
                              setProgramDetail((value) => ({
                                ...value,
                                nameEng: e.target.value,
                              }));
                            }}
                          />
                        </div>
                      </div>
                      <div className="row mb-3">
                        <div className="col-3">관리시간</div>
                        <div className="col-1">
                          <input
                            className="form-control"
                            type="number"
                            style={{ width: "70px" }}
                            required
                            value={programDetail.time || ""}
                            onChange={(e) => {
                              const val =
                                0 > parseInt(e.target.value)
                                  ? 0
                                  : parseInt(e.target.value);
                              setProgramDetail((value) => ({
                                ...value,
                                time: val,
                              }));
                            }}
                          />
                        </div>
                        <div
                          className="col-1"
                          style={{ display: "flex", alignItems: "center" }}
                        >
                          분
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-12">프로그램 설명</div>
                        <div className="col-12">
                          <textarea
                            className="form-control textarea-resize"
                            style={{ minHeight: "100px" }}
                            value={programDetail.description || ""}
                            onChange={(e) => {
                              setProgramDetail((value) => ({
                                ...value,
                                description: e.target.value,
                              }));
                            }}
                          ></textarea>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-12 mt-3 mb-3">
                          관리순서
                          <Button
                            className="ml-2"
                            onClick={() => addProgramOrder()}
                          >
                            순서추가
                          </Button>
                        </div>
                        {programDetail.order !== undefined &&
                        programDetail.order !== null ? (
                          programDetail.order.map((order, i) => {
                            return (
                              <div
                                className="mb-5 progrma-order"
                                key={i}
                                style={{ marginLeft: "20px" }}
                              >
                                {i + 1} 단계{" "}
                                <FontAwesomeIcon
                                  icon={faTimes}
                                  style={{ cursor: "pointer" }}
                                  onClick={() => {
                                    programDetail.order.splice(i, 1);
                                    setProgramDetail((value) => ({
                                      ...value,
                                      order: [...value.order],
                                    }));
                                  }}
                                />
                                <div className="progrma-order__icons">
                                  <input
                                    type="text"
                                    value={order.programManageContent || ""}
                                    onChange={(e) => {
                                      programDetail.order[
                                        i
                                      ].programManageContent = e.target.value;
                                      setProgramDetail((value) => ({
                                        ...value,
                                        order: [...value.order],
                                      }));
                                    }}
                                  />
                                  {programDetail.order.length !== i + 1 ? (
                                    <span>
                                      <FontAwesomeIcon
                                        icon={faArrowRight}
                                        style={{ cursor: "pointer" }}
                                        onClick={() => {
                                          programDetail.order.splice(i, 1);
                                          setProgramDetail((value) => ({
                                            ...value,
                                            order: [...value.order],
                                          }));
                                        }}
                                      />
                                    </span>
                                  ) : (
                                    <span></span>
                                  )}
                                </div>
                              </div>
                            );
                          })
                        ) : (
                          <div></div>
                        )}
                      </div>
                      <div className="row">
                        <div className="col-12">
                          가격
                          <Button
                            className="ml-2 mt-3 mb-3"
                            onClick={addProgramPrice}
                          >
                            가격추가
                          </Button>
                        </div>
                        <div className="price-selection">
                          <div
                            className="price-selection__title"
                            style={{
                              display: "flex",
                              justifyContent: "space-around",
                            }}
                          >
                            <span>구분</span>
                            <span>가격</span>
                          </div>
                          {programDetail.priceInfo !== undefined &&
                          programDetail.priceInfo !== null ? (
                            programDetail.priceInfo.map((column, i) => {
                              return (
                                <div key={i} style={{ display: "flex" }}>
                                  <div
                                    style={{
                                      display: "flex",
                                      alignItems: "center",
                                    }}
                                  >
                                    <input
                                      type="number"
                                      style={{
                                        textAlign: "center",
                                        width: "100%",
                                        height: "50px",
                                      }}
                                      value={column.programCount}
                                      onChange={(e) => {
                                        const val =
                                          0 > parseInt(e.target.value)
                                            ? 0
                                            : parseInt(e.target.value);
                                        programDetail.priceInfo[
                                          i
                                        ].programCount = val;
                                        setProgramDetail((value) => ({
                                          ...value,
                                          priceInfo: [...value.priceInfo],
                                        }));
                                      }}
                                    />{" "}
                                  </div>
                                  <div
                                    style={{
                                      display: "flex",
                                      alignItems: "center",
                                    }}
                                  >
                                    <input
                                      type="text"
                                      style={{
                                        textAlign: "center",
                                        width: "100%",
                                        height: "50px",
                                      }}
                                      value={column.programPrice.toLocaleString(
                                        "ko-KR"
                                      )}
                                      onChange={(e) => {
                                        programDetail.priceInfo[
                                          i
                                        ].programPrice = e.target.value;
                                        setProgramDetail((value) => ({
                                          ...value,
                                          priceInfo: [...value.priceInfo],
                                        }));
                                      }}
                                    />{" "}
                                  </div>
                                  <div
                                    style={{
                                      display: "flex",
                                      alignItems: "center",
                                    }}
                                  >
                                    <FontAwesomeIcon
                                      icon={faTimes}
                                      className="price-selection__icon"
                                      style={{ cursor: "pointer" }}
                                      onClick={() => {
                                        programDetail.priceInfo.splice(i, 1);
                                        setProgramDetail((value) => ({
                                          ...value,
                                          priceInfo: [...value.priceInfo],
                                        }));
                                      }}
                                    />
                                  </div>
                                </div>
                              );
                            })
                          ) : (
                            <div></div>
                          )}
                        </div>
                      </div>
                      <div className="text-center mt-4">
                        <Button className="ml-2" onClick={deleteData}>
                          삭제
                        </Button>
                        <Button className="ml-2" onClick={sendData}>
                          저장
                        </Button>
                        <Button
                          className="ml-2"
                          onClick={() => {
                            window.location.replace("/admin/menus/programs");
                          }}
                        >
                          취소
                        </Button>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </Table>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
