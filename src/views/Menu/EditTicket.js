import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import "../css/menu/tableInput.scss";
import { axiosUrlFunction } from "utils/AxiosUrl";
import axios from "axios";

export const EditTicket = (props) => {
  const [addParam, setAddParam] = useState({
    title: "",
    use: "y",
    count: "",
    number: "",
    disCountPrice: "",
    price: "",
  });
  const [price, setPrice] = useState([]);
  const [programList, setProgramList] = useState([]);
  const [programYlist, setProgramYlist] = useState([]);
  const [isEnabled, setIsEnabled] = useState(true);

  useEffect(() => {
    getList();
  }, []);

  const getList = async () => {
    const programUrl = axiosUrlFunction("program/prepaid", "검색");
    const res = await axios
      .get(`/api/prepaid?prepaid_id=${props.match.params.id}`)
      .catch((err) => {
        alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
      });

    const res2 = await axios
      .get(`${programUrl.apiUrl}`, {
        headers: {
          Authorization: `Bearer ${programUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getList();
            });
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });

    setAddParam({
      title: res.data.data.prepaid_name,
      use: res.data.data.useYN,
      count: res.data.data.count,
      number: res.data.data.num,
      disCountPrice: res.data.data.prepaid_percent,
      price: res.data.data.prepaid_amount,
    });
    const data = [];
    res.data.data.list.map((value) => {
      data.push({
        id: value.program_id,
        id2: value.prepaid_program_id,
        name: value.program_name,
        price: value.price,
        disCount: value.discount_price,
      });
    });

    const programs = [];

    res2.data.data.map((value) => {
      if (value.use === "Y" || value.use === "y") {
        programs.push({
          id: value.program_id,
          id2: "",
          name: value.program_ko,
          price: value.program_price,
          disCount: "",
        });
      }
    });
    for (let i = 0; i < data.length; i++) {
      for (let j = 0; j < programs.length; j++) {
        if (data[i].name === programs[j].name) {
          programs[j].disCount = data[i].disCount;
          programs[j].id2 = data[i].id2;
        }
      }
    }

    setProgramYlist(programs);
  };

  // 선불권 등록
  let sw = 0;
  const sendData = async (event) => {
    event.preventDefault();
    if (sw === 1) return;
    const sendUrl = axiosUrlFunction("prepaid", "검색");
    const prepaidProgramList = [];

    programYlist.map((value) => {
      if (value.disCount !== "" && value.disCount !== 0) {
        prepaidProgramList.push({
          program_id: value.id,
          prepaid_program_id: value.id2,
          discountPrice: value.disCount,
        });
      }
    });
    const data = {
      prepaid_id: parseInt(props.match.params.id),
      name: addParam.title,
      use: addParam.use,
      count: addParam.count,
      amount: addParam.price,
      num: addParam.number,
      percent: addParam.disCountPrice,
      prepaidProgramList,
    };

    sw = 1;
    axios
      .put(`${sendUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${sendUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then((res) => {
        if (res.data.data === 0) {
          sw = 0;
          alert("선불권을 사용중인 고객이 있어 선불권을 수정 할 수 없습니다.");
          return;
        } else if (res.data.data === 1) {
          sw = 0;
          alert("수정을 완료 했습니다.");
          props.history.push("/admin/menus/tickets/list");
        }
      })
      .catch((err) => {
        sw = 0;
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else sendData(event);
            });
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  return (
    <Container fluid>
      <form onSubmit={sendData}>
        <Row>
          <Col md="12">
            <Card className="card-plain table-plain-bg">
              <Card.Header>
                <Card.Title as="h4">
                  메뉴관리 - 선불권 - 카테고리 조회/수정
                </Card.Title>
              </Card.Header>
              <Card.Body className="table-full-width table-responsive px-0">
                <Table className="table-hover tickets-table">
                  <colgroup>
                    <col style={{ width: "15%" }}></col>
                    <col style={{ width: "85%" }}></col>
                  </colgroup>
                  <tbody>
                    <tr>
                      <th colSpan={2} style={{ fontSize: "23px" }}>
                        선불권 정보
                      </th>
                    </tr>
                    <tr>
                      <th>선불권명</th>
                      <td>
                        <input
                          className="form-control mb-3 tickets-table__title"
                          type="text"
                          required
                          placeholder="제목"
                          value={addParam.title}
                          maxLength={50}
                          onChange={(e) =>
                            setAddParam((value) => ({
                              ...value,
                              title: e.target.value,
                            }))
                          }
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>활성화 여부</th>
                      <td>
                        <Button
                          className={
                            addParam.use === "y" || addParam.use === "Y"
                              ? "select-btn selected mb-3"
                              : "select-btn mb-3"
                          }
                          onClick={(e) => {
                            setAddParam((value) => ({
                              ...value,
                              use: "y",
                            }));
                          }}
                        >
                          ON
                        </Button>
                        <Button
                          className={
                            addParam.use === "y" || addParam.use === "Y"
                              ? "select-btn mb-3"
                              : "select-btn selected mb-3"
                          }
                          onClick={(e) => {
                            setAddParam((value) => ({
                              ...value,
                              use: "n",
                            }));
                          }}
                        >
                          OFF
                        </Button>
                        <br />※ 활성화 여부가 ON일 경우 앱에 해당 배너가
                        표시됩니다!
                      </td>
                    </tr>
                    <tr>
                      <th>선택가능한 프로그램 수 </th>
                      <td>
                        <input
                          className="tickets-table__sm"
                          type="number"
                          value={addParam.count}
                          required
                          onChange={(e) => {
                            const val =
                              0 > parseInt(e.target.value)
                                ? 0
                                : parseInt(e.target.value);
                            setAddParam((value) => ({
                              ...value,
                              count: val,
                            }));
                          }}
                        />{" "}
                        개
                      </td>
                    </tr>
                    <tr>
                      <th>할인율 설정 </th>
                      <td>
                        <input
                          className="tickets-table__sm"
                          type="number"
                          value={addParam.number}
                          required
                          onChange={(e) => {
                            const val =
                              0 > parseInt(e.target.value)
                                ? 0
                                : parseInt(e.target.value);
                            setAddParam((value) => ({
                              ...value,
                              number: val,
                            }));
                          }}
                        />{" "}
                        회 기준{" "}
                        <input
                          className="tickets-table__sm"
                          type="number"
                          value={addParam.disCountPrice}
                          required
                          onChange={(e) => {
                            const val =
                              0 > parseInt(e.target.value)
                                ? 0
                                : parseInt(e.target.value);
                            setAddParam((value) => ({
                              ...value,
                              disCountPrice: val,
                            }));
                          }}
                        />{" "}
                        % 할인
                      </td>
                    </tr>
                    <tr>
                      <th>선불권 금액</th>
                      <td>
                        <input
                          className="input-appearance mr-1 tickets-table__lg"
                          type="number"
                          required
                          value={addParam.price}
                          onChange={(e) => {
                            const val =
                              0 > parseInt(e.target.value)
                                ? 0
                                : parseInt(e.target.value);
                            setAddParam((value) => ({
                              ...value,
                              price: val,
                            }));
                          }}
                        />
                        원
                      </td>
                    </tr>
                    <tr>
                      <th style={{ fontSize: "23px" }}>선불권 할인가격</th>
                      <td>
                        <span className="tickets-table__listTitle">
                          프로그램 가격
                        </span>{" "}
                        <span className="tickets-table__listTitle">
                          선불권 할인가격
                        </span>
                      </td>
                    </tr>
                    {programYlist !== null && programYlist !== undefined ? (
                      programYlist.map((list, i) => {
                        return (
                          <tr key={i}>
                            <th>{list.name}</th>
                            <td>
                              <input
                                type="text"
                                className="text-center tickets-table__lg"
                                readOnly={true}
                                defaultValue={list.price.toLocaleString(
                                  "ko-KR"
                                )}
                              />{" "}
                              <span className="mr-5">원</span>{" "}
                              <input
                                className="input-appearance tickets-table__lg"
                                type="number"
                                value={list.disCount || ""}
                                onChange={(e) => {
                                  const val =
                                    0 > parseInt(e.target.value)
                                      ? 0
                                      : parseInt(e.target.value);
                                  list.disCount = val;
                                  setProgramYlist([...programYlist]);
                                }}
                              />{" "}
                              원
                            </td>
                          </tr>
                        );
                      })
                    ) : (
                      <tr>
                        <th></th>
                        <td></td>
                      </tr>
                    )}
                  </tbody>
                </Table>
                <div className="text-center">
                  <Button type="submit">저장</Button>
                  <Button
                    className="ml-2"
                    onClick={() => {
                      props.history.push("/admin/menus/tickets/list");
                    }}
                  >
                    취소
                  </Button>
                </div>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </form>
    </Container>
  );
};
