import { useState } from "react";
import "../css/Login.scss";
import { Modal, Button, Row, Col, Form, Card, Table } from "react-bootstrap";

export const AddVariablePopup = (props) => {
  const [isEnabled, setIsEnabled] = useState(true);
  const [list, setList] = useState(props.valueList);
  const [addList, setAddList] = useState([]);
  const [idErrorMsg, setIdErrorMsg] = useState("");

  // addButtonEvent
  const addListHandler = () => {
    const inputData = {
      seq:
        props.valueList !== null && props.valueList !== undefined
          ? list.length + 1
          : "",
      content: "",
    };

    setList([...list, inputData]);
  };

  // total setList
  const setListItems = () => {
    setIdErrorMsg("");
    const data = list;

    for (let i = 0; i < data.length; i++) {
      for (let j = i + 1; j < data.length; j++) {
        if (data[i].seq === data[j].seq) {
          setIdErrorMsg("우선순위가 동일한 질문이 있습니다.");
          return;
        }
      }
    }

    props.onSaveEvent(data);
  };

  return (
    <Modal show={true} onHide={props.onClickClose} size="lg">
      <Modal.Header closeButton>
        <Modal.Title>
          <b style={{ fontWeight: "bold" }}>질문 수정</b>
          <button
            className="ml-3"
            style={{ backgroundColor: "white" }}
            onClick={() => {
              addListHandler();
            }}
          >
            + 추가
          </button>
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Row>
          <Col md="12">
            <Card className="card-plain table-plain-bg">
              <Card.Body className="table-full-width table-responsive px-0">
                <div className="table-scroll2">
                  <Table className="table-hover table-scroll2__table mt-0">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>우선순위</th>
                        <th>부가정보 내용</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody id="addTbody">
                      {list !== null && list !== undefined ? (
                        list.map((value, i) => {
                          return (
                            <tr key={i}>
                              <td>{i + 1}</td>
                              <td>
                                <input
                                  type="number"
                                  style={{ width: "50px", border: "none" }}
                                  value={value.seq}
                                  onChange={(e) => {
                                    const val =
                                      0 > parseInt(e.target.value)
                                        ? 0
                                        : parseInt(e.target.value);
                                    list[i].seq = val;
                                    setList([...list]);
                                  }}
                                />
                              </td>
                              <td>
                                <input
                                  type="text"
                                  style={{ width: "250px", border: "none" }}
                                  value={value.content}
                                  onChange={(e) => {
                                    list[i].content = e.target.value;
                                    setList([...list]);
                                  }}
                                />
                              </td>
                              <td>
                                <button
                                  style={{ backgroundColor: "white" }}
                                  onClick={() => {
                                    list.splice(i, 1);
                                    setList([...list]);
                                  }}
                                >
                                  삭제
                                </button>
                              </td>
                            </tr>
                          );
                        })
                      ) : (
                        <tr></tr>
                      )}
                    </tbody>
                  </Table>
                </div>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Modal.Body>

      <Modal.Footer className="text-center" style={{ display: "block" }}>
        <Button variant="primary" onClick={setListItems}>
          저장
        </Button>
        {idErrorMsg && (
          <div className="ml-4" style={{ color: "red" }}>
            {idErrorMsg}
          </div>
        )}
      </Modal.Footer>
    </Modal>
  );
};
