import { useState, useEffect } from "react";
import "../css/Login.scss";
import { Modal, Button, Row, Col, Form, Card, Table } from "react-bootstrap";
import { FileBtn } from "components/FileUpload";
import axios from "axios";
import { axiosUrlFunction } from "utils/AxiosUrl";

export const EditCategoryPopup = (props) => {
  const [categoryInfo, setCategoryInfo] = useState({
    title: "",
    use: "y",
    badgeImg: "",
    detailImg: "",
  });
  const [img1, setImg1] = useState("");
  const [img2, setImg2] = useState("");
  const [isEnabled, setIsEnabled] = useState(true);

  useEffect(() => {
    getList();
  }, []);

  const getList = async () => {
    const getUrl = axiosUrlFunction("categoryImg", "검색");

    axios
      .get(`${getUrl.apiUrl}?category_id=${props.id}`, {
        headers: {
          Authorization: `Bearer ${getUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        setCategoryInfo({
          title: res.data.data.category_name,
          use: res.data.data.use,
          badgeImg: res.data.data.list_img,
          detailImg: res.data.data.detail_img,
        });
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getList();
            });
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };
  // 카테고리 저장
  const sendData = async () => {
    const sendUrl = axiosUrlFunction("categoryImg/Update", "검색");

    if (categoryInfo.title === "") {
      alert("카테고리명을 입력해 주세요.");
      return;
    } else if (categoryInfo.badgeImg === "") {
      alert("카테고리 배지 이미지를 업로드 해 주세요.");
      return;
    } else if (categoryInfo.detailImg === "") {
      alert("카테고리 상세 이미지를 업로드 해 주세요.");
      return;
    }

    const formData = new FormData();
    formData.append("category_id", props.id);
    formData.append("title", categoryInfo.title);
    formData.append("use", categoryInfo.use);
    if (img1 !== "") formData.append("list_img", img1[0]);
    if (img2 !== "") formData.append("detail_img", img2[0]);
    formData.append("type", "program");

    axios
      .put(`${sendUrl.apiUrl}`, formData, {
        headers: {
          Authorization: `Bearer ${sendUrl.token}`,
          "Content-Type": `multipart/form-data`,
        },
      })
      .then(() => {
        alert("카테고리 수정을 완료했습니다.");
        window.location.replace("/admin/menus/categories");
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else sendData();
            });
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  return (
    <Modal show={true} onHide={props.onClickClose} size="lg">
      <Modal.Header closeButton>
        <Modal.Title>
          <b>카테고리 수정</b>
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Row>
          <Col md="12">
            <Card className="card-plain table-plain-bg">
              <Card.Body className="table-full-width table-responsive px-0">
                <Table className="table-hover">
                  <tbody>
                    <tr>
                      <th>카테고리명</th>
                      <td>
                        <input
                          className="form-control"
                          type="text"
                          required
                          placeholder="카테고리명"
                          value={categoryInfo.title}
                          maxLength={15}
                          onChange={(e) => {
                            setCategoryInfo((value) => ({
                              ...value,
                              title: e.target.value,
                            }));
                          }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>활성화 여부</th>
                      <td>
                        <Button
                          className={
                            categoryInfo.use === "y" || categoryInfo.use === "Y"
                              ? "select-btn selected"
                              : "select-btn"
                          }
                          onClick={(e) => {
                            setIsEnabled(true);
                            setCategoryInfo((value) => ({
                              ...value,
                              use: "y",
                            }));
                          }}
                        >
                          ON
                        </Button>
                        <Button
                          className={
                            categoryInfo.use === "y" || categoryInfo.use === "Y"
                              ? "select-btn"
                              : "select-btn selected"
                          }
                          onClick={(e) => {
                            setIsEnabled(false);
                            setCategoryInfo((value) => ({
                              ...value,
                              use: "n",
                            }));
                          }}
                        >
                          OFF
                        </Button>
                      </td>
                    </tr>
                    <tr>
                      <th>카테고리 배지 이미지</th>
                      <td>
                        <FileBtn
                          name="업로드"
                          fileData={(data) => {
                            setImg1(data);
                          }}
                          accept="image/*"
                          id="badgeImg"
                          imageUrl={categoryInfo.badgeImg}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>카테고리 상세 이미지</th>
                      <td>
                        <FileBtn
                          name="업로드"
                          fileData={(data) => {
                            setImg2(data);
                          }}
                          accept="image/*"
                          id="detailImg"
                          imageUrl={categoryInfo.detailImg}
                        />
                      </td>
                    </tr>
                  </tbody>
                </Table>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Modal.Body>

      <Modal.Footer className="text-center" style={{ display: "block" }}>
        <Button variant="primary" onClick={sendData}>
          저장
        </Button>
        <Button variant="primary" onClick={props.onClickClose} className="ml-2">
          취소
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
