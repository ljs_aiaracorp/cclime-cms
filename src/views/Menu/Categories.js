import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import "../css/paymentTable/payTable.scss";
import Loading from "components/Loading";
import { AddCategoryPopup } from "./AddCategoryPopup";
import { EditCategoryPopup } from "./EditCategoryPopup";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";

export const Categories = (props) => {
  const [list, setList] = useState(null);
  const [editId, setEditId] = useState("");
  const [showAddCategoryPopup, setShowAddCategoryPopup] = useState(false);
  const [showEditCategoryPopup, setShowEditCategoryPopup] = useState(false);

  useEffect(() => {
    getList();
  }, []);

  // 카테고리 리스트
  const getList = async () => {
    const getUrl = axiosUrlFunction("");

    axios
      .get(`/api/categoryList?type=program`, {
        headers: {
          Authorization: `Bearer ${getUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        setList(res.data.data);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getList();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 카테고리 삭제
  const deleteData = async (id, index) => {
    const deleteUrl = axiosUrlFunction("category/delete", "검색");
    const data = {
      category_id: id,
    };

    axios
      .put(`${deleteUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${deleteUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        if (res.data.data === 0) {
          alert(res.data.message);
        } else if (res.data.data === 1) {
          alert("카테고리 삭제를 완료했습니다.");
          list.splice(index, 1);
          setList([...list]);
        }
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else deleteData(id, index);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };
  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">
                <span
                  className="cursor"
                  onClick={() => {
                    props.history.push("/admin/menus/programs");
                  }}
                >
                  &#60;
                </span>
                &nbsp;메뉴관리 - 프로그램 - 카테고리 관리
              </Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <div className="text-right">
                <Button
                  onClick={(e) => setShowAddCategoryPopup(true)}
                  className="mb-1"
                >
                  카테고리 추가
                </Button>
              </div>
              <div
                style={{ maxHeight: "1000px", overflow: "auto" }}
                className="mb-5"
              >
                <table className="table-hover mt-0 table-center">
                  <thead>
                    <tr>
                      <th className="border-0">NO.</th>
                      <th className="border-0">카테고리명</th>
                      <th className="border-0">활성화여부</th>
                      <th className="border-0"></th>
                    </tr>
                  </thead>
                  <tbody>
                    {list !== undefined && list !== null ? (
                      list.map((cate, i) => {
                        const use =
                          cate.use === "y" || cate.use === "Y" ? "ON" : "OFF";
                        return (
                          <tr key={i} className="cursor">
                            <td>{i + 1}</td>
                            <td>{cate.categoryTitle}</td>
                            <td>{use}</td>
                            <td>
                              <Button
                                className="mr-3"
                                onClick={() => {
                                  setShowEditCategoryPopup(true);
                                  setEditId(cate.categoryId);
                                }}
                              >
                                수정
                              </Button>
                              <Button
                                onClick={() => deleteData(cate.categoryId, i)}
                              >
                                삭제
                              </Button>
                            </td>
                          </tr>
                        );
                      })
                    ) : (
                      <tr>
                        <td colSpan={4} style={{ textAlign: "center" }}>
                          <Loading />
                        </td>
                      </tr>
                    )}
                  </tbody>
                </table>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
      {showAddCategoryPopup && (
        <AddCategoryPopup
          onClickClose={(e) => setShowAddCategoryPopup(false)}
        />
      )}
      {showEditCategoryPopup && (
        <EditCategoryPopup
          id={editId}
          onClickClose={(e) => setShowEditCategoryPopup(false)}
        />
      )}
    </Container>
  );
};
