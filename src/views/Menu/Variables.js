import React, { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import "../css/paymentTable/payTable.scss";
import Pagination from "components/Pagination";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faQuestionCircle } from "@fortawesome/free-solid-svg-icons";
import {
  RESERVATION,
  MEMBER_SHIP,
  BUSINESS_INFO,
  TOS,
  ADDITIONAL_INFO0,
  ADDITIONAL_INFO3,
} from "../../static/helpArticle";
import { HelpPopup } from "components/Popup/HelpPopup";
import { AddVariablePopup } from "./AddVariablePopup";
import { isValidMobile } from "utils/validator";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";

export const Variables = (props) => {
  const [selectedTab, setSelectedTab] = useState(0);
  const [selectedPage, setSelectedPage] = useState(0);
  const [list, setList] = useState(null);
  const [listSurvey, setListSurvey] = useState(null);
  const [gradeList, setGradeList] = useState(null);
  const [isSurveyPopup, setIsSurveyPopup] = useState(false);
  const [isHelpPopup, setisHelpPopup] = useState(false);
  const [helpDes, setHelpDes] = useState("");

  // 제어변수 List
  const getList = async () => {
    axios
      .get("/api/settingList")
      .then(async (res) => {
        setList(res.data.data);
      })
      .catch((err) => {
        alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
      });
  };

  // 저장
  const sendData = async (start, end) => {
    const sendUrl = axiosUrlFunction("setting", "검색");
    const data = [];

    if (selectedTab === 2) {
      let phoneCheck = checkingPhone(); // 전화번호 확인
      if (phoneCheck === "") return;
      list[11].variableContent = phoneCheck;
      setList([...list]);
    }
    for (let i = start; i <= end; i++) {
      data.push({
        type: list[i].variableName,
        word: list[i].variableContent,
      });
    }

    if (selectedTab === 0) {
      data.push({
        type: list[19].variableName,
        word: list[19].variableContent,
      });
      data.push({
        type: list[20].variableName,
        word: list[20].variableContent,
      });
    }

    if (selectedTab === 2) {
      data.push({
        type: list[18].variableName,
        word: list[18].variableContent,
      });
    }
    axios
      .put(`${sendUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${sendUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then((res) => {
        alert("정보 수정을 완료했습니다.");
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else sendData(start, end);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // Phone 정규화
  const checkingPhone = () => {
    const phone =
      list[11].variableContent === null &&
      list[11].variableContent === undefined
        ? ""
        : list[11].variableContent;
    if (phone === "") return;

    if (!isValidMobile(phone)) {
      const value = phone.replace(
        /(^02.{0}|^01.{1}|[0-9]{3})([0-9]{3,4})([0-9]{4})/,
        "$1-$2-$3"
      );
      if (value.indexOf("-") === -1) {
        alert("전화번호를 확인해 주세요.");
        return "";
      }
      return value;
    } else return phone;
  };

  // 등급목록
  const getGradeList = async () => {
    const gradeUrl = axiosUrlFunction("gradeList");

    axios
      .get(`${gradeUrl.apiUrl}`, {
        headers: {
          Authorization: `Bearer ${gradeUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then((res) => {
        setGradeList(res.data.data);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getGradeList();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 관심 있는 관리 List
  const getSurveyList = async (typeName) => {
    axios
      .get(`/api/survey?type=${typeName}`)
      .then((res) => {
        setListSurvey(res.data.data);
      })
      .catch((err) => {
        alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
      });
  };

  // 질문 수정
  const addNewList = (newlist) => {
    setListSurvey(newlist);
    setIsSurveyPopup(false);
  };

  // 관심있는 관리 send
  const sendSurvey = async () => {
    const sendUrl = axiosUrlFunction("survey", "검색");

    const data = [];
    let dto = {};

    listSurvey.map((value) => {
      data.push({
        survey_content: value.content,
        survey_seq: value.seq,
      });
    });

    if (selectedPage === 0) {
      dto = {
        type: "program",
        list: data,
      };
    } else if (selectedPage === 3) {
      dto = {
        type: "route",
        list: data,
      };
    } else {
      return;
    }

    axios
      .post(`${sendUrl.apiUrl}`, dto, {
        headers: {
          Authorization: `Bearer ${sendUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then((res) => {
        alert("정보 수정을 완료했습니다.");
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else sendSurvey();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 도움말
  const helpDescription = () => {
    // 팝업 오픈
    if (selectedTab === 0) {
      setHelpDes(RESERVATION.split("\n"));
    } else if (selectedTab === 1) {
      setHelpDes(MEMBER_SHIP.split("\n"));
    } else if (selectedTab === 2) {
      setHelpDes(BUSINESS_INFO.split("\n"));
    } else if (selectedTab === 3) {
      setHelpDes(TOS.split("\n"));
    } else if (selectedTab === 4) {
      if (selectedPage === 0) {
        setHelpDes(ADDITIONAL_INFO0.split("\n"));
      } else if (selectedPage === 3) {
        setHelpDes(ADDITIONAL_INFO0.split("\n"));
      }
    }
    setisHelpPopup(true);
  };

  useEffect(() => {
    getList();
  }, []);

  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">메뉴관리 - 제어 변수 관리</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <div className="row">
                <div
                  className={`col-2 tab-btn ${
                    selectedTab === 0 ? "selected-tab" : ""
                  }`}
                  onClick={(e) => setSelectedTab(0)}
                >
                  예약
                </div>
                <div
                  className={`col-2 tab-btn ${
                    selectedTab === 1 ? "selected-tab" : ""
                  }`}
                  onClick={(e) => {
                    setSelectedTab(1);
                    getGradeList();
                  }}
                >
                  회원등급
                </div>
                <div
                  className={`col-2 tab-btn ${
                    selectedTab === 2 ? "selected-tab" : ""
                  }`}
                  onClick={(e) => setSelectedTab(2)}
                >
                  사업자 정보
                </div>
                <div
                  className={`col-2 tab-btn ${
                    selectedTab === 3 ? "selected-tab" : ""
                  }`}
                  onClick={(e) => setSelectedTab(3)}
                >
                  이용약관
                </div>
                <div
                  className={`col-2 tab-btn ${
                    selectedTab === 4 ? "selected-tab" : ""
                  }`}
                  onClick={(e) => {
                    setSelectedTab(4);
                    getSurveyList("program");
                  }}
                >
                  회원가입 부가정보
                </div>
              </div>

              <div className="title-background" style={{ display: "flex" }}>
                제어 변수 항목{" "}
                {selectedPage !== 1 && selectedPage !== 2 ? (
                  <div className="ml-2">
                    <FontAwesomeIcon
                      icon={faQuestionCircle}
                      style={{ cursor: "pointer" }}
                      onClick={helpDescription}
                    />
                  </div>
                ) : (
                  <div></div>
                )}
              </div>
              {selectedTab === 0 && (
                <>
                  <h4>예약 일자 수정</h4>
                  <div style={{ display: "flex" }}>
                    <div className="mr-5">
                      예약일{" "}
                      <input
                        className="form-control"
                        type="number"
                        required
                        value={list === null ? 0 : list[19].variableContent}
                        style={{
                          display: "inline",
                          width: "100px",
                        }}
                        onChange={(e) => {
                          const val =
                            0 > parseInt(e.target.value)
                              ? 0
                              : parseInt(e.target.value);
                          list[19].variableContent = val;
                          setList([...list]);
                        }}
                      />{" "}
                      전까지 취소 가능
                    </div>
                    <div>
                      예약일{" "}
                      <input
                        className="form-control"
                        type="number"
                        required
                        value={list === null ? 0 : list[0].variableContent}
                        style={{
                          display: "inline",
                          width: "100px",
                        }}
                        onChange={(e) => {
                          const val =
                            0 > parseInt(e.target.value)
                              ? 0
                              : parseInt(e.target.value);
                          list[0].variableContent = val;
                          setList([...list]);
                        }}
                      />{" "}
                      전까지 변경 가능
                    </div>
                    <div className="ml-5">
                      예약가능일{" "}
                      <input
                        className="form-control"
                        type="number"
                        required
                        value={list === null ? 0 : list[20].variableContent}
                        style={{
                          display: "inline",
                          width: "100px",
                        }}
                        onChange={(e) => {
                          const val =
                            0 > parseInt(e.target.value)
                              ? 0
                              : parseInt(e.target.value);
                          list[20].variableContent = val;
                          setList([...list]);
                        }}
                      />{" "}
                      까지 예약 가능
                    </div>
                  </div>
                  <h4>알립니다 pop-up 수정</h4>
                  <textarea
                    className="form-control mb-4 textarea-resize"
                    value={list === null ? 0 : list[1].variableContent}
                    onChange={(e) => {
                      list[1].variableContent = e.target.value;
                      setList([...list]);
                    }}
                    style={{
                      minHeight: "300px",
                    }}
                  ></textarea>
                  <div className="text-center">
                    <Button onClick={() => sendData(0, 1)}>저장</Button>
                    <Button className="ml-2" onClick={() => getList()}>
                      취소
                    </Button>
                  </div>
                </>
              )}
              {selectedTab === 1 && (
                <>
                  <h4>등급별 회원 문구 수정</h4>
                  <div className="row">
                    {gradeList !== null &&
                      gradeList !== undefined &&
                      gradeList.map((grade, index) => {
                        return (
                          <React.Fragment key={index}>
                            <div className="col-1">{grade.gradeName}</div>
                            <div className="col-3">
                              <textarea
                                className="form-control textarea-resize"
                                value={
                                  list === null
                                    ? ""
                                    : list[index + 2].variableContent
                                }
                                onChange={(e) => {
                                  list[index + 2].variableContent =
                                    e.target.value;
                                  setList([...list]);
                                }}
                                style={{
                                  minHeight: "100px",
                                }}
                              ></textarea>
                            </div>
                          </React.Fragment>
                        );
                      })}
                  </div>
                  <h4>회원등급 유의사항 문구 수정</h4>
                  <textarea
                    className="form-control mb-4 textarea-resize"
                    value={list === null ? "" : list[8].variableContent}
                    onChange={(e) => {
                      list[8].variableContent = e.target.value;
                      setList([...list]);
                    }}
                    style={{
                      minHeight: "300px",
                    }}
                  ></textarea>
                  <div className="text-center">
                    <Button onClick={() => sendData(2, 8)}>저장</Button>
                    <Button className="ml-2" onClick={() => getList()}>
                      취소
                    </Button>
                  </div>
                </>
              )}
              {selectedTab === 2 && (
                <>
                  <h4 className="mb-5">사업자 정보 수정</h4>
                  <div className="row">
                    <div className="col-2">기업명</div>
                    <div className="col-3 mb-4">
                      <input
                        className="form-control"
                        type="text"
                        required
                        value={list === null ? "" : list[9].variableContent}
                        onChange={(e) => {
                          list[9].variableContent = e.target.value;
                          setList([...list]);
                        }}
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-2">대표자명</div>
                    <div className="col-3 mb-4">
                      <input
                        className="form-control"
                        type="text"
                        required
                        value={list === null ? "" : list[18].variableContent}
                        onChange={(e) => {
                          list[18].variableContent = e.target.value;
                          setList([...list]);
                        }}
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-2">주소</div>
                    <div className="col-3 mb-4">
                      <input
                        className="form-control"
                        type="text"
                        required
                        value={list === null ? "" : list[10].variableContent}
                        onChange={(e) => {
                          list[10].variableContent = e.target.value;
                          setList([...list]);
                        }}
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-2">TEL</div>
                    <div className="col-3 mb-4">
                      <input
                        className="form-control"
                        type="text"
                        required
                        maxLength={13}
                        value={list === null ? "" : list[11].variableContent}
                        onChange={(e) => {
                          list[11].variableContent = e.target.value;
                          setList([...list]);
                        }}
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-2">사업자등록번호</div>
                    <div className="col-3 mb-4">
                      <input
                        className="form-control"
                        type="text"
                        required
                        value={list === null ? 0 : list[12].variableContent}
                        onChange={(e) => {
                          list[12].variableContent = e.target.value;
                          setList([...list]);
                        }}
                      />
                    </div>
                  </div>
                  <div className="text-center">
                    <Button onClick={() => sendData(9, 12)}>저장</Button>
                    <Button className="ml-2" onClick={() => getList()}>
                      취소
                    </Button>
                  </div>
                </>
              )}
              {selectedTab === 3 && (
                <>
                  <div className="row">
                    <div className="col-6">
                      <h4>개인정보 처리방침</h4>
                      <textarea
                        className="form-control textarea-resize"
                        value={list === null ? "" : list[13].variableContent}
                        onChange={(e) => {
                          list[13].variableContent = e.target.value;
                          setList([...list]);
                        }}
                        style={{
                          minHeight: "300px",
                        }}
                      ></textarea>
                    </div>
                    <div className="col-6">
                      <h4>이용약관</h4>
                      <textarea
                        className="form-control textarea-resize"
                        value={list === null ? "" : list[14].variableContent}
                        onChange={(e) => {
                          list[14].variableContent = e.target.value;
                          setList([...list]);
                        }}
                        style={{
                          minHeight: "300px",
                        }}
                      ></textarea>
                    </div>
                    <div className="col-6">
                      <h4>회원약관</h4>
                      <textarea
                        className="form-control textarea-resize"
                        value={list === null ? "" : list[15].variableContent}
                        onChange={(e) => {
                          list[15].variableContent = e.target.value;
                          setList([...list]);
                        }}
                        style={{
                          minHeight: "300px",
                        }}
                      ></textarea>
                    </div>
                    <div className="col-6">
                      <h4>환불규정</h4>
                      <textarea
                        className="form-control textarea-resize"
                        value={list === null ? "" : list[16].variableContent}
                        onChange={(e) => {
                          list[16].variableContent = e.target.value;
                          setList([...list]);
                        }}
                        style={{
                          minHeight: "300px",
                        }}
                      ></textarea>
                    </div>
                    <div className="col-6">
                      <h4>개인정보 활용 약관</h4>
                      <textarea
                        className="form-control textarea-resize"
                        value={list === null ? "" : list[17].variableContent}
                        onChange={(e) => {
                          list[17].variableContent = e.target.value;
                          setList([...list]);
                        }}
                        style={{
                          minHeight: "300px",
                        }}
                      ></textarea>
                    </div>
                  </div>
                  <div className="text-center mt-4">
                    <Button onClick={() => sendData(13, 17)}>저장</Button>
                    <Button className="ml-2" onClick={() => getList()}>
                      취소
                    </Button>
                  </div>
                </>
              )}
              {selectedTab === 4 && (
                <>
                  <h4>
                    부가정보({selectedPage + 1}/4)
                    {selectedPage === 0 && " - 관심 있는 관리 선택"}
                    {selectedPage === 1 && " - 피부 정보"}
                    {selectedPage === 2 && " - 건강 정보"}
                    {selectedPage === 3 && " - 끌리메 이용 경로"}
                    {selectedPage === 0 || selectedPage === 3 ? (
                      <Button
                        className="float-right"
                        onClick={() => setIsSurveyPopup(true)}
                      >
                        질문 수정
                      </Button>
                    ) : (
                      <div></div>
                    )}
                  </h4>
                  {selectedPage === 0 && (
                    <>
                      <Table className="table-hover">
                        <thead>
                          <tr>
                            <th>No.</th>
                            <th>우선순위</th>
                            <th>부가정보 내용</th>
                          </tr>
                        </thead>
                        <tbody>
                          {listSurvey !== null && listSurvey !== undefined ? (
                            listSurvey.map((value, i) => {
                              return (
                                <tr key={i}>
                                  <td>{i + 1}</td>
                                  <td>{value.seq}</td>
                                  <td>{value.content}</td>
                                </tr>
                              );
                            })
                          ) : (
                            <tr>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                            </tr>
                          )}
                        </tbody>
                      </Table>
                    </>
                  )}
                  {selectedPage === 1 && (
                    <>
                      <Table className="table-hover text-center">
                        <thead>
                          <tr>
                            <th>No.</th>
                            <th>우선순위</th>
                            <th>부가정보 내용</th>
                            <th>선택 내용</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>1</td>
                            <td>1</td>
                            <td>피부타입을 알려주세요</td>
                            <td>지성/복합성/건성</td>
                          </tr>
                          <tr>
                            <td>2</td>
                            <td>2</td>
                            <td>피부 알레르기가 있으신가요?</td>
                            <td>네/아니요</td>
                          </tr>
                          <tr>
                            <td>3</td>
                            <td>3</td>
                            <td>피부 트러블 경험이 있나요?</td>
                            <td>네/아니요</td>
                          </tr>
                          <tr>
                            <td>4</td>
                            <td>4</td>
                            <td>다른 에스테틱을 경험한 적이 있나요?</td>
                            <td>네/아니요</td>
                          </tr>
                        </tbody>
                      </Table>
                    </>
                  )}
                  {selectedPage === 2 && (
                    <>
                      <Table className="table-hover text-center">
                        <thead>
                          <tr>
                            <th>No.</th>
                            <th>우선순위</th>
                            <th>부가정보 내용</th>
                            <th>선택 내용</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>1</td>
                            <td>1</td>
                            <td>현재 복용중인 약이 있나요?</td>
                            <td>네/아니요</td>
                          </tr>
                          <tr>
                            <td>2</td>
                            <td>2</td>
                            <td>시술/수술 경험이 있으신가요?</td>
                            <td>네/아니요</td>
                          </tr>
                          <tr>
                            <td>3</td>
                            <td>3</td>
                            <td>여성일경우 현재 임신중입니까?</td>
                            <td>네/아니요/해당없음</td>
                          </tr>
                          <tr>
                            <td>4</td>
                            <td>4</td>
                            <td>이외의 주의사항이 있다면 적어주세요</td>
                            <td>자율입력</td>
                          </tr>
                        </tbody>
                      </Table>
                    </>
                  )}
                  {selectedPage === 3 && (
                    <>
                      <Table className="table-hover text-center">
                        <thead>
                          <tr>
                            <th>No.</th>
                            <th>우선순위</th>
                            <th>부가정보 내용</th>
                          </tr>
                        </thead>
                        <tbody>
                          {listSurvey !== null && listSurvey !== undefined ? (
                            listSurvey.map((value, i) => {
                              return (
                                <tr key={i}>
                                  <td>{i + 1}</td>
                                  <td>{value.seq}</td>
                                  <td>{value.content}</td>
                                </tr>
                              );
                            })
                          ) : (
                            <tr>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                            </tr>
                          )}
                        </tbody>
                      </Table>
                    </>
                  )}
                  <div style={{ display: "flex", justifyContent: "flex-end" }}>
                    <Pagination
                      count={40}
                      selected={(pageSelect) => {
                        if (pageSelect - 1 === 0) getSurveyList("program");
                        else if (pageSelect - 1 === 3) getSurveyList("route");
                        setSelectedPage(pageSelect - 1);
                      }}
                      center={true}
                    />
                  </div>
                  <div className="text-center">
                    <Button onClick={sendSurvey}>저장</Button>
                    <Button
                      className="ml-2"
                      onClick={() => {
                        if (selectedPage === 0) getSurveyList("program");
                        else if (selectedPage === 3) getSurveyList("route");
                      }}
                    >
                      취소
                    </Button>
                  </div>
                </>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
      {isSurveyPopup && (
        <AddVariablePopup
          onClickClose={() => setIsSurveyPopup(false)}
          onSaveEvent={addNewList}
          valueList={listSurvey}
        />
      )}
      {isHelpPopup && (
        <HelpPopup
          onClickClose={() => setisHelpPopup(false)}
          onContent={helpDes}
        />
      )}
    </Container>
  );
};
