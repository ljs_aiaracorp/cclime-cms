import { useState, useEffect } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import { FileBtn } from "components/FileUpload";
import AddressPopup from "components/Popup/AddressPopup";
import { StorePopup2 } from "components/Popup/StorePopup2";
import { isValidMobile } from "utils/validator";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";

export const EditBeautist = (props) => {
  const user = axiosUrlFunction("");
  // 뷰티스트 정보
  const [beautistInfo, setBeautistInfo] = useState({
    img: "",
    name: "",
    storeId: "",
    storeName: "",
    birth: "",
    position: "",
    phone: "",
    email: "",
    beautystCareerYear: "",
    beautystCareerMonth: "",
    cclimeCareerYear: "",
    cclimeCareerMonth: "",
    certificate: "",
    address: "",
    addressDetail: "",
    note: "",
  });
  const [addressModal, setAddressModal] = useState(false); // Address Modal
  const [storeModal, setStoreModal] = useState(false); // StoreModal
  const [img, setImg] = useState("");
  // 뷰티스트 데이터
  const getList = async () => {
    axios
      .get(`/api/store/beautyst?beautyst_id=${props.match.params.id}`, {
        headers: {
          Authorization: `Bearer ${user.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        let addressDivision = "";
        if (res.data.data.beautyst.address !== null)
          addressDivision = res.data.data.beautyst.address.split(")");
        setBeautistInfo({
          img:
            res.data.data.beautyst.img === null
              ? ""
              : res.data.data.beautyst.img,
          name: res.data.data.beautyst.name,
          storeId: res.data.data.beautyst.storeId,
          storeName: res.data.data.storeName,
          birth: res.data.data.beautyst.birth,
          position: res.data.data.beautyst.position,
          phone: res.data.data.beautyst.phone,
          email: res.data.data.beautyst.email,
          beautystCareerYear:
            res.data.data.beautyst.beautystCareerYear === null
              ? ""
              : res.data.data.beautyst.beautystCareerYear,
          beautystCareerMonth:
            res.data.data.beautyst.beautystCareerMonth === null
              ? ""
              : res.data.data.beautyst.beautystCareerMonth,
          cclimeCareerYear:
            res.data.data.beautyst.cclimeCareerYear === null
              ? ""
              : res.data.data.beautyst.cclimeCareerYear,
          cclimeCareerMonth:
            res.data.data.beautyst.cclimeCareerMonth === null
              ? ""
              : res.data.data.beautyst.cclimeCareerMonth,
          certificate:
            res.data.data.beautyst.certificate === null
              ? ""
              : res.data.data.beautyst.certificate,
          address:
            res.data.data.beautyst.address === null
              ? ""
              : addressDivision[0] + ")",
          addressDetail:
            res.data.data.beautyst.address === null ? "" : addressDivision[1],
          note:
            res.data.data.beautyst.note === null
              ? ""
              : res.data.data.beautyst.note,
        });
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getList();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };
  // address Component Modal close
  const addressCloseModal = (fullAddr) => {
    setAddressModal(false);
    if (fullAddr !== "") {
      setBeautistInfo((value) => ({
        ...value,
        address: fullAddr,
      }));
    }
  };

  // StoreModal setter
  const addStoreEvent = (store) => {
    if (store.storeId !== "") {
      setBeautistInfo((value) => ({
        ...value,
        storeId: store.storeId,
        storeName: store.value,
      }));
    } else if (store.storeId === "") {
      setStoreModal(false);
      return;
    }
    setStoreModal(false);
  };

  // 핸드폰 정규화 하이픈 추가
  const checkingPhone = () => {
    if (!isValidMobile(beautistInfo.phone)) {
      const value = beautistInfo.phone.replace(
        /(^02.{0}|^01.{1}|[0-9]{3})([0-9]{3,4})([0-9]{4})/,
        "$1-$2-$3"
      );
      if (value.indexOf("-") === -1) {
        alert("전화번호를 확인해 주세요.");
        return "";
      }
      return value;
    } else return beautistInfo.phone;
  };

  // 뷰티스트 저장
  const sendData = async (event) => {
    event.preventDefault();
    const formUrl = axiosUrlFunction("");

    let phoneCheck = checkingPhone(); // 전화번호 확인
    if (phoneCheck === "") {
      alert("전화번호를 입력해 주세요.");
      return null;
    }

    const formData = new FormData();
    formData.append("beautyst_id", props.match.params.id);
    formData.append("name", beautistInfo.name);
    formData.append("store_id", beautistInfo.storeId);
    formData.append("birth", beautistInfo.birth.replace(/\-/g, ""));
    formData.append("position", beautistInfo.position);
    formData.append("phone", phoneCheck);
    formData.append("email", beautistInfo.email);

    if (img !== "") {
      formData.append("images", img[0]);
    }
    if (beautistInfo.beautystCareerYear !== "")
      formData.append("beautyst_career_year", beautistInfo.beautystCareerYear);
    if (beautistInfo.beautystCareerMonth !== "")
      formData.append(
        "beautyst_career_month",
        beautistInfo.beautystCareerMonth
      );
    if (beautistInfo.cclimeCareerYear !== "")
      formData.append("cclime_career_year", beautistInfo.cclimeCareerYear);
    if (beautistInfo.cclimeCareerMonth !== "")
      formData.append("cclime_career_month", beautistInfo.cclimeCareerMonth);
    if (beautistInfo.certificate !== "")
      formData.append("certificate", beautistInfo.certificate);
    if (beautistInfo.address !== "")
      formData.append(
        "address",
        beautistInfo.address + " " + beautistInfo.addressDetail
      );
    if (beautistInfo.note !== "") formData.append("note", beautistInfo.note);

    axios
      .put("/api/store/beautyst", formData, {
        headers: {
          Authorization: `Bearer ${formUrl.token}`,
          "Content-Type": `multipart/form-data`,
        },
      })
      .then(() => {
        alert("수정이 완료되었습니다.");
        props.history.push(`${formUrl.accessPath}/beautists/list`);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else sendData(event);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 뷰티스트 삭제
  const deletebeauty = async () => {
    const deleteUrl = axiosUrlFunction("");
    const data = {
      beautyst_id: parseInt(props.match.params.id),
    };

    axios
      .put("/api/store/beautyst/delete", data, {
        headers: {
          Authorization: `Bearer ${deleteUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(() => {
        alert("뷰티스트 삭제가 완료되었습니다.");
        props.history.push(`${deleteUrl.accessPath}/beautists/list`);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else deletebeauty();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };
  useEffect(() => {
    getList();
  }, []);

  return (
    <Container fluid>
      <form onSubmit={sendData}>
        <Row>
          <Col md="12">
            <Card className="card-plain table-plain-bg">
              <Card.Header>
                <Card.Title as="h4">
                  뷰티스트 관리 - 뷰티스트 명단 - 뷰티스트 조회/수정
                </Card.Title>
              </Card.Header>
              <Card.Body className="table-full-width table-responsive px-0">
                <Table className="table-hover">
                  <colgroup>
                    <col style={{ width: "10%" }}></col>
                    <col style={{ width: "40%" }}></col>
                    <col style={{ width: "10%" }}></col>
                    <col style={{ width: "40%" }}></col>
                  </colgroup>
                  <tbody>
                    <tr>
                      <th className="required">이름</th>
                      <td>
                        <input
                          className="form-control"
                          type="text"
                          required
                          placeholder="이름"
                          value={beautistInfo.name}
                          maxLength={15}
                          onChange={(e) => {
                            setBeautistInfo((value) => ({
                              ...value,
                              name: e.target.value,
                            }));
                          }}
                        />
                      </td>
                      <th rowSpan={6}>사진</th>
                      <td rowSpan={6}>
                        <FileBtn
                          name="업로드"
                          fileData={(data) => {
                            setImg(data);
                          }}
                          accept="image/*"
                          id="picture"
                          imageUrl={beautistInfo.img}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th className="required">소속지점</th>
                      <td>
                        <input
                          className="form-control"
                          type="text"
                          required
                          placeholder="지점을 검색해주세요"
                          readOnly={true}
                          defaultValue={beautistInfo.storeName}
                          style={{
                            width: "76%",
                            display: "inline-block",
                            marginRight: "10px",
                          }}
                        />
                        <Button onClick={() => setStoreModal(true)}>
                          지점검색
                        </Button>
                      </td>
                    </tr>
                    <tr>
                      <th className="required">생년월일</th>
                      <td>
                        <input
                          className="form-control"
                          type="date"
                          required
                          placeholder="YYYY/MM/DD"
                          value={beautistInfo.birth}
                          onChange={(e) => {
                            setBeautistInfo((value) => ({
                              ...value,
                              birth: e.target.value,
                            }));
                          }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th className="required">직책</th>
                      <td>
                        <input
                          className="form-control"
                          type="text"
                          required
                          placeholder="직책"
                          value={beautistInfo.position}
                          maxLength={15}
                          onChange={(e) => {
                            setBeautistInfo((value) => ({
                              ...value,
                              position: e.target.value,
                            }));
                          }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th className="required">전화번호</th>
                      <td>
                        <input
                          className="form-control"
                          type="text"
                          required
                          placeholder="전화번호"
                          value={beautistInfo.phone}
                          maxLength={13}
                          onChange={(e) => {
                            setBeautistInfo((value) => ({
                              ...value,
                              phone: e.target.value,
                            }));
                          }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th className="required">이메일</th>
                      <td>
                        <input
                          className="form-control"
                          type="email"
                          required
                          placeholder="이메일 주소"
                          value={beautistInfo.email}
                          onChange={(e) => {
                            setBeautistInfo((value) => ({
                              ...value,
                              email: e.target.value,
                            }));
                          }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>뷰티스트 경력</th>
                      <td>
                        <input
                          className="form-control"
                          type="number"
                          value={beautistInfo.beautystCareerYear}
                          onChange={(e) => {
                            const val =
                              0 > parseInt(e.target.value)
                                ? 0
                                : parseInt(e.target.value);
                            setBeautistInfo((value) => ({
                              ...value,
                              beautystCareerYear: val,
                            }));
                          }}
                          style={{
                            width: "40%",
                            display: "inline-block",
                            marginRight: "10px",
                          }}
                        />{" "}
                        <span
                          style={{
                            marginRight: "10px",
                            whiteSpace: "nowrap",
                            width: "10%",
                          }}
                        >
                          년
                        </span>
                        <input
                          className="form-control"
                          type="number"
                          value={beautistInfo.beautystCareerMonth}
                          onChange={(e) => {
                            const val =
                              0 > parseInt(e.target.value)
                                ? 0
                                : parseInt(e.target.value);
                            setBeautistInfo((value) => ({
                              ...value,
                              beautystCareerMonth: val,
                            }));
                          }}
                          style={{
                            width: "40%",
                            display: "inline-block",
                            marginRight: "10px",
                          }}
                        />{" "}
                        <span style={{ whiteSpace: "nowrap", width: "10%" }}>
                          개월
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <th>끌리메 경력</th>
                      <td>
                        <div style={{ width: "100%" }}>
                          <input
                            className="form-control"
                            type="number"
                            value={beautistInfo.cclimeCareerYear}
                            onChange={(e) => {
                              const val =
                                0 > parseInt(e.target.value)
                                  ? 0
                                  : parseInt(e.target.value);
                              setBeautistInfo((value) => ({
                                ...value,
                                cclimeCareerYear: val,
                              }));
                            }}
                            style={{
                              width: "40%",
                              display: "inline-block",
                              marginRight: "10px",
                            }}
                          />{" "}
                          <span
                            style={{
                              marginRight: "10px",
                              whiteSpace: "nowrap",
                              width: "10%",
                            }}
                          >
                            년
                          </span>
                          <input
                            className="form-control"
                            type="number"
                            value={beautistInfo.cclimeCareerMonth}
                            onChange={(e) => {
                              const val =
                                0 > parseInt(e.target.value)
                                  ? 0
                                  : parseInt(e.target.value);
                              setBeautistInfo((value) => ({
                                ...value,
                                cclimeCareerMonth: val,
                              }));
                            }}
                            style={{
                              width: "40%",
                              display: "inline-block",
                              marginRight: "10px",
                            }}
                          />{" "}
                          <span style={{ whiteSpace: "nowrap", width: "10%" }}>
                            개월
                          </span>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <th>자격증</th>
                      <td>
                        <textarea
                          className="form-control"
                          style={{
                            minHeight: "100px",
                            resize: "none",
                          }}
                          value={beautistInfo.certificate}
                          onChange={(e) => {
                            setBeautistInfo((value) => ({
                              ...value,
                              certificate: e.target.value,
                            }));
                          }}
                        ></textarea>
                      </td>
                    </tr>
                    <tr>
                      <th>주소</th>
                      <td colSpan={2}>
                        <input
                          className="form-control"
                          type="text"
                          placeholder="주소를 검색해주세요"
                          readOnly={true}
                          defaultValue={beautistInfo.address || ""}
                          style={{
                            width: "76%",
                            display: "inline-block",
                            marginRight: "10px",
                          }}
                        />
                        <Button onClick={() => setAddressModal(true)}>
                          주소검색
                        </Button>
                        <div className="mt-3">
                          {addressModal && (
                            <AddressPopup onmakeclose={addressCloseModal} />
                          )}
                        </div>
                        <input
                          className="form-control"
                          type="text"
                          placeholder="상세주소"
                          value={beautistInfo.addressDetail || ""}
                          maxLength={30}
                          onChange={(e) => {
                            setBeautistInfo((value) => ({
                              ...value,
                              addressDetail: e.target.value,
                            }));
                          }}
                          style={{
                            width: "76%",
                            display: "inline-block",
                            marginRight: "10px",
                          }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>비고</th>
                      <td colSpan={3}>
                        <textarea
                          className="form-control"
                          style={{
                            minHeight: "100px",
                            resize: "none",
                          }}
                          value={beautistInfo.note}
                          onChange={(e) => {
                            setBeautistInfo((value) => ({
                              ...value,
                              note: e.target.value,
                            }));
                          }}
                        ></textarea>
                      </td>
                    </tr>
                  </tbody>
                </Table>
                <div className="text-center">
                  <Button onClick={deletebeauty}>삭제</Button>
                  <Button type="submit" className="ml-2">
                    저장
                  </Button>
                  <Button
                    className="ml-2"
                    onClick={() => {
                      props.history.push(`${user.accessPath}/beautists/list`);
                    }}
                  >
                    취소
                  </Button>
                </div>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </form>
      {storeModal && (
        <StorePopup2
          onClickClose={() => setStoreModal(false)}
          onSaveEvent={addStoreEvent}
        />
      )}
    </Container>
  );
};
