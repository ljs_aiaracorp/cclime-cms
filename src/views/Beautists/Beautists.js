import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import Pagination from "components/Pagination";
import Loading from "components/Loading";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";

export const Beautists = (props) => {
  const userApiData = axiosUrlFunction("beautystList", "뷰티스트");
  const searchType = userApiData.accessor === "main" ? "store" : "name";
  const [selected, setSelected] = useState(1);
  const [searchTarget, setSearchTarget] = useState(searchType);
  const [keyword, setKeyword] = useState("");
  const [pagenum, setPagenum] = useState();
  const [list, setList] = useState([]);
  const [count, setCount] = useState(0);

  useEffect(() => {
    getList();
  }, []);

  const getList = async (params) => {
    setList(null);

    if (!params) {
      params = {};
    }
    if (params.searchTarget === undefined) {
      params.searchTarget = searchTarget;
    }
    if (params.keyword === undefined) {
      params.keyword = keyword;
    }
    if (params.page === undefined) {
      params.page = selected - 1;
    }
    setPagenum(params.page);

    const queries = [];
    if (params.searchTarget !== "" && params.keyword !== "") {
      queries.push(`type=${searchTarget}`);
      queries.push(`word=${params.keyword}`);
    }
    queries.push(`page=${params.page}`);
    queries.push(`size=10`);

    const queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";

    await axios
      .get(`${userApiData.apiUrl}${queryStr}`, {
        headers: {
          Authorization: `Bearer ${userApiData.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        setCount(res.data.data.totalElements);
        setList(res.data.data.content);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getList(params);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">뷰티스트 관리 - 뷰티스트 명단</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <Table className="table-hover search-box">
                <tbody>
                  <tr>
                    <th className="select-search-target">
                      <Form.Control
                        as="select"
                        value={searchTarget}
                        onChange={(e) => setSearchTarget(e.target.value)}
                      >
                        {userApiData.accessor === "main" ? (
                          <>
                            <option value="store">지점명</option>
                            <option value="name">이름</option>
                            <option value="position">직책</option>
                          </>
                        ) : (
                          <>
                            <option value="name">이름</option>
                            <option value="position">직책</option>
                          </>
                        )}
                      </Form.Control>
                    </th>
                    <th>
                      <Form.Control
                        type="text"
                        placeholder="검색"
                        value={keyword}
                        onChange={(e) => setKeyword(e.target.value)}
                        onKeyUp={(e) => {
                          if (e.keyCode === 13) {
                            setSelected(1);
                            getList({
                              page: 0,
                            });
                          }
                        }}
                        maxLength={50}
                        style={{
                          display: "inline-block",
                          width: "400px",
                          verticalAlign: "middle",
                        }}
                      />
                      <Button
                        variant="light"
                        onClick={(e) => {
                          setSelected(1);
                          getList({
                            page: 0,
                          });
                        }}
                        style={{
                          display: "inline-block",
                          border: "solid 1px #E3E3E3",
                          color: "#565656",
                          verticalAlign: "middle",
                        }}
                        className="search-btn"
                      >
                        <FontAwesomeIcon icon={faSearch} />
                      </Button>
                    </th>
                  </tr>
                </tbody>
              </Table>
              <div className="text-right">
                <Button
                  onClick={(e) => {
                    window.location.replace(
                      `${userApiData.accessPath}/beautists/add`
                    );
                  }}
                >
                  뷰티스트 추가
                </Button>
              </div>
              <Table className="table-hover mt-0">
                <thead>
                  <tr>
                    <th className="border-0">NO.</th>
                    <th className="border-0">지점명</th>
                    <th className="border-0">이름</th>
                    <th className="border-0">직책</th>
                    <th className="border-0">뷰티스트 경력</th>
                    <th className="border-0">끌리메 경력</th>
                    <th className="border-0"></th>
                  </tr>
                </thead>
                <tbody>
                  {list !== undefined && list !== null ? (
                    list.map((user, i) => {
                      return (
                        <tr key={i} className="cursor">
                          <td>{i + 1 + 10 * pagenum}</td>
                          <td>{user.store_name}</td>
                          <td>{user.name}</td>
                          <td>
                            {user.position === null ? "미설정" : user.position}
                          </td>
                          <td>
                            {user.beautyst_career_year === null
                              ? "0"
                              : user.beautyst_career_year}
                            년{" "}
                            {user.beautyst_career_month === null
                              ? "0"
                              : user.beautyst_career_month}
                            개월
                          </td>
                          <td>
                            {user.cclime_career_year === null
                              ? "0"
                              : user.cclime_career_year}
                            년{" "}
                            {user.cclime_career_month === null
                              ? "0"
                              : user.cclime_career_month}
                            개월
                          </td>
                          <td>
                            <Button
                              onClick={() => {
                                props.history.push(
                                  `${userApiData.accessPath}/beautists/edit/${user.beautyst_id}`
                                );
                              }}
                            >
                              상세정보 조회/수정
                            </Button>
                          </td>
                        </tr>
                      );
                    })
                  ) : (
                    <tr>
                      <td colSpan={7} style={{ textAlign: "center" }}>
                        <Loading />
                      </td>
                    </tr>
                  )}
                </tbody>
              </Table>
              <Pagination
                count={count}
                forcePage={selected - 1}
                selected={(pageSelect) => {
                  setSelected(pageSelect);
                  getList({ page: pageSelect - 1 });
                }}
              />
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
