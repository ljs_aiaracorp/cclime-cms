import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import { useSelector } from "react-redux";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";

export const SendPush = (props) => {
  const [message, setMessage] = useState("");

  const sendPush = async () => {
    if (message === "") {
      return alert("전송할 내용을 입력해주세요.");
    }
    if (
      !confirm(
        "푸시알림은 실시간으로 모든 회원에게 발송됩니다.\n발송하시겠습니까?"
      )
    ) {
      return;
    }

    const data = {
      msg: message,
    };

    const storeUrl = axiosUrlFunction("push/all", "검색");
    const axiosconfig = {
      headers: {
        Authorization: `Bearer ${storeUrl.token}`,
        "Content-Type": `application/json`,
      },
    };
    axios
      .post(storeUrl.apiUrl, data, axiosconfig)
      .then((response) => {
        if (response.status === 200 && response.data) {
          return response.data;
        }
      })
      .catch((e) => {
        alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        return null;
      })
      .then((response) => {
        if (response === null) return;
        else {
          alert("푸시알림을 발송했습니다.");
          setMessage("");
        }
      });
  };

  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">알림전송 - 푸시알림 전송</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <div className="row">
                <div className="col-8 push-notice">
                  ※ 푸시알림은{" "}
                  <span className="important">실시간으로 모든 회원</span>에게
                  발송됩니다.
                  <br />※ <span className="important">테스트 용도</span>로
                  푸시알림을 발송하지 말아주세요.
                </div>
                <div className="col-4">
                  <div className="phone">
                    <div className="title">전송할 내용</div>
                    <div className="screen">
                      <textarea
                        className="form-control"
                        value={message}
                        onChange={(e) => setMessage(e.target.value)}
                      ></textarea>
                      <Button onClick={sendPush}>푸시알림 전송하기</Button>
                    </div>
                    <div className="length">{message.length}/1000자</div>
                  </div>
                </div>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
