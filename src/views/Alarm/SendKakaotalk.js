import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import Pagination from "components/Pagination";
import Loading from "components/Loading";
import { useSelector } from "react-redux";
import moment from "moment";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { UserListPopup } from "../../components/Popup/UserListPopup";
import { getBizppurioToken, sendMessage } from "../../utils/Bizppurio";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";
import axios from "axios";

export const SendKakaotalk = (props) => {
  const [message, setMessage] = useState("");

  const [target, setTarget] = useState("all");

  const [gradeList, setGradeList] = useState([]);
  const [selectedGrade, setSelectedGrade] = useState(0);

  const [userList, setUserList] = useState([]);
  const [userListModal, setUserListModal] = useState(false);

  const [reservateSending, setReservateSending] = useState(false);

  const [reservateYear, setReservateYear] = useState("");
  const [reservateMonth, setReservateMonth] = useState("");
  const [reservateDate, setReservateDate] = useState("");
  const [reservateHour, setReservateHour] = useState("");
  const [reservateMinute, setReservateMinute] = useState("");

  const [templatecode, setTemplatecode] = useState("");
  const [templateContent, setTemplateContent] = useState("");

  useEffect(() => {
    const _getGradeList = async () => {
      const gradeUrl = axiosUrlFunction("gradeList");
      const res = await axios.get(`${gradeUrl.apiUrl}`, {
        headers: {
          Authorization: `Bearer ${gradeUrl.token}`,
          "Content-Type": `application/json`,
        },
      });
      setGradeList(res.data.data);
    };
    _getGradeList();
  }, []);

  let sw = 0;
  const send = async () => {
    if (sw === 1) return;
    let targetUsers = null;
    if (target === "all") {
      // 고객 전체 리스트 조회
      const user = axiosUrlFunction("user/search", "검색");
      const axiosconfig = {
        headers: {
          Authorization: `Bearer ${user.token}`,
          "Content-Type": `application/json`,
        },
      };
      const searchUsers = await axios
        .get(user.apiUrl, axiosconfig)
        .catch((err) => {
          sw = 0;
          if (err.response.status === 401) {
            if (err.response.data.error === "Unauthorized") {
              // 토큰 재발급
              const isCheck = tokenCheck();
              isCheck.then((res) => {
                if (!res) return;
                else send();
              });
            } else {
              alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
            }
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        });
      targetUsers = searchUsers.data.data;
    }

    if (target === "private") {
      // 추가한 고객 리스트
      targetUsers = userList;
    }
    if (target === "grade") {
      // 등급 선택 체크
      if (selectedGrade === 0) {
        return alert("등급을 선택해주세요.");
      }
      // 선택한 등급의 고객 조회
      const user = axiosUrlFunction("user/search", "검색");
      const axiosconfig = {
        headers: {
          Authorization: `Bearer ${user.token}`,
          "Content-Type": `application/json`,
        },
      };
      const searchUsers = await axios.get(
        `${user.apiUrl}?grade_id=${selectedGrade}`,
        axiosconfig
      );
      targetUsers = searchUsers.data.data;
    }
    if (!targetUsers || targetUsers.length === 0) {
      // 전송대상이 없음
      return alert("전송대상 고객이 없습니다.");
    }
    // Template Check
    // if (templatecode === "") {
    //   alert("Templatecode를 입력해 주세요.");
    //   return;
    // } else if (message === "") {
    //   alert("Template을 입력해 주세요.");
    //   return;
    // }

    const token = await getBizppurioToken();
    if (!token) {
      // 비즈뿌리오 인증 실패
      return;
    }
    if (reservateSending) {
      const reservateDate = validateReservateDate();
      if (!reservateDate) {
        return alert("예약 날짜/시간을 다시 확인해주세요.");
      }
      // 예약한 시간에 전송
    } else {
      // 즉시 전송
      sw = 1;
      const allResult = await Promise.all(
        targetUsers.map(async (user) => {
          // 전송대상에게 메세지 전송
          return await sendMessage(token, {
            to: user.phone,
            refkey: user.user_id.toString(),
            content: {
              ft: {
                senderkey: "0815b401abe94d5efb22dc410a009904a47e3f15",
                // templatecode: templatecode,
                message: message,
                // button: [
                //   {
                //     name: "채널추가",
                //     type: "AC",
                //   },
                // ],
              },
            },
          });
        })
      );
      if (
        allResult.filter((result) => result).length > 0 &&
        allResult.filter((result) => !result).length === 0
      ) {
        // 모두 전송 성공
        logKakao(allResult);
        alert("카카오톡이 전송되었습니다.");
      }
      if (
        allResult.filter((result) => result).length > 0 &&
        allResult.filter((result) => !result).length > 0
      ) {
        // 일부 전송 성공 && 일부 전송 실패
        logKakao(allResult);
        alert(
          "카카오톡이 전송되었습니다.\n일부 전송대상은 전송에 실패했습니다."
        );
      }
      if (
        allResult.filter((result) => result).length === 0 &&
        allResult.filter((result) => !result).length > 0
      ) {
        // 모두 전송 실패
        alert("카카오톡 전송에 실패했습니다.");
        sw = 0;
      }
    }
  };

  // 카카오전송 로그 저장 메소드
  const logKakao = async (allResult) => {
    const successUrl = axiosUrlFunction("kakaoLog", "검색");
    const axiosconfig = {
      headers: {
        Authorization: `Bearer ${successUrl.token}`,
        "Content-Type": `application/json`,
      },
    };
    const dataArr = [];
    for (let i = 0; i < allResult.length; i++) {
      if (allResult[i].data.code === 1000) {
        if (target === "all") {
          dataArr.push({
            recipient: 0,
            content: message,
            send_time: moment(allResult[i].headers.date).format(
              "YYYY-MM-DD-HH:mm:ss"
            ),
          });
          break;
        } else {
          const des = JSON.parse(allResult[i].config.data);
          dataArr.push({
            recipient: allResult[i].data.refkey,
            content: des.content.at.message,
            send_time: moment(allResult[i].headers.date).format(
              "YYYY-MM-DD-HH:mm:ss"
            ),
          });
        }
      } else {
        continue;
      }
    }

    await axios
      .post(`${successUrl.apiUrl}`, dataArr, axiosconfig)
      .then(() => {
        sw = 0;
      })
      .catch((err) => {
        sw = 0;
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else logKakao(allResult);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  const checkMinMax = (value, min, max) => {
    if (value === "") return value;
    if (value < min) {
      return min;
    }
    if (value > max) {
      return max;
    }
    return value;
  };

  const numberTo2LengthString = (num) => {
    return num >= 100 ? `${num % 100}` : num < 10 ? `0${num}` : `${num}`;
  };

  const validateReservateDate = () => {
    if (
      reservateYear === "" ||
      reservateMonth === "" ||
      reservateDate === "" ||
      reservateHour === "" ||
      reservateMinute === ""
    )
      return null;
    const date = moment(
      `${reservateYear}-${numberTo2LengthString(
        reservateMonth
      )}-${numberTo2LengthString(reservateDate)} ${numberTo2LengthString(
        reservateHour
      )}:${numberTo2LengthString(reservateMinute)}:00`,
      "YYYY-MM-DD HH:mm:ss"
    );
    if (!date.isValid()) return null;
    const current = moment();
    if (date.isSameOrBefore(current)) return null;
    return date;
  };

  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">알림전송 - 카카오톡 전송</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <div className="row">
                <div className="col-8 push-notice">
                  <div className="row">
                    <div className="col-2">전송대상</div>
                    <div className="col-2">
                      <Button
                        style={{ width: "104px" }}
                        className={
                          target === "all"
                            ? "select-btn selected"
                            : "select-btn"
                        }
                        onClick={(e) => setTarget("all")}
                      >
                        고객전체
                      </Button>
                    </div>
                    <div className="col-2">
                      <Button
                        style={{ width: "104px" }}
                        className={
                          target === "private"
                            ? "select-btn selected"
                            : "select-btn"
                        }
                        onClick={(e) => setTarget("private")}
                      >
                        개인
                      </Button>
                    </div>
                    <div className="col-2">
                      <Button
                        style={{ width: "104px" }}
                        className={
                          target === "grade"
                            ? "select-btn selected"
                            : "select-btn"
                        }
                        onClick={(e) => setTarget("grade")}
                      >
                        등급
                      </Button>
                    </div>
                    {/* <div className="mt-5 mb-5">
                      <span className="ml-3 mr-3">Templatecode</span>
                      <input
                        type="text"
                        value={templatecode}
                        onChange={(e) => setTemplatecode(e.target.value)}
                      />
                    </div> */}
                  </div>
                  {target === "private" && (
                    <div className="row">
                      <div className="col-12 text-right">
                        <Button onClick={(e) => setUserListModal(true)}>
                          고객추가
                        </Button>
                        <Table className="table-hover">
                          <thead>
                            <tr>
                              <th className="border-0">NO.</th>
                              <th className="border-0">이름</th>
                              <th className="border-0">전화번호</th>
                              <th className="border-0"></th>
                            </tr>
                          </thead>
                          <tbody>
                            {userList.length > 0 ? (
                              userList.map((user, i) => {
                                return (
                                  <tr key={i} className="cursor">
                                    <td
                                      style={{
                                        fontSize: "16px",
                                        textAlign: "left",
                                      }}
                                    >
                                      {i + 1}
                                    </td>
                                    <td
                                      style={{
                                        fontSize: "16px",
                                        textAlign: "left",
                                      }}
                                    >
                                      {user.user_name}
                                    </td>
                                    <td
                                      style={{
                                        fontSize: "16px",
                                        textAlign: "left",
                                      }}
                                    >
                                      {user.phone}
                                    </td>
                                    <td
                                      style={{
                                        fontSize: "16px",
                                        textAlign: "left",
                                      }}
                                    >
                                      <FontAwesomeIcon
                                        icon={faTimes}
                                        style={{ cursor: "pointer" }}
                                        onClick={(e) => {
                                          setUserList(
                                            userList.filter(
                                              (user2) =>
                                                user2.user_id !== user.user_id
                                            )
                                          );
                                        }}
                                      />
                                    </td>
                                  </tr>
                                );
                              })
                            ) : (
                              <tr>
                                <td
                                  colSpan={4}
                                  style={{ textAlign: "center" }}
                                ></td>
                              </tr>
                            )}
                          </tbody>
                        </Table>
                      </div>
                    </div>
                  )}
                  {target === "grade" && (
                    <div className="row">
                      <div className="col-12 mb-3">등급선택</div>
                      {gradeList.map((grade, index) => {
                        return (
                          <>
                            <div className="col-2 mb-3" key={index}>
                              <Button
                                style={{ width: "113px" }}
                                className={
                                  selectedGrade === grade.gradeId
                                    ? "select-btn selected"
                                    : "select-btn"
                                }
                                onClick={(e) => setSelectedGrade(grade.gradeId)}
                              >
                                {grade.gradeName}
                              </Button>
                            </div>
                            {index % 3 === 2 && <div className="col-6"></div>}
                          </>
                        );
                      })}
                    </div>
                  )}
                </div>
                <div className="col-4">
                  <div className="phone">
                    <div className="title">전송할 내용</div>
                    <div className="screen">
                      <textarea
                        className="form-control"
                        value={message}
                        onChange={(e) => setMessage(e.target.value)}
                      ></textarea>
                      <Button onClick={send}>푸시알림 전송하기</Button>
                    </div>
                    <div className="length">{message.length}/1000자</div>
                  </div>
                </div>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
      {userListModal && (
        <UserListPopup
          onclose={() => setUserListModal(false)}
          onClickUser={(user) => {
            setUserListModal(false);
            setUserList(userList.concat([user]));
          }}
        />
      )}
    </Container>
  );
};
