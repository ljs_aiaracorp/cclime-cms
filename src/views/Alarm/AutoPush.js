import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import { useSelector } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faQuestionCircle } from "@fortawesome/free-solid-svg-icons";
import { InformationPopup } from "./InformationPopup";
import { AvailableTagPopup } from "./AvailableTagPopup";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";
import axios from "axios";

export const AutoPush = (props) => {
  const messageKeys = [
    {
      type: "regist",
      typeLabel: "회원가입",
      key: "regist",
      label: "회원가입",
    },
    {
      type: "event",
      typeLabel: "이벤트",
      key: "answer-event",
      label: "이벤트 답변",
    },
    {
      type: "event",
      typeLabel: "이벤트",
      key: "new-event",
      label: "신규 이벤트",
    },
    {
      type: "reservation",
      typeLabel: "예약",
      key: "reservation-7",
      label: "예약 7일전",
    },
    {
      type: "reservation",
      typeLabel: "예약",
      key: "reservation-3",
      label: "예약 3일전",
    },
    {
      type: "reservation",
      typeLabel: "예약",
      key: "reservation-1",
      label: "예약 1일전",
    },
    {
      type: "reservation",
      typeLabel: "예약",
      key: "reservation-0",
      label: "예약 당일",
    },
    {
      type: "report",
      typeLabel: "불편신고",
      key: "answer-report",
      label: "신고 답변",
    },
    {
      type: "birthday",
      typeLabel: "생일",
      key: "birthday",
      label: "생일",
    },
    {
      type: "anniversary",
      typeLabel: "결혼기념일",
      key: "anniversary",
      label: "결혼기념일",
    },
    {
      type: "satisfaction",
      typeLabel: "만족도조사",
      key: "satisfaction",
      label: "만족도조사",
    },
    // {
    //   type: "end",
    //   typeLabel: "프로그램 만료",
    //   key: "end-1month",
    //   label: "만료 한달전",
    // },
    // {
    //   type: "end",
    //   typeLabel: "프로그램 만료",
    //   key: "end-7",
    //   label: "만료 7일전",
    // },
    // {
    //   type: "end",
    //   typeLabel: "프로그램 만료",
    //   key: "end-3",
    //   label: "만료 3일전",
    // },
    // {
    //   type: "end",
    //   typeLabel: "프로그램 만료",
    //   key: "end-1",
    //   label: "만료 1일전",
    // },
    // {
    //   type: "end",
    //   typeLabel: "프로그램 만료",
    //   key: "end-0",
    //   label: "만료 당일",
    // },
  ];

  const [orgMessage, setOrgMessage] = useState({});
  const [message, setMessage] = useState({});

  const [selected, setSelected] = useState("regist");

  const [showInformationPopup, setShowInformationPopup] = useState(false);
  const [showAvailableTagPopup, setShowAvailableTagPopup] = useState(false);

  useEffect(() => {
    const _getSavedMessages = async () => {
      let messageObj = {};
      const getPush = axiosUrlFunction("push", "검색");
      const axiosconfig = {
        headers: {
          Authorization: `Bearer ${getPush.token}`,
          "Content-Type": `application/json`,
        },
      };
      for (const data of messageKeys) {
        const response = await axios
          .get(`${getPush.apiUrl}?condition=${data.key}`, axiosconfig)
          .catch((err) => {
            return null;
          });
        if (!response) {
          messageObj = getMessageObj(data.key, "", messageObj);
        } else {
          messageObj = getMessageObj(
            data.key,
            response.data.data.pushMessage,
            messageObj
          );
        }
      }
      setMessage(messageObj);
      setOrgMessage(messageObj);
    };
    _getSavedMessages();
  }, []);

  const getMessageObj = (selected, value, prevMessage = message) => {
    const obj = {};
    for (const data of messageKeys) {
      if (selected === data.key) {
        obj[data.key] = value;
      } else {
        obj[data.key] = prevMessage[data.key];
      }
    }
    return obj;
  };

  const save = async () => {
    const promises = [];
    const postPush = axiosUrlFunction("push", "검색");
    const axiosconfig = {
      headers: {
        Authorization: `Bearer ${postPush.token}`,
        "Content-Type": `application/json`,
      },
    };
    for (const data of messageKeys) {
      if (orgMessage[data.key] !== message[data.key])
        promises.push(
          axios.post(
            postPush.apiUrl,
            {
              condition: data.key,
              message: message[data.key],
            },
            axiosconfig
          )
        );
    }
    await Promise.all(promises).then(() => {
      alert("푸시설정 저장을 완료했습니다.");
    });

    setOrgMessage(message);
  };

  const cancel = () => {
    setMessage(orgMessage);
  };

  const getMessageKeysByType = () => {
    const obj = {};
    for (const data of messageKeys) {
      if (!obj[data.type]) {
        obj[data.type] = [];
      }
      obj[data.type].push(data);
    }
    const arr = [];
    for (const type in obj) {
      arr.push(obj[type]);
    }
    return arr;
  };

  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">알림전송 - 자동푸시 설정</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <div className="row">
                <div className="col-8 push-notice">
                  <h4>
                    전송상황{" "}
                    <span className="ml-2">
                      <FontAwesomeIcon
                        icon={faQuestionCircle}
                        style={{ cursor: "pointer" }}
                        onClick={() => {
                          setShowInformationPopup(true);
                        }}
                      />
                    </span>
                  </h4>
                  {getMessageKeysByType().map((data, index) => {
                    return (
                      <div className="row" key={index}>
                        <div className="col-2">{data[0].typeLabel}</div>
                        {data.map((data2, index2) => {
                          return (
                            <div className="col-2 mb-3" key={index2}>
                              <Button
                                style={{ width: "120px" }}
                                className={
                                  selected === data2.key
                                    ? "select-btn selected"
                                    : "select-btn"
                                }
                                onClick={(e) => setSelected(data2.key)}
                              >
                                {data2.label}
                              </Button>
                            </div>
                          );
                        })}
                      </div>
                    );
                  })}
                  <h4>
                    사용 가능한 태그{" "}
                    <span className="ml-2">
                      <FontAwesomeIcon
                        icon={faQuestionCircle}
                        style={{ cursor: "pointer" }}
                        onClick={() => {
                          setShowAvailableTagPopup(true);
                        }}
                      />
                    </span>
                  </h4>
                  &lt;닉네임&gt; &lt;회원등급&gt; &lt;신규 이벤트&gt;
                  &lt;프로그램명&gt;
                </div>
                <div className="col-4">
                  <div className="phone">
                    <div className="title">전송할 내용</div>
                    <div className="screen">
                      <textarea
                        className="form-control"
                        value={message[selected]}
                        onChange={(e) =>
                          setMessage(getMessageObj(selected, e.target.value))
                        }
                      ></textarea>
                    </div>
                    <div className="length">
                      {message[selected] ? message[selected].length : 0}/1000자
                    </div>
                  </div>
                </div>
                <div className="col-12 text-center">
                  <Button onClick={save}>저장</Button>
                  <Button onClick={cancel} className="ml-2">
                    취소
                  </Button>
                </div>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
      {showInformationPopup && (
        <InformationPopup
          onClickClose={(e) => {
            setShowInformationPopup(false);
          }}
        />
      )}
      {showAvailableTagPopup && (
        <AvailableTagPopup
          onClickClose={(e) => {
            setShowAvailableTagPopup(false);
          }}
        />
      )}
    </Container>
  );
};
