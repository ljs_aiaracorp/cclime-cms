import { useState, useEffect } from "react";
import "../css/Login.scss";
import { Modal, Button, Row, Col, Form, Card, Table } from "react-bootstrap";
import Pagination from "components/Pagination";
import axios from "axios";

export const AvailableTagPopup = (props) => {
  return (
    <Modal show={true} onHide={props.onClickClose} size="lg">
      <Modal.Header closeButton>
        <Modal.Title>
          <b>사용 가능한 태그</b>
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Row>
          <Col md="12">
            <Card className="card-plain table-plain-bg">
              <Card.Body className="table-full-width table-responsive px-0">
                <div>
                  전송할 내용에 태그를 추가하여 고객정보와 연동된 맞춤 메시지를
                  전송할 수 있습니다.
                </div>
                <div>1. &lt;닉네임&gt;</div>
                <div>푸시를 수신하는 고객의 닉네임이 표시됩니다.</div>
                <div>
                  사용 가능한 전송상황: 회원 가입, 예약, 이벤트 답변, 신고 답변,
                  생일, 결혼기념일, 만족도 조사
                </div>
                <div>2. &lt;신규 이벤트&gt;</div>
                <div>가장 최근에 등록 된 이벤트명이 표시됩니다.</div>
                <div>사용 가능한 전송상황: 신규 이벤트, 이벤트 답변</div>
                <div>3. &lt;프로그램명&gt;</div>
                <div>푸시 알림이 전송되는 프로그램명이 표시됩니다.</div>
                <div>사용 가능한 전송상황: 모든 예약, 만족도 조사</div>
                {/* <div>4. &lt;프로그램명&gt;</div>
                <div>푸시알림이 전송되는 프로그램명이 표시됩니다.</div> */}
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Modal.Body>
    </Modal>
  );
};
