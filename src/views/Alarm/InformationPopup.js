import { useState, useEffect } from "react";
import "../css/Login.scss";
import { Modal, Button, Row, Col, Form, Card, Table } from "react-bootstrap";
import Pagination from "components/Pagination";
import axios from "axios";

export const InformationPopup = (props) => {
  return (
    <Modal show={true} onHide={props.onClickClose} size="lg">
      <Modal.Header closeButton>
        <Modal.Title>
          <b>전송상황 설정 항목</b>
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Row>
          <Col md="12">
            <Card className="card-plain table-plain-bg">
              <Card.Body className="table-full-width table-responsive px-0">
                <div>
                  1. 회원가입: 회원가입 완료 시 푸시메시지가 전송됩니다.
                </div>
                <div>
                  2. 이벤트답변: 작성한 이벤트 댓글에 관리자 답변이 작성되었을
                  경우 푸시메시지가 전송됩니다.
                </div>
                <div>
                  3. 신규 이벤트: 새로운 이벤트 게시글이 작성되었을 때
                  푸시메시지가 전송됩니다.
                </div>
                <div>
                  4. 예약: 7일전:예약일 7일전에 푸시메시지가 전송됩니다.
                </div>
                <div>
                  5. 불편신고: 신고 답변이 달렸을 때 푸시메시지가 전송됩니다.
                </div>
                <div>6. 생일: 고객 생일 당일 푸시메시지가 전송됩니다.</div>
                <div>
                  7. 결혼기념일: 고객의 결혼기념일 당일 푸시메시지가 전송됩니다.
                </div>
                <div>
                  8. 만족도조사: 프로그램 사용처리후 설문조사 푸시메시지가
                  전송됩니다.
                </div>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Modal.Body>
    </Modal>
  );
};
