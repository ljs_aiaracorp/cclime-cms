import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import Pagination from "components/Pagination";
import Loading from "components/Loading";
import { useSelector } from "react-redux";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";
import moment from "moment";

export const PushHistory = (props) => {
  const [titles, setTitles] = useState([
    { label: "NO." },
    { label: "전송일자" },
    { label: "전송시간" },
    { label: "전송자" },
    { label: "수신자" },
    { label: "고객 전화번호" },
  ]);

  const [selected, setSelected] = useState(1);

  const [list, setList] = useState(null);

  const [count, setCount] = useState(0);

  const [pagenum, setPagenum] = useState();
  const [content, setContent] = useState("");
  useEffect(() => {
    getList();
  }, []);

  const getList = async (params) => {
    const storeUrl = axiosUrlFunction("notificationList", "알림");
    if (!params) {
      params = {};
    }
    if (params.page === undefined) {
      params.page = selected - 1;
    }

    setPagenum(params.page);
    const axiosconfig = {
      headers: {
        Authorization: `Bearer ${storeUrl.token}`,
        "Content-Type": `application/json`,
      },
    };
    await axios
      .get(`${storeUrl.apiUrl}?page=${params.page}`, axiosconfig)
      .then(async (response) => {
        setList(response.data.data.content);
        setCount(response.data.data.totalElements);
        if (response.data.data.content.length > 0)
          setContent(response.data.data.content[0].notification_content);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getBeautysList();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 전송할 내용
  const contentView = (index) => {
    setContent(list[index].notification_content);
  };

  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">알림전송 - 푸시알림 내역</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <div className="row">
                <div className="col-8 push-notice">
                  <Table className="table-hover">
                    <thead>
                      <tr>
                        {titles.map((v, i) => {
                          return (
                            <th key={i} className="border-0">
                              {v.label}
                            </th>
                          );
                        })}
                      </tr>
                    </thead>
                    <tbody>
                      {list !== undefined && list !== null ? (
                        list.map((noti, i) => {
                          return (
                            <tr
                              key={i}
                              className="cursor"
                              onClick={() => contentView(i)}
                            >
                              <td>{i + 1 + 10 * pagenum}</td>
                              <td>
                                {moment(noti.created_at).format("YYYY.MM.DD")}
                              </td>
                              <td>
                                {moment(noti.created_at).format("HH:mm:ss")}
                              </td>
                              <td>{noti.sender}</td>
                              <td>{noti.recipient}</td>
                              <td>{noti.phone}</td>
                            </tr>
                          );
                        })
                      ) : (
                        <tr>
                          <td
                            colSpan={titles.length}
                            style={{ textAlign: "center" }}
                          >
                            <Loading />
                          </td>
                        </tr>
                      )}
                    </tbody>
                  </Table>
                  <Pagination
                    count={count}
                    forcePage={selected - 1}
                    selected={(pageSelect) => {
                      setSelected(pageSelect);
                      getList({ page: pageSelect - 1 });
                    }}
                  />
                </div>
                <div className="col-4">
                  <div className="phone">
                    <div className="title">전송한 내용</div>
                    <div className="screen">
                      <textarea
                        className="form-control"
                        value={content}
                        onChange={() => {}}
                      ></textarea>
                    </div>
                    <div className="length">{content.length}/1000자</div>
                  </div>
                </div>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
