import { useState, useEffect } from "react";
import "../css/Login.scss";
import { Modal, Button, Row, Col, Form, Card, Table } from "react-bootstrap";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";

export const SelectProgramPopup = (props) => {
  const [list, setList] = useState([
    {
      value: "",
      checked: false,
    },
  ]);
  const [allChecked, setAllChecked] = useState(false);

  // 프로그램 리스트
  const getList = async () => {
    const programUrl = axiosUrlFunction("programPrepaidList");

    axios
      .get(`${programUrl.apiUrl}`, {
        headers: {
          Authorization: `Bearer ${programUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        let putData = [];
        res.data.data.map((v) => {
          if (
            props.onProgramListChecked.findIndex((element) => {
              return v === element;
            }) > -1
          ) {
            putData.push({
              value: v,
              checked: true,
            });
          } else {
            putData.push({
              value: v,
              checked: false,
            });
          }
        });
        setList(putData);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getList();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  const checkboxEventHandler = (event) => {
    const { value, checked } = event.target;
    const data = [];
    list.map((check) => {
      data.push(check);
    });

    if (checked) {
      for (let i = 0; i < data.length; i++) {
        if (data[i].value === value) {
          data[i].checked = true;
          setList(data);
          return;
        }
      }
    } else {
      for (let i = 0; i < data.length; i++) {
        if (data[i].value === value) {
          data[i].checked = false;
          setList(data);
          return;
        }
      }
    }
  };
  const allCheckedList = (checked) => {
    if (checked) {
      for (let i = 0; i < list.length; i++) {
        list[i].checked = true;
      }
      setAllChecked(true);
    } else {
      for (let i = 0; i < list.length; i++) {
        list[i].checked = false;
      }
      setAllChecked(false);
    }
  };

  const sendList = () => {
    const data = [];
    list.map((v) => {
      if (v.checked) {
        data.push(v.value);
      }
    });
    return data;
  };

  useEffect(() => {
    getList();
  }, []);

  return (
    <Modal show={true} onHide={props.onClickClose} size="lg">
      <Modal.Header closeButton>
        <Modal.Title>
          <b>프로그램 선택</b>
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Row>
          <Col md="12">
            <Card className="card-plain table-plain-bg">
              <Card.Body className="table-full-width table-responsive px-0">
                <div style={{ height: "600px" }}>
                  <Table className="table-hover mt-0">
                    <thead>
                      <tr>
                        <td>
                          <input
                            className="cursor"
                            type="checkbox"
                            checked={allChecked}
                            onChange={(e) => {
                              allCheckedList(e.target.checked);
                            }}
                          />
                        </td>
                        <td>NO</td>
                        <td>프로그램 명</td>
                      </tr>
                    </thead>
                    <tbody>
                      {list &&
                        list.map((v, i) => {
                          return (
                            <tr key={i}>
                              <td>
                                <input
                                  className="cursor"
                                  type="checkbox"
                                  value={v.value}
                                  checked={v.checked}
                                  onChange={(e) => {
                                    checkboxEventHandler(e);
                                  }}
                                />
                              </td>
                              <td>{i + 1}</td>
                              <td>{v.value}</td>
                            </tr>
                          );
                        })}
                    </tbody>
                  </Table>
                </div>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Modal.Body>
      <Modal.Footer className="text-center" style={{ display: "block" }}>
        <Button
          variant="primary"
          onClick={() => props.onCompleteSelection(sendList())}
        >
          선택완료
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
