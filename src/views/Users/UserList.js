import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import Pagination from "components/Pagination";
import Loading from "components/Loading";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import moment from "moment";
import axios from "axios";
import xlsx from "xlsx";

import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";

export const UserList = (props) => {
  const userApiData = axiosUrlFunction("userList", "검색"); // axios url & user Inform
  const cardTitle =
    userApiData.accessPath === "/admin"
      ? null
      : "( " + userApiData.store_name + " )";

  const [titles, setTitles] = useState([
    { label: "NO." },
    { label: "고객성명" },
    { label: "고객번호" },
    { label: "이메일" },
    { label: "핸드폰번호" },
    { label: "고객등급" },
    { label: "" },
  ]);

  const [selected, setSelected] = useState(1);

  const [list, setList] = useState();
  const [filters, setFilters] = useState();
  const [searchTarget, setSearchTarget] = useState("name");
  const [keyword, setKeyword] = useState("");
  const [count, setCount] = useState(0);
  const [pagenum, setPagenum] = useState();
  const [searchParam, setSearchParam] = useState({});

  useEffect(() => {
    getList();
  }, []);

  const getList = async (params) => {
    setList(null);
    if (!params) {
      params = {};
    }
    if (params.searchTarget === undefined) {
      params.searchTarget = searchTarget;
    }
    if (params.keyword === undefined) {
      params.keyword = keyword;
    }
    if (params.page === undefined) {
      params.page = selected - 1;
    }
    setPagenum(params.page);
    setSearchParam(params);

    const queries = [];
    if (params.searchTarget !== "" && params.keyword !== "") {
      queries.push(`type=${searchTarget}`);
      queries.push(`word=${params.keyword}`);
    }
    queries.push(`page=${params.page}`);
    queries.push(`size=10`);

    const queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";
    setFilters(queryStr);
    await axios
      .get(`${userApiData.apiUrl}${queryStr}`, {
        headers: {
          Authorization: `Bearer ${userApiData.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        setCount(res.data.data.totalElements);
        setList(
          res.data.data.content.map((contents) => {
            const testarray = [];
            testarray.push(contents);
            return testarray;
          })
        );
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();

            isCheck.then((res) => {
              if (!res) return;
              else getList(params);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  const xlsxUser = async () => {
    const userXlsxApiData = axiosUrlFunction("userList/excel", "검색");
    const res = await axios
      .get(`${userXlsxApiData.apiUrl}${filters}`, {
        headers: {
          Authorization: `Bearer ${userApiData.token}`,
          "Content-Type": `application/json`,
        },
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();

            isCheck.then((res) => {
              if (!res) return;
              else getList(params);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });

    if (!res) return;

    if (res.data.data.length === 0) {
      alert("인쇄 가능한 데이터가 없습니다.");
      return;
    }
    var data = res.data.data.map((item, index) => {
      var obj = {};
      obj.no = index + 1;
      obj.name = item.user_name;
      obj.userId = item.user_id;
      obj.email = item.email;
      obj.phone = item.phone;
      obj.grade = item.grade_name;
      return obj;
    });

    var heading = [titles.map((item) => item.label)];
    const wb = xlsx.utils.book_new();
    const ws = xlsx.utils.json_to_sheet([]);
    ws["!cols"] = [
      { wch: 10 },
      { wch: 10 },
      { wch: 10 },
      { wch: 25 },
      { wch: 20 },
      { wch: 10 },
    ];
    xlsx.utils.sheet_add_aoa(ws, heading, { skipHeader: true });
    xlsx.utils.sheet_add_json(ws, data, { origin: "A2", skipHeader: true });
    xlsx.utils.book_append_sheet(wb, ws, "Sheet1");
    xlsx.writeFile(
      wb,
      moment().format("YYYY_MM_DD").toString() + " 고객명단.xlsx"
    );
  };

  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">고객정보 - 고객명단 {cardTitle}</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <Table className="table-hover search-box">
                <tbody>
                  <tr>
                    <th
                      className="title text-center"
                      style={{ width: "100px" }}
                    >
                      총 <span className="total-count">{count}</span>명
                    </th>
                    <th className="select-search-target">
                      <Form.Control
                        as="select"
                        value={searchTarget}
                        onChange={(e) => {
                          setSearchTarget(e.target.value);
                        }}
                      >
                        <option value="name">고객명</option>
                        <option value="store">관리지점</option>
                        <option value="phone">핸드폰번호</option>
                        <option value="email">이메일</option>
                        <option value="grade">고객등급</option>
                      </Form.Control>
                    </th>
                    <th>
                      <Form.Control
                        type="text"
                        placeholder="검색"
                        value={keyword}
                        onChange={(e) => setKeyword(e.target.value)}
                        onKeyUp={(e) => {
                          if (e.keyCode === 13) {
                            setSelected(1);
                            getList({
                              page: 0,
                            });
                          }
                        }}
                        maxLength={50}
                        style={{
                          display: "inline-block",
                          width: "400px",
                          verticalAlign: "middle",
                        }}
                      />
                      <Button
                        variant="light"
                        onClick={(e) => {
                          setSelected(1);
                          getList({
                            page: 0,
                          });
                        }}
                        style={{
                          display: "inline-block",
                          border: "solid 1px #E3E3E3",
                          color: "#565656",
                          verticalAlign: "middle",
                        }}
                        className="search-btn"
                      >
                        <FontAwesomeIcon icon={faSearch} />
                      </Button>
                    </th>
                    <th>
                      <Button
                        onClick={(e) => {
                          window.location.replace(
                            `${userApiData.accessPath}/users/add`
                          );
                        }}
                      >
                        신규등록
                      </Button>
                    </th>
                  </tr>
                </tbody>
              </Table>
              <div
                style={{
                  display: "flex",
                  justifyContent: "flex-end",
                  padding: "0px",
                }}
              >
                <Button
                  onClick={(e) => {
                    xlsxUser();
                  }}
                >
                  인쇄
                </Button>
              </div>
              <Table className="table-hover mt-3">
                <thead>
                  <tr>
                    {titles.map((v, i) => {
                      return (
                        <th key={i} className="border-0">
                          {v.label}
                        </th>
                      );
                    })}
                  </tr>
                </thead>
                <tbody>
                  {list !== undefined && list !== null ? (
                    list.map((user, i) => {
                      return (
                        <tr key={i} className="cursor">
                          <td>{i + 1 + 10 * pagenum}</td>
                          <td>{user[0]["user_name"]}</td>
                          <td>{user[0]["user_id"]}</td>
                          <td>{user[0]["email"]}</td>
                          <td>{user[0]["phone"]}</td>
                          <td>{user[0]["grade_name"]}</td>
                          <td>
                            <Button
                              onClick={(e) => {
                                window.location.replace(
                                  `${userApiData.accessPath}/users/edit/${user[0]["user_id"]}`
                                );
                              }}
                              className="mr-2"
                            >
                              수정
                            </Button>
                          </td>
                        </tr>
                      );
                    })
                  ) : (
                    <tr>
                      <td
                        colSpan={titles.length}
                        style={{ textAlign: "center" }}
                      >
                        <Loading />
                      </td>
                    </tr>
                  )}
                </tbody>
              </Table>
              <Pagination
                count={count}
                forcePage={selected - 1}
                selected={(pageSelect) => {
                  setSelected(pageSelect);
                  getList({ page: pageSelect - 1 });
                }}
              />
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
