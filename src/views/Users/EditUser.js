import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import { GradeModal1, StorePopup } from "../../components/Popup/StorePopup";
import { CheckboxBtnPopup } from "../../components/Popup/CheckboxBtnPopup";
import AddressPopup from "components/Popup/AddressPopup";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import "../css/AddUser.scss";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";
import { isValidMobile } from "utils/validator";
export const EditUser = (props) => {
  // UserId
  const userNumver = props.match.params.id;

  // axios url & user Inform 지점
  const userApiData = axiosUrlFunction("");
  const userChecked = userApiData.accessPath === "/admin" ? true : false;

  // Form-data
  const [customername, setCustomername] = useState(""); // 고객 이름
  const [birthday, setBirthday] = useState(""); // 고객 생일
  const [email, setEmail] = useState(""); // 고객 이메일
  const [phone, setPhone] = useState(""); // 고객 휴대폰
  const [gender, setGender] = useState(""); // 고객 성별
  const [userNumbering, setUserNumbering] = useState(userNumver);
  const [checkList, setCheckList] = useState([]); // 지점 목록 Check List setter
  const [address, setAddress] = useState(""); // 고객 주소
  const [detailAddr, setDetailAddr] = useState(""); // 고객 상세주소
  const [tel, setTel] = useState(""); // 고객 전화번호
  const [marry, setMarry] = useState(""); // 고객 결혼여부
  const [weddingDay, setWeddingDay] = useState(""); // 고객 결혼기념일
  const [checkAttention, setCheckAttention] = useState([]); // 관심 있는 관리 종류
  const [checkboxList, setCheckboxList] = useState({
    skin: "",
    allergy: "",
    trouble: "",
    aesthetic: "",
    medicine: "",
    surgery: "",
    pregnancy: "",
    cclimeRoute: "",
  }); // 고객 체크박스 정보
  const [visit, setVisit] = useState(""); // 고객 최초방문일 ( 지점 )
  const [grade, setGrade] = useState(""); // 고객 등급
  const [couponList, setCouponList] = useState([]); // 쿠폰 정보
  const [managerComment, setManagerComment] = useState(""); // 기타주의사항
  const [reference, setreference] = useState(""); // 참고사항 ( 지점 )

  // Modal Info
  const [modalVisible, setModalVisible] = useState(false); // Modal
  const [addressModal, setAddressModal] = useState(false); // Address Modal
  const [gradeModal, setGradeModal] = useState(false); // Grade Modal
  const [buttonModal, setButtonModal] = useState(false); // Button Modal
  const [deleteButtonModal, setDeleteButtonModal] = useState(false); // Button Delete Modal
  const [deleteCheckModal, setDeleteCheckModal] = useState(false); // Button Delete Ok bt Check
  const [gradeinfo, setGradeinfo] = useState([]); // 고객등급 list
  const [gradeName, setGradeName] = useState(""); // 고객등급 명
  const [storeName, setStoreName] = useState([]); // 지점 목록
  const [getCheckList, setGetCheckList] = useState([]); // 지점 목록 Check List getter

  // 지점 Modal visible
  const openModal = () => {
    setModalVisible(true);
  };
  const closeModal = () => {
    setModalVisible(false);
  };

  // 지점추가 api연동
  const getStoreList = async () => {
    const storeUrl = axiosUrlFunction("storeList2", "검색");
    const res = await axios
      .get(`${storeUrl.apiUrl}`, {
        headers: {
          Authorization: `Bearer ${storeUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getStoreList();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
    setStoreName(res.data.data);
  };

  // user Data 불러오기
  const getUserData = async () => {
    const getUrl = axiosUrlFunction("user");
    axios
      .get(`${getUrl.apiUrl}?user_id=${userNumver}`, {
        headers: {
          Authorization: `Bearer ${getUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        const userData = res.data.data;
        saveUserDetail(
          userData.customerUser,
          userData.storeCustomerList,
          userData.userRetentionCouponList
        );
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getUserData();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };
  // User Data setter
  const saveUserDetail = (userData, store = [], coupon = []) => {
    let checkidArr = [];
    let checknameArr = [];
    let couponArr = [];

    if (userData === undefined) return;
    const addr = userData.address;
    const atten = userData.attention;
    setCustomername(userData.user_name);
    setBirthday(userData.birth);
    setEmail(userData.email);
    setPhone(userData.phone);
    setGender(userData.gender);

    // 지점 id && 지점 names
    if (userChecked) {
      store.map((id) => {
        checkidArr.push(id.store_id);
        checknameArr.push(id.store_name);
      });
      setCheckList(checkidArr);
      setGetCheckList(checknameArr);
    } else setGetCheckList([userApiData.store_name]);

    if (addr !== null) {
      addressSeparation(addr);
    }
    if (atten !== null) {
      setAttention(atten);
    }

    setTel(userData.tel);
    setMarry(userData.marry);
    setWeddingDay(userData.wedding_day);
    setCheckboxList({
      skin: userData.skin,
      allergy: userData.allergy,
      trouble: userData.trouble,
      aesthetic: userData.aesthetic,
      medicine: userData.medicine,
      surgery: userData.surgery,
      pregnancy: userData.pregnancy,
      cclimeRoute: userData.cclimeRoute,
    });
    setVisit(userData.visit);
    setGradeName(userData.grade);
    setGrade(userData.grade_id);
    coupon.map((list) => {
      couponArr.push(list.coupon_title);
    });
    setCouponList(couponArr);
    setManagerComment(userData.managerComment);
    setreference(userData.reference ? userData.reference : "");
  };

  // 관심있는관리 배열 분배
  const setAttention = (param) => {
    const attention = param.split(",");
    attention.map(async (data) => {
      checkAttention.push(data);
    });
  };

  // address 주소와 상세주소 분리 setter
  const addressSeparation = (addr) => {
    let addressDivision = addr.split(")");
    setAddress(addressDivision[0] + ")");
    setDetailAddr(addressDivision[1]);
  };

  // 지점 Box delete
  const checkListDelete = (index) => {
    checkList.splice(index, 1);
    setCheckList([...checkList]);
    getCheckList.splice(index, 1);
    setGetCheckList([...getCheckList]);
  };

  // address Component Modal open & close
  const addressOpenModal = () => {
    setAddressModal(true);
  };
  const addressCloseModal = (fullAddr) => {
    setAddressModal(false);
    if (fullAddr !== "") setAddress(fullAddr);
  };

  // GradeModal open & close
  const openGradeModal = () => {
    setGradeModal(true);
  };
  const closeGradeModal = () => {
    setGradeModal(false);
  };

  // ButtonModal open & close
  const openButtonModal = () => {
    setButtonModal(true);
  };
  const closeButtonModal = () => {
    setButtonModal(false);
    props.history.push(`${userApiData.accessPath}/users/list`);
  };

  // Delete ButtonModal open & close
  const deleteOpenButtonModal = () => {
    setDeleteButtonModal(true);
  };
  const deleteCloseButtonModal = () => {
    setDeleteButtonModal(false);
  };

  // Delete Modal 2
  const deleteCheckButtonModal = () => {
    setDeleteCheckModal(false);
    props.history.push(`${userApiData.accessPath}/users/list`);
  };

  // Delete User Data
  const deleteOkButtonModal = async () => {
    const userApiData = axiosUrlFunction("user/delete", "검색");
    const deleteUser = {
      user_id: userNumver,
    };
    axios
      .put(`${userApiData.apiUrl}`, deleteUser, {
        headers: {
          Authorization: `Bearer ${userApiData.token}`,
          "content-type": "application/json;charset=UTF-8",
        },
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else deleteOkButtonModal();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
    deleteCloseButtonModal();
    setDeleteCheckModal(true);
  };

  // check항목 중복관리 & 정보관리
  const checkOnlyOne = (element) => {
    const checkName = element.target.name;
    const checkboxes = document.getElementsByName(checkName);

    if (checkName === "attention") {
      // 관심 있는 관리 항목
      if (element.target.checked) {
        setCheckAttention([...checkAttention, element.target.value]);
      } else {
        const pos = checkAttention.indexOf(element.target.value);
        checkAttention.splice(pos, 1);
      }
      return;
    }
    checkboxes.forEach((cb) => {
      cb.checked = false;
    });

    element.target.checked = true;
    setCheckboxList({
      ...checkboxList,
      [checkName]: element.target.value,
    });
  };

  // 주소상세 검색 setter
  const saveDetailAddress = (e) => {
    const { value, name } = e.target;
    setDetailAddr(value);
  };

  // 고객 등급
  const getGrade = async () => {
    const gradeUrl = axiosUrlFunction("gradeList");
    axios
      .get(`${gradeUrl.apiUrl}`, {
        headers: {
          Authorization: `Bearer ${gradeUrl.token}`,
          "content-type": "application/json;charset=UTF-8",
        },
      })
      .then(async (res) => {
        setGradeinfo(res.data.data);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getGrade();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // grade_id setter
  const setGradeData = (e) => {
    const { checked, value } = e.target;
    if (checked) {
      gradeinfo.map((grade) => {
        if (grade.gradeId == value) {
          setGradeName(grade.gradeName);
        }
      });
      setGrade(value);
      closeGradeModal();
    }
  };

  // User Data Modify put
  const formPostAxios = async (event) => {
    const userApiData = axiosUrlFunction("user", "검색");

    let phoneCheck = checkingPhone(); // 전화번호 확인

    // 번호 확인
    if (phoneCheck === "") return null;
    // save user Form Data
    let userData = {
      user_id: userNumver,
      user_name: customername,
      birth: birthday.replace(/\-/g, ""),
      email: email,
      phone: phoneCheck,
      gender: gender,
      address: address + detailAddr,
      tel: tel,
      marry: marry,
      weddingDay: weddingDay === null ? "" : weddingDay.replace(/\-/g, ""),
      attention: checkAttention.toString(),
      skin: checkboxList.skin,
      allergy: checkboxList.allergy,
      trouble: checkboxList.trouble,
      aesthetic: checkboxList.aesthetic,
      medicine: checkboxList.medicine,
      surgery: checkboxList.surgery,
      pregnancy: checkboxList.pregnancy,
      cclimeRoute: checkboxList.cclimeRoute,
      managerComment: managerComment,
      grade_id: parseInt(grade),
    };
    if (userChecked) {
      userData = {
        ...userData,
        store_ids: checkList,
      };
    } else {
      if (visit !== null) {
        userData = {
          ...userData,
          visit: visit.replace(/\-/g, ""),
        };
      }
      if (reference !== null) {
        userData = {
          ...userData,
          reference: reference,
        };
      }
    }

    const key1 = Object.keys(userData);
    for (let i = 0; i < key1.length; i++) {
      if (userData[key1[i]] === "" || userData[key1[i]] === null) {
        delete userData[key1[i]];
      }
    }

    axios
      .put(`${userApiData.apiUrl}`, userData, {
        headers: {
          Authorization: `Bearer ${userApiData.token}`,
          "content-type": "application/json;charset=UTF-8",
        },
      })
      .then(() => {
        setButtonModal(true);
      })
      .catch((e) => {
        if (e.response.status === 409) {
          if (e.response.data.code === 31) {
            alert("이미 존재하는 이메일 입니다.\n다시 입력해주세요.");
          } else if (e.response.data.code === 32) {
            alert("이미 존재하는 핸드폰 번호 입니다.\n다시 입력해주세요.");
          }
        } else if (e.response.status === 401) {
          if (e.response.data.error === "Unauthorized") {
            // 토큰 재발급
            tokenCheck();
            formPostAxios(event);
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 핸드폰 정규화 하이픈 추가
  const checkingPhone = () => {
    if (!isValidMobile(phone)) {
      const value = phone.replace(
        /(^02.{0}|^01.{1}|[0-9]{3})([0-9]{3,4})([0-9]{4})/,
        "$1-$2-$3"
      );
      if (value.indexOf("-") === -1) {
        alert("전화번호를 확인해 주세요.");
        return "";
      }
      return value;
    } else return phone;
  };

  // 지점 Modal Add Button
  const storeSaveData = () => {
    let chk_obj = document.getElementsByName("branchCheckBox");

    let chk_leng = chk_obj.length;
    let checked = 0; //체크된 개수 파악을 위한 초기변수
    const arr = [];
    const arr2 = [];
    for (let i = 0; i < chk_leng; i++) {
      if (chk_obj[i].checked === true) {
        checked += 1;
        arr.push(parseInt(chk_obj[i].value));
        setCheckList(arr);
        if (chk_obj[i].value == storeName[i].store_id) {
          arr2.push(storeName[i].store_name);
          setGetCheckList(arr2);
        }
      }
    }

    closeModal();
  };

  useEffect(() => {
    if (userChecked) getStoreList();
    getUserData();
  }, []);

  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">
                고객정보 - 고객명단 - 기존고객 수정
              </Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <Table className="table-hover">
                <colgroup>
                  <col style={{ width: "15%" }}></col>
                  <col style={{ width: "35%" }}></col>
                  <col style={{ width: "15%" }}></col>
                  <col style={{ width: "35%" }}></col>
                </colgroup>
                <tbody>
                  <tr>
                    <th colSpan={2} className="sub-title">
                      필수정보
                    </th>
                    <th rowSpan={4}>
                      지점리스트
                      <br />
                      {userChecked && (
                        <Button onClick={openModal}>지점추가</Button>
                      )}
                      {modalVisible && (
                        <StorePopup
                          visible={modalVisible}
                          closable={true}
                          onClose={closeModal}
                          onSave={storeSaveData}
                        >
                          {storeName.map((store, index) => {
                            if (checkList.includes(store.store_id)) {
                              return (
                                <div
                                  key={store.store_id}
                                  className="branchCheck"
                                >
                                  <label
                                    style={{
                                      fontSize: "18px",
                                      color: "inherit",
                                    }}
                                  >
                                    <input
                                      type="checkbox"
                                      name="branchCheckBox"
                                      defaultChecked={true}
                                      value={store.store_id}
                                    />
                                    　{store.store_name}
                                  </label>
                                </div>
                              );
                            } else {
                              return (
                                <div
                                  key={store.store_id}
                                  className="branchCheck"
                                >
                                  <label
                                    style={{
                                      fontSize: "18px",
                                      color: "inherit",
                                    }}
                                  >
                                    <input
                                      type="checkbox"
                                      name="branchCheckBox"
                                      value={store.store_id}
                                    />
                                    　{store.store_name}
                                  </label>
                                </div>
                              );
                            }
                          })}
                        </StorePopup>
                      )}
                    </th>
                    <td rowSpan={4}>
                      <div
                        className="border"
                        style={{
                          minHeight: "150px",
                        }}
                      >
                        {getCheckList.map((lists, index) => {
                          return (
                            <div key={index} className="branch__list">
                              <span className="branch__list-name">
                                {index + 1}) {lists}
                              </span>
                              {userChecked && (
                                <FontAwesomeIcon
                                  onClick={() => {
                                    if (userChecked) checkListDelete(index);
                                  }}
                                  icon={faTimes}
                                  style={{ cursor: "pointer" }}
                                />
                              )}
                            </div>
                          );
                        })}
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th className="required">고객성명</th>
                    <td>
                      <input
                        className="form-control"
                        type="text"
                        required
                        placeholder="이름"
                        name="user_name"
                        defaultValue={customername}
                        onChange={(e) => setCustomername(e.target.value)}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th className="required">생년월일</th>
                    <td>
                      <input
                        className="form-control"
                        type="date"
                        required
                        placeholder="날짜를 선택해주세요"
                        name="birth"
                        defaultValue={birthday}
                        onChange={(e) => setBirthday(e.target.value)}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th className="required">이메일</th>
                    <td>
                      <input
                        className="form-control"
                        type="email"
                        required
                        placeholder="이메일 주소"
                        name="email"
                        defaultValue={email}
                        onChange={(e) => setEmail(e.target.value)}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th className="required">휴대폰 번호</th>
                    <td>
                      <input
                        className="form-control"
                        type="text"
                        required
                        placeholder="휴대폰 번호"
                        name="phone"
                        defaultValue={phone}
                        maxLength={15}
                        onChange={(e) => setPhone(e.target.value)}
                      />
                    </td>
                    <th>고객 번호</th>
                    <td>
                      <input
                        className="form-control"
                        type="text"
                        placeholder="고객 번호"
                        readOnly
                        defaultValue={userNumbering}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th className="required">성별</th>
                    <td>
                      <Button
                        className={
                          gender === "M" ? "select-btn selected" : "select-btn"
                        }
                        onClick={(e) => setGender("M")}
                      >
                        남성
                      </Button>
                      <Button
                        className={
                          gender === "M" ? "select-btn" : "select-btn selected"
                        }
                        onClick={(e) => setGender("F")}
                      >
                        여성
                      </Button>
                    </td>
                  </tr>
                  <tr>
                    <th colSpan={2} className="sub-title">
                      부가정보
                    </th>
                    <td colSpan={2}></td>
                  </tr>
                  <tr>
                    <th>주소</th>
                    <td>
                      <input
                        className="form-control"
                        type="text"
                        placeholder="주소를 검색해주세요"
                        name="address1"
                        style={{
                          width: "76%",
                          display: "inline-block",
                          marginRight: "10px",
                        }}
                        defaultValue={address || ""}
                        readOnly
                      />
                      <Button onClick={addressOpenModal}>주소검색</Button>
                      {addressModal && (
                        <AddressPopup onmakeclose={addressCloseModal} />
                      )}

                      <input
                        className="form-control"
                        type="text"
                        placeholder="상세주소"
                        name="address2"
                        defaultValue={detailAddr}
                        onChange={saveDetailAddress}
                        style={{
                          width: "76%",
                          display: "inline-block",
                          marginRight: "10px",
                        }}
                      />
                    </td>
                    <td colSpan={2}></td>
                  </tr>
                  <tr>
                    <th>전화번호</th>
                    <td>
                      <input
                        className="form-control"
                        type="text"
                        placeholder="전화번호"
                        name="tel"
                        defaultValue={tel}
                        maxLength={15}
                        onChange={(e) => setTel(e.target.value)}
                      />
                    </td>
                    <td colSpan={2}></td>
                  </tr>
                  <tr>
                    <th>결혼 여부</th>
                    <td>
                      <Button
                        className={
                          marry === "Y" ? "select-btn selected" : "select-btn"
                        }
                        onClick={(e) => {
                          setMarry("Y");
                        }}
                      >
                        기혼
                      </Button>
                      <Button
                        className={
                          marry === "Y" ? "select-btn" : "select-btn selected"
                        }
                        onClick={(e) => {
                          setMarry("N");
                        }}
                      >
                        미혼
                      </Button>
                    </td>
                    <td colSpan={2}></td>
                  </tr>
                  <tr>
                    <th>결혼 기념일</th>
                    <td>
                      <input
                        className="form-control"
                        type="date"
                        placeholder="날짜를 선택해주세요"
                        disabled={marry === "Y" ? false : true}
                        defaultValue={weddingDay}
                        onChange={(e) => setWeddingDay(e.target.value)}
                      />
                    </td>
                    <td colSpan={2}></td>
                  </tr>
                  <tr>
                    <th colSpan={4}>
                      관심 있는 관리
                      <br />
                      <Form.Check
                        type="checkbox"
                        label="작은얼굴(큰얼굴/비대칭/광대축소/피부탄력)"
                        name="attention"
                        value="작은얼굴"
                        onChange={(e) => checkOnlyOne(e)}
                        checked={
                          checkAttention.indexOf("작은얼굴") !== -1
                            ? true
                            : false
                        }
                      />
                      <Form.Check
                        type="checkbox"
                        label="문제성피부(여드름/기미/화이트닝/모공)"
                        id="attention2"
                        name="attention"
                        value="문제성피부"
                        onChange={(e) => checkOnlyOne(e)}
                        checked={
                          checkAttention.indexOf("문제성피부") !== -1
                            ? true
                            : false
                        }
                      />
                      <Form.Check
                        type="checkbox"
                        label="다이어트(라인관리/처진살/복부/상하체/팔뚝)"
                        id="attention3"
                        name="attention"
                        value="다이어트"
                        onChange={(e) => checkOnlyOne(e)}
                        checked={
                          checkAttention.indexOf("다이어트") !== -1
                            ? true
                            : false
                        }
                      />
                      <Form.Check
                        type="checkbox"
                        label="웨딩관리"
                        id="attention4"
                        name="attention"
                        value="웨딩관리"
                        onChange={(e) => checkOnlyOne(e)}
                        checked={
                          checkAttention.indexOf("웨딩관리") !== -1
                            ? true
                            : false
                        }
                      />
                      <Form.Check
                        type="checkbox"
                        label="산후관리"
                        id="attention5"
                        name="attention"
                        value="산후관리"
                        onChange={(e) => checkOnlyOne(e)}
                        checked={
                          checkAttention.indexOf("산후관리") !== -1
                            ? true
                            : false
                        }
                      />
                      <Form.Check
                        type="checkbox"
                        label="성형전후관리"
                        id="attention6"
                        name="attention"
                        value="성형전후관리"
                        onChange={(e) => checkOnlyOne(e)}
                        checked={
                          checkAttention.indexOf("성형전후관리") !== -1
                            ? true
                            : false
                        }
                      />
                      <Form.Check
                        type="checkbox"
                        label="면접맞춤관리"
                        id="attention7"
                        name="attention"
                        value="면접맞춤관리"
                        onChange={(e) => checkOnlyOne(e)}
                        checked={
                          checkAttention.indexOf("면접맞춤관리") !== -1
                            ? true
                            : false
                        }
                      />
                    </th>
                  </tr>
                  <tr>
                    <th colSpan={4}>
                      <div className="row">
                        <div className="col-3">
                          피부타입
                          <br />
                          <Form.Check
                            type="checkbox"
                            label="지성"
                            name="skin"
                            value="oily"
                            checked={
                              checkboxList.skin === "oily" ? true : false
                            }
                            onChange={(e) => checkOnlyOne(e)}
                          />
                          <Form.Check
                            type="checkbox"
                            label="복합성"
                            name="skin"
                            value="combination"
                            checked={
                              checkboxList.skin === "combination" ? true : false
                            }
                            onChange={(e) => checkOnlyOne(e)}
                          />
                          <Form.Check
                            type="checkbox"
                            label="건성"
                            name="skin"
                            value="dry"
                            checked={checkboxList.skin === "dry" ? true : false}
                            onChange={(e) => checkOnlyOne(e)}
                          />
                        </div>
                        <div className="col-3">
                          피부 알레르기 여부
                          <br />
                          <Form.Check
                            type="checkbox"
                            label="네"
                            name="allergy"
                            value="Y"
                            checked={
                              checkboxList.allergy === "Y" ? true : false
                            }
                            onChange={(e) => checkOnlyOne(e)}
                          />
                          <Form.Check
                            type="checkbox"
                            label="아니오"
                            name="allergy"
                            value="N"
                            checked={
                              checkboxList.allergy === "N" ? true : false
                            }
                            onChange={(e) => checkOnlyOne(e)}
                          />
                        </div>
                        <div className="col-3">
                          피부 트러블 경험 여부
                          <br />
                          <Form.Check
                            type="checkbox"
                            label="네"
                            name="trouble"
                            value="Y"
                            checked={
                              checkboxList.trouble === "Y" ? true : false
                            }
                            onChange={(e) => checkOnlyOne(e)}
                          />
                          <Form.Check
                            type="checkbox"
                            label="아니오"
                            name="trouble"
                            value="N"
                            checked={
                              checkboxList.trouble === "N" ? true : false
                            }
                            onChange={(e) => checkOnlyOne(e)}
                          />
                        </div>
                        <div className="col-3">
                          타 에스테틱을 경험 여부
                          <br />
                          <Form.Check
                            type="checkbox"
                            label="네"
                            name="aesthetic"
                            value="Y"
                            checked={
                              checkboxList.aesthetic === "Y" ? true : false
                            }
                            onChange={(e) => checkOnlyOne(e)}
                          />
                          <Form.Check
                            type="checkbox"
                            label="아니오"
                            name="aesthetic"
                            value="n"
                            onChange={(e) => checkOnlyOne(e)}
                            checked={
                              checkboxList.aesthetic === "N" ? true : false
                            }
                          />
                        </div>
                      </div>
                    </th>
                  </tr>
                  <tr>
                    <th colSpan={3}>
                      <div className="row">
                        <div className="col-4">
                          복용중인 약 여부
                          <br />
                          <Form.Check
                            type="checkbox"
                            label="네"
                            name="medicine"
                            value="Y"
                            onChange={(e) => checkOnlyOne(e)}
                            checked={
                              checkboxList.medicine === "Y" ? true : false
                            }
                          />
                          <Form.Check
                            type="checkbox"
                            label="아니오"
                            name="medicine"
                            value="N"
                            onChange={(e) => checkOnlyOne(e)}
                            checked={
                              checkboxList.medicine === "N" ? true : false
                            }
                            // onChange={() => setIsEnable(true)}
                          />
                        </div>
                        <div className="col-4">
                          시술/수술 경험 여부
                          <br />
                          <Form.Check
                            type="checkbox"
                            label="네"
                            name="surgery"
                            value="Y"
                            onChange={(e) => checkOnlyOne(e)}
                            checked={
                              checkboxList.surgery === "Y" ? true : false
                            }
                          />
                          <Form.Check
                            type="checkbox"
                            label="아니오"
                            name="surgery"
                            value="N"
                            onChange={(e) => checkOnlyOne(e)}
                            checked={
                              checkboxList.surgery === "N" ? true : false
                            }
                          />
                        </div>
                        <div className="col-4">
                          임신 여부(여성일 경우)
                          <br />
                          <Form.Check
                            type="checkbox"
                            label="네"
                            name="pregnancy"
                            value="Y"
                            onChange={(e) => checkOnlyOne(e)}
                            checked={
                              checkboxList.pregnancy === "Y" ? true : false
                            }
                          />
                          <Form.Check
                            type="checkbox"
                            label="아니오"
                            name="pregnancy"
                            value="N"
                            onChange={(e) => checkOnlyOne(e)}
                            checked={
                              checkboxList.pregnancy === "N" ? true : false
                            }
                          />
                        </div>
                      </div>
                    </th>
                    <th rowSpan={2}>
                      기타 주의사항
                      <br />
                      <textarea
                        className="form-control"
                        style={{
                          minHeight: "150px",
                        }}
                        onChange={(e) => setManagerComment(e.target.value)}
                        defaultValue={managerComment}
                      ></textarea>
                    </th>
                  </tr>
                  <tr>
                    <th colSpan={3}>
                      끌리메를 알게된 경로
                      <br />
                      <Form.Check
                        type="checkbox"
                        label="홈페이지"
                        name="cclimeRoute"
                        value="홈페이지"
                        onChange={(e) => checkOnlyOne(e)}
                        checked={
                          checkboxList.cclimeRoute === "홈페이지" ? true : false
                        }
                      />
                      <Form.Check
                        type="checkbox"
                        label="네이버(광고/검색/블로그/카페/예약)"
                        name="cclimeRoute"
                        value="네이버"
                        onChange={(e) => checkOnlyOne(e)}
                        checked={
                          checkboxList.cclimeRoute === "네이버" ? true : false
                        }
                      />
                      <Form.Check
                        type="checkbox"
                        label="다음"
                        name="cclimeRoute"
                        value="다음"
                        onChange={(e) => checkOnlyOne(e)}
                        checked={
                          checkboxList.cclimeRoute === "다음" ? true : false
                        }
                      />
                      <Form.Check
                        type="checkbox"
                        label="카카오톡(검색/선물 등)"
                        name="cclimeRoute"
                        value="카카오톡"
                        onChange={(e) => checkOnlyOne(e)}
                        checked={
                          checkboxList.cclimeRoute === "카카오톡" ? true : false
                        }
                      />
                      <Form.Check
                        type="checkbox"
                        label="인스타그램"
                        name="cclimeRoute"
                        value="인스타그램"
                        onChange={(e) => checkOnlyOne(e)}
                        checked={
                          checkboxList.cclimeRoute === "인스타그램"
                            ? true
                            : false
                        }
                      />
                      <Form.Check
                        type="checkbox"
                        label="페이스북"
                        name="cclimeRoute"
                        value="페이스북"
                        onChange={(e) => checkOnlyOne(e)}
                        checked={
                          checkboxList.cclimeRoute === "페이스북" ? true : false
                        }
                      />
                      <Form.Check
                        type="checkbox"
                        label="유튜브"
                        name="cclimeRoute"
                        value="유튜브"
                        onChange={(e) => checkOnlyOne(e)}
                        checked={
                          checkboxList.cclimeRoute === "유튜브" ? true : false
                        }
                      />
                    </th>
                  </tr>
                  <tr>
                    <th>최초 방문일</th>
                    <td>
                      <input
                        className="form-control"
                        type="date"
                        placeholder="날짜를 선택해주세요"
                        disabled={userChecked}
                        defaultValue={visit}
                        onChange={(e) => setVisit(e.target.value)}
                      />
                    </td>
                    <td colSpan={2}></td>
                  </tr>
                  <tr>
                    <th>고객등급</th>
                    <td>
                      <input
                        className="form-control"
                        type="text"
                        placeholder="등급을 선택해주세요"
                        defaultValue={gradeName}
                        readOnly={true}
                      />
                    </td>
                    <td colSpan={2}>
                      <Button
                        onClick={() => {
                          openGradeModal();
                          getGrade();
                        }}
                        className="ml-2"
                        style={{ width: "80px", height: "45px" }}
                      >
                        선택
                      </Button>
                      {gradeModal && (
                        <GradeModal1 onclose={closeGradeModal}>
                          {gradeinfo.map((grade, index) => (
                            <div key={index}>
                              <label>
                                <input
                                  type="checkBox"
                                  value={grade.gradeId}
                                  onChange={(e) => setGradeData(e)}
                                />
                                　{grade.gradeName}
                              </label>
                            </div>
                          ))}
                        </GradeModal1>
                      )}
                    </td>
                  </tr>
                  <tr>
                    <th>적립금</th>
                    <td>
                      <input
                        className="form-control"
                        type="text"
                        placeholder="적립금"
                        readOnly={true}
                      />
                    </td>
                    <td colSpan={2}></td>
                  </tr>
                  <tr>
                    <th>보유쿠폰</th>
                    <td>
                      <div
                        className="border"
                        style={{
                          minHeight: "150px",
                        }}
                      >
                        {couponList.map((list, key) => (
                          <div key={key}>
                            <span>{list}</span>
                          </div>
                        ))}
                      </div>
                    </td>
                    <td colSpan={2}></td>
                  </tr>
                  <tr>
                    <th>
                      참고사항
                      <br />
                      (지점별)
                    </th>
                    <td colSpan={3}>
                      {userChecked ? (
                        <div
                          className="border"
                          style={{
                            minHeight: "150px",
                          }}
                        ></div>
                      ) : (
                        <textarea
                          className="form-control"
                          style={{
                            minHeight: "150px",
                          }}
                          onChange={(e) => setreference(e.target.value)}
                          value={reference}
                        ></textarea>
                      )}
                    </td>
                  </tr>
                </tbody>
              </Table>
              <div className="text-center">
                {userChecked && (
                  <Button onClick={deleteOpenButtonModal} className="mr-2">
                    삭제
                  </Button>
                )}
                {deleteButtonModal && (
                  <CheckboxBtnPopup
                    onClose={deleteCloseButtonModal}
                    onConfirm={true}
                    onOk={deleteOkButtonModal}
                  >
                    정말로 해당 고객을 삭제하시겠습니까?
                  </CheckboxBtnPopup>
                )}
                {deleteCheckModal && (
                  <CheckboxBtnPopup onClose={deleteCheckButtonModal}>
                    해당 고객정보가 삭제되었습니다.
                  </CheckboxBtnPopup>
                )}
                <Button
                  type="submit"
                  onClick={() => {
                    formPostAxios();
                  }}
                >
                  저장
                </Button>
                {buttonModal && (
                  <CheckboxBtnPopup onClose={closeButtonModal}>
                    고객정보 수정이 완료되었습니다.
                  </CheckboxBtnPopup>
                )}
                <Button
                  className="ml-2"
                  onClick={() => {
                    props.history.push(`${userApiData.accessPath}/users/list`);
                  }}
                >
                  취소
                </Button>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
