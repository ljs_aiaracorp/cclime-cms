import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import "../css/paymentTable/payTable.scss";
import Loading from "components/Loading";
import { SelectProgramOnCheckPopup } from "./SelectProgramOnCheckPopup";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";

export const UserUsageHistory = (props) => {
  const [searchAll, setSearchAll] = useState(true);

  const [userList, setUserList] = useState({
    user_id: "",
    user_name: "",
    gender: "",
    birth: "",
    email: "",
    phone: "",
    grade: "",
  });
  const [programUseList, setProgramUseList] = useState({
    userPaymentListVOList: [],
    userUseListVOList: [],
  });
  const [startDay, setStartDay] = useState("");
  const [endDay, setEndDay] = useState("");
  const [programList, setProgramList] = useState({
    value: "",
    programId: "",
  }); // 프로그램/선불권 선택 리스트
  const [showSelectProgramPopup, setShowSelectProgramPopup] = useState(false);

  useEffect(() => {
    getUserList();
    getProgramList();
  }, []);

  // 고객 조회
  const getUserList = async () => {
    const getUserUrl = axiosUrlFunction("user");

    axios
      .get(`${getUserUrl.apiUrl}?user_id=${props.match.params.id}`, {
        headers: {
          Authorization: `Bearer ${getUserUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        setUserList(res.data.data.customerUser);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getUserList();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 프로그램 조회
  const getProgramList = async () => {
    const getProgramUrl = axiosUrlFunction("reservation/useDetail", "검색");

    axios
      .get(`${getProgramUrl.apiUrl}?user_id=${props.match.params.id}`, {
        headers: {
          Authorization: `Bearer ${getProgramUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        if (
          res.data.data.userPaymentListVOList.length === 0 &&
          res.data.data.userUseListVOList === 0
        ) {
          alert("사용내역이 없습니다.");
          return;
        }
        setProgramUseList(res.data.data);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getProgramList();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else if (err.response.status === 404) {
          if (err.response.data.error === "Not Found") {
            alert("사용내역이 없습니다.");
            return;
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 조건검색
  const getSearchProgramList = async () => {
    const searchUrl = axiosUrlFunction("reservation/useDetail", "검색");
    const queries = [];
    if (programList.value !== "") {
      queries.push(`program_id=${programList.programId}`);
    }
    queries.push(`user_id=${props.match.params.id}`);

    if (startDay !== "") {
      queries.push(`start=${startDay.replace(/\-/g, "")}`);
      if (endDay !== "") queries.push(`end=${endDay.replace(/\-/g, "")}`);
      else {
        alert("기간을 모두 선택해 주세요.");
        return;
      }
    }

    if (startDay === "" && endDay !== "") {
      alert("기간을 모두 선택해 주세요.");
      return;
    }

    let queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";

    axios
      .get(`${searchUrl.apiUrl}${queryStr}`, {
        headers: {
          Authorization: `Bearer ${searchUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        if (res.data.data.userPaymentListVOList.length === 0) {
          alert("사용내역이 없습니다.");
          return;
        }
        setProgramUseList(res.data.data);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getProgramList();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else if (err.response.status === 404) {
          if (err.response.data.error === "Not Found") {
            alert("사용내역이 없습니다.");
            return;
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };
  // 프로그램/선불권 선택 리스트
  const programCheckedList = (list) => {
    setProgramList(list);
    setShowSelectProgramPopup(false);
  };
  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">
                고객정보 - 고객 사용내역 조회 - 고객 히스토리 보기
              </Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <div className="title-background">고객 필수 정보</div>
              <Table className="table-hover search-box">
                <tbody>
                  <tr>
                    <th>고객성명</th>
                    <th>
                      <Form.Control
                        type="text"
                        readOnly={true}
                        value={userList.user_name}
                      />
                    </th>
                    <th>이메일</th>
                    <th>
                      <Form.Control
                        type="email"
                        readOnly={true}
                        value={userList.email}
                      />
                    </th>
                    <th>성별</th>
                    <th>
                      <Form.Control
                        type="text"
                        readOnly={true}
                        value={userList.gender === "M" ? "남자" : "여자"}
                      />
                    </th>
                  </tr>
                  <tr>
                    <th>생년월일</th>
                    <th>
                      <Form.Control
                        type="date"
                        readOnly={true}
                        value={userList.birth}
                      />
                    </th>
                    <th>휴대폰</th>
                    <th>
                      <Form.Control
                        type="text"
                        readOnly={true}
                        value={userList.phone}
                      />
                    </th>
                    <th>고객등급</th>
                    <th>
                      <Form.Control
                        type="text"
                        readOnly={true}
                        value={userList.grade}
                      />
                    </th>
                  </tr>
                </tbody>
              </Table>
              <div className="title-background">
                검색필터
                <Button
                  className={`ml-3 select-btn ${searchAll ? "selected" : ""}`}
                  onClick={(e) => setSearchAll(true)}
                >
                  전체
                </Button>
                <Button
                  className={`ml-2 select-btn ${searchAll ? "" : "selected"}`}
                  onClick={(e) => setSearchAll(false)}
                >
                  조건검색
                </Button>
              </div>
              {!searchAll && (
                <>
                  <Table className="table-hover search-box">
                    <tbody>
                      <tr>
                        <th
                          className="title text-center"
                          style={{ width: "100px" }}
                        >
                          프로그램
                        </th>
                        <th className="select-search-target">
                          <Form.Control
                            type="text"
                            maxLength={50}
                            value={programList.value}
                            onChange={(e) => setProgram(e.target.value)}
                            style={{
                              display: "inline-block",
                              width: "72%",
                            }}
                          />
                          <Button
                            onClick={(e) => setShowSelectProgramPopup(true)}
                          >
                            선택
                          </Button>
                        </th>
                        <th
                          className="title text-center"
                          style={{ width: "100px" }}
                        >
                          기간
                        </th>
                        <th className="select-search-target" colSpan={3}>
                          <Form.Group as={Row} className="mb-0">
                            <Col sm={5} className="pr-0">
                              <Form.Control
                                className="mb-0"
                                type="date"
                                placeholder="시작 날짜"
                                value={startDay}
                                onChange={(e) => setStartDay(e.target.value)}
                              />
                            </Col>
                            <Col
                              sm={1}
                              style={{ fontSize: "24px", textAlign: "center" }}
                            >
                              ~
                            </Col>
                            <Col sm={5} className="pl-0">
                              <Form.Control
                                className="mb-0"
                                type="date"
                                placeholder="종료 날짜"
                                value={endDay}
                                onChange={(e) => setEndDay(e.target.value)}
                              />
                            </Col>
                          </Form.Group>
                        </th>
                      </tr>
                    </tbody>
                  </Table>
                  <div className="text-center">
                    <Button
                      onClick={() => {
                        getSearchProgramList();
                      }}
                    >
                      검색
                    </Button>
                    <Button
                      onClick={() => {
                        if (!window.confirm("초기화 하시겠습니까?")) return;
                        setProgramList({
                          value: "",
                          programId: "",
                        });
                        setStartDay("");
                        setEndDay("");
                        getProgramList();
                      }}
                      className="ml-3 cancel"
                    >
                      초기화
                    </Button>
                  </div>
                </>
              )}
              <div className="row">
                <div className="col-6">
                  <div className="title-background">결제내역</div>
                  <div className="table-scroll2">
                    <Table className="table-hover mt-0 table-scroll2__table">
                      <thead>
                        <tr>
                          <th className="border-0">NO</th>
                          <th className="border-0" style={{ width: "200px" }}>
                            상품명
                          </th>
                          <th className="border-0">결제금액</th>
                          <th className="border-0">환불금액</th>
                          <th className="border-0">결제일시</th>
                          <th className="border-0">결과</th>
                        </tr>
                      </thead>
                      <tbody>
                        {programUseList.userPaymentListVOList !== undefined &&
                        programUseList.userPaymentListVOList !== null ? (
                          programUseList.userPaymentListVOList.map(
                            (value, i) => {
                              const colorChecked = {
                                color: value.state === "paid" ? "blue" : "red",
                              };
                              return (
                                <tr key={i} className="cursor">
                                  <td>{i + 1}</td>
                                  <td>{value.payment_explanation}</td>
                                  <td>{value.payment_amount}</td>
                                  <td>{value.refund_amount}</td>
                                  <td>{value.payment_at.slice(0, 10)}</td>
                                  <td style={colorChecked}>
                                    {value.state === "paid"
                                      ? "결제완료"
                                      : "결제취소"}
                                  </td>
                                </tr>
                              );
                            }
                          )
                        ) : (
                          <tr>
                            <td colSpan={5} style={{ textAlign: "center" }}>
                              <Loading />
                            </td>
                          </tr>
                        )}
                      </tbody>
                    </Table>
                  </div>
                </div>
                <div className="col-6">
                  <div className="title-background">사용처리내역</div>
                  <div className="table-scroll2">
                    <Table className="table-hover mt-0 table-scroll2__table">
                      <thead>
                        <tr>
                          <th className="border-0">NO</th>
                          <th className="border-0" style={{ width: "200px" }}>
                            상품명
                          </th>
                          <th className="border-0">사용지점</th>
                          <th className="border-0">사용처리일시</th>
                        </tr>
                      </thead>
                      <tbody>
                        {programUseList.userUseListVOList !== undefined &&
                        programUseList.userUseListVOList !== null ? (
                          programUseList.userUseListVOList.map((value, i) => {
                            return (
                              <tr key={i} className="cursor">
                                <td>{i + 1}</td>
                                <td>{value.use_explanation}</td>
                                <td>{value.store_name}</td>
                                <td>{value.store_sales_at.slice(0, 10)}</td>
                              </tr>
                            );
                          })
                        ) : (
                          <tr>
                            <td
                              colSpan={4}
                              style={{ textAlign: "center" }}
                            ></td>
                          </tr>
                        )}
                      </tbody>
                    </Table>
                  </div>
                </div>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
      {showSelectProgramPopup && (
        <SelectProgramOnCheckPopup
          onCompleteSelection={programCheckedList}
          onClickClose={(e) => {
            setShowSelectProgramPopup(false);
          }}
        />
      )}
    </Container>
  );
};
