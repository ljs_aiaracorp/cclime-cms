import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";
import Pagination from "components/Pagination";
import Loading from "components/Loading";
import { SelectProgramOnCheckPopup } from "./SelectProgramOnCheckPopup";
import { isValidMobile } from "utils/validator";

export const UserUsageList = (props) => {
  const [titles, setTitles] = useState([
    { label: "NO." },
    { label: "고객성명" },
    { label: "고객번호" },
    { label: "이메일" },
    { label: "핸드폰번호" },
    { label: "고객등급" },
    { label: "" },
  ]);

  const [selected, setSelected] = useState(1);
  const [pagenum, setPagenum] = useState();
  const [count, setCount] = useState(0);

  const [list, setList] = useState(null);
  const [showSelectProgramPopup, setShowSelectProgramPopup] = useState(false);
  const [userName, setUserName] = useState(""); // 고객명
  const [phone, setPhone] = useState(""); // 핸드폰번호
  const [userNumber, setUserNumber] = useState(""); // 고객번호
  const [isday, setIsDay] = useState(true); // 기간
  const [startDay, setStartDay] = useState(""); // 시작기간
  const [endDay, setEndDay] = useState(""); // 종료기간
  const [minDiscount, setMinDiscount] = useState(""); // 최소금액
  const [maxDiscount, setMaxDiscount] = useState(""); // 최대금액
  const [programList, setProgramList] = useState({
    value: "",
    programId: "",
  }); // 프로그램/선불권 선택 리스트

  useEffect(() => {
    getList();
  }, []);

  // 고객 사용내역 조회
  const getList = async (params) => {
    setList(null);
    if (!params) {
      params = {};
    }

    const getUrl = axiosUrlFunction("reservation/useList", "검색");

    const queries = [];

    if (params.page === undefined) {
      params.page = selected - 1;
    }
    if (userName !== "") {
      queries.push(`name=${userName}`);
    }
    if (phone !== "") {
      queries.push(`phone=${phone}`);
    }
    if (userNumber !== "") {
      queries.push(`user_id=${userNumber}`);
    }

    if (programList.value !== "") {
      queries.push(`program_id=${programList.programId}`);
    }

    if (startDay !== "") {
      queries.push(`start=${startDay.replace(/\-/g, "")}`);
      if (endDay !== "") queries.push(`end=${endDay.replace(/\-/g, "")}`);
      else {
        alert("기간을 모두 선택해 주세요.");
        return;
      }
    }

    if (startDay === "" && endDay !== "") {
      alert("기간을 모두 선택해 주세요.");
      return;
    }

    if (minDiscount !== "") {
      queries.push(`amount_start=${minDiscount}`);
    }
    if (maxDiscount !== "") {
      queries.push(`amount_end=${maxDiscount}`);
    }
    queries.push(`page=${params.page}`);
    queries.push(`size=10`);
    let queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";

    setPagenum(params.page);

    if (params.reset) {
      queryStr = `?page=${params.page}&size=10`;
    }

    axios
      .get(`${getUrl.apiUrl}${queryStr}`, {
        headers: {
          Authorization: `Bearer ${getUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        setCount(res.data.data.totalElements);
        setList(res.data.data.content);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getList(params);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else if (err.response.status === 400) {
          alert("입력을 올바르게 해주세요.");
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 프로그램/선불권 선택 리스트
  const programCheckedList = (list) => {
    setProgramList(list);
    setIsDay(false);
    setShowSelectProgramPopup(false);
  };

  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">고객정보 - 고객 사용내역 조회</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <Table className="table-hover search-box">
                <tbody>
                  <tr>
                    <th
                      className="title text-center"
                      style={{ width: "100px" }}
                    >
                      고객명
                    </th>
                    <th className="select-search-target">
                      <Form.Control
                        type="text"
                        value={userName}
                        onChange={(e) => setUserName(e.target.value)}
                        maxLength={50}
                        onKeyUp={(e) => {
                          if (e.keyCode === 13) {
                            setSelected(1);
                            getList({
                              page: 0,
                            });
                          }
                        }}
                      />
                    </th>
                    <th
                      className="title text-center"
                      style={{ width: "100px" }}
                    >
                      핸드폰번호
                    </th>
                    <th className="select-search-target">
                      <Form.Control
                        type="text"
                        value={phone}
                        onChange={(e) => setPhone(e.target.value)}
                        maxLength={13}
                        onKeyUp={(e) => {
                          if (e.keyCode === 13) {
                            setSelected(1);
                            getList({
                              page: 0,
                            });
                          }
                        }}
                      />
                    </th>
                    <th
                      className="title text-center"
                      style={{ width: "100px" }}
                    >
                      고객번호
                    </th>
                    <th className="select-search-target">
                      <Form.Control
                        type="text"
                        value={userNumber}
                        onChange={(e) => setUserNumber(e.target.value)}
                        maxLength={50}
                        onKeyUp={(e) => {
                          if (e.keyCode === 13) {
                            setSelected(1);
                            getList({
                              page: 0,
                            });
                          }
                        }}
                      />
                    </th>
                  </tr>
                  <tr>
                    <th
                      className="title text-center"
                      style={{ width: "100px" }}
                    >
                      프로그램
                    </th>
                    <th className="select-search-target">
                      <Form.Control
                        type="text"
                        readOnly
                        maxLength={50}
                        style={{
                          display: "inline-block",
                          width: "72%",
                        }}
                        value={programList.value}
                        onChange={(e) => setProgramName(e.target.value)}
                      />
                      <Button onClick={(e) => setShowSelectProgramPopup(true)}>
                        선택
                      </Button>
                    </th>
                    <th
                      className="title text-center"
                      style={{ width: "100px" }}
                    >
                      기간
                    </th>
                    <th className="select-search-target" colSpan={3}>
                      <Form.Group as={Row} className="mb-0">
                        <Col sm={5} className="pr-0">
                          <Form.Control
                            className="mb-0"
                            type="date"
                            placeholder="시작 날짜"
                            value={startDay}
                            onChange={(e) => setStartDay(e.target.value)}
                          />
                        </Col>
                        <Col
                          sm={1}
                          style={{ fontSize: "24px", textAlign: "center" }}
                        >
                          ~
                        </Col>
                        <Col sm={5} className="pl-0">
                          <Form.Control
                            className="mb-0"
                            type="date"
                            placeholder="종료 날짜"
                            value={endDay}
                            onChange={(e) => setEndDay(e.target.value)}
                          />
                        </Col>
                      </Form.Group>
                    </th>
                  </tr>
                  <tr>
                    <th
                      className="title text-center"
                      style={{ width: "100px" }}
                    >
                      사용 금액
                    </th>
                    <th className="select-search-target" colSpan={5}>
                      <Form.Group as={Row} className="mb-0">
                        <Col sm={5} className="pr-0">
                          <Form.Control
                            className="mb-0"
                            type="text"
                            value={minDiscount}
                            onChange={(e) => setMinDiscount(e.target.value)}
                            onKeyUp={(e) => {
                              if (e.keyCode === 13) {
                                setSelected(1);
                                getList({
                                  page: 0,
                                });
                              }
                            }}
                          />
                        </Col>
                        <Col
                          sm={1}
                          style={{ fontSize: "24px", textAlign: "center" }}
                        >
                          ~
                        </Col>
                        <Col sm={5} className="pl-0">
                          <Form.Control
                            className="mb-0"
                            type="text"
                            value={maxDiscount}
                            onChange={(e) => setMaxDiscount(e.target.value)}
                            onKeyUp={(e) => {
                              if (e.keyCode === 13) {
                                setSelected(1);
                                getList({
                                  page: 0,
                                });
                              }
                            }}
                          />
                        </Col>
                      </Form.Group>
                    </th>
                  </tr>
                </tbody>
              </Table>
              <div className="text-center">
                <Button
                  onClick={() => {
                    setSelected(1);
                    getList({
                      page: 0,
                    });
                  }}
                >
                  검색
                </Button>
                <Button
                  onClick={() => {
                    if (!window.confirm("초기화 하시겠습니까?")) return;
                    setUserName("");
                    setPhone("");
                    setUserNumber("");
                    setProgramList({
                      value: "",
                      programId: "",
                    });
                    setStartDay("");
                    setEndDay("");
                    setMinDiscount("");
                    setMaxDiscount("");
                    setIsDay(true);
                    setSelected(1);
                    getList({
                      page: 0,
                      reset: true,
                    });
                  }}
                  className="ml-3 cancel"
                >
                  초기화
                </Button>
              </div>
              <div className="text-left">
                총 <span className="total-count">{count}</span>명
              </div>
              <Table className="table-hover">
                <thead>
                  <tr>
                    {titles.map((v, i) => {
                      return (
                        <th key={i} className="border-0">
                          {v.label}
                        </th>
                      );
                    })}
                  </tr>
                </thead>
                <tbody>
                  {list !== undefined && list !== null ? (
                    list.map((user, i) => {
                      return (
                        <tr key={i} className="cursor">
                          <td>{i + 1 + 10 * pagenum}</td>
                          <td>{user.user_name}</td>
                          <td>{user.user_id}</td>
                          <td>{user.email}</td>
                          <td>{user.phone}</td>
                          <td>{user.grade_name}</td>
                          <td>
                            <Button
                              onClick={(e) => {
                                window.location.replace(
                                  `/admin/users/history/${user.user_id}`
                                );
                              }}
                              className="mr-2"
                            >
                              보기
                            </Button>
                          </td>
                        </tr>
                      );
                    })
                  ) : (
                    <tr>
                      <td
                        colSpan={titles.length}
                        style={{ textAlign: "center" }}
                      >
                        <Loading />
                      </td>
                    </tr>
                  )}
                </tbody>
              </Table>
              <Pagination
                count={count}
                forcePage={selected - 1}
                selected={(pageSelect) => {
                  setSelected(pageSelect);
                  getList({ page: pageSelect - 1 });
                }}
              />
            </Card.Body>
          </Card>
        </Col>
      </Row>
      {showSelectProgramPopup && (
        <SelectProgramOnCheckPopup
          onCompleteSelection={programCheckedList}
          onClickClose={(e) => {
            setShowSelectProgramPopup(false);
          }}
        />
      )}
    </Container>
  );
};
