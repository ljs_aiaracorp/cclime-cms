import axios from "axios";

class MovieDetailService {
  getMovieDetail(contents_id) {
    return axios.get("/api/admin/storeList", {
      headers: {
        "Content-Type": "application/json",
        Authorization:
          "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxIiwiYXV0aCI6IlJPTEVfTUFJTiIsImV4cCI6MTYzNDc4NjE3OH0.zM3S7LMj6YU85c-r6yvrACIZycV9CmOKKkq6aaaLcNK6LHZCWWw0OakUh8nVCuC411n5XcvF6AXlzeW9L38cNA",
      },
    });
  }
}

export default new MovieDetailService();
