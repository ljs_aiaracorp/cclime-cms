import { useState } from "react";
import { useSelector } from "react-redux";
import "../css/Login.scss";
import axios from "axios";

export const Login = (props) => {
  // Login Check
  const isLoggedIn = useSelector((state) => state.login.isLoggedIn);

  // id & passwort state
  const [identifier, setIdentifier] = useState("");
  const [password, setPassword] = useState("");

  // Error Message state
  const [idErrorMsg, setIdErrorMsg] = useState("");
  const [pwErrorMsg, setPwErrorMsg] = useState("");

  const loginExecute = async () => {
    if (!identifier || identifier === "") {
      setIdErrorMsg("아이디를 확인해주세요.");
      return null;
    }
    if (!password || password === "") {
      setPwErrorMsg("비밀번호를 확인해주세요.");
      return null;
    }
    if (identifier && password) {
      // userid, password Save to axios
      const data = {
        id: identifier,
        password: password,
      };
      await axios
        .post(`${process.env.REACT_APP_API_URL}/adminLogin`, data, {
          headers: { "Content-Type": `application/json` },
        })
        .then((response) => {
          // 로그인 유저정보 저장.
          // 로그인 성공 시 localStorage에 admin data 저장
          if (response.status === 200 && response.data) {
            localStorage.setItem("admin", JSON.stringify(response.data));
            return response.data;
          }
        })
        .catch((e) => {
          if (e.message === "Network Error") {
            alert("서버에 문제가 발생하였습니다.");
            return;
          }
          if (e.response.status === 401) {
            if (e.response.data.code === 6) {
              // Id error check
              setIdErrorMsg("아이디를 확인해주세요");
              return null;
            } else if (e.response.data.code === 7) {
              // Password error check
              setIdErrorMsg("");
              setPwErrorMsg("비밀번호를 확인해주세요.");
              return null;
            } else {
              alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
              return null;
            }
          } else if (e) {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
            return null;
          }
        })
        .then((response) => {
          if (response === null) return;
          else {
            const user = JSON.parse(localStorage.getItem("admin"));
            if (user !== null) {
              const url = user.data.role === "main" ? "/admin" : "/staff";
              alert("로그인이 되었습니다.");
              window.location.href = `${url}/users/list`;
            }
          }
        });
    }
  };
  if (isLoggedIn) {
    const user = JSON.parse(localStorage.getItem("admin"));
    const url = user.data.role === "main" ? "/admin" : "/staff";
    props.history.push(`${url}/users/list`);
  }
  // id, password setter
  const onChangeIdentifier = (value) => {
    setIdentifier(value);
  };
  const onChangePassword = (value) => {
    setPassword(value);
  };

  return (
    <div className="bg-img">
      <div className="content">
        <header>
          <img src="/loginLogo.png" />
        </header>
        <div className="field row">
          <div className="col-4">
            <label>아이디</label>
          </div>
          <div className="col-8">
            <input
              type="text"
              required
              placeholder="아이디"
              defaultValue={identifier}
              maxLength={15}
              onChange={(e) => onChangeIdentifier(e.target.value)}
            />
            {idErrorMsg && <div className="error-msg">{idErrorMsg}</div>}
          </div>
        </div>
        <div className="field row mt-4">
          <div className="col-4">
            <label>비밀번호</label>
          </div>
          <div className="col-8">
            <input
              type="password"
              required
              placeholder="패스워드"
              defaultValue={password}
              maxLength={15}
              onChange={(e) => onChangePassword(e.target.value)}
              onKeyPress={(e) => {
                if (e.key === "Enter") {
                  loginExecute();
                }
              }}
            />
            {pwErrorMsg && <div className="error-msg">{pwErrorMsg}</div>}
          </div>
        </div>
        <div className="field mb-4 mt-5">
          <button onClick={loginExecute}>로그인</button>
        </div>
      </div>
    </div>
  );
};
