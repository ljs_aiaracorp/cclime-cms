import { useEffect, useState } from "react";
import { Container, Row, Col, Card, Table, Button } from "react-bootstrap";
import Pagination from "components/Pagination";
import Loading from "components/Loading";
import { AddAccountPopup } from "./AddAccountPopup";
import { EditAccountPopup } from "./EditAccountPopup";
import { CheckboxBtnPopup } from "../../components/Popup/CheckboxBtnPopup";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";

export const Account = (props) => {
  const [list, setList] = useState(null); // 지점 계정관리 정보
  const [count, setCount] = useState(0); // 자료 카운트
  const [selected, setSelected] = useState(1);
  const [pagenum, setPagenum] = useState("");
  const [deleteId, setDeleteId] = useState("");
  const [adminId, setAdminId] = useState("");

  const [showAddAccountPopup, setShowAddAccountPopup] = useState(false);
  const [editPopup, setEditPopup] = useState(false);
  const [deleteButtonModal, setDeleteButtonModal] = useState(false); // Button Delete Modal
  const [deleteCheckModal, setDeleteCheckModal] = useState(false); // Button Delete Ok bt Check

  useEffect(() => {
    getList();
  }, []);

  // 계정 정보
  const getList = async (params) => {
    setList(null);

    const getUrl = axiosUrlFunction("adminList", "검색");

    if (!params) {
      params = {};
    }
    if (params.page === undefined) {
      params.page = selected - 1;
    }
    setPagenum(params.page);

    const queries = [];
    queries.push(`page=${params.page}`);
    queries.push(`size=10`);
    const queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";

    await axios
      .get(`${getUrl.apiUrl}${queryStr}`, {
        headers: {
          Authorization: `Bearer ${getUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        setCount(res.data.data.totalElements);
        setList(res.data.data.content);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getList(params);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // Delete ButtonModal open & close
  const deleteOpenButtonModal = () => {
    setDeleteButtonModal(true);
  };
  const deleteCloseButtonModal = () => {
    setDeleteButtonModal(false);
  };

  // Delete Modal 2
  const deleteCheckButtonModal = () => {
    setDeleteCheckModal(false);
    window.location.replace("/admin/accounts/list");
  };

  // ID 삭제
  const deleteUser = async (id) => {
    const deleteUrl = axiosUrlFunction("delete", "검색");
    const data = {
      admin_id: parseInt(deleteId),
    };

    await axios
      .put(`${deleteUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${deleteUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        deleteCloseButtonModal();
        setDeleteCheckModal(true);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else deleteUser();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };
  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">계정관리 - 지점 계정 관리</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <div className="row">
                <div
                  className={`col-2 tab-btn selected-tab`}
                  onClick={(e) => {}}
                >
                  지점 계정 관리
                </div>
                <div
                  className={`col-2 tab-btn`}
                  onClick={(e) =>
                    props.history.push("/admin/accounts/identifier")
                  }
                >
                  아이디 변경
                </div>
                <div
                  className={`col-2 tab-btn`}
                  onClick={(e) =>
                    props.history.push("/admin/accounts/password")
                  }
                >
                  비밀번호 변경
                </div>
              </div>
              <div className="text-right">
                <Button onClick={(e) => setShowAddAccountPopup(true)}>
                  계정 추가
                </Button>
              </div>
              <table className="table-hover mt-0 table-center">
                <thead>
                  <tr>
                    <th className="border-0">NO.</th>
                    <th className="border-0">지점명</th>
                    <th className="border-0">권한</th>
                    <th className="border-0">아이디</th>
                    <th className="border-0"></th>
                  </tr>
                </thead>
                <tbody>
                  {list !== undefined && list !== null ? (
                    list.map((list, i) => {
                      return (
                        <tr key={i} className="cursor">
                          <td>{i + 1 + 10 * pagenum}</td>
                          <td>{list.store_name}</td>
                          <td>{list.role}</td>
                          <td>{list.id}</td>
                          <td>
                            <Button
                              className="mr-3"
                              onClick={() => {
                                setAdminId(list.admin_id);
                                setEditPopup(true);
                              }}
                            >
                              변경
                            </Button>
                            <Button
                              onClick={() => {
                                setDeleteId(list.admin_id);
                                setDeleteButtonModal(true);
                              }}
                            >
                              삭제
                            </Button>
                          </td>
                        </tr>
                      );
                    })
                  ) : (
                    <tr>
                      <td colSpan={5} style={{ textAlign: "center" }}>
                        <Loading />
                      </td>
                    </tr>
                  )}
                </tbody>
              </table>
              <Pagination
                count={count}
                forcePage={selected - 1}
                selected={(pageSelect) => {
                  setSelected(pageSelect);
                  getList({ page: pageSelect - 1 });
                }}
              />
            </Card.Body>
          </Card>
        </Col>
      </Row>
      {showAddAccountPopup && (
        <AddAccountPopup
          onClickSave={() => {}}
          onClickClose={(e) => {
            setShowAddAccountPopup(false);
          }}
        />
      )}
      {editPopup && (
        <EditAccountPopup
          id={adminId}
          onClickClose={(e) => {
            setEditPopup(false);
          }}
        />
      )}
      {deleteButtonModal && (
        <CheckboxBtnPopup
          onClose={deleteCloseButtonModal}
          onConfirm={true}
          onOk={deleteUser}
        >
          정말로 해당 계정을 삭제하시겠습니까?
        </CheckboxBtnPopup>
      )}
      {deleteCheckModal && (
        <CheckboxBtnPopup onClose={deleteCheckButtonModal}>
          해당 계정정보가 삭제되었습니다.
        </CheckboxBtnPopup>
      )}
    </Container>
  );
};
