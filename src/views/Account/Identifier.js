import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";
import { isValidId } from "utils/validator";

export const Identifier = (props) => {
  const user = axiosUrlFunction("updateId");
  const title =
    user.accessor === "main" ? '"관리자"' : '"' + user.store_name + '"';
  const isUser = user.accessor === "main" ? true : false;
  const [userId, setUserId] = useState(user.user_id); // 기존 아이디
  const [changeId, setChangeId] = useState(""); // 변경 할 아이디
  const [password, setPassword] = useState(""); // 비밀번호
  // 회원가입 Error Message
  const [idErrorMsg, setIdErrorMsg] = useState("");
  const [pwErrorMsg, setPwErrorMsg] = useState("");

  const sendUserData = async () => {
    setIdErrorMsg("");
    setPwErrorMsg("");
    const data = {
      new_id: changeId,
      password,
    };

    if (!isValidId(changeId)) {
      setIdErrorMsg("영문, 숫자 조합으로 입력해 주세요.");
      return null;
    }

    axios
      .put(`${user.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${user.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then((res) => {
        alert("아이디가 변경 됐습니다.");
        setUserId(changeId);
        setChangeId("");
        setPassword("");
        let userInfo = JSON.parse(localStorage.getItem("admin"));
        userInfo.data.id = changeId;
        localStorage.setItem("admin", JSON.stringify(userInfo));
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.message.indexOf("401") > -1) {
            if (err.response.data.error === "Unauthorized") {
              // 토큰 재발급
              const isCheck = tokenCheck();
              isCheck.then((res) => {
                if (!res) return;
                else sendUserData();
              });
            } else if (err.response.data.code === 10) {
              alert("로그인 정보가 만료 되었습니다.\n다시 로그인해주세요.");
              window.location.href = "/Login";
            } else if (err.response.data.code === 7) {
              setPwErrorMsg("비밀번호를 확인해 주세요.");
            }
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else if (err.response.status === 409) {
          if (err.response.data.code === 2) {
            setIdErrorMsg("중복된 아이디 입니다.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">계정관리 - 아이디 변경</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <div className="row">
                {isUser && (
                  <div
                    className={`col-2 tab-btn`}
                    onClick={(e) =>
                      props.history.push(`${user.accessPath}/accounts/list`)
                    }
                  >
                    지점 계정 관리
                  </div>
                )}
                <div className={`col-2 tab-btn selected-tab`}>아이디 변경</div>
                <div
                  className={`col-2 tab-btn`}
                  onClick={(e) =>
                    props.history.push(`${user.accessPath}/accounts/password`)
                  }
                >
                  비밀번호 변경
                </div>
              </div>
              <h2>접속한 계정은 {title} 계정입니다</h2>
              <Table className="table-hover">
                <colgroup>
                  <col style={{ width: "15%" }}></col>
                  <col style={{ width: "85%" }}></col>
                </colgroup>
                <tbody>
                  <tr>
                    <th>현재 ID</th>
                    <td>
                      <input
                        className="form-control"
                        type="text"
                        required
                        placeholder="ID"
                        value={userId}
                        readOnly={true}
                        maxLength={15}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>변경할 ID</th>
                    <td>
                      <input
                        className="form-control"
                        type="text"
                        required
                        placeholder="영문 또는 숫자를 입력해주세요"
                        value={changeId}
                        maxLength={15}
                        onChange={(e) => setChangeId(e.target.value)}
                      />
                      {idErrorMsg && (
                        <div className="error-msg" style={{ color: "red" }}>
                          {idErrorMsg}
                        </div>
                      )}
                    </td>
                  </tr>
                  <tr>
                    <th>비밀번호 확인</th>
                    <td>
                      <input
                        className="form-control"
                        type="password"
                        required
                        placeholder="비밀번호 확인"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                      />
                      {pwErrorMsg && (
                        <div className="error-msg" style={{ color: "red" }}>
                          {pwErrorMsg}
                        </div>
                      )}
                    </td>
                  </tr>
                </tbody>
              </Table>
              <div className="text-center">
                <Button onClick={sendUserData}>저장</Button>
                <Button
                  className="ml-2"
                  onClick={() => {
                    window.location.replace(
                      `${user.accessPath}/accounts/identifier`
                    );
                  }}
                >
                  취소
                </Button>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
