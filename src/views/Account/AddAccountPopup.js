import { useState, useEffect } from "react";
import "../css/Login.scss";
import { Modal, Button, Row, Col, Form, Card, Table } from "react-bootstrap";
import { StorePopup } from "components/Popup/StorePopup";
import { isValidId } from "utils/validator";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";

export const AddAccountPopup = (props) => {
  const [permission, setPermission] = useState(0);
  const [userId, setUserId] = useState("");
  const [userPassword1, setUserPassword1] = useState("");
  const [userPassword2, setUserPassword2] = useState("");
  const [userStoreId, setUserStoreId] = useState("");
  const [userStoreName, setUserStoreName] = useState("");
  const [userAccess, setUserAccess] = useState("store");
  const [storeList, setStoreList] = useState(null); // 지점 목록 정보
  const [isStoreModal, setIsStoreModal] = useState(false); // Modal
  // 회원가입 Error Message
  const [idErrorMsg, setIdErrorMsg] = useState("");
  const [pwErrorMsg, setPwErrorMsg] = useState("");

  useEffect(() => {
    getStoreList();
  }, []);

  // 지점 정보
  const getStoreList = async () => {
    const getStoreUrl = axiosUrlFunction("storeList2", "검색");

    axios
      .get(`${getStoreUrl.apiUrl}`, {
        headers: {
          Authorization: `Bearer ${getStoreUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        setStoreList(res.data.data);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getStoreList();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // Store Modal open & close
  const openModal = () => {
    setIsStoreModal(true);
  };
  const closeModal = (event) => {
    const { value, checked } = event.target;
    if (checked) {
      storeList.map((store) => {
        if (store.store_id === parseInt(value)) {
          setUserStoreName(store.store_name);
        }
      });
      setUserStoreId(value);
    }

    setIsStoreModal(false);
  };

  // 회원가입
  const sendSignUp = async () => {
    setIdErrorMsg("");
    if (userPassword1 === userPassword2) {
      setPwErrorMsg("");
      // 비밀번호 6자리 이상
      if (userPassword2.length < 6) {
        setPwErrorMsg("비밀번호 6자리 이상 입력해 주세요.");
        return null;
      }
      // ID 특수문자 금지
      if (!isValidId(userId)) {
        setIdErrorMsg("영문, 숫자 조합으로 입력해 주세요.");
        return null;
      }
      if (userStoreId === "") {
        alert("지점을 선택해 주세요.");
        return;
      }
      const signUrl = axiosUrlFunction("adminSignUp", "검색");
      const data = {
        id: userId,
        password: userPassword2,
        store_id: userStoreId,
        role: userAccess,
      };

      axios
        .post(`${signUrl.apiUrl}`, data, {
          headers: {
            Authorization: `Bearer ${signUrl.token}`,
            "Content-Type": `application/json`,
          },
        })
        .then(async (res) => {
          if (res.status === 200 && res.data) {
            window.location.replace("/admin/accounts/list");
          }
        })
        .catch((err) => {
          if (err.message.indexOf("409") > -1) {
            setIdErrorMsg("중복된 아이디 입니다.");
            return null;
          } else if (err.response.status === 401) {
            if (err.response.data.error === "Unauthorized") {
              // 토큰 재발급
              const isCheck = tokenCheck();
              isCheck.then((res) => {
                if (!res) return;
                else sendSignUp();
              });
            } else {
              alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
            }
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        });
    } else {
      setPwErrorMsg("비밀번호가 같지 않습니다.");
    }
  };
  return (
    <Modal show={true} onHide={props.onClickClose} size="lg">
      <Modal.Header closeButton>
        <Modal.Title>
          <b>계정추가</b>
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Row>
          <Col md="12">
            <Card className="card-plain table-plain-bg">
              <Card.Body className="table-full-width table-responsive px-0">
                <Table className="table-hover">
                  <colgroup>
                    <col style={{ width: "15%" }}></col>
                    <col style={{ width: "85%" }}></col>
                  </colgroup>
                  <tbody>
                    <tr>
                      <th>ID</th>
                      <td>
                        <input
                          className="form-control"
                          type="text"
                          required
                          placeholder="ID"
                          defaultValue={userId}
                          maxLength={15}
                          onChange={(e) => setUserId(e.target.value)}
                        />
                        {idErrorMsg && (
                          <div className="error-msg" style={{ color: "red" }}>
                            {idErrorMsg}
                          </div>
                        )}
                      </td>
                    </tr>
                    <tr>
                      <th>비밀번호</th>
                      <td>
                        <input
                          className="form-control"
                          type="password"
                          required
                          placeholder="비밀번호"
                          defaultValue={userPassword1}
                          minLength={6}
                          maxLength={15}
                          onChange={(e) => setUserPassword1(e.target.value)}
                        />
                        {pwErrorMsg && (
                          <div className="error-msg" style={{ color: "red" }}>
                            {pwErrorMsg}
                          </div>
                        )}
                      </td>
                    </tr>
                    <tr>
                      <th>비밀번호 확인</th>
                      <td>
                        <input
                          className="form-control"
                          type="password"
                          required
                          placeholder="비밀번호 확인"
                          defaultValue={userPassword2}
                          minLength={6}
                          maxLength={15}
                          onChange={(e) => setUserPassword2(e.target.value)}
                        />
                        {pwErrorMsg && (
                          <div className="error-msg" style={{ color: "red" }}>
                            {pwErrorMsg}
                          </div>
                        )}
                      </td>
                    </tr>
                    <tr>
                      <th>지점선택</th>
                      <td>
                        <span>{userStoreName}&nbsp;&nbsp;&nbsp;</span>
                        <Button onClick={openModal}>지점선택</Button>
                        {isStoreModal && (
                          <StorePopup
                            visible={isStoreModal}
                            closable={true}
                            onClose={closeModal}
                          >
                            {storeList.map((store, index) => (
                              <div key={index} className="branchCheck">
                                <label
                                  style={{ fontSize: "18px", color: "inherit" }}
                                >
                                  <input
                                    type="checkbox"
                                    value={store.store_id}
                                    onChange={(e) => closeModal(e)}
                                  />
                                  　{store.store_name}
                                </label>
                              </div>
                            ))}
                          </StorePopup>
                        )}
                      </td>
                    </tr>
                    <tr>
                      <th>권한선택</th>
                      <td>
                        <Button
                          className={
                            permission === 0
                              ? "select-btn selected"
                              : "select-btn"
                          }
                          onClick={(e) => {
                            setPermission(0);
                            setUserAccess("store");
                          }}
                        >
                          원장
                        </Button>
                        <Button
                          className={
                            permission === 1
                              ? "select-btn selected"
                              : "select-btn"
                          }
                          onClick={(e) => {
                            setPermission(1);
                            setUserAccess("staff");
                          }}
                        >
                          일반
                        </Button>
                      </td>
                    </tr>
                  </tbody>
                </Table>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Modal.Body>

      <Modal.Footer className="text-center" style={{ display: "block" }}>
        <Button variant="primary" onClick={sendSignUp}>
          저장
        </Button>
        <Button variant="primary" onClick={props.onClickClose} className="ml-2">
          취소
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
