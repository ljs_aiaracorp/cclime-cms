import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";

export const Password = (props) => {
  const user = axiosUrlFunction("updatePw");
  const title =
    user.accessor === "main" ? '"관리자"' : '"' + user.store_name + '"';
  const isUser = user.accessor === "main" ? true : false;
  const [nowPassword, setNowPassword] = useState(""); // 기존 비밀번호
  const [newPassword1, setNewPassword1] = useState(""); // 변경 할 비밀번호
  const [newPassword2, setNewPassword2] = useState(""); // 비밀번호 확인

  // 회원가입 Error Message
  const [pwErrorMsg1, setIdErrorMsg1] = useState("");
  const [pwErrorMsg2, setPwErrorMsg2] = useState("");

  const sendUserData = async () => {
    setIdErrorMsg1("");
    setPwErrorMsg2("");
    if (newPassword1 === newPassword2) {
      if (newPassword2.length < 6) {
        setPwErrorMsg2("비밀번호 6자리 이상 입력해 주세요.");
        return null;
      }
      const data = {
        password: nowPassword,
        new_password: newPassword2,
      };

      axios
        .put(`${user.apiUrl}`, data, {
          headers: {
            Authorization: `Bearer ${user.token}`,
            "Content-Type": `application/json`,
          },
        })
        .then((res) => {
          alert("비밀번호가 변경 됐습니다.");
          window.location.replace(`${user.accessPath}/accounts/password`);
        })
        .catch((err) => {
          if (err.response.status === 401) {
            if (err.message.indexOf("401") > -1) {
              if (err.response.data.error === "Unauthorized") {
                // 토큰 재발급
                const isCheck = tokenCheck();
                isCheck.then((res) => {
                  if (!res) return;
                  else sendUserData();
                });
              } else if (err.response.data.code === 10) {
                alert("로그인 정보가 만료 되었습니다.\n다시 로그인해주세요.");
                window.location.href = "/Login";
              } else if (err.response.data.code === 7) {
                setIdErrorMsg1("비밀번호를 확인해 주세요.");
              }
            } else {
              alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
            }
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        });
    } else {
      setPwErrorMsg2("비밀번호가 같지 않습니다. 다시 입력해주세요.");
    }
  };

  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">계정관리 - 비밀번호 변경</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <div className="row">
                {isUser && (
                  <div
                    className={`col-2 tab-btn`}
                    onClick={(e) =>
                      props.history.push(`${user.accessPath}/accounts/list`)
                    }
                  >
                    지점 계정 관리
                  </div>
                )}
                <div
                  className={`col-2 tab-btn`}
                  onClick={(e) =>
                    props.history.push(`${user.accessPath}/accounts/identifier`)
                  }
                >
                  아이디 변경
                </div>
                <div className={`col-2 tab-btn selected-tab`}>
                  비밀번호 변경
                </div>
              </div>
              <h2>접속한 계정은 {title} 계정입니다</h2>
              <Table className="table-hover">
                <colgroup>
                  <col style={{ width: "15%" }}></col>
                  <col style={{ width: "85%" }}></col>
                </colgroup>
                <tbody>
                  <tr>
                    <th>현재 비밀번호</th>
                    <td>
                      <input
                        className="form-control"
                        type="password"
                        required
                        placeholder="현재 비밀번호"
                        defaultValue={nowPassword}
                        onChange={(e) => setNowPassword(e.target.value)}
                      />
                      {pwErrorMsg1 && (
                        <div className="error-msg" style={{ color: "red" }}>
                          {pwErrorMsg1}
                        </div>
                      )}
                    </td>
                  </tr>
                  <tr>
                    <th>변경할 비밀번호</th>
                    <td>
                      <input
                        className="form-control"
                        type="password"
                        required
                        placeholder="변경할 비밀번호"
                        defaultValue={newPassword1}
                        onChange={(e) => setNewPassword1(e.target.value)}
                      />
                      {pwErrorMsg2 && (
                        <div className="error-msg" style={{ color: "red" }}>
                          {pwErrorMsg2}
                        </div>
                      )}
                    </td>
                  </tr>
                  <tr>
                    <th>변경할 비밀번호 확인</th>
                    <td>
                      <input
                        className="form-control"
                        type="password"
                        required
                        placeholder="변경할 비밀번호 확인"
                        defaultValue={newPassword2}
                        maxLength={15}
                        onChange={(e) => setNewPassword2(e.target.value)}
                      />
                      {pwErrorMsg2 && (
                        <div className="error-msg" style={{ color: "red" }}>
                          {pwErrorMsg2}
                        </div>
                      )}
                    </td>
                  </tr>
                </tbody>
              </Table>
              <div className="text-center">
                <Button onClick={sendUserData}>저장</Button>
                <Button
                  className="ml-2"
                  onClick={() => {
                    window.location.replace(
                      `${user.accessPath}/accounts/password`
                    );
                  }}
                >
                  취소
                </Button>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
