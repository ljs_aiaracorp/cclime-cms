import React, { useEffect, useState } from "react";
import { Modal, Button, Table } from "react-bootstrap";
import Loading from "components/Loading";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";

export const CancelPopup = ({ id, onclose }) => {
  const user = axiosUrlFunction("");
  const [list, setList] = useState([]);

  // 결제취소 목록
  const getList = async () => {
    setList(null);
    const getUrl = axiosUrlFunction("paymentMerchant", "검색");

    axios
      .get(`${getUrl.apiUrl}?payment_id=${id}`, {
        headers: {
          Authorization: `Bearer ${getUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        setList(res.data.data);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getList();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  useEffect(() => {
    getList();
  }, []);
  return (
    <Modal show={true} onHide={onclose} size="lg">
      <Modal.Header closeButton>
        <Modal.Title>
          <b>결제취소 항목 선택</b>
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <div className="table-scroll2">
          <Table className="table-hover mt-0 table-scroll2__table">
            <thead>
              <tr>
                <th>NO</th>
                <th>결제분류</th>
                <th>결제내용</th>
                <th>정상가</th>
                <th>실 결제금액</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {list !== null && list !== undefined ? (
                list.map((value, i) => {
                  let type = "";
                  if (value.type === "program") type = "프로그램";
                  else if (value.type === "prepaid") type = "선불권";
                  else type = "제품";
                  return (
                    <tr key={i}>
                      <td>{i + 1}</td>
                      <td>{type}</td>
                      <td>{value.explanation}</td>
                      <td>{value.payment_price}</td>
                      <td>{value.payment_amount}</td>
                      <td>
                        <Button
                          onClick={() =>
                            window.location.replace(
                              `${user.accessPath}/statistics/cancel/${value.merchant_id}`
                            )
                          }
                        >
                          선택
                        </Button>
                      </td>
                    </tr>
                  );
                })
              ) : (
                <tr>
                  <td colSpan={6} style={{ textAlign: "center" }}>
                    <Loading />
                  </td>
                </tr>
              )}
            </tbody>
          </Table>
        </div>
      </Modal.Body>

      <Modal.Footer></Modal.Footer>
    </Modal>
  );
};
