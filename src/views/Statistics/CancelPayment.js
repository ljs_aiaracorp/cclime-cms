import { useEffect, useState } from "react";
import { Container, Row, Col, Card, Button } from "react-bootstrap";
import "../css/paymentTable/payTable.scss";
import "../css/menu/tableInput.scss";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";

export const CancelPayment = (props) => {
  const [list, setList] = useState({
    title: "",
    userNumber: "",
    phone: "",
    description: "",
    price: "",
    refund: "",
    retentionCount: "",
    retentionTotal: "",
    amount: "",
  });
  const [refundAmount, setrefundAmount] = useState(0);
  const [errMessage, setErrMessage] = useState("");

  // 결제 주문서
  const getList = async () => {
    const getUrl = axiosUrlFunction("paymentMerchant/user", "검색");
    axios
      .get(`${getUrl.apiUrl}?merchant_id=${props.match.params.id}`, {
        headers: {
          Authorization: `Bearer ${getUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        setList({
          title: res.data.data.user_name,
          userNumber: res.data.data.user_id,
          phone: res.data.data.phone,
          description: res.data.data.explanation,
          price: res.data.data.payment_price,
          refund: res.data.data.merchant_refund,
          retentionCount:
            res.data.data.retention_count === null
              ? "0"
              : res.data.data.retention_count,
          retentionTotal:
            res.data.data.retention_total === null
              ? "0"
              : res.data.data.retention_total,
          amount: res.data.data.payment_amount,
        });
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getList();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 결제 환불
  let sw = 0;
  const sendData = async () => {
    if (sw === 1) return;

    setErrMessage("");
    const sendUrl = axiosUrlFunction("refund", "검색");
    const amount = refundAmount === "" ? 0 : refundAmount;

    if (list.amount - list.refund === 0) {
      setErrMessage("이미 환불받은 결제내역 입니다.");
      return;
    }
    if (amount > list.amount - list.refund) {
      setErrMessage("환불금액을 초과했습니다. 다시 입력해 주세요.");
      return;
    }
    const data = {
      merchant_id: parseInt(props.match.params.id),
      refund_price: amount,
    };
    sw = 1;
    axios
      .put(`${sendUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${sendUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then((res) => {
        sw = 0;
        alert("결제취소가 완료되었습니다.");
        props.history.push(`${sendUrl.accessPath}/statistics/all`);
      })
      .catch((err) => {
        sw = 0;
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else sendData();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };
  useEffect(() => {
    getList();
  }, []);
  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">
                앱 매출통계 - 전체 매출 - 결제 취소
              </Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0 tickets-table">
              <div className="cancel-Payment">
                <div className="title-background">결제 고객 정보</div>
                <div className="cancel-Payment__column">
                  <div className="mb-4">
                    <span className="span-costom">고객이름</span>
                    <input
                      className="tickets-table__lg"
                      type="text"
                      readOnly={true}
                      defaultValue={list.title}
                    />
                  </div>
                  <div className="mb-4">
                    <span className="span-costom">고객번호</span>
                    <input
                      className="tickets-table__lg"
                      type="text"
                      readOnly={true}
                      defaultValue={list.userNumber}
                    />
                  </div>
                  <div className="mb-4">
                    <span className="span-costom">전화번호</span>
                    <input
                      className="tickets-table__lg"
                      type="text"
                      readOnly={true}
                      defaultValue={list.phone}
                    />
                  </div>
                </div>
                <div className="title-background">결제 정보</div>
                <div className="cancel-Payment__column">
                  <div className="mb-4">
                    <span className="span-costom">결제내용</span>
                    <input
                      className="tickets-table__lg"
                      type="text"
                      readOnly={true}
                      defaultValue={list.description}
                    />
                  </div>
                  <div className="mb-4">
                    <span className="span-costom">정산가</span>
                    <input
                      className="tickets-table__lg mr-4"
                      type="text"
                      readOnly={true}
                      defaultValue={list.price}
                    />
                    <span className="span-costom">환불 받은 금액</span>
                    <input
                      className="tickets-table__lg"
                      type="text"
                      readOnly={true}
                      defaultValue={list.refund}
                    />
                  </div>
                  <div className="mb-4">
                    <span className="span-costom">사용회수</span>
                    <input
                      className="tickets-table__sm"
                      type="text"
                      readOnly={true}
                      defaultValue={list.retentionCount}
                    />{" "}
                    &#47;&nbsp;
                    <input
                      className="tickets-table__sm"
                      type="text"
                      readOnly={true}
                      defaultValue={list.retentionTotal}
                    />
                  </div>
                  <div className="mb-4">
                    <span className="span-costom">실 결제금액</span>
                    <input
                      className="tickets-table__lg"
                      type="text"
                      readOnly={true}
                      defaultValue={list.amount}
                    />
                  </div>
                </div>
                <div className="title-background">환불 정보</div>
                <div className="cancel-Payment__column">
                  <span className="span-costom">환불금액</span>
                  <input
                    className="tickets-table__lg input-appearance"
                    type="number"
                    value={refundAmount}
                    onChange={(e) => {
                      const val =
                        0 > parseInt(e.target.value)
                          ? 0
                          : parseInt(e.target.value);
                      setrefundAmount(val);
                    }}
                  />
                  <span> 원</span>
                </div>
                <div className="cancel-Payment__column">
                  <p>
                    ※ 환불금액 기입 후 결제취소 시, 해당 금액만큼 환불이
                    진행되며 고객의 앱에 결제상태가 결제취소로 표시됩니다.
                  </p>
                </div>
              </div>

              <div className="text-center mt-5">
                {errMessage && (
                  <div className="mb-2" style={{ color: "red" }}>
                    {errMessage}
                  </div>
                )}
                <Button onClick={sendData}>환불</Button>
                <Button
                  className="ml-2"
                  onClick={() => {
                    props.history.push("/admin/statistics/all");
                  }}
                >
                  취소
                </Button>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
