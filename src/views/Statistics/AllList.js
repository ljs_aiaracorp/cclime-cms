import { useEffect, useState } from "react";
import uuid from "react-uuid";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import "../css/paymentTable/payTable.scss";
import Loading from "components/Loading";
import moment from "moment";
import { SelectProgramPopup } from "../Users/SelectProgramPopup";
import { CancelPopup } from "./CancelPopup";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";
import xlsx from "xlsx";

export const AllList = (props) => {
  const [selectedTab, setSelectedTab] = useState(0);

  const [selectedYear, setSelectedYear] = useState(
    Number(moment().format("yyyy"))
  );
  const [selectedMonth, setSelectedMonth] = useState(
    Number(moment().format("MM"))
  );
  const [selectedYear2, setSelectedYear2] = useState(
    Number(moment().format("yyyy"))
  );
  const [calendarMonth, setCalendarMonth] = useState("");
  const [lastDay, setLastDay] = useState(null);

  const [list, setList] = useState(null);
  const [listMonthValue, setListMonthValue] = useState([[]]);
  const [listYearValue, setListYearValue] = useState([[]]);
  const [prices, setPrices] = useState([[]]);
  const [yearPrices, setYearPrices] = useState([[]]);
  const [total, setTotal] = useState(0); // 기간별 총 금액
  const [totalMonth, setTotalMonth] = useState(0); // 월별 총액
  const [totalYear, setTotalYear] = useState(0); // 연별 총액
  const [startDay, setStartDay] = useState(moment().format("YYYY-MM-DD")); // 시작기간
  const [endDay, setEndDay] = useState(moment().format("YYYY-MM-DD")); // 종료기간
  const [payContent, setPayContent] = useState(""); // 결제내용
  const [userName, setUserName] = useState(""); // 고객이름
  const [orderBy, setOrderBy] = useState("created_at_desc"); // 필터
  const [programList, setProgramList] = useState([]);
  const [programList2, setProgramList2] = useState([]);
  const [showSelectProgramPopup, setShowSelectProgramPopup] = useState(false);
  const [showSelectProgramPopup2, setShowSelectProgramPopup2] = useState(false);
  const [isCancelPopup, setIsCancelPopup] = useState({
    is: false,
    id: "",
  }); // 결제취소 팝업

  useEffect(() => {
    if (selectedTab === 0) {
      getList();
    } else if (selectedTab === 1) {
      getMonthList();
    } else if (selectedTab === 2) {
      getYearList();
    }
  }, [selectedMonth, selectedYear, selectedYear2]);

  // 기간별 매출내역
  const getList = async (params) => {
    setList(null);
    if (!params) {
      params = {};
    }

    const getUrl = axiosUrlFunction("period/sales", "검색");
    const start = startDay.replace(/\-/g, "");
    const end = endDay.replace(/\-/g, "");

    const queries = [];

    if (params.filters === undefined) {
      params.filters = orderBy;
    }
    // 기간
    if (start !== "") {
      queries.push(`start=${start}`);
      queries.push(`end=${end}`);
    }
    // 필터
    queries.push(`order_by=${params.filters}`);
    // 결제내용
    if (payContent !== "") queries.push(`explanation=${payContent}`);
    // 고객이름
    if (userName !== "") queries.push(`user_name=${userName}`);

    const queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";

    await axios
      .get(`${getUrl.apiUrl}${queryStr}`, {
        headers: {
          Authorization: `Bearer ${getUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        if (res.data.data[0]) setTotal(res.data.data[0].total_amount);
        setList(res.data.data);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getList(params);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else if (err.response.status === 500 || err.response.status === 400) {
          alert("기간을 선택해 주세요.");
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 월 매출내역
  const getMonthList = async (params) => {
    setListMonthValue(null);
    if (!params) {
      params = {};
    }

    if (params.programs === undefined) {
      params.programs = programList;
    }

    const monthUrl = axiosUrlFunction("month/sales", "검색");
    const month = selectedMonth < 10 ? "0" + selectedMonth : selectedMonth;
    let queries = "";
    let programs = "";
    if (params.programs.length !== 0) {
      programs = params.programs.join(",");
    }
    if (programs !== "") {
      queries = `&names=${encodeURIComponent(programs)}`;
    }
    await axios
      .get(`${monthUrl.apiUrl}?ym=${selectedYear}${month}${queries}`, {
        headers: {
          Authorization: `Bearer ${monthUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        // 할인 전 · 된 금액
        const price = new Array(4);
        for (let i = 0; i < price.length; i++) {
          price[i] = new Array(
            new Date(selectedYear, selectedMonth, 0).getDate() + 1
          ).fill(0);
        }
        price[0][0] = "할인전 금액";
        price[1][0] = "할인된 금액";
        price[2][0] = "환불된 금액";
        price[3][0] = "실제 일별 결제총액";

        // 일별 매출
        const listname = new Array(res.data.data[0].total_count);
        listname[0] = new Array(
          new Date(selectedYear, selectedMonth, 0).getDate() + 1
        );
        listname[0].fill(0);
        listname[0][0] = res.data.data[0].name;

        let j = 1;
        for (let i = 0; i < res.data.data.length; i++) {
          if (listname[j - 1][0] !== res.data.data[i].name) {
            listname[j] = new Array(
              new Date(selectedYear, selectedMonth, 0).getDate() + 1
            );
            listname[j].fill(0);
            listname[j][0] = res.data.data[i].name;
            j++;
          }
        }

        // 일별 매출
        for (let i = 0; i < listname.length; i++) {
          for (let j = 0; j < res.data.data.length; j++) {
            if (listname[i] !== undefined) {
              if (listname[i][0] === res.data.data[j].name) {
                if (res.data.data[j].created_at !== null) {
                  if (res.data.data[j].created_at.substring(8, 9) === "0") {
                    listname[i][res.data.data[j].created_at.substring(9, 10)] +=
                      res.data.data[j].total_amount;
                    price[3][res.data.data[j].created_at.substring(9, 10)] +=
                      res.data.data[j].total_amount;
                  } else {
                    listname[i][res.data.data[j].created_at.substring(8, 10)] +=
                      res.data.data[j].total_amount;
                    price[3][res.data.data[j].created_at.substring(8, 10)] +=
                      res.data.data[j].total_amount;
                  }
                }
              }
              // 매출 통계
              if (res.data.data[j].created_at !== null) {
                if (i === 0) {
                  if (res.data.data[j].created_at.substring(8, 9) === "0") {
                    price[i][res.data.data[j].created_at.substring(9, 10)] +=
                      res.data.data[j].price;
                  } else {
                    price[i][res.data.data[j].created_at.substring(8, 10)] +=
                      res.data.data[j].price;
                  }
                } else if (i === 1) {
                  if (res.data.data[j].created_at.substring(8, 9) === "0") {
                    price[i][res.data.data[j].created_at.substring(9, 10)] +=
                      res.data.data[j].discount_price;
                  } else {
                    price[i][res.data.data[j].created_at.substring(8, 10)] +=
                      res.data.data[j].discount_price;
                  }
                } else if (i === 2) {
                  if (res.data.data[j].created_at.substring(8, 9) === "0") {
                    price[i][res.data.data[j].created_at.substring(9, 10)] +=
                      res.data.data[j].refund_price;
                  } else {
                    price[i][res.data.data[j].created_at.substring(8, 10)] +=
                      res.data.data[j].refund_price;
                  }
                }
              }
            }
          }
        }
        setPrices(price);
        setListMonthValue(listname);
        setLastDay(new Date(selectedYear, selectedMonth, 0).getDate());
        setCalendarMonth(selectedMonth);
        setTotalMonth(res.data.data[0].month_total);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getMonthList(params);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else if (err.response.status === 500 || err.response.status === 400) {
          alert("기간을 선택해 주세요.");
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 메인 출력
  const tableData = (list) => {
    const result = [];
    for (let i = 1; i < list.length; i++) {
      result.push(<td key={uuid()}>{list[i].toLocaleString("ko-KR")}</td>);
    }

    return result;
  };

  // 매출 총액 출력
  const price = (list) => {
    const result = [];
    for (let i = 1; i < list.length; i++) {
      result.push(<td key={uuid()}>{list[i].toLocaleString("ko-KR")}</td>);
    }
    return result;
  };

  // selected 1 date
  const days = () => {
    const result = [];
    for (let i = 1; i <= lastDay; i++) {
      let month = "";
      if (parseInt(calendarMonth) < 10) {
        month = "0" + calendarMonth;
      } else {
        month = calendarMonth;
      }
      if (i < 10) {
        result.push(
          <th key={i}>
            {month}-0{i}
          </th>
        );
      } else {
        result.push(
          <th key={i}>
            {month}-{i}
          </th>
        );
      }
    }
    return result;
  };

  // 연 매출내역
  const getYearList = async (params) => {
    setListYearValue(null);

    if (!params) {
      params = {};
    }

    if (params.programs === undefined) {
      params.programs = programList2;
    }

    const yearUrl = axiosUrlFunction("year/sales", "검색");
    const queries = [];
    let programs = "";
    if (params.programs.length !== 0) {
      programs = params.programs.join(",");
    }
    if (programs !== "") {
      queries.push(`names=${programs}`);
    }
    queries.push(`year=${selectedYear2}`);
    const queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";

    axios
      .get(`${yearUrl.apiUrl}${queryStr}`, {
        headers: {
          Authorization: `Bearer ${yearUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        // 할인 전 · 된 금액
        const price = new Array(4);
        for (let i = 0; i < price.length; i++) {
          price[i] = new Array(13).fill(0);
        }
        price[0][0] = "할인전 금액";
        price[1][0] = "할인된 금액";
        price[2][0] = "환불된 금액";
        price[3][0] = "실제 월별 결제총액";

        // 일별 매출
        const listname = new Array(res.data.data[0].total_count);
        listname[0] = new Array(13);
        listname[0].fill(0);
        listname[0][0] = res.data.data[0].name;

        let j = 1;
        for (let i = 0; i < res.data.data.length; i++) {
          if (listname[j - 1][0] !== res.data.data[i].name) {
            listname[j] = new Array(13);
            listname[j].fill(0);
            listname[j][0] = res.data.data[i].name;
            j++;
          }
        }

        // 연별 매출
        for (let i = 0; i < listname.length; i++) {
          for (let j = 0; j < res.data.data.length; j++) {
            if (listname[i] !== undefined) {
              if (listname[i][0] === res.data.data[j].name) {
                if (res.data.data[j].created_at !== null) {
                  if (res.data.data[j].created_at.substring(5, 6) === "0") {
                    listname[i][res.data.data[j].created_at.substring(6, 7)] +=
                      res.data.data[j].total_amount;
                    price[3][res.data.data[j].created_at.substring(6, 7)] +=
                      res.data.data[j].total_amount;
                  } else {
                    listname[i][res.data.data[j].created_at.substring(5, 7)] +=
                      res.data.data[j].total_amount;
                    price[3][res.data.data[j].created_at.substring(5, 7)] +=
                      res.data.data[j].total_amount;
                  }
                }
              }
              // 매출 통계
              if (res.data.data[j].created_at !== null) {
                if (i === 0) {
                  if (res.data.data[j].created_at.substring(5, 6) === "0") {
                    price[i][res.data.data[j].created_at.substring(6, 7)] +=
                      res.data.data[j].price;
                  } else {
                    price[i][res.data.data[j].created_at.substring(5, 7)] +=
                      res.data.data[j].price;
                  }
                } else if (i === 1) {
                  if (res.data.data[j].created_at.substring(5, 6) === "0") {
                    price[i][res.data.data[j].created_at.substring(6, 7)] +=
                      res.data.data[j].discount_price;
                  } else {
                    price[i][res.data.data[j].created_at.substring(5, 7)] +=
                      res.data.data[j].discount_price;
                  }
                } else if (i === 2) {
                  if (res.data.data[j].created_at.substring(5, 6) === "0") {
                    price[i][res.data.data[j].created_at.substring(6, 7)] +=
                      res.data.data[j].refund_price;
                  } else {
                    price[i][res.data.data[j].created_at.substring(5, 7)] +=
                      res.data.data[j].refund_price;
                  }
                }
              }
            }
          }
        }

        setYearPrices(price);
        setListYearValue(listname);
        setTotalYear(res.data.data[0].year_total);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getYearList(params);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else if (err.response.status === 500 || err.response.status === 400) {
          alert("기간을 선택해 주세요.");
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 프로그램/선불권 선택 리스트
  const programCheckedList = (list) => {
    if (selectedTab === 1) {
      setProgramList([]);
      const programs = [];
      list.map((value) => {
        programs.push(value);
      });
      setProgramList(programs);
      getMonthList({
        programs,
      });
      setShowSelectProgramPopup(false);
    } else if (selectedTab === 2) {
      setProgramList2([]);
      const programs = [];
      list.map((value2) => {
        programs.push(value2);
      });
      setProgramList2(programs);
      getYearList({
        programs,
      });
      setShowSelectProgramPopup2(false);
    }
  };

  // 달력 이벤트 ( 월 매출 )
  const calendarEventHandler = (direction) => {
    if (direction === "left") {
      if (selectedMonth > 1) setSelectedMonth(selectedMonth - 1);
      else {
        setSelectedMonth(12);
        setSelectedYear(selectedYear - 1);
      }
    } else {
      if (selectedMonth < 12) setSelectedMonth(selectedMonth + 1);
      else {
        setSelectedMonth(1);
        setSelectedYear(selectedYear + 1);
      }
    }
  };

  // 달력 이벤트 ( 연 매출 )
  const yearEventHandler = (direction) => {
    if (direction === "left") setSelectedYear2(selectedYear2 - 1);
    else setSelectedYear2(selectedYear2 + 1);
  };

  // 테이블 엑셀 변환 ( 기간별 결제내역 )
  const tableToxlsx = (id, maxColumn, xlsxTitle) => {
    const table = document.getElementById(id);

    const wscols = [];

    for (let i = 0; i < maxColumn; i++) {
      wscols.push({ wch: 20 });
    }

    const wb = xlsx.utils.book_new();
    const ws = xlsx.utils.table_to_sheet(table, {
      dateNF: id === "tableTab0" ? "yyyy-mm-dd hh:mm:ss;@" : "mm-dd;@",
      cellDates: true,
    });
    ws["!cols"] = wscols;
    xlsx.utils.book_append_sheet(wb, ws, "Sheet1");
    xlsx.writeFile(
      wb,
      `${moment().format("YYYY_MM_DD").toString()} ${xlsxTitle}.xlsx`
    );
  };

  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">앱 매출통계 - 전체 매출</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <div className="row">
                <div
                  className={`col-2 tab-btn ${
                    selectedTab === 0 ? "selected-tab" : ""
                  }`}
                  onClick={(e) => setSelectedTab(0)}
                >
                  기간별 결제내역
                </div>
                <div
                  className={`col-2 tab-btn ${
                    selectedTab === 1 ? "selected-tab" : ""
                  }`}
                  onClick={(e) => {
                    setSelectedTab(1);
                    getMonthList();
                  }}
                >
                  월 결제내역
                </div>
                <div
                  className={`col-2 tab-btn ${
                    selectedTab === 2 ? "selected-tab" : ""
                  }`}
                  onClick={(e) => {
                    setSelectedTab(2);
                    getYearList();
                  }}
                >
                  연 결제내역
                </div>
              </div>
              {selectedTab === 0 && (
                <>
                  <div className="title-background">검색</div>
                  <Table className="table-hover search-box">
                    <tbody>
                      <tr>
                        <th
                          className="title text-center"
                          style={{ width: "5%" }}
                        >
                          기간
                        </th>
                        <th
                          className="select-search-target"
                          style={{ width: "40%" }}
                        >
                          <Form.Group as={Row} className="mb-0">
                            <Col sm={5} className="pr-0">
                              <Form.Control
                                className="mb-0"
                                type="date"
                                placeholder="시작 날짜"
                                value={startDay}
                                onChange={(e) => setStartDay(e.target.value)}
                              />
                            </Col>
                            <Col
                              sm={1}
                              style={{
                                fontSize: "24px",
                                textAlign: "center",
                              }}
                            >
                              ~
                            </Col>
                            <Col sm={5} className="pl-0">
                              <Form.Control
                                className="mb-0"
                                type="date"
                                placeholder="종료 날짜"
                                value={endDay}
                                onChange={(e) => setEndDay(e.target.value)}
                              />
                            </Col>
                          </Form.Group>
                        </th>
                        <th
                          className="title text-center"
                          style={{ width: "7.5%" }}
                        >
                          결제내용
                        </th>
                        <th
                          className="select-search-target"
                          style={{ width: "22.5%" }}
                        >
                          <Form.Control
                            style={{ width: "100%" }}
                            type="text"
                            maxLength={50}
                            value={payContent}
                            onChange={(e) => setPayContent(e.target.value)}
                            onKeyUp={(e) => {
                              if (e.keyCode === 13) {
                                getList();
                              }
                            }}
                          />
                        </th>
                        <th
                          className="title text-center"
                          style={{ width: "7.5%" }}
                        >
                          고객이름
                        </th>
                        <th
                          className="select-search-target"
                          style={{ width: "22.5%" }}
                        >
                          <Form.Control
                            style={{ width: "100%" }}
                            type="text"
                            maxLength={50}
                            value={userName}
                            onChange={(e) => setUserName(e.target.value)}
                            onKeyUp={(e) => {
                              if (e.keyCode === 13) {
                                getList();
                              }
                            }}
                          />
                        </th>
                      </tr>
                    </tbody>
                  </Table>
                  <div className="text-center">
                    <Button
                      onClick={() => {
                        getList();
                      }}
                    >
                      검색
                    </Button>
                    <Button
                      onClick={() => {
                        if (!window.confirm("초기화 하시겠습니까?")) return;
                        setList([]);
                        setStartDay("");
                        setEndDay("");
                        setPayContent("");
                        setUserName("");
                        setTotal(0);
                      }}
                      className="ml-3 cancel"
                    >
                      초기화
                    </Button>
                  </div>
                  <div className="title-background">매출내역</div>
                  <div className="row">
                    <div className="col-3">
                      필터{" "}
                      <select
                        className="payment-select"
                        onChange={(e) => {
                          getList({
                            filters: e.target.value,
                          });
                        }}
                      >
                        <option value="created_at_desc">
                          결제일시: 최신 순
                        </option>
                        <option value="created_at_asc">
                          결제일시: 오래된 순
                        </option>
                        <option value="">----------------------------</option>
                        <option value="amount_asc">결제금액: 낮은 순</option>
                        <option value="amount_desc">결제금액: 높은 순</option>
                      </select>
                    </div>
                    <div className="col-6">
                      기간별 결제총액{" "}
                      <input
                        readOnly={true}
                        style={{ textAlign: "right", color: "red" }}
                        value={total.toLocaleString("ko-KR")}
                      />{" "}
                      원
                    </div>
                    <div
                      className="col-3"
                      style={{
                        display: "flex",
                        justifyContent: "flex-end",
                        padding: "0px",
                      }}
                    >
                      <Button
                        onClick={(e) => {
                          tableToxlsx("tableTab0", 11, "기간별 결제총액");
                        }}
                      >
                        인쇄
                      </Button>
                    </div>
                  </div>

                  <div className="payment-div">
                    <table className="payment-div__table" id="tableTab0">
                      <table style={{ display: "none" }}>
                        <thead>
                          <tr>
                            <th>기간별 결제총액</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>{total.toLocaleString("ko-KR")}</td>
                          </tr>
                        </tbody>
                      </table>
                      <thead>
                        <tr>
                          <th>고객이름</th>
                          <th>고객번호</th>
                          <th>전화번호</th>
                          <th>결제일시</th>
                          <th>메뉴</th>
                          <th>정상가</th>
                          <th>할인금액</th>
                          <th>환불금액</th>
                          <th>실제 결제금액</th>
                          <th>결제취소</th>
                        </tr>
                      </thead>
                      <tbody>
                        {list !== undefined && list !== null ? (
                          list.map((user, i) => {
                            const refund_amount =
                              user.refund_amount === null
                                ? 0
                                : user.refund_amount.toLocaleString("ko-KR");
                            return (
                              <tr key={i} className="cursor">
                                <td>{user.user_name}</td>
                                <td>{user.user_id}</td>
                                <td>{user.phone}</td>
                                <td>
                                  {moment(user.created_at).format(
                                    "YYYY-MM-DD HH:mm:ss"
                                  )}
                                </td>
                                <td>{user.payment_explanation}</td>
                                <td>
                                  {user.payment_price.toLocaleString("ko-KR")}
                                </td>
                                <td>
                                  {user.discount_price.toLocaleString("ko-KR")}
                                </td>
                                <td>{refund_amount}</td>
                                <td>
                                  {user.payment_amount.toLocaleString("ko-KR")}
                                </td>
                                <td>
                                  <Button
                                    onClick={() =>
                                      setIsCancelPopup({
                                        is: true,
                                        id: user.payment_id,
                                      })
                                    }
                                  >
                                    취소
                                  </Button>
                                </td>
                              </tr>
                            );
                          })
                        ) : (
                          <tr>
                            <td colSpan={10} style={{ textAlign: "center" }}>
                              <Loading />
                            </td>
                          </tr>
                        )}
                      </tbody>
                    </table>
                  </div>
                </>
              )}
              {selectedTab === 1 && (
                <>
                  <div className="title-background">검색</div>
                  <div className="row">
                    <div
                      className="col-5 text-right cursor"
                      onClick={() => calendarEventHandler("left")}
                    >
                      &lt;
                    </div>
                    <div
                      className="col-2 text-center"
                      style={{
                        fontSize: "30px",
                      }}
                    >
                      {selectedYear}
                      <br />
                      {selectedMonth}월
                    </div>
                    <div
                      className="col-5 text-left cursor"
                      onClick={() => calendarEventHandler("right")}
                    >
                      &gt;
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 text-center">
                      <Button
                        onClick={() => {
                          getMonthList();
                        }}
                      >
                        변경
                      </Button>
                    </div>
                  </div>
                  <div className="title-background">매출내역</div>
                  <div className="row">
                    <div className="col-3">
                      프로그램 선택{" "}
                      <Button onClick={(e) => setShowSelectProgramPopup(true)}>
                        선택
                      </Button>
                    </div>
                    <div className="col-6">
                      월 결제총액{" "}
                      <input
                        readOnly={true}
                        style={{ textAlign: "right", color: "red" }}
                        value={totalMonth.toLocaleString("ko-KR")}
                      />{" "}
                      원
                    </div>
                    <div
                      className="col-3"
                      style={{
                        display: "flex",
                        justifyContent: "flex-end",
                        padding: "0px",
                      }}
                    >
                      <Button
                        onClick={(e) => {
                          tableToxlsx("tableTab1", lastDay + 1, "월 결제총액");
                        }}
                      >
                        인쇄
                      </Button>
                    </div>
                  </div>
                  <div className="payment-div">
                    <table
                      className="payment-div__table payment-div__table__th"
                      id="tableTab1"
                    >
                      <table style={{ display: "none" }}>
                        <thead>
                          <tr>
                            <th>월 결제총액</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>{totalMonth.toLocaleString("ko-KR")}</td>
                          </tr>
                        </tbody>
                      </table>
                      <thead>
                        <tr>
                          <th>프로그램/ 선불권 명</th>
                          {days()}
                        </tr>
                      </thead>
                      <tbody>
                        {listMonthValue !== undefined &&
                        listMonthValue !== null ? (
                          listMonthValue.map((list, i) => {
                            return (
                              <tr key={uuid()} className="cursor">
                                <td>{list[0]}</td>
                                {tableData(list)}
                              </tr>
                            );
                          })
                        ) : (
                          <tr>
                            <td colSpan={13} style={{ textAlign: "center" }}>
                              <Loading />
                            </td>
                          </tr>
                        )}
                      </tbody>
                      <tfoot>
                        {prices !== undefined && prices !== null ? (
                          prices.map((list, i) => {
                            return (
                              <tr key={uuid()} className="cursor">
                                <td>{list[0]}</td>
                                {price(list)}
                              </tr>
                            );
                          })
                        ) : (
                          <tr>
                            <td colSpan={13} style={{ textAlign: "center" }}>
                              <Loading />
                            </td>
                          </tr>
                        )}
                      </tfoot>
                    </table>
                  </div>
                </>
              )}
              {selectedTab === 2 && (
                <>
                  <div className="title-background">검색</div>
                  <div className="row">
                    <div
                      className="col-5 text-right cursor"
                      onClick={() => yearEventHandler("left")}
                    >
                      &lt;
                    </div>
                    <div
                      className="col-2 text-center"
                      style={{
                        fontSize: "30px",
                      }}
                    >
                      {selectedYear2}년
                    </div>
                    <div
                      className="col-5 text-left cursor"
                      onClick={() => yearEventHandler("right")}
                    >
                      &gt;
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 text-center">
                      <Button onClick={() => getYearList()}>변경</Button>
                    </div>
                  </div>
                  <div className="title-background">매출내역</div>
                  <div className="row">
                    <div className="col-3">
                      프로그램 선택{" "}
                      <Button onClick={(e) => setShowSelectProgramPopup2(true)}>
                        선택
                      </Button>
                    </div>
                    <div className="col-6">
                      연 결제총액{" "}
                      <input
                        readOnly={true}
                        style={{ textAlign: "right", color: "red" }}
                        value={totalYear.toLocaleString("ko-KR")}
                      />{" "}
                      원
                    </div>
                    <div
                      className="col-3"
                      style={{
                        display: "flex",
                        justifyContent: "flex-end",
                        padding: "0px",
                      }}
                    >
                      <Button
                        onClick={(e) => {
                          tableToxlsx("tableTab2", 13, "연 결제총액");
                        }}
                      >
                        인쇄
                      </Button>
                    </div>
                  </div>
                  <div className="payment-div">
                    <table
                      className="payment-div__table mt-0 payment-div__table__th"
                      id="tableTab2"
                    >
                      <table style={{ display: "none" }}>
                        <thead>
                          <tr>
                            <th>연 결제총액</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>{totalYear.toLocaleString("ko-KR")}</td>
                          </tr>
                        </tbody>
                      </table>
                      <thead>
                        <tr>
                          <th>프로그램/선불권 명</th>
                          <th>1월</th>
                          <th>2월</th>
                          <th>3월</th>
                          <th>4월</th>
                          <th>5월</th>
                          <th>6월</th>
                          <th>7월</th>
                          <th>8월</th>
                          <th>9월</th>
                          <th>10월</th>
                          <th>11월</th>
                          <th>12월</th>
                        </tr>
                      </thead>
                      <tbody>
                        {listYearValue !== undefined &&
                        listYearValue !== null ? (
                          listYearValue.map((list, i) => {
                            return (
                              <tr key={uuid()} className="cursor">
                                <td>{list[0]}</td>
                                {tableData(list)}
                              </tr>
                            );
                          })
                        ) : (
                          <tr>
                            <td colSpan={13} style={{ textAlign: "center" }}>
                              <Loading />
                            </td>
                          </tr>
                        )}
                      </tbody>
                      <tfoot>
                        {yearPrices !== undefined && yearPrices !== null ? (
                          yearPrices.map((list, i) => {
                            return (
                              <tr key={uuid()} className="cursor">
                                <td>{list[0]}</td>
                                {price(list)}
                              </tr>
                            );
                          })
                        ) : (
                          <tr>
                            <td colSpan={13} style={{ textAlign: "center" }}>
                              <Loading />
                            </td>
                          </tr>
                        )}
                      </tfoot>
                    </table>
                  </div>
                </>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
      {showSelectProgramPopup && (
        <SelectProgramPopup
          onProgramListChecked={programList}
          onCompleteSelection={programCheckedList}
          onClickClose={(e) => {
            setShowSelectProgramPopup(false);
          }}
        />
      )}
      {showSelectProgramPopup2 && (
        <SelectProgramPopup
          onProgramListChecked={programList2}
          onCompleteSelection={programCheckedList}
          onClickClose={(e) => {
            setShowSelectProgramPopup2(false);
          }}
        />
      )}
      {isCancelPopup.is && (
        <CancelPopup
          onclose={() => {
            setIsCancelPopup({
              is: false,
              id: "",
            });
          }}
          id={isCancelPopup.id}
        />
      )}
    </Container>
  );
};
