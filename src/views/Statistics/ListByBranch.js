import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import uuid from "react-uuid";
import "../css/paymentTable/payTable.scss";
import Loading from "components/Loading";
import moment from "moment";
import { SelectProgramPopup } from "../Users/SelectProgramPopup";
import { StorePopup } from "../../components/Popup/StorePopup";
import { CancelPopup } from "./CancelPopup";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";
import xlsx from "xlsx";

export const ListByBranch = (props) => {
  const [selectedTab, setSelectedTab] = useState(0);

  const [selectedYear, setSelectedYear] = useState(
    Number(moment().format("yyyy"))
  );
  const [selectedMonth, setSelectedMonth] = useState(
    Number(moment().format("MM"))
  );
  const [selectedYear2, setSelectedYear2] = useState(
    Number(moment().format("yyyy"))
  );

  const [list, setList] = useState([]);
  const [startDay, setStartDay] = useState(moment().format("YYYY-MM-DD")); // 시작기간
  const [endDay, setEndDay] = useState(moment().format("YYYY-MM-DD")); // 종료기간
  const [payContent, setPayContent] = useState(""); // 결제내용
  const [userName, setUserName] = useState(""); // 고객이름
  const [orderBy, setOrderBy] = useState("created_at_desc"); // 필터
  const [total, setTotal] = useState(0); // 기간별 총 금액
  const [storeName, setStoreName] = useState({
    // selectedTab 0
    store_id: "",
    store_name: "",
  });

  const [listMonthValue, setListMonthValue] = useState([[]]);
  const [prices, setPrices] = useState([[]]);
  const [lastDay, setLastDay] = useState(null);
  const [calendarMonth, setCalendarMonth] = useState("");
  const [totalMonth, setTotalMonth] = useState(0); // 월별 총액
  const [programList, setProgramList] = useState([]);
  const [storeName2, setStoreName2] = useState({
    // selectedTab 1
    store_id: "",
    store_name: "",
  });

  const [listYearValue, setListYearValue] = useState(null);
  const [yearPrices, setYearPrices] = useState(null);
  const [totalYear, setTotalYear] = useState(0); // 연별 총액
  const [programList2, setProgramList2] = useState([]);
  const [storeName3, setStoreName3] = useState({
    // selectedTab 2
    store_id: "",
    store_name: "",
  });

  const [store, setStore] = useState([]); // 지점
  const [storeModal, setStoreModal] = useState(false); // 지점선택 모달
  const [showSelectProgramPopup, setShowSelectProgramPopup] = useState(false);
  const [showSelectProgramPopup2, setShowSelectProgramPopup2] = useState(false);
  const [isCancelPopup, setIsCancelPopup] = useState({
    is: false,
    id: "",
  }); // 결제취소 팝업
  const user = axiosUrlFunction("", "검색");
  const userCheck = user.accessPath === "/admin" ? true : false;
  const axiosconfig = {
    headers: {
      Authorization: `Bearer ${user.token}`,
      "Content-Type": `application/json`,
    },
  };

  useEffect(() => {
    if (userCheck) getStoreList();
    if (selectedTab === 1) {
      getMonthList();
    }
  }, [selectedYear, selectedMonth]);

  // Store Modal open & close
  const openStoreModal = () => {
    setStoreModal(true);
  };
  const closeStoreModal = () => {
    setStoreModal(false);
  };
  // 지점선택한 지점명 & 지점ID
  const setStoreData = (event) => {
    const { value } = event.target;
    const index = store.findIndex((i) => i.store_name === value);

    if (selectedTab === 0) {
      setStoreName({
        store_id: store[index].store_id,
        store_name: value,
      });
    } else if (selectedTab === 1) {
      setStoreName2({
        store_id: store[index].store_id,
        store_name: value,
      });
    } else if (selectedTab === 2) {
      setStoreName3({
        store_id: store[index].store_id,
        store_name: value,
      });
    }
    closeStoreModal();
  };
  // 지점 선택 목록
  const getStoreList = async () => {
    const storeUrl = axiosUrlFunction("storeList2", "검색");

    axios.get(`${storeUrl.apiUrl}`, axiosconfig).then(async (res) => {
      setStore(res.data.data);
    });
  };

  const getList = async (params) => {
    setList(null);
    if (!params) {
      params = {};
    }

    const getUrl = axiosUrlFunction("storeSalesList");
    const start = startDay.replace(/\-/g, "");
    const end = endDay.replace(/\-/g, "");

    if (userCheck) {
      if (storeName.store_name === "") {
        alert("지점을 선택해 주세요.");
        return;
      }
    }

    const queries = [];

    if (params.filters === undefined) {
      params.filters = orderBy;
    }

    // 기간
    if (start !== "") {
      queries.push(`start=${start}`);
      queries.push(`end=${end}`);
    }
    // 필터
    queries.push(`order_by=${params.filters}`);

    // 결제내용
    if (payContent !== "") queries.push(`explanation=${payContent}`);

    // 고객이름
    if (userName !== "") queries.push(`user_name=${userName}`);

    // 지점선택
    if (storeName.store_name !== "")
      queries.push(`store_id=${storeName.store_id}`);
    const queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";

    axios
      .get(`${getUrl.apiUrl}${queryStr}`, {
        headers: {
          Authorization: `Bearer ${getUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        if (res.data.data[0]) setTotal(res.data.data[0].total);
        setList(res.data.data);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getList(params);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else if (err.response.status === 500 || err.response.status === 400) {
          alert("기간을 선택해 주세요.");
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 월 매출내역
  const getMonthList = async (params) => {
    if (userCheck) {
      // admin 지점
      if (storeName2.store_name === "") {
        alert("지점을 선택해 주세요.");
        return;
      }
    }
    if (!params) {
      params = {};
    }

    if (params.programs === undefined) {
      params.programs = programList;
    }

    setListMonthValue(null);
    const monthUrl = axiosUrlFunction("storeSales/month");
    const month = selectedMonth < 10 ? "0" + selectedMonth : selectedMonth;
    let programs = "";

    const queries = [];
    if (params.programs.length !== 0) {
      programs = params.programs.join(",");
    }
    if (programs !== "") {
      queries.push(`names=${encodeURIComponent(programs)}`);
    }

    if (storeName2.store_name !== "") {
      // 지점선택
      queries.push(`store_id=${storeName2.store_id}`);
    }
    queries.push(`ym=${selectedYear}${month}`); // 선택 달력

    const queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";

    axios
      .get(`${monthUrl.apiUrl}${queryStr}`, {
        headers: {
          Authorization: `Bearer ${monthUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        // 할인 전 · 된 금액
        const price = new Array(1);
        price[0] = new Array(
          new Date(selectedYear, selectedMonth, 0).getDate() + 1
        ).fill(0);
        price[0][0] = "실제 일별 사용처리";

        // 일별 매출
        const listname = new Array(res.data.data[0].total_count);
        listname[0] = new Array(
          new Date(selectedYear, selectedMonth, 0).getDate() + 1
        );
        listname[0].fill(0);
        listname[0][0] = res.data.data[0].name;

        let j = 1;
        for (let i = 0; i < res.data.data.length; i++) {
          if (listname[j - 1][0] !== res.data.data[i].name) {
            listname[j] = new Array(
              new Date(selectedYear, selectedMonth, 0).getDate() + 1
            );
            listname[j].fill(0);
            listname[j][0] = res.data.data[i].name;
            j++;
          }
        }

        // 일별 매출
        for (let i = 0; i < listname.length; i++) {
          for (let j = 0; j < res.data.data.length; j++) {
            if (listname[i] !== undefined) {
              if (listname[i][0] === res.data.data[j].name) {
                if (res.data.data[j].created_at !== null) {
                  if (res.data.data[j].created_at.substring(8, 9) === "0") {
                    listname[i][res.data.data[j].created_at.substring(9, 10)] +=
                      res.data.data[j].total_amount;
                    price[0][res.data.data[j].created_at.substring(9, 10)] +=
                      res.data.data[j].total_amount;
                  } else {
                    listname[i][res.data.data[j].created_at.substring(8, 10)] +=
                      res.data.data[j].total_amount;
                    price[0][res.data.data[j].created_at.substring(8, 10)] +=
                      res.data.data[j].total_amount;
                  }
                }
              }
            }
          }
        }

        setPrices(price);
        setListMonthValue(listname);
        setLastDay(new Date(selectedYear, selectedMonth, 0).getDate());
        setCalendarMonth(selectedMonth);
        setTotalMonth(res.data.data[0].month_total);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getMonthList(params);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else if (err.response.status === 500 || err.response.status === 400) {
          alert("기간을 선택해 주세요.");
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 월 메인 출력
  const tableData = (list) => {
    const result = [];
    for (let i = 1; i < list.length; i++) {
      result.push(<td key={uuid()}>{list[i].toLocaleString("ko-KR")}</td>);
    }
    return result;
  };

  // 매출 총액 출력
  const price = (list) => {
    const result = [];

    for (let i = 1; i < list.length; i++) {
      result.push(<td key={uuid()}>{list[i].toLocaleString("ko-KR")}</td>);
    }
    return result;
  };

  // selected 1 date
  const days = () => {
    const result = [];
    for (let i = 1; i <= lastDay; i++) {
      let month = "";
      if (parseInt(calendarMonth) < 10) {
        month = "0" + calendarMonth;
      } else {
        month = calendarMonth;
      }
      if (i < 10) {
        result.push(
          <th key={i}>
            {month}-0{i}
          </th>
        );
      } else {
        result.push(
          <th key={i}>
            {month}-{i}
          </th>
        );
      }
    }
    return result;
  };

  // 연 매출내역
  const getYearList = async (params) => {
    setListYearValue(null);

    if (!params) {
      params = {};
    }

    const yearUrl = axiosUrlFunction("storeSales/year");
    let programs = "";

    if (userCheck) {
      // admin 지점
      if (storeName3.store_name === "") {
        alert("지점을 선택해 주세요.");
        return;
      }
    }

    if (params.programs === undefined) {
      params.programs = programList2;
    }

    const queries = [];
    if (params.programs.length !== 0) {
      programs = params.programs.join(",");
    }
    if (programs !== "") {
      queries.push(`names=${encodeURIComponent(programs)}`);
    }

    if (storeName3.store_name !== "") {
      // 지점선택
      queries.push(`store_id=${storeName3.store_id}`);
    }
    queries.push(`year=${selectedYear2}`); // 선택 달력

    const queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";

    axios
      .get(`${yearUrl.apiUrl}${queryStr}`, {
        headers: {
          Authorization: `Bearer ${yearUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        // 할인 전 · 된 금액
        const price = new Array(1);
        price[0] = new Array(13).fill(0);
        price[0][0] = "실제 월별 사용처리";

        // 연별 매출
        const listname = new Array(res.data.data[0].total_count);
        listname[0] = new Array(13);
        listname[0].fill(0);
        listname[0][0] = res.data.data[0].name;

        let j = 1;
        for (let i = 0; i < res.data.data.length; i++) {
          if (listname[j - 1][0] !== res.data.data[i].name) {
            listname[j] = new Array(13);
            listname[j].fill(0);
            listname[j][0] = res.data.data[i].name;
            j++;
          }
        }

        // 연별 매출
        for (let i = 0; i < listname.length; i++) {
          for (let j = 0; j < res.data.data.length; j++) {
            if (listname[i] !== undefined) {
              if (listname[i][0] === res.data.data[j].name) {
                if (res.data.data[j].created_at !== null) {
                  if (res.data.data[j].created_at.substring(5, 6) === "0") {
                    listname[i][res.data.data[j].created_at.substring(6, 7)] +=
                      res.data.data[j].total_amount;
                    price[0][res.data.data[j].created_at.substring(6, 7)] +=
                      res.data.data[j].total_amount;
                  } else {
                    listname[i][res.data.data[j].created_at.substring(5, 7)] +=
                      res.data.data[j].total_amount;
                    price[0][res.data.data[j].created_at.substring(5, 7)] +=
                      res.data.data[j].total_amount;
                  }
                }
              }
            }
          }
        }

        setYearPrices(price);
        setListYearValue(listname);
        setTotalYear(res.data.data[0].year_total);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getYearList(params);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else if (err.response.status === 500 || err.response.status === 400) {
          alert("기간을 선택해 주세요.");
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 프로그램/선불권 선택 리스트
  const programCheckedList = (list) => {
    if (selectedTab === 1) {
      setProgramList([]);
      const programs = [];
      list.map((value) => {
        programs.push(value);
      });
      setProgramList(programs);
      getMonthList({
        programs,
      });
      setShowSelectProgramPopup(false);
    } else if (selectedTab === 2) {
      setProgramList2([]);
      const programs = [];
      list.map((value2) => {
        programs.push(value2);
      });
      setProgramList2(programs);
      getYearList({
        programs,
      });
      setShowSelectProgramPopup2(false);
    }
  };

  // 달력 이벤트 ( 월 매출, 연매출 따로 관리 )
  const calendarEventHandler = (direction) => {
    if (direction === "left") {
      if (selectedMonth > 1) setSelectedMonth(selectedMonth - 1);
      else {
        setSelectedMonth(12);
        setSelectedYear(selectedYear - 1);
      }
    } else {
      if (selectedMonth < 12) setSelectedMonth(selectedMonth + 1);
      else {
        setSelectedMonth(1);
        setSelectedYear(selectedYear + 1);
      }
    }
  };

  // 달력 연 매출 이벤트
  const yearEventHandler = (direction) => {
    if (direction === "left") setSelectedYear2(selectedYear2 - 1);
    else setSelectedYear2(selectedYear2 + 1);
  };

  // 테이블 엑셀 변환 ( 기간별 결제내역 )
  const tableToxlsx = (id, maxColumn, xlsxTitle) => {
    const table = document.getElementById(id);

    const wscols = [];

    for (let i = 0; i < maxColumn; i++) {
      wscols.push({ wch: 20 });
    }

    const wb = xlsx.utils.book_new();
    const ws = xlsx.utils.table_to_sheet(table, {
      dateNF: id === "tableTab0" ? "yyyy-mm-dd hh:mm:ss;@" : "mm-dd;@",
      cellDates: true,
    });
    ws["!cols"] = wscols;
    xlsx.utils.book_append_sheet(wb, ws, "Sheet1");
    xlsx.writeFile(
      wb,
      `${moment().format("YYYY_MM_DD").toString()} ${xlsxTitle}.xlsx`
    );
  };

  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">앱 매출통계 - 지점 매출</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <div className="row">
                <div
                  className={`col-2 tab-btn ${
                    selectedTab === 0 ? "selected-tab" : ""
                  }`}
                  onClick={(e) => setSelectedTab(0)}
                >
                  기간별 사용처리 내역
                </div>
                <div
                  className={`col-2 tab-btn ${
                    selectedTab === 1 ? "selected-tab" : ""
                  }`}
                  onClick={(e) => setSelectedTab(1)}
                >
                  월 사용처리 내역
                </div>
                <div
                  className={`col-2 tab-btn ${
                    selectedTab === 2 ? "selected-tab" : ""
                  }`}
                  onClick={(e) => setSelectedTab(2)}
                >
                  연 사용처리 내역
                </div>
              </div>
              {selectedTab === 0 && (
                <>
                  <div className="title-background">검색</div>
                  <Table className="table-hover search-box">
                    <tbody>
                      <tr>
                        <th
                          className="title text-center"
                          style={{ width: "5%" }}
                        >
                          기간
                        </th>
                        <th
                          className="select-search-target"
                          style={{
                            width: "40%",
                          }}
                        >
                          <Form.Group as={Row} className="mb-0">
                            <Col sm={5} className="pr-0">
                              <Form.Control
                                className="mb-0"
                                type="date"
                                placeholder="시작 날짜"
                                value={startDay}
                                onChange={(e) => setStartDay(e.target.value)}
                              />
                            </Col>
                            <Col
                              sm={1}
                              style={{ fontSize: "24px", textAlign: "center" }}
                            >
                              ~
                            </Col>
                            <Col sm={5} className="pl-0">
                              <Form.Control
                                className="mb-0"
                                type="date"
                                placeholder="종료 날짜"
                                value={endDay}
                                onChange={(e) => setEndDay(e.target.value)}
                              />
                            </Col>
                          </Form.Group>
                        </th>
                        <th
                          className="title text-center"
                          style={{ width: "100px" }}
                        >
                          결제내용
                        </th>
                        <th className="select-search-target" colSpan={3}>
                          <Form.Control
                            type="text"
                            maxLength={50}
                            value={payContent}
                            onChange={(e) => setPayContent(e.target.value)}
                            onKeyUp={(e) => {
                              if (e.keyCode === 13) {
                                getList();
                              }
                            }}
                          />
                        </th>
                        <th
                          className="title text-center"
                          style={{ width: "100px" }}
                        >
                          고객이름
                        </th>
                        <th className="select-search-target" colSpan={3}>
                          <Form.Control
                            type="text"
                            maxLength={50}
                            value={userName}
                            onChange={(e) => setUserName(e.target.value)}
                            onKeyUp={(e) => {
                              if (e.keyCode === 13) {
                                getList();
                              }
                            }}
                          />
                        </th>
                      </tr>
                      <tr>
                        <th
                          className="title text-center"
                          style={{ width: "100px" }}
                        >
                          지점선택
                        </th>
                        {userCheck ? (
                          <>
                            <th className="select-search-target" colSpan={3}>
                              <Form.Control
                                type="text"
                                maxLength={50}
                                readOnly={true}
                                value={storeName.store_name}
                              />
                            </th>
                            <th></th>
                          </>
                        ) : (
                          <>
                            <th className="select-search-target" colSpan={3}>
                              <Form.Control
                                type="text"
                                maxLength={50}
                                readOnly={true}
                                value={user.store_name}
                              />
                            </th>
                            <th></th>
                          </>
                        )}
                        <th className="select-search-target" colSpan={3}>
                          {userCheck && (
                            <Button onClick={openStoreModal}>지점검색</Button>
                          )}
                        </th>
                      </tr>
                    </tbody>
                  </Table>
                  <div className="text-center">
                    <Button
                      onClick={() => {
                        getList();
                      }}
                    >
                      검색
                    </Button>
                    <Button
                      onClick={() => {
                        if (!window.confirm("초기화 하시겠습니까?")) return;
                        setList([]);
                        setStartDay(moment().format("YYYY-MM-DD"));
                        setEndDay(moment().format("YYYY-MM-DD"));
                        setPayContent("");
                        setUserName("");
                        setStoreName({
                          store_id: "",
                          store_name: "",
                        });
                        setTotal(0);
                      }}
                      className="ml-3 cancel"
                    >
                      초기화
                    </Button>
                  </div>
                  <div className="title-background">매출내역</div>
                  <div className="row mt-5">
                    <div className="col-3">
                      필터{" "}
                      <select
                        className="payment-select"
                        onChange={(e) => {
                          getList({
                            filters: e.target.value,
                          });
                        }}
                      >
                        <option value="created_at_desc">
                          결제일시: 최신 순
                        </option>
                        <option value="created_at_asc">
                          결제일시: 오래된 순
                        </option>
                        <option value="">----------------------------</option>
                        <option value="amount_asc">결제금액: 낮은 순</option>
                        <option value="amount_desc">결제금액: 높은 순</option>
                      </select>
                    </div>
                    <div className="col-6">
                      사용처리 총액{" "}
                      <input
                        readOnly={true}
                        style={{ textAlign: "right", color: "red" }}
                        value={total.toLocaleString("ko-KR")}
                      />{" "}
                      원
                    </div>
                    <div
                      className="col-3"
                      style={{
                        display: "flex",
                        justifyContent: "flex-end",
                        padding: "0px",
                      }}
                    >
                      <Button
                        onClick={(e) => {
                          tableToxlsx("tableTab0", 6, "기간별 사용처리 총액");
                        }}
                      >
                        인쇄
                      </Button>
                    </div>
                  </div>
                  <div className="payment-div">
                    <table className="payment-div__table" id="tableTab0">
                      <table style={{ display: "none" }}>
                        <thead>
                          <tr>
                            <th>사용처리 총액</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>{total.toLocaleString("ko-KR")}</td>
                          </tr>
                        </tbody>
                      </table>
                      <thead>
                        <tr>
                          <th>고객이름</th>
                          <th>고객번호</th>
                          <th>전화번호</th>
                          <th>사용일시</th>
                          <th>메뉴</th>
                          <th>실제 결제금액</th>
                        </tr>
                      </thead>
                      <tbody>
                        {list !== undefined && list !== null ? (
                          list.map((user, i) => {
                            return (
                              <tr key={i} className="cursor">
                                <td>{user.user_name}</td>
                                <td>{user.user_id}</td>
                                <td>{user.phone}</td>
                                <td>{user.sales_at.replace("T", " ")}</td>
                                <td>{user.explanation}</td>
                                <td>
                                  {user.store_sales_amount.toLocaleString(
                                    "ko-KR"
                                  )}
                                </td>
                              </tr>
                            );
                          })
                        ) : (
                          <tr>
                            <td colSpan={6} style={{ textAlign: "center" }}>
                              <Loading />
                            </td>
                          </tr>
                        )}
                      </tbody>
                    </table>
                  </div>
                </>
              )}
              {selectedTab === 1 && (
                <>
                  <div className="title-background">검색</div>
                  <div className="row">
                    <div className="col-4">
                      <Table className="table-hover search-box">
                        <tbody>
                          <tr>
                            <th
                              className="title text-center"
                              style={{ width: "100px" }}
                            >
                              지점선택
                            </th>
                            {userCheck ? (
                              <th className="select-search-target" colSpan={3}>
                                <Form.Control
                                  type="text"
                                  maxLength={50}
                                  readOnly={true}
                                  value={storeName2.store_name}
                                />
                              </th>
                            ) : (
                              <th className="select-search-target" colSpan={1}>
                                <Form.Control
                                  type="text"
                                  maxLength={50}
                                  readOnly={true}
                                  value={user.store_name}
                                />
                              </th>
                            )}
                            <th className="select-search-target" colSpan={3}>
                              {userCheck && (
                                <Button onClick={openStoreModal}>
                                  지점검색
                                </Button>
                              )}
                            </th>
                          </tr>
                        </tbody>
                      </Table>
                    </div>
                    <div
                      className="col-1 text-right cursor"
                      onClick={() => calendarEventHandler("left")}
                    >
                      &lt;
                    </div>
                    <div
                      className="col-2 text-center"
                      style={{
                        fontSize: "30px",
                      }}
                    >
                      {selectedYear}
                      <br />
                      {selectedMonth}월
                    </div>
                    <div
                      className="col-5 text-left cursor"
                      onClick={() => calendarEventHandler("right")}
                    >
                      &gt;
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 text-center">
                      <Button
                        onClick={() => {
                          getMonthList();
                        }}
                      >
                        변경
                      </Button>
                    </div>
                  </div>
                  <div className="title-background">매출내역</div>
                  <div className="row">
                    <div className="col-3">
                      프로그램 선택{" "}
                      <Button onClick={(e) => setShowSelectProgramPopup(true)}>
                        선택
                      </Button>
                    </div>
                    <div className="col-6">
                      월 사용처리 총액{" "}
                      <input
                        readOnly={true}
                        style={{ textAlign: "right", color: "red" }}
                        value={totalMonth.toLocaleString("ko-KR")}
                      />{" "}
                      원
                    </div>
                    <div
                      className="col-3"
                      style={{
                        display: "flex",
                        justifyContent: "flex-end",
                        padding: "0px",
                      }}
                    >
                      <Button
                        onClick={(e) => {
                          tableToxlsx(
                            "tableTab1",
                            lastDay + 1,
                            "월 사용처리 총액"
                          );
                        }}
                      >
                        인쇄
                      </Button>
                    </div>
                  </div>

                  <div className="payment-div">
                    <table
                      className="payment-div__table payment-div__table__th"
                      id="tableTab1"
                    >
                      <table style={{ display: "none" }}>
                        <thead>
                          <tr>
                            <th>월 사용처리 총액</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>{totalMonth.toLocaleString("ko-KR")}</td>
                          </tr>
                        </tbody>
                      </table>
                      <thead>
                        <tr>
                          <th>프로그램/선불권 명</th>
                          {days()}
                        </tr>
                      </thead>
                      <tbody>
                        {listMonthValue !== undefined &&
                        listMonthValue !== null ? (
                          listMonthValue.map((list, i) => {
                            return (
                              <tr key={uuid()} className="cursor">
                                <td>{list[0]}</td>
                                {tableData(list)}
                              </tr>
                            );
                          })
                        ) : (
                          <tr>
                            <td colSpan={13} style={{ textAlign: "center" }}>
                              <Loading />
                            </td>
                          </tr>
                        )}
                      </tbody>
                      <tfoot>
                        {prices !== undefined && prices !== null ? (
                          prices.map((list, i) => {
                            return (
                              <tr key={uuid()} className="cursor">
                                <td>{list[0]}</td>
                                {price(list)}
                              </tr>
                            );
                          })
                        ) : (
                          <tr>
                            <td colSpan={13} style={{ textAlign: "center" }}>
                              <Loading />
                            </td>
                          </tr>
                        )}
                      </tfoot>
                    </table>
                  </div>
                </>
              )}
              {selectedTab === 2 && (
                <>
                  <div className="title-background">검색</div>
                  <div className="row">
                    <div className="col-4">
                      <Table className="table-hover search-box">
                        <tbody>
                          <tr>
                            <th
                              className="title text-center"
                              style={{ width: "100px" }}
                            >
                              지점선택
                            </th>
                            {userCheck ? (
                              <th className="select-search-target" colSpan={3}>
                                <Form.Control
                                  type="text"
                                  maxLength={50}
                                  readOnly={true}
                                  value={storeName3.store_name}
                                />
                              </th>
                            ) : (
                              <th className="select-search-target" colSpan={1}>
                                <Form.Control
                                  type="text"
                                  maxLength={50}
                                  readOnly={true}
                                  value={user.store_name}
                                />
                              </th>
                            )}
                            <th className="select-search-target" colSpan={3}>
                              {userCheck && (
                                <Button onClick={openStoreModal}>
                                  지점검색
                                </Button>
                              )}
                            </th>
                          </tr>
                        </tbody>
                      </Table>
                    </div>
                    <div
                      className="col-1 text-right cursor"
                      onClick={() => yearEventHandler("left")}
                    >
                      &lt;
                    </div>
                    <div
                      className="col-2 text-center"
                      style={{
                        fontSize: "30px",
                      }}
                    >
                      {selectedYear2}년
                    </div>
                    <div
                      className="col-5 text-left cursor"
                      onClick={() => yearEventHandler("rigth")}
                    >
                      &gt;
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 text-center">
                      <Button onClick={getYearList}>변경</Button>
                    </div>
                  </div>
                  <div className="title-background">매출내역</div>
                  <div className="row">
                    <div className="col-3">
                      프로그램 선택{" "}
                      <Button onClick={(e) => setShowSelectProgramPopup2(true)}>
                        선택
                      </Button>
                    </div>
                    <div className="col-6">
                      연 사용처리 총액{" "}
                      <input
                        readOnly={true}
                        style={{ textAlign: "right", color: "red" }}
                        value={totalYear.toLocaleString("ko-KR")}
                      />{" "}
                      원
                    </div>
                    <div
                      className="col-3"
                      style={{
                        display: "flex",
                        justifyContent: "flex-end",
                        padding: "0px",
                      }}
                    >
                      <Button
                        onClick={(e) => {
                          tableToxlsx("tableTab2", 13, "연 사용처리 총액");
                        }}
                      >
                        인쇄
                      </Button>
                    </div>
                  </div>
                  <div className="payment-div">
                    <table
                      className="payment-div__table payment-div__table__th"
                      id="tableTab2"
                    >
                      <table style={{ display: "none" }}>
                        <thead>
                          <tr>
                            <th>연 사용처리 총액</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>{total.toLocaleString("ko-KR")}</td>
                          </tr>
                        </tbody>
                      </table>
                      <thead>
                        <tr>
                          <th>프로그램/선불권 명</th>
                          <th>1월</th>
                          <th>2월</th>
                          <th>3월</th>
                          <th>4월</th>
                          <th>5월</th>
                          <th>6월</th>
                          <th>7월</th>
                          <th>8월</th>
                          <th>9월</th>
                          <th>10월</th>
                          <th>11월</th>
                          <th>12월</th>
                        </tr>
                      </thead>
                      <tbody>
                        {listYearValue !== undefined &&
                        listYearValue !== null ? (
                          listYearValue.map((list, i) => {
                            return (
                              <tr key={uuid()} className="cursor">
                                <td>{list[0]}</td>
                                {tableData(list)}
                              </tr>
                            );
                          })
                        ) : (
                          <tr>
                            <td
                              colSpan={13}
                              style={{ textAlign: "center" }}
                            ></td>
                          </tr>
                        )}
                      </tbody>
                      <tfoot>
                        {yearPrices !== undefined && yearPrices !== null ? (
                          yearPrices.map((list, i) => {
                            return (
                              <tr key={uuid()} className="cursor">
                                <td>{list[0]}</td>
                                {price(list)}
                              </tr>
                            );
                          })
                        ) : (
                          <tr>
                            <td
                              colSpan={13}
                              style={{ textAlign: "center" }}
                            ></td>
                          </tr>
                        )}
                      </tfoot>
                    </table>
                  </div>
                </>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
      {showSelectProgramPopup && (
        <SelectProgramPopup
          onProgramListChecked={programList}
          onCompleteSelection={programCheckedList}
          onClickClose={(e) => {
            setShowSelectProgramPopup(false);
          }}
        />
      )}
      {showSelectProgramPopup2 && (
        <SelectProgramPopup
          onProgramListChecked={programList2}
          onCompleteSelection={programCheckedList}
          onClickClose={(e) => {
            setShowSelectProgramPopup2(false);
          }}
        />
      )}
      {storeModal && (
        <StorePopup visible={storeModal} onClose={closeStoreModal}>
          {store.map((param) => {
            return (
              <div key={param.store_id}>
                <label>
                  <input
                    type="checkbox"
                    value={param.store_name}
                    onChange={(e) => setStoreData(e)}
                  />
                  <span>&nbsp; {param.store_name}</span>
                </label>
              </div>
            );
          })}
        </StorePopup>
      )}
      {isCancelPopup.is && (
        <CancelPopup
          onclose={() => {
            setIsCancelPopup({
              is: false,
              id: "",
            });
          }}
          id={isCancelPopup.id}
        />
      )}
    </Container>
  );
};
