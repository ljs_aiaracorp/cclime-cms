import { useEffect } from "react";
import {
  Container,
  Card,
} from "react-bootstrap";

export const Privacy = (props) => {
  return (
    <Container fluid>
      <Card className="card-plain table-plain-bg">
        <Card.Body className="table-full-width table-responsive px-0">
          ■ ㈜ 끌리메 개인정보처리방침
          <br/><br/>
          ㈜끌리메(이하 “회사”)는 이용자들의 개인정보보호를 매우 중요시하며, 이용자가 회사의 모바일 앱서비스(이하 “끌리메앱”)을 이용함과 동시에 온라인상에서 회사에 제공한 개인정보가 보호 받을 수 있도록 최선을 다하고 있으며, 『개인정보 보호법』,『정보통신망 이용촉진 및 정보보호 등에 관한 법률』등 개인정보와 관련된 법령 상의 개인정보보호 규정을 준수하고 있습니다.
          <br/><br/>
          회사는 아래와 같이 개인정보처리방침을 명시하여 이용자가 회사에 제공한 개인정보가 어떠한 용도와 방식으로 이용되고 있으며 개인정보보호를 위해 어떠한 조치를 취하는지 알려드립니다. 회사의 개인정보처리방침은 법령 및 고시 등의 변경 또는 회사의 약관 및 내부 정책에 따라 변경될 수 있으며 이를 개정하는 경우 회사는 변경사항에 대하여 서비스 화면에 게시하거나 이용자에게 고지합니다.
          <br/><br/>
          이용자는 개인정보의 수집, 이용, 제공, 위탁 등과 관련한 아래 사항에 대하여 원하지 않는 경우 동의를 거부할 수 있습니다. 다만, 이용자가 동의를 거부하는 경우 서비스의 전부 또는 일부를 이용할 수 없음을 알려드립니다.
          <br/><br/>
          이용자께서는 끌리메앱 방문시 수시로 본 개인정보처리방침을 확인하시기 바랍니다
          <br/><br/>
          제1조 개인정보의 수집목적 및 이용목적
          <br/><br/>
          회사는 이용자의 사전 동의 없이는 이용자의 개인 정보를 공개하지 않으며, 다음과 같은 목적을 위하여 개인정보를 수집하고 이용합니다.
          <br/><br/>
          서비스 제공 : 멤버십과 관련혜택 등의 기본적인 서비스 제공, 서비스 제공에 관한 계약 체결·유지·이행·관리·개선 및 서비스 제공에 따른 요금 정산 및 컨텐츠 서비스 이용, 구매 및 요금결제, 예약관리, 서비스 이용 정보 제공 등
          <br/><br/>
          회원관리 : 회원제 서비스에 따른 본인 확인, 개인 식별, 불량회원의 부정 이용 방지와 비인가 사용 방지, 회원 가입∙유지∙탈퇴 의사 확인, 연령확인, 만 14 세 미만 아동 개인정보수집 시 법정 대리인 동의 여부 확인, 법정 대리인 권리행사 시 본인 확인, 법령상 의무 이행, 법령 및 약관위반 여부에 관한 조사, 고객 센터 운영 불만처리 등 민원 처리, 고지사항 전달, 각종 경품 및 혜택 등의 지급시 본인 확인 등
          <br/><br/>
          마케팅 및 광고에 활용 : 신규 서비스(제품) 개발 및 특화, 뉴스레터, 이벤트 등 광고성 정보 전달, 인구통계학적 특성에 따른 서비스 제공 및 광고 게재, 마케팅 및 광고 등에 활용, 접속 빈도 파악 또는 회원의 서비스 이용에 대한 통계
          <br/><br/>
          수집하는 개인정보 항목에 따른 이용 목적은 다음과 같습니다.
          <br/><br/>
          성명, 아이디, 비밀번호 : 회원제 서비스 이용에 따른 본인 확인, 회원 가입·유지·탈퇴
          <br/><br/>
          이메일주소, 전화번호 : 회원제 서비스 이용에 따른 본인 확인, 회원 가입·유지·탈퇴, 고지사항전달, 불만처리 등을 위한 원활한 의사소통 경로의 확보, 새로운 서비스 및 신상품이나 이벤트 정보 등의 안내
          <br/><br/>
          신용카드정보(카드사 이름, 카드 번호 16 자리, 카드 유효기간, 카드 명의자 생년월일) : 서비스 및 부가 서비스 이용에 대한 요금 결제
          <br/><br/>
          고객센터와의 상담내용 : 고객센터 및 기본 서비스 개선
          <br/><br/>
          이메일, 전화번호 및 기타 선택 항목: 개인맞춤 서비스를 제공하기 위한 자료, 마케팅 활동
          <br/><br/>
          제2조 수집하는 개인정보 항목
          <br/><br/>
          회사는 이용자들이 회원서비스를 이용하기 위해 회원으로 가입할 때 서비스 제공을 위하여 아래와 같은 개인정보를 필수적으로 수집하고 있습니다.
          <br/><br/>
          필수 항목 : 이용자의 성명, 아이디, 비밀번호, 이메일 주소, 휴대전화번호, 법정대리인정보(14 세미만의 경우), 서비스 이용기록, 접속 로그기록, 결제기록, 쿠키, IP Address, 불량 이용 기록, 고객센터 이용시 상담내용
          <br/><br/>
          선택 항목 : 마케팅·이벤트 정보제공을 위하여 수집한 추가 개인정보
          <br/><br/>
          회사는 끌리메앱내에서의 설문조사나 이벤트 행사 시 통계분석이나 경품제공 등을 위해 선별적으로 개인정보 입력을 요청할 수 있습니다. 회사가 발송하는 푸시알림, 메일, 뉴스레터 등의 수신에 동의하셔서, 이벤트, 경품 등의 혜택을 받는 광고의 수신자가 되는 경우에도 선별적으로 개인정보 입력을 요청받을 수 있습니다.
          <br/><br/>
          회사는 이용자의 기본적 인권 침해의 우려가 있는 민감한 개인정보(인종 및 민족, 사상 및 신조, 출신지 및 본적지, 정치적 성향 및 범죄기록, 건강상태 및 성생활 등)는 수집하지 않으며 부득이하게 수집해야 할 경우 이용자들의 사전동의를 반드시 구할 것입니다.
          <br/><br/>
          회사는 어떤 경우에라도 입력하신 정보를 이용자들에게 사전에 밝힌 목적 이외에 다른 목적으로는 사용하지 않으며 외부로 유출하지 않습니다.
          <br/><br/>
          회사는 이용자 및 회원 간 거래에서 분쟁을 예방하기 위하여 이용자에게 실명인증을 요구할 수 있습니다.
          <br/><br/>
          제3조 개인정보 수집 방법
          <br/><br/>
          회사는 끌리메앱 회원가입, 회원정보수정, 서비스 이용, 제휴사로부터의 제공, 이메일, 이벤트 참여, 고객센터, 게시판과 같은 방법을 통하여 개인정보를 수집합니다.
          <br/><br/>
          이용자는 회사가 마련한 개인정보 처리방침 또는 이용약관의 내용에 대해 동의 버튼을 클릭함으로써 개인정보 수집에 대하여 동의여부를 표시할 수 있으며, 동의 버튼을 클릭할 경우에는 개인정보수집에 동의한 것으로 봅니다.
          <br/><br/>
          제4조 수집하는 개인정보의 보유 및 이용기간
          <br/><br/>
          법령에서 별도로 정하거나 귀하와 별도 합의하는 등의 특별한 사정이 없는 한 이용자가 끌리메앱 회원으로서 회사에 제공하는 서비스를 이용하는 동안 또는 제 1 조에서 정한 목적을 달성할 때까지 회사는 이용자들의 개인정보를 계속적으로 보유하며 서비스 제공 등을 위해 이용합니다.
          <br/><br/>
          이용자의 개인정보는 다음과 같이 개인정보의 수집목적 또는 제공받은 목적이 달성되면 파기하는 것을 원칙으로 합니다. 다만, 회사는 서비스 혼선 방지, 수사기관에 대한 협조, 불량 회원의 부정한 이용의 재발 및 재가입을 방지하고 분쟁 해결을 위하여 이용계약 해지일로부터 6개월간 해당 회원의 이름, 아이디, 연락처, 부정이용 내역(서비스 이용기록, 접속로그, 쿠키, 접속 IP 정보)을 보관합니다.
          <br/><br/>
          상법, 전자상거래 등에서의 소비자보호에 관한 법률 등 관계법령의 규정에 의하여 보존할 필요가 있는 경우 회사는 관계법령에서 정한 일정한 기간 동안 회원정보를 보관합니다. 이 경우 회사는 보관하는 정보를 그 보관의 목적으로만 이용하며 보존기간은 아래와 같습니다.
          <br/>
          • 표시. 광고에 관한 기록: 6 월 (전자상거래등에서의 소비자보호에 관한 법률)
          <br/>
          • 계약 또는 청약철회 등에 관한 기록 : 5 년 (전자상거래등에서의 소비자보호에 관한 법률)
          <br/>
          • 대금결제 및 재화 등의 공급에 관한 기록 : 5 년 (전자상거래등에서의 소비자보호에 관한 법률)
          <br/>
          • 소비자의 불만 또는 분쟁처리에 관한 기록 : 3 년 (전자상거래등에서의 소비자보호에 관한 법률)
          <br/><br/>
          제5조 개인정보 제3자 제공
          <br/><br/>
          회사는 이용자들의 개인정보를 제 1 조에서 고지한 범위 내에서 사용하며, 이용자의 사전 동의 없이는 동 범위를 초과하여 이용하거나 원칙적으로 이용자의 개인정보를 외부에 공개하지 않습니다.
          <br/><br/>
          다만, 아래의 경우에는 예외로 합니다.
          <br/><br/>
          이용자들이 사전에 동의한 경우
          <br/><br/>
          서비스 제공에 따른 요금정산을 위하여 필요한 경우
          <br/><br/>
          법령에 특별한 규정이 있는 경우
          <br/><br/>
          통계작성, 학술연구나 시장조사를 위하여 특정개인을 식별할 수 없는 형태로 광고주, 협력업체나 연구단체 등에 제공하는 경우
          <br/><br/>
          제6조 개인정보의 위탁 처리
          <br/><br/>
          회사는 서비스 향상을 위해서 이용자의 개인정보를 서비스 제공을 위해서 반드시 필요한 업무 중 일부의 수행을 위하여 본 조 제 2 항과 같이 개인정보를 위탁하고 있으며, 관계 법령에 따라 위탁계약 시 개인정보가 안전하게 관리될 수 있도록 필요한 사항을 규정하고 있습니다. 또한 공유하는 정보는 당해 목적을 달성하기 위하여 필요한 최소한의 정보에 국한됩니다.
          <br/><br/>
          수탁자 및 수탁업무 내용은 아래와 같습니다.
          <br/><br/>
          〈국내업체〉
          <br/><br/>
          회원가입, 로그인 : 카카오  (보유 및 이용기간 : 회원 탈퇴 및 위탁 계약 만료 시까지)
          <br/><br/>
          전자 결제 서비스 : NHN KCP, 네이버, 카카오, 페이코, 차이, 토스 (보유 및 이용기간 : 회원 탈퇴 및 위탁 계약 만료 시까지)
          <br/><br/>
          본인인증 서비스 : 다날  (보유 및 이용기간 : 목적 달성 시 즉시 파기)
          <br/><br/>
          SMS/LMS, 이메일, 카카오 알림톡 메시지 발송 :  비즈뿌리오/다우기술 (보유 및 이용기간 : 목적 달성 시 즉시 파기)
          <br/><br/>
          〈국외업체〉
          <br/><br/>
          국적 : 미국
          <br/><br/>
          회원가입, 로그인, 결제정보 : Apple  (보유 및 이용기간 : 회원 탈퇴 및 위탁 계약 만료시까지)
          <br/><br/>
          제7조 이용자의 권리
          <br/><br/>
          회사는 이용자의 권리를 다음과 같이 보호하고 있습니다.
          <br/><br/>
          언제든지 자신의 개인정보를 조회하고 수정할 수 있습니다.
          <br/><br/>
          언제든지 개인정보 제공에 관한 동의철회/회원가입해지를 요청할 수 있습니다.
          <br/><br/>
          이용자가 개인정보에 대한 열람·증명 또는 정정을 요청하는 경우 회사는 정정 또는 삭제를 완료할 때까지 당해 개인정보를 이용하거나 제공하지 않습니다. 회사는 개인정보에 오류가 있거나 보존기간을 경과한 것이 판명되는 등 정정 또는 삭제할 필요가 있다고 인정되는 경우 지체 없이 그에 따른 조치를 취합니다.
          <br/><br/>
          회사는 만 14 세 미만 아동 이용자의 법정대리인에 대하여 아래와 같은 권리를 보장합니다.
          <br/><br/>
          법정대리인은 만 14 세 미만 아동의 개인정보 수집·이용 또는 제공에 대한 동의를 철회할 수 있으며, 해당 아동이 제공한 개인정보에 대한 열람 또는 오류의 정정을 요구할 수 있습니다. (아동의 개인정보에 대한 열람, 정정·삭제, 개인정보처리정지요구권).
          <br/><br/>
          만 14 세 미만 아동인 이용자의 법정대리인이 열람∙증명, 정정을 요구하는 경우 회사는 대리관계를 증명하는 증표를 요구하여 진정한 법정대리인인지 여부를 확인할 수 있습니다.
          <br/><br/>
          제8조 개인정보 파기절차 및 방법
          <br/><br/>
          이용자가 끌리메앱을 통해 입력한 정보는 목적이 달성된 후 별도의 DB 로 옮겨져(출력물의 경우 별도의 서류함) 내부 방침 및 기타 관계법령에 의한 정보보호 사유에 따라 (보유 및 이용기간 참조) 일정 기간 저장된 후 파기됩니다. 별도 DB 로 옮겨진 개인정보는 법률에 의한 경우가 아니고서는 보유되어지는 이외의 다른 목적으로 이용되지 않습니다.
          <br/><br/>
          회사는 귀중한 이용자의 개인정보를 안전하게 처리하며, 유출의 방지를 위하여 다음과 같은 방법을 통하여 개인정보를 파기합니다.
          <br/><br/>
          종이에 출력된 개인정보는 분쇄기로 분쇄하거나 소각을 통하여 파기합니다.
          <br/><br/>
          전자적 파일 형태로 저장된 개인정보는 기록을 재생할 수 없는 기술적 방법을 사용하여 삭제합니다.
          <br/><br/>
          제9조 개인정보관련 의견수렴 및 불만처리에 관한 사항
          <br/><br/>
          회사는 개인정보보호와 관련하여 이용자 여러분들의 의견을 수렴하고 있으며 불만을 처리하기 위하여 모든 절차와 방법을 마련하고 있습니다. 이용자들은 전화나 메일을 통하여 불만사항을 신고할 수 있고, 회사는 이용자들의 신고사항에 대하여 신속하고도 충분한 답변을 해 드릴 것입니다.
          <br/><br/>
          제10조 개인정보처리방침의 적용 제외
          <br/><br/>
          회사는 이용자에게 웹사이트를 통하여 다른 회사의 웹사이트 또는 자료에 대한 링크를 제공할 수 있습니다. 이 경우 회사는 외부사이트 및 자료에 대하여 통제권이 없을 뿐만 아니라 이들이 개인정보를 수집하는 행위에 대하여 회사의 '개인정보처리방침'이 적용되지 않습니다. 따라서, 회사가 포함하고 있는 링크를 클릭하여 타 사이트의 페이지로 이동할 경우에는 새로 방문한 사이트의 개인정보처리방침을 반드시 확인하시기 바랍니다.
          <br/><br/>
          제11조 권익침해 구제방법
          <br/><br/>
          이용자는 다음 각 호의 기관에 대해 개인정보 침해에 대한 피해구제, 상담 등을 문의할 수 있습니다. 아래의 기관은 회사와 별개의 기관으로서, 회사의 자체적인 개인정보 불만처리, 피해구제 결과에 만족하지 못하시거나 보다 구체적이고 자세한 도움이 필요하시면 문의하여 주시기 바랍니다.
          <br/><br/>
          개인정보침해에 대한 신고나 상담이 필요하신 경우에는 아래 기관에 문의하시기 바랍니다.
          <br/><br/>
          개인정보 분쟁조정위원회 :<a href="tel:1833-6972">1833-6972</a> (<a href="www.kopico.go.kr">www.kopico.go.kr</a>)
          <br/><br/>
          개인정보 침해신고센터 : (국번없이) <a href="tel:118">118</a> (<a href="privacy.kisa.or.kr">privacy.kisa.or.kr</a>)
          <br/><br/>
          대검찰청 사이버범죄수사단 : (국번없이) <a href="tel:1301">1301</a> (<a href="www.spo.go.kr">www.spo.go.kr</a>)
          <br/><br/>
          경찰청 사이버안전국 : (국번없이) <a href="tel:182">182</a> (<a href="cyberbureau.police.go.kr">cyberbureau.police.go.kr</a>)
          <br/><br/>
          제12조 고지의 의무
          <br/>
          현 개인정보처리방침의 내용은 정부의 정책 또는 보안기술의 변경에 따라 내용의 추가 삭제 및 수정이 있을 시에는 당사 홈페이지 공지사항 등을 통해 개별 고지할 것입니다.
          <br/><br/>
          제13조 시행시기
          <br/>
          본 방침은 2021 년 10 월 20 일부터 시행됩니다
        </Card.Body>
      </Card>
    </Container>
  );
};
