import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import { FileBtn } from "components/FileUpload";
import AddressPopup from "components/Popup/AddressPopup";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";
import { isValidMobile, isBusinessNumber } from "utils/validator";

export const AddBranch = (props) => {
  const sendUrl = axiosUrlFunction("store", "검색");

  const [storeName, setStoreName] = useState(""); // 매장명
  const [businessNumber, setBusinessNumber] = useState(""); // 사업자번호
  const [storeType, setStoreType] = useState("직영점"); // 지점구분
  const [storeRT, setStoreRT] = useState(""); // 대표자 명
  const [phone, setPhone] = useState(""); // 전화번호
  const [email, setEmail] = useState(""); // 이메일
  const [address, setAddress] = useState(""); // 기본 주소
  const [addressDetail, setAddressDetail] = useState(""); // 상세주소
  const [bedCount, setBedCount] = useState(""); // 배드 수량
  const [weekendOpen, setWeekendOpen] = useState(""); // 평일 오픈
  const [weekendClose, setWeekendClose] = useState(""); // 평일 마감
  const [satOpen, setSatOpen] = useState(""); // 토요일 오픈
  const [satClose, setSatClose] = useState(""); // 토요일 마감
  const [sunOpen, setSunOpen] = useState(""); // 일요일 오픈
  const [sunClose, setSunClose] = useState(""); // 일요일 마감
  const [holidayOpen, setHolidayOpen] = useState(""); // 공휴일 오픈
  const [holidayClose, setHolidayClose] = useState(""); // 공휴일 마감
  const [parking, setParking] = useState(""); // 주차가능
  const [thumbnailImg, setThumbnailImg] = useState(""); // 썸네일 사진
  const [storeImg, setStoreImg] = useState(""); // 지점이미지 사진
  // Address Modal
  const [addressModal, setAddressModal] = useState(false);

  // address Component Modal open & close
  const addressOpenModal = () => {
    setAddressModal(true);
  };
  const addressCloseModal = (fullAddr) => {
    setAddressModal(false);
    if (fullAddr !== "") setAddress(fullAddr);
  };

  // 핸드폰 정규화 하이픈 추가
  const checkingPhone = () => {
    if (!isValidMobile(phone)) {
      const value = phone.replace(
        /(^02.{0}|^01.{1}|[0-9]{3})([0-9]{3,4})([0-9]{4})/,
        "$1-$2-$3"
      );
      if (value.indexOf("-") === -1) {
        alert("전화번호를 확인해 주세요.");
        return "";
      }
      return value;
    } else return phone;
  };

  // 사업자등록 하이픈 추가
  const checkingBusiness = () => {
    if (!isBusinessNumber(businessNumber)) {
      const value = businessNumber.replace(/(\d{3})(\d{2})(\d{5})/, "$1-$2-$3");
      if (value.indexOf("-") === -1) {
        alert("사업자번호를 확인해 주세요.");
        return "";
      }
      return value;
    } else return businessNumber;
  };

  // send form Data
  let sw = 0;
  const formPostAxios = async (event) => {
    event.preventDefault();
    if (sw === 1) return;
    const formData = new FormData(); // Form Data
    let phoneCheck = checkingPhone(); // 전화번호 확인
    let businessCheck = checkingBusiness(); // 사업자번호 확인
    const keys = []; // 삭제할 키

    // 번호 확인
    if (phoneCheck === "") return null;
    if (businessCheck === "") return null;

    // 업로드 확인
    if (thumbnailImg === "") {
      alert("썸네일 이미지를 업로드 해 주세요.");
      return null;
    }
    if (storeImg === "") {
      alert("지점 이미지를 업로드 해 주세요.");
      return null;
    }

    formData.append("store_name", storeName);
    formData.append("business_num", businessCheck);
    formData.append("store_type", storeType);
    formData.append("representative", storeRT);
    formData.append("tel", phoneCheck);
    formData.append("email", email);
    formData.append("address", address);
    formData.append("detail_address", addressDetail);
    formData.append("bed", parseInt(bedCount));
    formData.append("week_open", weekendOpen);
    formData.append("week_close", weekendClose);
    formData.append("sat_open", satOpen);
    formData.append("sat_close", satClose);
    formData.append("sun_open", sunOpen);
    formData.append("sun_close", sunClose);
    formData.append("holiday_open", holidayOpen);
    formData.append("holiday_close", holidayClose);
    formData.append("parking", parking);
    formData.append("thumbnail", thumbnailImg[0]);
    formData.append("image", storeImg[0]);

    // 빈 값 삭제
    for (let key of formData.keys()) {
      if (formData.get(key) === "" || formData.get(key) === "undefined") {
        keys.push(key);
      }
    }
    for (let i = 0; i < keys.length; i++) {
      formData.delete(keys[i]);
    }

    sw = 1;
    axios
      .post(`${sendUrl.apiUrl}`, formData, {
        headers: {
          Authorization: `Bearer ${sendUrl.token}`,
          "Content-Type": `multipart/form-data`,
        },
      })
      .then(async (res) => {
        sw = 0;
        props.history.push(`/admin/branchs/list`);
      })
      .catch((err) => {
        sw = 0;
        if (err.message.indexOf("500") > -1)
          alert("모든 빈 칸을 기입해 주세요.");
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else formPostAxios(event);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };
  return (
    <Container fluid>
      <form onSubmit={formPostAxios}>
        <Row>
          <Col md="12">
            <Card className="card-plain table-plain-bg">
              <Card.Header>
                <Card.Title as="h4">
                  지점관리 - 지점리스트 - 지점 추가
                </Card.Title>
              </Card.Header>
              <Card.Body className="table-full-width table-responsive px-0">
                <Table className="table-hover">
                  <colgroup>
                    <col style={{ width: "10%" }}></col>
                    <col style={{ width: "90%" }}></col>
                  </colgroup>
                  <tbody>
                    <tr>
                      <th className="required">매장명</th>
                      <td>
                        <input
                          className="form-control"
                          type="text"
                          required
                          placeholder="매장명"
                          defaultValue={storeName}
                          maxLength={30}
                          onChange={(e) => setStoreName(e.target.value)}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th className="required">사업자번호</th>
                      <td>
                        <input
                          className="form-control"
                          type="text"
                          required
                          placeholder="사업자번호"
                          defaultValue={businessNumber}
                          maxLength={12}
                          onChange={(e) => setBusinessNumber(e.target.value)}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th className="required">지점구분</th>
                      <td>
                        <Button
                          className={
                            storeType === "체인점"
                              ? "select-btn"
                              : "select-btn selected"
                          }
                          onClick={(e) => {
                            setStoreType("직영점");
                          }}
                        >
                          직영점
                        </Button>
                        <Button
                          className={
                            storeType === "체인점"
                              ? "select-btn selected"
                              : "select-btn"
                          }
                          onClick={(e) => {
                            setStoreType("체인점");
                          }}
                        >
                          체인점
                        </Button>
                      </td>
                    </tr>
                    <tr>
                      <th className="required">대표자</th>
                      <td>
                        <input
                          className="form-control"
                          type="text"
                          required
                          placeholder="대표자명"
                          defaultValue={storeRT}
                          maxLength={15}
                          onChange={(e) => setStoreRT(e.target.value)}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th className="required">전화번호</th>
                      <td>
                        <input
                          className="form-control"
                          type="text"
                          required
                          placeholder="지점 전화번호"
                          defaultValue={phone}
                          maxLength={13}
                          onChange={(e) => setPhone(e.target.value)}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th className="required">이메일</th>
                      <td>
                        <input
                          className="form-control"
                          type="email"
                          required
                          placeholder="지점 이메일 주소"
                          defaultValue={email}
                          maxLength={30}
                          onChange={(e) => setEmail(e.target.value)}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th className="required">주소</th>
                      <td>
                        <input
                          className="form-control"
                          type="text"
                          placeholder="주소를 검색해주세요"
                          defaultValue={address}
                          onChange={(e) => setAddress(e.target.value)}
                          required
                          readOnly={true}
                          style={{
                            width: "76%",
                            display: "inline-block",
                            marginRight: "10px",
                          }}
                        />
                        <Button onClick={addressOpenModal}>주소검색</Button>
                        {addressModal && (
                          <AddressPopup onmakeclose={addressCloseModal} />
                        )}
                        <input
                          className="form-control"
                          type="text"
                          placeholder="상세주소"
                          defaultValue={addressDetail}
                          required
                          maxLength={35}
                          onChange={(e) => setAddressDetail(e.target.value)}
                          style={{
                            width: "76%",
                            display: "inline-block",
                            marginRight: "10px",
                          }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th className="required">뷰티스트 인원</th>
                      <td>
                        * 뷰티스트 인원은 뷰티스트 등록시 자동으로 변경됩니다.
                      </td>
                    </tr>
                    <tr>
                      <th className="required">베드 수량</th>
                      <td>
                        <input
                          className="form-control"
                          type="number"
                          placeholder="N"
                          defaultValue={bedCount}
                          required
                          maxLength={15}
                          onChange={(e) => {
                            const val =
                              0 > parseInt(e.target.value)
                                ? 0
                                : parseInt(e.target.value);
                            setBedCount(val);
                          }}
                          style={{
                            width: "10%",
                            display: "inline-block",
                            marginRight: "10px",
                          }}
                        />
                        <b className="mr-2">개</b>* 베드 수량만큼 예약 가능
                        인원이 정해집니다.
                      </td>
                    </tr>
                    <tr>
                      <th className="required">영업시간</th>
                      <td>
                        <div className="row">
                          <div className="col-1">평일</div>
                          <div className="col-5">
                            <Form.Group as={Row} className="mb-3">
                              <Col sm={5} className="pr-0">
                                <Form.Control
                                  required
                                  className="mb-0"
                                  type="time"
                                  defaultValue={weekendOpen}
                                  onChange={(e) =>
                                    setWeekendOpen(e.target.value)
                                  }
                                />
                              </Col>
                              <Col
                                sm={1}
                                style={{
                                  fontSize: "24px",
                                  textAlign: "center",
                                }}
                              >
                                ~
                              </Col>
                              <Col sm={5} className="pl-0">
                                <Form.Control
                                  required
                                  className="mb-0"
                                  type="time"
                                  defaultValue={weekendClose}
                                  onChange={(e) =>
                                    setWeekendClose(e.target.value)
                                  }
                                />
                              </Col>
                            </Form.Group>
                          </div>
                          <div className="col-1">토요일</div>
                          <div className="col-5">
                            <Form.Group as={Row} className="mb-3">
                              <Col sm={5} className="pr-0">
                                <Form.Control
                                  className="mb-0"
                                  type="time"
                                  defaultValue={satOpen}
                                  onChange={(e) => setSatOpen(e.target.value)}
                                />
                              </Col>
                              <Col
                                sm={1}
                                style={{
                                  fontSize: "24px",
                                  textAlign: "center",
                                }}
                              >
                                ~
                              </Col>
                              <Col sm={5} className="pl-0">
                                <Form.Control
                                  className="mb-0"
                                  type="time"
                                  defaultValue={satClose}
                                  onChange={(e) => setSatClose(e.target.value)}
                                />
                              </Col>
                            </Form.Group>
                          </div>
                          <div className="col-1">일요일</div>
                          <div className="col-5">
                            <Form.Group as={Row} className="mb-3">
                              <Col sm={5} className="pr-0">
                                <Form.Control
                                  className="mb-0"
                                  type="time"
                                  defaultValue={sunOpen}
                                  onChange={(e) => setSunOpen(e.target.value)}
                                />
                              </Col>
                              <Col
                                sm={1}
                                style={{
                                  fontSize: "24px",
                                  textAlign: "center",
                                }}
                              >
                                ~
                              </Col>
                              <Col sm={5} className="pl-0">
                                <Form.Control
                                  className="mb-0"
                                  type="time"
                                  defaultValue={sunClose}
                                  onChange={(e) => setSunClose(e.target.value)}
                                />
                              </Col>
                            </Form.Group>
                          </div>
                          <div className="col-1">공휴일</div>
                          <div className="col-5">
                            <Form.Group as={Row} className="mb-0">
                              <Col sm={5} className="pr-0">
                                <Form.Control
                                  className="mb-0"
                                  type="time"
                                  defaultValue={holidayOpen}
                                  onChange={(e) =>
                                    setHolidayOpen(e.target.value)
                                  }
                                />
                              </Col>
                              <Col
                                sm={1}
                                style={{
                                  fontSize: "24px",
                                  textAlign: "center",
                                }}
                              >
                                ~
                              </Col>
                              <Col sm={5} className="pl-0">
                                <Form.Control
                                  className="mb-0"
                                  type="time"
                                  defaultValue={holidayClose}
                                  onChange={(e) =>
                                    setHolidayClose(e.target.value)
                                  }
                                />
                              </Col>
                            </Form.Group>
                          </div>
                        </div>
                        <span>* 빈 칸으로 기입시 휴무일 입니다.</span>
                      </td>
                    </tr>
                    <tr>
                      <th>주차안내</th>
                      <td>
                        <textarea
                          className="form-control"
                          defaultValue={parking}
                          onChange={(e) => setParking(e.target.value)}
                          style={{
                            minHeight: "100px",
                          }}
                        ></textarea>
                      </td>
                    </tr>
                    <tr>
                      <th className="required">썸네일 이미지</th>
                      <td>
                        <FileBtn
                          name="업로드"
                          required
                          fileData={(data) => {
                            setThumbnailImg(data);
                          }}
                          imageUrl={""}
                          accept="image/*"
                          id="thumbnailImg"
                        />
                      </td>
                    </tr>
                    <tr>
                      <th className="required">지점 이미지</th>
                      <td>
                        <FileBtn
                          name="업로드"
                          required
                          fileData={(data) => {
                            setStoreImg(data);
                          }}
                          imageUrl={""}
                          accept="image/*"
                          id="storeImg"
                        />
                      </td>
                    </tr>
                  </tbody>
                </Table>
                <div className="text-center">
                  <Button type="submit">저장</Button>
                  <Button
                    className="ml-2"
                    onClick={() => {
                      props.history.push(`/admin/branchs/list`);
                    }}
                  >
                    취소
                  </Button>
                </div>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </form>
    </Container>
  );
};
