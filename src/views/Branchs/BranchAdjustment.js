import React, { useEffect, useState } from "react";
import uuid from "react-uuid";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import "../css/paymentTable/payTable.scss";
import Loading from "components/Loading";
import moment from "moment";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";
import xlsx from "xlsx";

export const BranchAdjustment = (props) => {
  const [selectedYear, setSelectedYear] = useState(
    Number(moment().format("yyyy"))
  );
  const [selectedMonth, setSelectedMonth] = useState(moment().format("MM"));
  const [weekList, setWeekList] = useState([]);
  const [list, setList] = useState([]);
  const [storeName, setStoreName] = useState([]);
  useEffect(() => {
    firstMonth(selectedYear, selectedMonth);
    getList();
  }, [selectedMonth, selectedYear]);

  // 정산 데이터 리스트
  const getList = async (params) => {
    setList(null);
    const getUrl = axiosUrlFunction("store/calculate", "검색");

    axios
      .get(`${getUrl.apiUrl}?ym=${selectedYear}${selectedMonth}`, {
        headers: {
          Authorization: `Bearer ${getUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        setList(res.data.data);
        const listname = [];
        for (let i = 0; i < res.data.data.length; i++) {
          listname.push({
            name: res.data.data[i].store_name,
            id: res.data.data[i].store_id,
          });
        }
        // const set = new Set(listname);
        const newListname = listname.filter((id, i) => {
          return (
            listname.findIndex((id2, j) => {
              return id.id === id2.id;
            }) === i
          );
        });
        // const newListname = [...set];
        setStoreName(newListname);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getList();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 주차 계산
  const firstMonth = (year, month) => {
    // 마지막 달
    let lastDay = new Date(year, month, 0).getDate();
    let lastWeekCount = new Date(year, month, 0).getDate();

    // 첫달 첫 월요일
    const monday = moment(new Date(year, month - 1, 1)).day();

    let firstMonth = "";
    if (monday === 1) {
      firstMonth = 1;
    } else if (monday === 0) {
      firstMonth = 2;
    } else {
      firstMonth = 1 + 7 - (monday - 1);
      lastWeekCount -= firstMonth - 1;
    }
    const week = [];
    const weekCount = Math.ceil(lastWeekCount / 7);

    // 주차 set
    for (let i = 0; i < weekCount; i++) {
      if (firstMonth + 6 > lastDay) {
        const month2 =
          parseInt(month) + 1 === 13 ? "01" : "0" + (parseInt(month) + 1);
        const monthPlus =
          parseInt(month2) < 10 ? month2 : month2.replace("0", "");
        if (firstMonth + 6 - lastDay < 10) {
          week.push(
            `${month}월 ${i + 1}주차 (${month}-${firstMonth}~${monthPlus}-0${
              firstMonth + 6 - lastDay
            })`
          );
        } else {
          week.push(
            `${month}월 ${i + 1}주차 (${month}-${firstMonth}~${
              parseInt(month) + 1 === 13 ? "01" : month + 1
            }-${firstMonth + 6 - lastDay})`
          );
        }
      } else {
        if (firstMonth < 10 && firstMonth + 6 < 10) {
          week.push(
            `${month}월 ${i + 1}주차 (${month}-0${firstMonth}~${month}-0${
              firstMonth + 6
            })`
          );
        } else if (firstMonth < 10) {
          week.push(
            `${month}월 ${i + 1}주차 (${month}-0${firstMonth}~${month}-${
              firstMonth + 6
            })`
          );
        } else {
          week.push(
            `${month}월 ${i + 1}주차 (${month}-${firstMonth}~${month}-${
              firstMonth + 6
            })`
          );
        }
      }
      firstMonth += 7;
    }
    setWeekList(week);
  };

  // 지점별 정산관리 View 출력
  const viewAmount = (name) => {
    const result = [];
    let z = 0;
    for (let i = 0; i < list.length; i++) {
      if (name === list[i].store_name) {
        for (let j = z; j < weekList.length; j++) {
          if (list[i].start !== null) {
            const content =
              list[i].calculate_check === "Y" ? "정산완료" : "정산미완료";
            const style = {
              color: list[i].calculate_check === "Y" ? "blue" : "red",
            };
            if (weekList[j].slice(9, 14) === list[i].start.slice(5, 10)) {
              result.push(<th key={uuid()}>{list[i].amount}</th>);
              result.push(
                <th
                  key={uuid()}
                  id={list[i].store_calculate_id}
                  style={style}
                  onClick={() =>
                    calculateHandler(list[i].store_calculate_id, i)
                  }
                  className="cursor"
                >
                  {content}
                </th>
              );
              z = j + 1;
              break;
            } else {
              result.push(<th key={uuid()}>-</th>);
              result.push(<th key={uuid()}>-</th>);
            }
          } else {
            result.push(<th key={uuid()}>-</th>);
            result.push(<th key={uuid()}>-</th>);
          }
        }
      }
    }
    let arrayCount = result.length;
    while (arrayCount < weekList.length * 2) {
      result.push(<th key={uuid()}>0</th>);
      result.push(
        <th key={uuid()} style={{ color: "red" }} className="cursor">
          정산미완료
        </th>
      );
      arrayCount += 2;
    }

    return result;
  };

  const calculateHandler = async (id, index) => {
    if (list[index].calculate_check === "Y") {
      list[index].calculate_check = "N";
    } else if (list[index].calculate_check === "N") {
      list[index].calculate_check = "Y";
    }

    const url = axiosUrlFunction("store/calculateCheck", "검색");
    const dataId = {
      store_calculate_id: id,
    };

    axios
      .put(`${url.apiUrl}`, dataId, {
        headers: {
          Authorization: `Bearer ${url.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        const th = document.getElementById(id); // th Text
        th.innerHTML =
          list[index].calculate_check === "Y" ? "정산완료" : "정산미완료";
        th.style.color = list[index].calculate_check === "Y" ? "blue" : "red";
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else calculateHandler(id, index);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 테이블 엑셀 변환
  const tableToxlsx = () => {
    const table = document.getElementById("tableChange");

    let wscols = [{ wch: 10 }, { wch: 30 }];
    for (let i = 0; i < weekList.length * 2; i++) {
      wscols.push({
        wch: 15,
      });
    }
    const wb = xlsx.utils.book_new();
    const ws = xlsx.utils.table_to_sheet(table);
    ws["!cols"] = wscols;
    xlsx.utils.book_append_sheet(wb, ws, "Sheet1");
    xlsx.writeFile(wb, `${selectedYear}-${selectedMonth} 정산.xlsx`);
  };

  // 달력 이벤트 ( 연 매출 )
  const yearEventHandler = (direction) => {
    if (direction === "left") setSelectedYear(selectedYear - 1);
    else setSelectedYear(selectedYear + 1);
  };

  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">지점관리 - 지점별 정산관리</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <div className="row mb-4">
                <div
                  className="col-5 text-right cursor"
                  onClick={() => {
                    yearEventHandler("left");
                  }}
                >
                  &lt;
                </div>
                <div
                  className="col-2 text-center"
                  style={{
                    fontSize: "30px",
                  }}
                >
                  {selectedYear}
                </div>
                <div
                  className="col-5 text-left cursor"
                  onClick={() => {
                    yearEventHandler("right");
                  }}
                >
                  &gt;
                </div>
              </div>
              <div className="row mb-2">
                <Button
                  className={
                    selectedMonth === "01"
                      ? "col-1 select-btn selected"
                      : "col-1 select-btn"
                  }
                  onClick={(e) => {
                    setSelectedMonth("01");
                    firstMonth(selectedYear, "01");
                  }}
                >
                  1월
                </Button>
                <Button
                  className={
                    selectedMonth === "02"
                      ? "col-1 select-btn selected"
                      : "col-1 select-btn"
                  }
                  onClick={(e) => {
                    setSelectedMonth("02");
                    firstMonth(selectedYear, "02");
                  }}
                >
                  2월
                </Button>
                <Button
                  className={
                    selectedMonth === "03"
                      ? "col-1 select-btn selected"
                      : "col-1 select-btn"
                  }
                  onClick={(e) => {
                    setSelectedMonth("03");
                    firstMonth(selectedYear, "03");
                  }}
                >
                  3월
                </Button>
                <Button
                  className={
                    selectedMonth === "04"
                      ? "col-1 select-btn selected"
                      : "col-1 select-btn"
                  }
                  onClick={(e) => {
                    setSelectedMonth("04");
                    firstMonth(selectedYear, "04");
                  }}
                >
                  4월
                </Button>
                <Button
                  className={
                    selectedMonth === "05"
                      ? "col-1 select-btn selected"
                      : "col-1 select-btn"
                  }
                  onClick={(e) => {
                    setSelectedMonth("05");
                    firstMonth(selectedYear, "05");
                  }}
                >
                  5월
                </Button>
                <Button
                  className={
                    selectedMonth === "06"
                      ? "col-1 select-btn selected"
                      : "col-1 select-btn"
                  }
                  onClick={(e) => {
                    setSelectedMonth("06");
                    firstMonth(selectedYear, "06");
                  }}
                >
                  6월
                </Button>
                <Button
                  className={
                    selectedMonth === "07"
                      ? "col-1 select-btn selected"
                      : "col-1 select-btn"
                  }
                  onClick={(e) => {
                    setSelectedMonth("07");
                    firstMonth(selectedYear, "07");
                  }}
                >
                  7월
                </Button>
                <Button
                  className={
                    selectedMonth === "08"
                      ? "col-1 select-btn selected"
                      : "col-1 select-btn"
                  }
                  onClick={(e) => {
                    setSelectedMonth("08");
                    firstMonth(selectedYear, "08");
                  }}
                >
                  8월
                </Button>
                <Button
                  className={
                    selectedMonth === "09"
                      ? "col-1 select-btn selected"
                      : "col-1 select-btn"
                  }
                  onClick={(e) => {
                    setSelectedMonth("09");
                    firstMonth(selectedYear, "09");
                  }}
                >
                  9월
                </Button>
                <Button
                  className={
                    selectedMonth === "10"
                      ? "col-1 select-btn selected"
                      : "col-1 select-btn"
                  }
                  onClick={(e) => {
                    setSelectedMonth("10");
                    firstMonth(selectedYear, "10");
                  }}
                >
                  10월
                </Button>
                <Button
                  className={
                    selectedMonth === "11"
                      ? "col-1 select-btn selected"
                      : "col-1 select-btn"
                  }
                  onClick={(e) => {
                    setSelectedMonth("11");
                    firstMonth(selectedYear, "11");
                  }}
                >
                  11월
                </Button>
                <Button
                  className={
                    selectedMonth === "12"
                      ? "col-1 select-btn selected"
                      : "col-1 select-btn"
                  }
                  onClick={(e) => {
                    setSelectedMonth("12");
                    firstMonth(selectedYear, "12");
                  }}
                >
                  12월
                </Button>
              </div>
              <div className="text-right">
                <Button
                  onClick={(e) => {
                    tableToxlsx();
                  }}
                >
                  인쇄
                </Button>
              </div>
              <div className="payment-div">
                <table className="payment-div__table" id="tableChange">
                  <thead>
                    <tr>
                      <th rowSpan={2}>NO</th>
                      <th rowSpan={2}>지점명</th>
                      {weekList.map((day) => {
                        return (
                          <React.Fragment key={uuid()}>
                            <th colSpan={2}>{day}</th>
                          </React.Fragment>
                        );
                      })}
                    </tr>
                    <tr>
                      {weekList.map((day, i) => {
                        return (
                          <React.Fragment key={uuid()}>
                            <th>
                              정산금액
                              <br />
                              (매출)
                            </th>
                            <th>정산여부</th>
                          </React.Fragment>
                        );
                      })}
                    </tr>
                  </thead>
                  <tbody>
                    {list !== undefined && list !== null ? (
                      storeName.map((name, i) => {
                        return (
                          <tr key={uuid()}>
                            <td>{i + 1}</td>
                            <td>{name.name}</td>
                            {viewAmount(name.id)}
                          </tr>
                        );
                      })
                    ) : (
                      <tr>
                        <td
                          colSpan={weekList.length * 2 + 2}
                          style={{ textAlign: "center" }}
                        >
                          <Loading />
                        </td>
                      </tr>
                    )}
                  </tbody>
                </table>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
