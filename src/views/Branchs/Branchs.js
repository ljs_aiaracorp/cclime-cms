import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import Pagination from "components/Pagination";
import Loading from "components/Loading";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";

export const Branchs = (props) => {
  const [selected, setSelected] = useState(1);
  const [list, setList] = useState(null);
  const [keyword, setKeyword] = useState("");
  const [count, setCount] = useState(0);
  const [pagenum, setPagenum] = useState();
  const [sold, setSold] = useState("");

  useEffect(() => {
    getList();
  }, []);

  const getList = async (params) => {
    setList(null);
    const storeUrl = axiosUrlFunction("storeList", "검색");

    if (!params) {
      params = {};
    }

    if (params.keyword === undefined) {
      params.keyword = keyword;
    }
    if (params.page === undefined) {
      params.page = selected - 1;
    }
    setPagenum(params.page);

    const queries = [];
    if (params.keyword !== "") {
      queries.push(`word=${params.keyword}`);
    }
    queries.push(`page=${params.page}`);
    queries.push(`size=10`);
    const queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";

    axios
      .get(`${storeUrl.apiUrl}${queryStr}`, {
        headers: {
          Authorization: `Bearer ${storeUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        setCount(res.data.data.totalElements);
        setList(res.data.data.content);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getList(params);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  const stockEventHandler = async (id, index) => {
    const stockUrl = axiosUrlFunction("soldOut", "검색"); // API url
    const btn = document.getElementById(id); // Button
    let value = list[index].sold_out; // Button value

    // List data recycle
    if (value === "Y" || value === "y") {
      value = "N";
      list[index].sold_out = "N";
    } else if (value === "N" || value === "n") {
      value = "Y";
      list[index].sold_out = "Y";
    }

    // stockUrl data set
    const data = {
      sold_out: value,
      store_id: id,
    };

    axios
      .put(`${stockUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${stockUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        // Button content change
        if (value === "Y") btn.firstChild.data = "ON";
        else btn.firstChild.data = "OFF";
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else stockEventHandler(id, index);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };
  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">지점관리 - 지점리스트</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <Table className="table-hover search-box">
                <tbody>
                  <tr>
                    <th className="select-search-target">
                      <Form.Control as="select">
                        <option value="name">지점명</option>
                      </Form.Control>
                    </th>
                    <th>
                      <Form.Control
                        type="text"
                        placeholder="검색"
                        value={keyword}
                        onChange={(e) => setKeyword(e.target.value)}
                        onKeyUp={(e) => {
                          if (e.keyCode === 13) {
                            setSelected(1);
                            getList({
                              page: 0,
                            });
                          }
                        }}
                        maxLength={50}
                        style={{
                          display: "inline-block",
                          width: "500px",
                          verticalAlign: "middle",
                        }}
                      />
                      <Button
                        variant="light"
                        onClick={(e) => {
                          setSelected(1);
                          getList({
                            page: 0,
                          });
                        }}
                        style={{
                          display: "inline-block",
                          border: "solid 1px #E3E3E3",
                          color: "#565656",
                          verticalAlign: "middle",
                        }}
                        className="search-btn"
                      >
                        <FontAwesomeIcon icon={faSearch} />
                      </Button>
                    </th>
                  </tr>
                </tbody>
              </Table>
              <div className="text-right">
                <Button
                  onClick={(e) => {
                    window.location.replace(`/admin/branchs/add`);
                  }}
                >
                  지점 추가
                </Button>
              </div>
              <Table className="table-hover mt-0">
                <thead>
                  <tr>
                    <th className="border-0">NO.</th>
                    <th className="border-0">지점명</th>
                    <th className="border-0">대표자</th>
                    <th className="border-0">전화번호</th>
                    <th className="border-0">주소</th>
                    <th className="border-0">고객평점</th>
                    <th className="border-0">재고표시</th>
                    <th className="border-0"></th>
                  </tr>
                </thead>
                <tbody>
                  {list !== undefined && list !== null ? (
                    list.map((store, i) => {
                      // 별점
                      const star = store.score;
                      const styles = { width: star + "0%" };
                      // Button value
                      const content =
                        store.sold_out === "Y" || store.sold_out === "y"
                          ? "ON"
                          : "OFF";

                      return (
                        <tr key={i} className="cursor">
                          <td>{i + 1 + 10 * pagenum}</td>
                          <td>{store.store_name}</td>
                          <td>{store.representative}</td>
                          <td>{store.tel}</td>
                          <td>
                            {store.address} {store.detail_address}
                          </td>
                          <td>
                            <div className="star-rating">
                              <span style={styles}></span>
                            </div>
                          </td>
                          <td>
                            <Button
                              id={store.store_id}
                              onClick={() => {
                                stockEventHandler(store.store_id, i);
                              }}
                            >
                              {content}
                            </Button>
                          </td>
                          <td>
                            <Button
                              onClick={() => {
                                props.history.push(
                                  `/admin/branchs/edit/${store.store_id}`
                                );
                              }}
                            >
                              조회/수정
                            </Button>
                          </td>
                        </tr>
                      );
                    })
                  ) : (
                    <tr>
                      <td colSpan={7} style={{ textAlign: "center" }}>
                        <Loading />
                      </td>
                    </tr>
                  )}
                </tbody>
              </Table>
              <Pagination
                count={count}
                forcePage={selected - 1}
                selected={(pageSelect) => {
                  setSelected(pageSelect);
                  getList({ page: pageSelect - 1 });
                }}
              />
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
