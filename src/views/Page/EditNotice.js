import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";
import { TextEditer } from "utils/TextEditer";

export const EditNotice = (props) => {
  const noticesEditUrl = axiosUrlFunction("notice", "검색");
  const noticeNumver = props.match.params.id; // 공지 id

  const [title, setTitle] = useState(""); // 공지사항 제목
  const [description, setDescription] = useState(""); // 공지사항 내용

  const config = {
    headers: {
      Authorization: `Bearer ${noticesEditUrl.token}`,
      "Content-Type": `application/json`,
    },
  };

  useEffect(() => {
    getList();
  }, []);

  // 공지사항 내용 get
  const getList = async () => {
    const getUrl = axiosUrlFunction("notice");
    axios
      .get(`${getUrl.apiUrl}?notice_id=${noticeNumver}`, config)
      .then(async (res) => {
        // 공지사항 description set
        setTitle(res.data.data.title);
        setDescription(res.data.data.content);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getList();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  const sendData = async () => {
    const data = {
      notice_id: parseInt(noticeNumver),
      title: title,
      content: description,
    };

    axios
      .put(`${noticesEditUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${noticesEditUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        window.location.replace(
          `${noticesEditUrl.accessPath}/pages/notices/list`
        );
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else sendData();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 공지사항 삭제
  const deleteNotices = async (id) => {
    const deleteNoticeUrl = axiosUrlFunction("notice/delete", "검색");
    const data = {
      notice_id: parseInt(noticeNumver),
    };

    axios
      .put(`${deleteNoticeUrl.apiUrl}`, data, config)
      .then(async () => {
        window.location.replace(
          `${deleteNoticeUrl.accessPath}/pages/notices/list`
        );
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else deleteNotices(id);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">페이지관리 - 공지사항 - 공지 수정</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <Table className="table-hover">
                <tbody>
                  <tr>
                    <th>제목</th>
                    <td>
                      <input
                        className="form-control"
                        type="text"
                        required
                        placeholder="제목"
                        defaultValue={title}
                        maxLength={50}
                        onChange={(e) => setTitle(e.target.value)}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>내용</th>
                    <td style={{ verticalAlign: "top", height: "700px" }}>
                      <TextEditer
                        value={description || ""}
                        onChange={setDescription}
                      />
                    </td>
                  </tr>
                </tbody>
              </Table>
              <div className="text-center">
                <Button onClick={deleteNotices}>삭제</Button>
                <Button className="ml-2" onClick={sendData}>
                  저장
                </Button>
                <Button
                  className="ml-2"
                  onClick={() => {
                    window.location.replace(
                      `${noticesEditUrl.accessPath}/pages/notices/list`
                    );
                  }}
                >
                  취소
                </Button>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
