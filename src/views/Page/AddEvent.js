import { useEffect, useState, useMemo, useRef } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";
import { FileBtn } from "components/FileUpload";
import { TextEditer } from "utils/TextEditer";

export const AddEvent = (props) => {
  const [title, setTitle] = useState(""); // 이벤트명
  const [startDay, setStartDay] = useState(""); // 시작기간
  const [endDay, setEndDay] = useState(""); // 종료기간
  const [picture, setPicture] = useState(""); // 업로드 사진
  const [description, setDescription] = useState(""); // 이벤트 내용
  const [comment, setComment] = useState("y"); // 댓글 On/Off

  const sendFormData = async () => {
    const formUrl = axiosUrlFunction("event", "검색");

    if (title === "") {
      alert("이벤트명을 입력해주세요.");
      return;
    } else if (startDay === "") {
      alert("시작기간을 선택해 주세요.");
      return;
    } else if (endDay === "") {
      alert("끝나는 시간을 선택해 주세요.");
      return;
    } else if (picture === "") {
      alert("사진을 업로드해 주세요.");
      return;
    } else if (description === "") {
      alert("이벤트 내용을 입력해 주세요.");
      return;
    }
    if (
      parseInt(startDay.replace(/\-/g, "")) >
      parseInt(endDay.replace(/\-/g, ""))
    ) {
      alert("이벤트 시작기간을 다시 설정해 주세요.");
      return;
    }
    const formData = new FormData();

    formData.append("images", picture[0]);
    formData.append("start", startDay);
    formData.append("end", endDay);
    formData.append("title", title);
    formData.append("content", description);
    formData.append("comment_enable", comment);

    axios
      .post(`${formUrl.apiUrl}`, formData, {
        headers: {
          Authorization: `Bearer ${formUrl.token}`,
          "Content-Type": `multipart/form-data`,
        },
      })
      .then(async (res) => {
        props.history.push(`/admin/pages/events/list`);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else sendFormData();
            });
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };
  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">페이지관리 - 이벤트 - 이벤트 추가</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <Table className="table-hover">
                <tbody>
                  <tr>
                    <th>이벤트명</th>
                    <td>
                      <input
                        className="form-control"
                        type="text"
                        required
                        placeholder="이벤트명"
                        defaultValue={title}
                        maxLength={50}
                        onChange={(e) => setTitle(e.target.value)}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>기간</th>
                    <td>
                      <div className="row">
                        {/* <div className="col-1">시작 ~ 종료</div> */}
                        <div className="col-5">
                          <Form.Group as={Row} className="mb-0">
                            <Col sm={5} className="pr-0">
                              <Form.Control
                                className="mb-0"
                                type="date"
                                required
                                placeholder="시작 날짜"
                                value={startDay}
                                onChange={(e) => setStartDay(e.target.value)}
                              />
                            </Col>
                            <Col
                              sm={1}
                              style={{ fontSize: "24px", textAlign: "center" }}
                            >
                              ~
                            </Col>
                            <Col sm={5} className="pl-0">
                              <Form.Control
                                className="mb-0"
                                type="date"
                                required
                                placeholder="종료 날짜"
                                value={endDay}
                                onChange={(e) => setEndDay(e.target.value)}
                              />
                            </Col>
                          </Form.Group>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>사진</th>
                    <td>
                      <FileBtn
                        name="업로드"
                        fileData={(data) => {
                          setPicture(data);
                        }}
                        accept="image/*"
                        id="picture"
                        imageUrl={""}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>이벤트 내용</th>
                    <td>
                      <div
                        style={{
                          width: "950px",
                          height: "650px",
                          color: "black",
                        }}
                      >
                        <TextEditer
                          value={description || ""}
                          onChange={setDescription}
                        />
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>댓글</th>
                    <td>
                      <Button
                        className={
                          comment === "y" ? "select-btn selected" : "select-btn"
                        }
                        onClick={(e) => setComment("y")}
                      >
                        ON
                      </Button>
                      <Button
                        className={
                          comment === "y" ? "select-btn" : "select-btn selected"
                        }
                        onClick={(e) => setComment("n")}
                      >
                        OFF
                      </Button>
                    </td>
                  </tr>
                </tbody>
              </Table>
              <div className="text-center">
                <Button onClick={sendFormData}>저장</Button>
                <Button
                  className="ml-2"
                  onClick={() => {
                    props.history.push(`/admin/pages/events/list`);
                  }}
                >
                  취소
                </Button>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
