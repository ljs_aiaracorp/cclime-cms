import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";
import { TextEditer } from "utils/TextEditer";

export const AddFAQ = (props) => {
  const addUrl = axiosUrlFunction("faq", "검색");

  const [title, setTitle] = useState(""); // FAQ 제목
  const [description, setDescription] = useState(""); // FAQ 내용

  // FAQ 등록
  const sendData = async () => {
    if (title === "") {
      alert("제목을 입력해 주세요.");
      return;
    } else if (description === "") {
      alert("내용을 입력해 주세요.");
      return;
    }

    const data = {
      title: title,
      content: description,
    };

    axios
      .post(`${addUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${addUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(() => {
        alert("작성을 완료했습니다.");
        window.location.replace(`${addUrl.accessPath}/pages/faq/list`);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else sendData();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">
                페이지관리 - 자주묻는질문 - 자주묻는질문 작성
              </Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <Table className="table-hover">
                <tbody>
                  <tr>
                    <th>제목</th>
                    <td>
                      <input
                        className="form-control"
                        type="text"
                        required
                        placeholder="제목"
                        defaultValue={title}
                        maxLength={50}
                        onChange={(e) => setTitle(e.target.value)}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>내용</th>
                    <td style={{ verticalAlign: "top", height: "700px" }}>
                      <TextEditer
                        value={description || ""}
                        onChange={setDescription}
                      />
                    </td>
                  </tr>
                </tbody>
              </Table>
              <div className="text-center">
                <Button onClick={sendData}>저장</Button>
                <Button
                  className="ml-2"
                  onClick={() => {
                    window.location.replace(
                      `${addUrl.accessPath}/pages/faq/list`
                    );
                  }}
                >
                  취소
                </Button>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
