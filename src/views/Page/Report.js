import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import Pagination from "components/Pagination";
import Loading from "components/Loading";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import moment from "moment";
import Calendar from "react-calendar";
import "react-calendar/dist/Calendar.css";
import axios from "axios";
import { axiosUrlFunction } from "utils/AxiosUrl";

export const Report = (props) => {
  const reportUrl = axiosUrlFunction("storeReport", "검색");

  const [selectedTab, setSelectedTab] = useState(0);
  const [selected, setSelected] = useState(1);
  const [appSelected, setAppSelected] = useState(1);

  // 달력
  const [isCalendar, setisCalendar] = useState(false);
  const [date, setDate] = useState(new Date()); // default 달력일
  const [selectedYear, setSelectedYear] = useState(moment().format("yyyy")); // 달력 년
  const [selectedMonth, setSelectedMonth] = useState(moment().format("MMMM")); // 달력 월
  const [selectedDay, setSelectedDay] = useState(moment().format("DD") + "일"); // 달력 일

  // 지점
  const [list, setList] = useState(null);
  const [searchTarget, setSearchTarget] = useState("user_name");
  const [keyword, setKeyword] = useState("");
  const [count, setCount] = useState(0);
  const [pagenum, setPagenum] = useState("");

  // 앱
  const [appList, setAppList] = useState(null);
  const [appSearchTarget, setAppSearchTarget] = useState("user_name");
  const [appKeyword, setAppKeyword] = useState("");
  const [appCount, setAppCount] = useState(0);
  const [appPagenum, setAppPagenum] = useState("");

  useEffect(() => {
    getList();
  }, []);

  // Calendar setter
  const getDate = (event) => {
    if (selectedTab === 0) {
      setDate(event);
      setKeyword(
        moment(event).format("YYYY") +
          "-" +
          moment(event).format("MM") +
          "-" +
          moment(event).format("DD")
      );
      setisCalendar(false);
    } else if (selectedTab === 1) {
      setDate(event);
      setAppKeyword(
        moment(event).format("YYYY") +
          "-" +
          moment(event).format("MM") +
          "-" +
          moment(event).format("DD")
      );
      setisCalendar(false);
    }
  };

  // 불편신고 (지점) List
  const getList = async (params) => {
    setList(null);
    if (!params) {
      params = {};
    }
    if (params.searchTarget === undefined) {
      params.searchTarget = searchTarget;
    }
    if (params.keyword === undefined) {
      params.keyword = keyword;
    }
    if (params.page === undefined) {
      params.page = selected - 1;
    }
    setPagenum(params.page);

    const queries = [];
    if (params.searchTarget !== "" && params.keyword !== "") {
      queries.push(`type=${searchTarget}`);
      queries.push(`word=${params.keyword}`);
    }
    queries.push(`page=${params.page}`);
    queries.push(`size=10`);

    const queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";
    axios
      .get(`${reportUrl.apiUrl}${queryStr}`, {
        headers: {
          Authorization: `Bearer ${reportUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        setCount(res.data.data.totalElements);
        setList(res.data.data.content);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getList(params);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 불편신고 삭제
  const deleteReport = async (id) => {
    const deleteUrl = axiosUrlFunction("report/delete", "검색");
    const data = {
      report_id: id,
    };

    axios
      .put(`${deleteUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${deleteUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async () => {
        if (selectedTab === 0) alert("지점 불편신고 삭제가 완료되었습니다.");
        else if (selectedTab === 1) alert("앱 불편신고 삭제가 완료되었습니다.");
        window.location.replace(`${deleteUrl.accessPath}/pages/reports/list`);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else deleteReport(id);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 불편신고 (앱) List
  const getAppList = async (params) => {
    const appUrl = axiosUrlFunction("appReport", "검색");
    setAppList(null);
    if (!params) {
      params = {};
    }
    if (params.searchTarget === undefined) {
      params.searchTarget = appSearchTarget;
    }
    if (params.keyword === undefined) {
      params.keyword = appKeyword;
    }
    if (params.page === undefined) {
      params.page = appSelected - 1;
    }
    setAppPagenum(params.page);

    const queries = [];
    if (params.searchTarget !== "" && params.keyword !== "") {
      queries.push(`type=${appSearchTarget}`);
      queries.push(`word=${params.keyword}`);
    }
    queries.push(`page=${params.page}`);
    queries.push(`size=10`);

    const queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";
    axios
      .get(`${appUrl.apiUrl}${queryStr}`, {
        headers: {
          Authorization: `Bearer ${appUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        setAppCount(res.data.data.totalElements);
        setAppList(res.data.data.content);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getAppList(params);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else if (err.message.indexOf("500") > -1) {
          alert("날짜를 정확히 입력해 주세요.");
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">페이지관리 - 불편신고</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <div className="row">
                <div
                  className={`col-2 tab-btn ${
                    selectedTab === 0 ? "selected-tab" : ""
                  }`}
                  onClick={(e) => {
                    setSelectedTab(0);
                  }}
                >
                  지점 불편신고
                </div>
                <div
                  className={`col-2 tab-btn ${
                    selectedTab === 1 ? "selected-tab" : ""
                  }`}
                  onClick={(e) => {
                    setSelectedTab(1);
                    getAppList();
                    setisCalendar(false);
                  }}
                >
                  앱 불편신고
                </div>
              </div>
              {selectedTab === 0 && (
                <>
                  <Table className="table-hover search-box">
                    <tbody>
                      <tr>
                        <th className="select-search-target">
                          <Form.Control
                            as="select"
                            value={searchTarget}
                            onChange={(e) => {
                              setSearchTarget(e.target.value);
                              if (e.target.value === "report_date") {
                                setKeyword("");
                                setisCalendar(true);
                              } else {
                                setKeyword("");
                                setisCalendar(false);
                              }
                            }}
                          >
                            <option value="user_name">고객명</option>
                            <option value="store_name">관리지점</option>
                            <option value="report_date">신고일자</option>
                            <option value="title">제목</option>
                          </Form.Control>
                        </th>
                        <th>
                          <Form.Control
                            type="text"
                            placeholder="검색"
                            value={keyword}
                            onChange={(e) => setKeyword(e.target.value)}
                            readOnly={isCalendar}
                            onKeyUp={(e) => {
                              if (e.keyCode === 13) {
                                setSelected(1);
                                getList({
                                  page: 0,
                                });
                              }
                            }}
                            maxLength={50}
                            style={{
                              display: "inline-block",
                              width: "500px",
                              verticalAlign: "middle",
                            }}
                          />

                          <Button
                            variant="light"
                            onClick={(e) => {
                              setSelected(1);
                              getList({
                                page: 0,
                              });
                            }}
                            style={{
                              display: "inline-block",
                              border: "solid 1px #E3E3E3",
                              color: "#565656",
                              verticalAlign: "middle",
                            }}
                            className="search-btn"
                          >
                            <FontAwesomeIcon icon={faSearch} />
                          </Button>
                        </th>
                      </tr>
                    </tbody>
                  </Table>
                  {isCalendar && (
                    <div style={{ marginBottom: "10px" }}>
                      <Calendar
                        className="react-calendar-custom"
                        onChange={(e) => getDate(e)}
                        value={date}
                        calendarType="US"
                        formatDay={(locale, date) => moment(date).format("D")}
                      />
                    </div>
                  )}
                  <Table className="table-hover mt-0">
                    <thead>
                      <tr>
                        <th className="border-0">NO.</th>
                        <th className="border-0">고객명</th>
                        <th className="border-0">관리지점</th>
                        <th className="border-0">신고일자</th>
                        <th className="border-0">제목</th>
                        <th className="border-0">상태</th>
                        <th className="border-0"></th>
                      </tr>
                    </thead>
                    <tbody>
                      {list !== undefined && list !== null ? (
                        list.map((report, i) => {
                          return (
                            <tr key={i} className="cursor">
                              <td>{i + 1 + 10 * pagenum}</td>
                              <td>{report.user_name}</td>
                              <td>{report.store_name}</td>
                              <td>
                                {report.report_createAt.replace("T", " ")}
                              </td>
                              <td>{report.title}</td>
                              <td>{report.state}</td>
                              <td>
                                <Button
                                  className="mr-3"
                                  onClick={() => {
                                    props.history.push(
                                      `${reportUrl.accessPath}/pages/reports/answer/${report.report_id}`
                                    );
                                  }}
                                >
                                  조회/답변
                                </Button>
                                <Button
                                  onClick={() => {
                                    deleteReport(report.report_id);
                                  }}
                                >
                                  삭제
                                </Button>
                              </td>
                            </tr>
                          );
                        })
                      ) : (
                        <tr>
                          <td colSpan={5} style={{ textAlign: "center" }}>
                            <Loading />
                          </td>
                        </tr>
                      )}
                    </tbody>
                  </Table>
                  <Pagination
                    count={count}
                    forcePage={selected - 1}
                    selected={(pageSelect) => {
                      setSelected(pageSelect);
                      getList({ page: pageSelect - 1 });
                    }}
                  />
                </>
              )}

              {selectedTab === 1 && (
                <>
                  <Table className="table-hover search-box">
                    <tbody>
                      <tr>
                        <th className="select-search-target">
                          <Form.Control
                            as="select"
                            value={appSearchTarget}
                            onChange={(e) => {
                              setAppSearchTarget(e.target.value);
                              if (e.target.value === "report_date")
                                setisCalendar(true);
                              else setisCalendar(false);
                            }}
                          >
                            <option value="user_name">고객명</option>
                            <option value="report_date">신고일자</option>
                            <option value="title">제목</option>
                          </Form.Control>
                        </th>
                        <th>
                          <Form.Control
                            type="text"
                            placeholder="검색"
                            value={appKeyword}
                            onChange={(e) => setAppKeyword(e.target.value)}
                            readOnly={isCalendar}
                            onKeyUp={(e) => {
                              if (e.keyCode === 13) {
                                setAppSelected(1);
                                getAppList({
                                  page: 0,
                                });
                              }
                            }}
                            maxLength={50}
                            style={{
                              display: "inline-block",
                              width: "500px",
                              verticalAlign: "middle",
                            }}
                          />
                          <Button
                            variant="light"
                            onClick={(e) => {
                              setAppSelected(1);
                              getAppList({
                                page: 0,
                              });
                            }}
                            style={{
                              display: "inline-block",
                              border: "solid 1px #E3E3E3",
                              color: "#565656",
                              verticalAlign: "middle",
                            }}
                            className="search-btn"
                          >
                            <FontAwesomeIcon icon={faSearch} />
                          </Button>
                        </th>
                      </tr>
                    </tbody>
                  </Table>
                  {isCalendar && (
                    <div style={{ marginBottom: "10px" }}>
                      <Calendar
                        className="react-calendar-custom"
                        onChange={(e) => getDate(e)}
                        value={date}
                        calendarType="US"
                        formatDay={(locale, date) => moment(date).format("D")}
                      />
                    </div>
                  )}
                  <Table className="table-hover mt-0">
                    <thead>
                      <tr>
                        <th className="border-0">NO.</th>
                        <th className="border-0">고객명</th>
                        {/* <th className="border-0"></th> */}
                        <th className="border-0">신고일자</th>
                        <th className="border-0">제목</th>
                        <th className="border-0">상태</th>
                        <th className="border-0"></th>
                      </tr>
                    </thead>
                    <tbody>
                      {appList !== undefined && appList !== null ? (
                        appList.map((report, i) => {
                          return (
                            <tr key={i} className="cursor">
                              <td>{i + 1 + 10 * appPagenum}</td>
                              <td>{report.user_name}</td>
                              {/* <td></td> */}
                              <td>{report.report_date.replace("T", " ")}</td>
                              <td>{report.title}</td>
                              <td>{report.state}</td>
                              <td>
                                <Button
                                  className="mr-3"
                                  onClick={() => {
                                    props.history.push(
                                      `${reportUrl.accessPath}/pages/reports/answer/${report.report_id}`
                                    );
                                  }}
                                >
                                  조회/답변
                                </Button>
                                <Button
                                  onClick={() => {
                                    deleteReport(report.report_id);
                                  }}
                                >
                                  삭제
                                </Button>
                              </td>
                            </tr>
                          );
                        })
                      ) : (
                        <tr>
                          <td colSpan={5} style={{ textAlign: "center" }}>
                            <Loading />
                          </td>
                        </tr>
                      )}
                    </tbody>
                  </Table>
                  <Pagination
                    count={appCount}
                    forcePage={appSelected - 1}
                    selected={(pageSelect) => {
                      setAppSelected(pageSelect);
                      getAppList({ page: pageSelect - 1 });
                    }}
                  />
                </>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
