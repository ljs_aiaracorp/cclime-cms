import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import axios from "axios";
import { axiosUrlFunction } from "utils/AxiosUrl";

export const AnswerReport = (props) => {
  const answerUrl = axiosUrlFunction("report", "검색");
  const config = {
    headers: {
      Authorization: `Bearer ${answerUrl.token}`,
      "Content-Type": `application/json`,
    },
  };

  const reportNumber = props.match.params.id; // report id

  const [title, setTitle] = useState(""); // 제목
  const [writer, setWriter] = useState(""); // 작성자
  const [date, setDate] = useState(""); // 신고일자
  const [storeName, setStoreName] = useState(""); // 관리지점
  const [description, setDescription] = useState(""); // 신고내용
  const [answer, setAnswer] = useState(""); // 답변내용
  const [isBranch, setIsBranch] = useState(true); // true = 지점 false = 앱

  // 불편신고 내용
  const getUserAnswer = async () => {
    axios
      .get(`${answerUrl.apiUrl}?report_id=${reportNumber}`, config)
      .then(async (res) => {
        setTitle(res.data.data.report_title);
        setWriter(res.data.data.user_name);
        setDate(res.data.data.temporarily);
        if (res.data.data.store_name !== null) {
          setStoreName(res.data.data.store_name);
          setIsBranch(true);
        } else {
          setIsBranch(false);
        }
        setDescription(res.data.data.report_content);
        setAnswer(res.data.data.report_answer);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getUserAnswer();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 불편신고 답변등록
  const sendAnswer = async () => {
    const sendUrl = axiosUrlFunction("answer", "검색");

    if (answer === null || answer === "") {
      alert("답변을 작성해 주세요.");
      return;
    }

    const data = {
      report_id: parseInt(reportNumber),
      answer,
    };

    axios
      .put(`${sendUrl.apiUrl}`, data, config)
      .then(async (res) => {
        props.history.push(`${answerUrl.accessPath}/pages/reports/list`);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else sendAnswer();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  useEffect(() => {
    getUserAnswer();
  }, []);

  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">
                페이지관리 - 불편신고 - 조회/답변(
                {isBranch ? "지점신고" : "앱 신고"})
              </Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <Table className="table-hover">
                <tbody>
                  <tr>
                    <th>제목</th>
                    <td>
                      <input
                        className="form-control"
                        type="text"
                        required
                        placeholder="제목"
                        defaultValue={title}
                        readOnly={true}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>작성자</th>
                    <td>
                      <input
                        className="form-control"
                        type="text"
                        required
                        placeholder="작성자"
                        defaultValue={writer}
                        readOnly={true}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>신고일자</th>
                    <td>
                      <input
                        className="form-control"
                        type="datetime-local"
                        required
                        placeholder="신고일자"
                        defaultValue={date}
                        readOnly={true}
                      />
                    </td>
                  </tr>
                  {isBranch && (
                    <tr>
                      <th>관리지점</th>
                      <td>
                        <input
                          className="form-control"
                          type="text"
                          required
                          placeholder="관리지점"
                          defaultValue={storeName}
                          readOnly={true}
                        />
                      </td>
                    </tr>
                  )}
                  <tr>
                    <th>신고내용</th>
                    <td>
                      <textarea
                        className="form-control"
                        defaultValue={description}
                        readOnly={true}
                        style={{
                          minHeight: "500px",
                        }}
                      ></textarea>
                    </td>
                  </tr>
                  <tr>
                    <th>답변내용</th>
                    <td>
                      <textarea
                        className="form-control"
                        defaultValue={answer}
                        onChange={(e) => {
                          setAnswer(e.target.value);
                        }}
                        style={{
                          minHeight: "500px",
                        }}
                      ></textarea>
                    </td>
                  </tr>
                </tbody>
              </Table>
              <div className="text-center">
                <Button onClick={sendAnswer}>저장</Button>
                <Button
                  className="ml-2"
                  onClick={() => {
                    props.history.push(
                      `${answerUrl.accessPath}/pages/reports/list`
                    );
                  }}
                >
                  취소
                </Button>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
