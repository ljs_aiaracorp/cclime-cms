import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import Pagination from "components/Pagination";
import Loading from "components/Loading";
import moment from "moment";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";

export const Event = (props) => {
  const eventListUrl = axiosUrlFunction("eventList", "검색");

  const [selectedTab, setSelectedTab] = useState(0);

  const [selectedYear, setSelectedYear] = useState(
    Number(moment().format("yyyy"))
  );
  const [selectedMonth, setSelectedMonth] = useState(
    Number(moment().format("MM"))
  );

  const [selected, setSelected] = useState(1);

  const [list, setList] = useState([]);

  const [pagenum, setPagenum] = useState("");
  const [keyword, setKeyword] = useState("");
  const [count, setCount] = useState(0);
  const [count0, setCount0] = useState(0);
  const [count1, setCount1] = useState(0);
  const [count2, setCount2] = useState(0);
  const [searchParam, setSearchParam] = useState({});

  let tab2Index = 0;
  let tab3Index = 0;

  useEffect(() => {
    getList();
  }, []);

  const getList = async (params) => {
    setList(null);
    if (!params) {
      params = {};
    }
    if (params.keyword === undefined) {
      params.keyword = keyword;
    }
    if (params.page === undefined) {
      params.page = 0;
    }
    setPagenum(params.page);
    setSearchParam(params);

    const queries = [];
    if (params.keyword !== "") {
      queries.push(`word=${params.keyword}`);
    }
    queries.push(`page=${params.page}`);
    queries.push(`size=10`);

    const queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";

    axios
      .get(`${eventListUrl.apiUrl}${queryStr}`, {
        headers: {
          Authorization: `Bearer ${eventListUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        let i = 0;
        let j = 0;
        setList(res.data.data.content);

        res.data.data.content.map((value) => {
          if (value.state === "진행") {
            i++;
          } else if (value.state === "종료") {
            j++;
          }
        });

        if (params.tab === undefined) {
          setCount(res.data.data.totalElements);
        } else {
          if (params.tab === 0) {
            setCount(res.data.data.totalElements);
          } else if (params.tab === 1) {
            setCount(i);
          } else if (params.tab === 2) {
            setCount(j);
          }
        }
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getList(params);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 이벤트 삭제
  const deleteEvent = async (id) => {
    const deleteUrl = axiosUrlFunction("event/delete", "검색");
    const data = {
      event_id: id,
    };

    axios
      .put(`${deleteUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${deleteUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(() => {
        alert("이벤트 삭제가 완료되었습니다.");
        window.location.replace(`${deleteUrl.accessPath}/pages/events/list`);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else deleteEvent(id);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };
  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">페이지관리 - 이벤트</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <div className="row">
                <div
                  className={`col-2 tab-btn ${
                    selectedTab === 0 ? "selected-tab" : ""
                  }`}
                  onClick={(e) => {
                    setSelected(1);
                    setSelectedTab(0);
                    getList({
                      page: 0,
                      tab: 0,
                    });
                  }}
                >
                  전체이벤트
                </div>
                <div
                  className={`col-2 tab-btn ${
                    selectedTab === 1 ? "selected-tab" : ""
                  }`}
                  onClick={(e) => {
                    setSelectedTab(1);
                    setSelected(1);
                    getList({
                      page: 0,
                      tab: 1,
                    });
                  }}
                >
                  진행중인 이벤트
                </div>
                <div
                  className={`col-2 tab-btn ${
                    selectedTab === 2 ? "selected-tab" : ""
                  }`}
                  onClick={(e) => {
                    setSelectedTab(2);
                    setSelected(1);
                    getList({
                      page: 0,
                      tab: 2,
                    });
                  }}
                >
                  종료된 이벤트
                </div>
              </div>
              <Table className="table-hover search-box">
                <tbody>
                  <tr>
                    <th className="select-search-target">
                      <Form.Control as="select">
                        <option value="name">제목</option>
                      </Form.Control>
                    </th>
                    <th>
                      <Form.Control
                        type="text"
                        placeholder="검색"
                        value={keyword}
                        onChange={(e) => setKeyword(e.target.value)}
                        onKeyUp={(e) => {
                          if (e.keyCode === 13) {
                            setSelected(1);
                            getList({
                              page: 0,
                            });
                          }
                        }}
                        maxLength={50}
                        style={{
                          display: "inline-block",
                          width: "500px",
                          verticalAlign: "middle",
                        }}
                      />
                      <Button
                        variant="light"
                        onClick={(e) => {
                          setSelected(1);
                          getList({
                            page: 0,
                          });
                        }}
                        style={{
                          display: "inline-block",
                          border: "solid 1px #E3E3E3",
                          color: "#565656",
                          verticalAlign: "middle",
                        }}
                        className="search-btn"
                      >
                        <FontAwesomeIcon icon={faSearch} />
                      </Button>
                    </th>
                  </tr>
                </tbody>
              </Table>
              <div className="text-right">
                <Button
                  onClick={(e) => {
                    window.location.replace(`/admin/pages/events/add`);
                  }}
                >
                  이벤트 추가
                </Button>
              </div>
              <table className="table-hover mt-0 table-center">
                <thead>
                  <tr>
                    <th className="border-0">NO.</th>
                    <th className="border-0">진행기간</th>
                    <th className="border-0">이벤트명</th>
                    <th className="border-0">상태</th>
                    <th className="border-0"></th>
                  </tr>
                </thead>
                <tbody>
                  {list !== undefined && list !== null ? (
                    list.map((event, i) => {
                      if (selectedTab === 0) {
                        return (
                          <tr key={i} className="cursor">
                            <td>{i + 1 + 10 * pagenum}</td>
                            <td>
                              {event.start.slice(0, 10)} ~{" "}
                              {event.end.slice(0, 10)}
                            </td>
                            <td>{event.title}</td>
                            <td>{event.state}</td>
                            <td>
                              <Button
                                className="mr-3"
                                onClick={() =>
                                  props.history.push(
                                    `/admin/pages/events/edit/${event.event_id}`
                                  )
                                }
                              >
                                조회/변경
                              </Button>
                              <Button
                                onClick={() => deleteEvent(event.event_id)}
                              >
                                삭제
                              </Button>
                            </td>
                          </tr>
                        );
                      } else if (selectedTab === 1 && event.state === "진행") {
                        tab2Index++;
                        return (
                          <tr key={i} className="cursor">
                            <td>{tab2Index + 10 * pagenum}</td>
                            <td>
                              {event.start.slice(0, 10)} ~{" "}
                              {event.end.slice(0, 10)}
                            </td>
                            <td>{event.title}</td>
                            <td>{event.state}</td>
                            <td>
                              <Button
                                className="mr-3"
                                onClick={() =>
                                  props.history.push(
                                    `/admin/pages/events/edit/${event.event_id}`
                                  )
                                }
                              >
                                조회/변경
                              </Button>
                              <Button
                                onClick={() => deleteEvent(event.event_id)}
                              >
                                삭제
                              </Button>
                            </td>
                          </tr>
                        );
                      } else if (selectedTab === 2 && event.state === "종료") {
                        tab3Index++;
                        return (
                          <tr key={i} className="cursor">
                            <td>{tab3Index + 10 * pagenum}</td>
                            <td>
                              {event.start.slice(0, 10)} ~{" "}
                              {event.end.slice(0, 10)}
                            </td>
                            <td>{event.title}</td>
                            <td>{event.state}</td>
                            <td>
                              <Button
                                className="mr-3"
                                onClick={() =>
                                  props.history.push(
                                    `/admin/pages/events/edit/${event.event_id}`
                                  )
                                }
                              >
                                조회/변경
                              </Button>
                              <Button
                                onClick={() => deleteEvent(event.event_id)}
                              >
                                삭제
                              </Button>
                            </td>
                          </tr>
                        );
                      }
                    })
                  ) : (
                    <tr>
                      <td colSpan={5} style={{ textAlign: "center" }}>
                        <Loading />
                      </td>
                    </tr>
                  )}
                </tbody>
              </table>
              <Pagination
                count={count}
                forcePage={selected - 1}
                selected={(pageSelect) => {
                  setSelected(pageSelect);
                  getList({ page: pageSelect - 1, tab: selectedTab });
                }}
              />
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
