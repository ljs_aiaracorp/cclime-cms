import { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import Pagination from "components/Pagination";
import Loading from "components/Loading";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";

export const FAQ = (props) => {
  const [selected, setSelected] = useState(1);

  const [keyword, setKeyword] = useState("");
  const [count, setCount] = useState(0);
  const [searchParam, setSearchParam] = useState({});

  const [list, setList] = useState([]);
  const [pagenum, setPagenum] = useState("");

  const faqUrl = axiosUrlFunction("faqList", "검색");

  useEffect(() => {
    getList();
  }, []);

  const getList = async (params) => {
    setList(null);
    if (!params) {
      params = {};
    }
    if (params.keyword === undefined) {
      params.keyword = keyword;
    }
    if (params.page === undefined) {
      params.page = selected - 1;
    }
    setPagenum(params.page);
    setSearchParam(params);

    const queries = [];
    if (params.keyword !== "") {
      queries.push(`word=${params.keyword}`);
    }
    queries.push(`page=${params.page}`);
    queries.push(`size=10`);

    const queryStr = queries.length > 0 ? `?${queries.join("&")}` : "";

    axios
      .get(`${faqUrl.apiUrl}${queryStr}`, {
        headers: {
          Authorization: `Bearer ${faqUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        setCount(res.data.data.totalElements);
        setList(res.data.data.content);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getList(params);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // faq delete
  const deleteFAQ = async (id) => {
    const deleteNoticeUrl = axiosUrlFunction("notice/delete", "검색");
    const data = {
      notice_id: id,
    };

    axios
      .put(`${deleteNoticeUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${deleteNoticeUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async () => {
        window.location.replace(`${deleteNoticeUrl.accessPath}/pages/faq/list`);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else deleteFAQ(id);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">페이지관리 - 자주묻는질문</Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <Table className="table-hover search-box">
                <tbody>
                  <tr>
                    <th className="select-search-target">
                      <Form.Control as="select">
                        <option value="name">제목</option>
                      </Form.Control>
                    </th>
                    <th>
                      <Form.Control
                        type="text"
                        placeholder="검색"
                        maxLength={50}
                        value={keyword}
                        onChange={(e) => setKeyword(e.target.value)}
                        onKeyUp={(e) => {
                          if (e.keyCode === 13) {
                            setSelected(1);
                            getList({
                              page: 0,
                            });
                          }
                        }}
                        style={{
                          display: "inline-block",
                          width: "500px",
                          verticalAlign: "middle",
                        }}
                      />
                      <Button
                        variant="light"
                        onClick={(e) => {
                          setSelected(1);
                          getList({
                            page: 0,
                          });
                        }}
                        style={{
                          display: "inline-block",
                          border: "solid 1px #E3E3E3",
                          color: "#565656",
                          verticalAlign: "middle",
                        }}
                        className="search-btn"
                      >
                        <FontAwesomeIcon icon={faSearch} />
                      </Button>
                    </th>
                  </tr>
                </tbody>
              </Table>
              <div className="text-right">
                <Button
                  onClick={(e) => {
                    window.location.replace(`/admin/pages/faq/add`);
                  }}
                >
                  자주묻는질문 작성
                </Button>
              </div>
              <table className="table-hover mt-0 table-center">
                <thead>
                  <tr>
                    <th className="border-0">NO.</th>
                    <th className="border-0">제목</th>
                    <th className="border-0"></th>
                  </tr>
                </thead>
                <tbody>
                  {list !== undefined && list !== null ? (
                    list.map((faq, i) => {
                      return (
                        <tr key={i} className="cursor">
                          <td>{i + 1 + 10 * pagenum}</td>
                          <td>{faq.title}</td>
                          <td>
                            <Button
                              className="mr-3"
                              onClick={() => {
                                props.history.push(
                                  `/admin/pages/faq/edit/${faq.id}`
                                );
                              }}
                            >
                              변경
                            </Button>

                            <Button onClick={() => deleteFAQ(faq.id)}>
                              삭제
                            </Button>
                          </td>
                        </tr>
                      );
                    })
                  ) : (
                    <tr>
                      <td colSpan={5} style={{ textAlign: "center" }}>
                        <Loading />
                      </td>
                    </tr>
                  )}
                </tbody>
              </table>
              <Pagination
                count={count}
                forcePage={selected - 1}
                selected={(pageSelect) => {
                  setSelected(pageSelect);
                  getList({ page: pageSelect - 1 });
                }}
              />
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
