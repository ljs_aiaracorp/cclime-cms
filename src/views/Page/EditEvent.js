import { useEffect, useState, useMemo, useRef } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Table,
  Button,
} from "react-bootstrap";
import { FileBtn } from "components/FileUpload";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";
import { TextEditer } from "utils/TextEditer";

export const EditEvent = (props) => {
  const [title, setTitle] = useState(""); // 이벤트명
  const [startDay, setStartDay] = useState(""); // 시작기간
  const [endDay, setEndDay] = useState(""); // 종료기간
  const [picture, setPicture] = useState(""); // 업로드 사진
  const [pictureUrl, setPictureUrl] = useState(""); // 기존 사진
  const [description, setDescription] = useState(""); // 이벤트 내용
  const [comment, setComment] = useState([]); // 댓글 On/Off
  const [isComment, setisComment] = useState(true); // 댓글보기 On/Off
  const [commentList, setCommentList] = useState([]); // 댓글 내용
  const [commentReply, setCommentReply] = useState(""); // 답글 내용
  const [commnetCount, setCommnetCount] = useState(0); // 댓글 수
  const [likeCount, setLikeCount] = useState(0); // 좋아요 수

  const eventNumver = props.match.params.id; // event ID
  const back = axiosUrlFunction("", "검색");

  let sw = 0;

  const getList = async () => {
    const eventListUrl = axiosUrlFunction("event", "검색");
    axios
      .get(`${eventListUrl.apiUrl}?event_id=${eventNumver}`, {
        headers: {
          Authorization: `Bearer ${eventListUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then((res) => {
        const startEvent = res.data.data.event.start.slice(0, 10);
        const endEvent = res.data.data.event.end.slice(0, 10);

        setTitle(res.data.data.event.title);
        setStartDay(startEvent);
        setEndDay(endEvent);
        setPictureUrl(res.data.data.event.img);
        setDescription(res.data.data.event.event_content);
        setComment(res.data.data.event.comment_enable.toLowerCase());
        setCommnetCount(res.data.data.event.comment_count);
        setCommentList(res.data.data.commentReply);
        setLikeCount(res.data.data.event.like_count);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getList();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  const sendEventData = async () => {
    const formUrl = axiosUrlFunction("event", "검색");

    if (title === "") {
      alert("이벤트명을 입력해주세요.");
      return;
    } else if (startDay === "") {
      alert("시작기간을 선택해 주세요.");
      return;
    } else if (endDay === "") {
      alert("끝나는 시간을 선택해 주세요.");
      return;
    } else if (description === "") {
      alert("이벤트 내용을 입력해 주세요.");
      return;
    }
    if (
      parseInt(startDay.replace(/\-/g, "")) >
      parseInt(endDay.replace(/\-/g, ""))
    ) {
      alert("이벤트 시작기간을 다시 설정해 주세요.");
      return;
    }

    const formData = new FormData();

    formData.append("event_id", parseInt(eventNumver));
    if (picture !== "") {
      formData.append("images", picture[0]);
    }
    formData.append("start", startDay);
    formData.append("end", endDay);
    formData.append("title", title);
    formData.append("content", description);
    formData.append("comment_enable", comment);

    axios
      .put(`${formUrl.apiUrl}`, formData, {
        headers: {
          Authorization: `Bearer ${formUrl.token}`,
          "Content-Type": `multipart/form-data`,
        },
      })
      .then(async (res) => {
        alert("이벤트 수정을 완료했습니다.");
        props.history.push(`/admin/pages/events/list`);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else sendEventData();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 이벤트 페이지 삭제
  const deleteEvent = async () => {
    const deleteUrl = axiosUrlFunction("event/delete", "검색");

    const data = {
      event_id: parseInt(eventNumver),
    };

    axios
      .put(`${deleteUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${deleteUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(() => {
        alert("이벤트 삭제가 완료되었습니다.");
        window.location.replace(`${deleteUrl.accessPath}/pages/events/list`);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else deleteEvent();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // New 답글 작성
  const getReply = (id) => {
    // if (sw === 0) {
    let value = "";
    const div = document.getElementById(id);
    const li = document.createElement("li");
    const textarea = document.createElement("textarea");
    const button = document.createElement("input");

    li.className = "comment";
    li.id = "new-reply";
    li.style.display = "flex";
    li.style.alignItems = "center";
    li.style.justifyContent = "flex-end";

    textarea.id = id;
    textarea.style.resize = "none";
    textarea.style.marginBottom = "10px";
    textarea.style.width = "900px";
    textarea.style.height = "100px";
    textarea.style.fontSize = "18px";
    textarea.style.outline = "none";

    button.type = "button";
    button.value = "작성";
    button.style.width = "80px";
    button.style.height = "50px";
    button.style.backgroundColor = "black";
    button.style.color = "white";
    button.style.marginLeft = "50px";

    li.appendChild(textarea);
    li.appendChild(button);
    div.appendChild(li);

    textarea.addEventListener("input", (event) => {
      value = event.target.value;
    });
    button.addEventListener("click", (event) => {
      setReply(id, value);
      textarea.remove();
      li.remove();
    });

    //   sw = 1;
    // }
  };

  // New 답글 저장
  const setReply = async (id, value) => {
    const replyUrl = axiosUrlFunction("reply", "검색");
    const li = document.getElementById("new-reply");
    li.style.display = "none";

    const data = {
      comment_id: id,
      content: value,
    };

    axios
      .post(`${replyUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${replyUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        setCommentReply(value);
        getList();
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else setReply(id, value);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 답글 수정 창
  const changeReply = (commentId, replyId, value) => {
    const div = document.getElementById(commentId);
    const liReply = document.getElementById(replyId);
    liReply.style.display = "none";

    const li = document.createElement("li");
    const textarea = document.createElement("textarea");
    const button = document.createElement("input");

    li.className = "comment";
    li.id = "new-reply";
    li.style.display = "flex";
    li.style.alignItems = "center";
    li.style.justifyContent = "flex-end";

    textarea.style.resize = "none";
    textarea.style.marginBottom = "10px";
    textarea.style.width = "900px";
    textarea.style.height = "100px";
    textarea.style.fontSize = "18px";
    textarea.style.outline = "none";
    textarea.innerHTML = value;

    button.type = "button";
    button.value = "작성";
    button.style.width = "80px";
    button.style.height = "50px";
    button.style.backgroundColor = "black";
    button.style.color = "white";
    button.style.marginLeft = "50px";

    li.appendChild(textarea);
    li.appendChild(button);
    div.appendChild(li);

    textarea.addEventListener("input", (event) => {
      value = event.target.value;
    });
    button.addEventListener("click", (event) => {
      putReply(replyId, value);
      textarea.remove();
      li.remove();
      liReply.style.display = "block";
    });
  };

  // 답글 수정
  const putReply = async (id, value) => {
    const putUrl = axiosUrlFunction("reply", "검색");

    const data = {
      reply_id: id,
      content: value,
    };

    axios
      .put(`${putUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${putUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async () => {
        setCommentReply(value);
        getList();
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else putReply(id, value);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 답글 삭제
  const deleteReply = async (id) => {
    const deleteReplyUrl = axiosUrlFunction("reply/delete", "검색");
    const data = {
      reply_id: id,
    };
    axios
      .put(`${deleteReplyUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${deleteReplyUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async () => {
        getList();
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else deleteReply(id);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  // 댓글 삭제
  const deleteComment = async (id) => {
    const deleteUrl = axiosUrlFunction("comment/delete", "검색");
    const data = {
      comment_id: id,
    };
    axios
      .put(`${deleteUrl.apiUrl}`, data, {
        headers: {
          Authorization: `Bearer ${deleteUrl.token}`,
          "Content-Type": `application/json`,
        },
      })
      .then(async (res) => {
        getList();
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else deleteComment(id);
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };
  useEffect(() => {
    getList();
  }, []);
  return (
    <Container fluid>
      <Row>
        <Col md="12">
          <Card className="card-plain table-plain-bg">
            <Card.Header>
              <Card.Title as="h4">
                페이지관리 - 이벤트 - 이벤트 조회/변경
              </Card.Title>
            </Card.Header>
            <Card.Body className="table-full-width table-responsive px-0">
              <Table className="table-hover">
                <tbody>
                  <tr>
                    <th>이벤트명</th>
                    <td>
                      <input
                        className="form-control"
                        type="text"
                        required
                        placeholder="이벤트명"
                        value={title}
                        onChange={(e) => setTitle(e.target.value)}
                        maxLength={50}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>기간</th>
                    <td>
                      <div className="row">
                        <div className="col-5">
                          <Form.Group as={Row} className="mb-0">
                            <Col sm={5} className="pr-0">
                              <Form.Control
                                className="mb-0"
                                type="date"
                                placeholder="시작 날짜"
                                value={startDay}
                                onChange={(e) => setStartDay(e.target.value)}
                              />
                            </Col>
                            <Col
                              sm={1}
                              style={{ fontSize: "24px", textAlign: "center" }}
                            >
                              ~
                            </Col>
                            <Col sm={5} className="pl-0">
                              <Form.Control
                                className="mb-0"
                                type="date"
                                placeholder="종료 날짜"
                                value={endDay}
                                onChange={(e) => setEndDay(e.target.value)}
                              />
                            </Col>
                          </Form.Group>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>사진</th>
                    <td>
                      <FileBtn
                        name="업로드"
                        fileData={(data) => {
                          setPicture(data);
                        }}
                        imageUrl={pictureUrl}
                        accept="image/*"
                        id="picture"
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>이벤트 내용</th>
                    <td>
                      <div
                        style={{
                          width: "950px",
                          height: "650px",
                          color: "black",
                        }}
                      >
                        <TextEditer
                          value={description || ""}
                          onChange={setDescription}
                        />
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>댓글</th>
                    <td>
                      <Button
                        className={
                          comment === "y" ? "select-btn selected" : "select-btn"
                        }
                        onClick={(e) => {
                          setisComment(true);
                          setComment("y");
                        }}
                      >
                        ON
                      </Button>
                      <Button
                        className={
                          comment === "y" ? "select-btn" : "select-btn selected"
                        }
                        onClick={(e) => {
                          setisComment(false);
                          setComment("n");
                        }}
                      >
                        OFF
                      </Button>
                      좋아요{" "}
                      <span style={{ color: "#e20346" }}>{likeCount}</span>개
                    </td>
                  </tr>
                </tbody>
              </Table>
              {isComment && (
                <>
                  <h4>댓글내역 {commnetCount}</h4>
                  <ul style={{ listStyle: "none", padding: "0px" }}>
                    {commentList.map((comment, index) => {
                      if (comment.reply_id !== null) {
                        return (
                          <div key={index} id={comment.comment_id}>
                            <li className="comment" key={comment.reply_id}>
                              <div>
                                <Button
                                  onClick={() => {
                                    deleteComment(comment.comment_id);
                                  }}
                                  className="float-right ml-2"
                                >
                                  삭제
                                </Button>
                                <div className="comment-writer">
                                  작성자:{comment.name}
                                </div>
                                <div className="comment-date">
                                  {comment.comment_at.slice(0, 10)}
                                </div>
                                <div className="comment-content">
                                  {comment.comment_content}
                                </div>
                              </div>
                            </li>
                            <li className="comment" id={comment.reply_id}>
                              <div>
                                <Button
                                  className="float-right ml-2"
                                  onClick={() => deleteReply(comment.reply_id)}
                                >
                                  삭제
                                </Button>
                                <Button
                                  className="float-right"
                                  onClick={() =>
                                    changeReply(
                                      comment.comment_id,
                                      comment.reply_id,
                                      comment.reply_content
                                    )
                                  }
                                >
                                  수정
                                </Button>
                                <div className="comment-writer">
                                  <span style={{ fontWeight: "bold" }}>
                                    ㄴ 관리자 댓글
                                  </span>
                                </div>
                                <div className="comment-date">
                                  {comment.reply_at.slice(0, 10)}
                                </div>
                                <div className="comment-content">
                                  {comment.reply_content}
                                </div>
                              </div>
                            </li>
                          </div>
                        );
                      } else {
                        return (
                          <div key={index} id={comment.comment_id}>
                            <li className="comment" key={index}>
                              <div>
                                <Button
                                  onClick={() => {
                                    deleteComment(comment.comment_id);
                                  }}
                                  className="float-right ml-2"
                                >
                                  삭제
                                </Button>
                                <Button
                                  className="float-right"
                                  onClick={() => getReply(comment.comment_id)}
                                >
                                  답글
                                </Button>
                                <div className="comment-writer">
                                  작성자:{comment.name}
                                </div>
                                <div className="comment-date">
                                  {comment.comment_at.slice(0, 10)}
                                </div>
                                <div className="comment-content">
                                  {comment.comment_content}
                                </div>
                              </div>
                            </li>
                          </div>
                        );
                      }
                    })}
                  </ul>
                </>
              )}
              <div className="text-center">
                <Button className="ml-2" onClick={() => deleteEvent()}>
                  삭제
                </Button>
                <Button className="ml-2" onClick={sendEventData}>
                  저장
                </Button>
                <Button
                  className="ml-2"
                  onClick={() => {
                    window.location.replace(
                      `${back.accessPath}/pages/events/list`
                    );
                  }}
                >
                  취소
                </Button>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
