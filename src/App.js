import { BrowserRouter, Route, Switch } from "react-router-dom";

import "./views/css/common.scss";

import AdminLayout from "./layouts/Admin";
import { Login } from "views/Login/Login";
import { Main } from "views/Main/Main";
import { useSelector } from "react-redux";

const App = ({ location }) => {
  const loginUser = useSelector((state) => state.login.admin);
  let accessPathName = null;

  if (loginUser) {
    const loginUserAccess = loginUser.data.role;
    if (loginUserAccess === "main") {
      accessPathName = "/admin";
    } else {
      accessPathName = "/staff";
    }
  } else {
    accessPathName = "/admin";
  }

  // console.log(admin);
  // if (window.location.pathname !== "/login") {
  //   if (!isLoggedIn || !admin) {
  //     window.location.href = '/login';
  //     return <></>;
  //   }
  // }
  // if (window.location.pathname === "/") {
  //   if (isLoggedIn && admin) {
  //     if (!admin.role) {
  //       window.location.href = "/admin/assets/list";
  //       return <></>;
  //     } else {
  //       if (admin.role.includes('asset')) {
  //         window.location.href = "/admin/assets/list";
  //         return <></>;
  //       } else if (admin.role.includes('offline')) {
  //         window.location.href = "/admin/offline/list";
  //         return <></>;
  //       } else if (admin.role.includes('online')) {
  //         window.location.href = "/admin/online/list";
  //         return <></>;
  //       } else if (admin.role.includes('sampling')) {
  //         window.location.href = "/admin/samplings/list";
  //         return <></>;
  //       } else if (admin.role.includes('code')) {
  //         window.location.href = "/admin/codes";
  //         return <></>;
  //       } else if (admin.role.includes('statistics')) {
  //         window.location.href = "/admin/statistics";
  //         return <></>;
  //       } else if (admin.role.includes('log')) {
  //         window.location.href = "/admin/logs";
  //         return <></>;
  //       } else if (admin.role.includes('permission')) {
  //         window.location.href = "/admin/permissions";
  //         return <></>;
  //       }
  //     }
  //   } else {
  //     window.location.href = "/login";
  //     return <></>;
  //   }
  // }
  if (window.location.pathname === "/") {
    window.location.href = "/admin/users/list";
    return <></>;
  }

  return (
    <BrowserRouter>
      <Route path="/login" component={Login} />
      <Route path="/main" component={Main} />
      <Switch>
        <Route
          path={accessPathName}
          render={(props) => <AdminLayout {...props} />}
        />
      </Switch>
    </BrowserRouter>
  );
};
//
export default App;
