import { useRef, useState, useEffect } from "react";
import { Form, Col, Row, Button } from "react-bootstrap";
import { text } from "views/css/style";

const acceptedImageExt = ["png", "bmp", "gif", "jpg", "jpeg", "webp"];

const FileUpload = (props) => {
  const { fileValue, prevFileList } = props;
  const [fileList, setFileList] = useState([]);
  const hiddenFileInput = useRef(null);

  const clickToFileSelect = (e) => {
    hiddenFileInput.current.click();
  };

  const checkExt = (filename) => {
    if (typeof filename !== "string") {
      return checkExt(filename.name);
    }
    const ext = filename.split(".").pop().toLowerCase();
    return acceptedImageExt.filter((e) => e === ext).length > 0;
  };

  const handleChangeFile = (event) => {
    if (event.target.files) {
      if (props.checkImageExt) {
        for (let i = 0; i < event.target.files.length; i++) {
          const file = event.target.files[i];
          if (!checkExt(file)) {
            alert(
              "png, bmp, gif, jpg, jpeg, jfif, webp 확장자 이미지 파일만 등록 가능합니다."
            );
            setFileList([]);
            props.fileData([]);
            return;
          }
        }
      }
      setFileList(event.target.files);
      props.fileData(event.target.files);
    }
  };

  const getValueOfFileName = () => {
    let file = [];
    for (let i = 0; i < fileList.length; i++) {
      file.push(
        <span key={i} className="mr-2">
          {fileList[i].name}
        </span>
      );
    }
    if (fileList.length === 0) {
      if (prevFileList !== undefined) {
        for (let i = 0; i < prevFileList.length; i++) {
          if (typeof prevFileList[i] === "string") {
            const filename = decodeURI(prevFileList[i]).split("/").pop();
            file.push(
              <span key={i} className="mr-2">
                {filename}
              </span>
            );
          }
        }
      }
      if (fileValue !== undefined) {
        if (typeof fileValue === "string") {
          const filename = decodeURI(fileValue).split("/").pop();
          file.push(<span className="mr-2">{filename}</span>);
        }
      }
    }
    return file;
  };

  return (
    <Form.Group as={Row}>
      {props.push && text()}
      {text(props.name)}
      <Col
        sm={
          props.colSize ??
          (props.onDelete && (fileList || prevFileList) ? 1 : 2)
        }
      >
        <Button onClick={clickToFileSelect}>업로드</Button>
        {props.accept ? (
          <input
            type="file"
            ref={hiddenFileInput}
            onChange={handleChangeFile}
            style={{ display: "none" }}
            multiple={props.multiple}
            accept={props.accept}
          />
        ) : (
          <input
            type="file"
            ref={hiddenFileInput}
            onChange={handleChangeFile}
            style={{ display: "none" }}
            multiple={props.multiple}
          />
        )}
      </Col>
      {props.onDelete && (fileList || prevFileList) && (
        <Col sm={props.colSize ?? 1}>
          <Button onClick={props.onDelete} className="cancel">
            삭제
          </Button>
        </Col>
      )}
      {!props.notShowFile && (
        <Col sm={7} className="mt-2 pt-2">
          {fileList ? (
            <div className="border-bottom">
              {"첨부파일: "}
              {getValueOfFileName()}
            </div>
          ) : null}
        </Col>
      )}
    </Form.Group>
  );
};

export const FileBtn = (props) => {
  const [fileList, setFileList] = useState("");
  const hiddenFileInput = useRef(null);

  const [first, setFirst] = useState(true);

  useEffect(() => {
    if (props.clickOnAdd) {
      if (first) {
        clickToFileSelect();
        setFirst(false);
      }
      s;
    }
  }, []);

  const clickToFileSelect = (e) => {
    hiddenFileInput.current.click();
  };

  const handleChangeFile = (event) => {
    if (event.target.files[0]) {
      setFileList(event.target.files);
      props.fileData(event.target.files);

      const reader = new FileReader();

      reader.onloadend = (e) => {
        const previewImage = document.getElementById(props.id);
        previewImage.style.display = "block";
        previewImage.src = e.target.result;
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  };

  const getValueOfFileName = () => {
    let file = [];
    for (let i = 0; i < fileList.length; i++) {
      file.push(
        <span key={i} className="mr-2">
          {fileList[i].name}
        </span>
      );
    }
    return file;
  };

  return (
    <>
      {props.imageUrl !== "" ? (
        <img
          style={{
            width: "300px",
            height: "300px",
            display: "block",
            marginBottom: "10px",
          }}
          src={props.imageUrl}
          id={props.id}
        />
      ) : (
        <img
          style={{
            width: "300px",
            height: "300px",
            display: "none",
            marginBottom: "10px",
          }}
          id={props.id}
        />
      )}
      <Button className={props.cls} onClick={clickToFileSelect}>
        {props.name}
      </Button>
      <input
        type="file"
        ref={hiddenFileInput}
        onChange={handleChangeFile}
        style={{ display: "none" }}
        multiple={props.multiple}
        accept={props.accept}
      />
    </>
  );
};

export default FileUpload;
