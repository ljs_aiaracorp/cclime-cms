import { Button } from "react-bootstrap";
import xlsx from "xlsx";

const arr = [
  { age: 10, gender: "Male", name: "홍길동" },
  { age: 20, gender: "Female", name: "심청" },
  { age: 30, gender: "Male", name: "곰돌이" },
];

const ExcelDownload = (props) => {
  const { fileName } = props;

  const clickToDownload = () => {
    const ws = xlsx.utils.json_to_sheet(arr);
    ["나이", "성별", "이름"].forEach((x, idx) => {
      const cellAdd = xlsx.utils.encode_cell({ c: idx, r: 0 });
      ws[cellAdd].v = x;
    });
    const wb = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, "Sheet1");
    xlsx.writeFile(wb, `${fileName}.xlsx`);
  };

  return (
    <Button className="float-right" variant="primary" onClick={clickToDownload}>
      목록 다운로드
    </Button>
  );
};

export default ExcelDownload;
