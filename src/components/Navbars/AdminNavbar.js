import React from "react";
import { useLocation } from "react-router-dom";
import { Navbar, Container, Nav, Button } from "react-bootstrap";
import routes, { headerMenu } from "routes.js";
import { useSelector } from "react-redux";

function Header() {
  const isLoggedIn = useSelector((state) => state.login.isLoggedIn); // Login Check
  const admin = useSelector((state) => state.login.admin);
  const accessUser = admin ? admin.data.role : null; // Login User role
  let accessName = null;
  let userAccess = null;

  if (accessUser) {
    accessName = admin.data.store_name ? admin.data.store_name : "본사";
  }

  if (accessUser === "main") {
    userAccess = "/admin";
  } else {
    userAccess = "/staff";
  }

  if (!isLoggedIn) {
    if (window.location.pathname.indexOf("/admin/policy") === -1) {
      window.location.href = "/login";
    }
  }
  const location = useLocation();
  const mobileSidebarToggle = (e) => {
    e.preventDefault();
    document.documentElement.classList.toggle("nav-open");
    var node = document.createElement("div");
    node.id = "bodyClick";
    node.onclick = function () {
      this.parentElement.removeChild(this);
      document.documentElement.classList.toggle("nav-open");
    };
    document.body.appendChild(node);
  };

  const checkActive = (v) => {
    const arr1 = window.location.pathname.split("/");
    const arr2 = v.path.split("/");
    return arr1[2] === arr2[1];
  };

  return (
    <>
      {window.location.pathname.indexOf("/admin/policy") === -1 && <div
        className="row"
        style={{
          width: "100%",
          margin: "0",
          backgroundColor: "#FFD8D9",
          color: "white",
          fontSize: "14px",
          padding: "16px 20px 0px 20px",
        }}
      >
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            width: "100%",
          }}
        >
          <div
            style={{
              width: "5%",
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <img src="/logo.png" alt="" style={{ height: "30px" }} />
            <div className="mt-2">CCLIME</div>
          </div>
          <span style={{ fontSize: "39px" }}>CCLIME</span>
          <div>
            <span
              style={{
                fontFamily: "NotoSansCJKkr",
                fontSize: "13px",
                fontWeight: "bold",
                lineHeight: "2.14",
                letterSpacing: "-0.35px",
                color: "#fff",
              }}
            >
              {accessUser} ({accessName}) 님
            </span>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <Button
              onClick={(e) => {
                e.preventDefault();
                localStorage.removeItem("admin");
                window.location.href = "/Login";
              }}
              style={{
                padding: "6px 10px",
                borderRadius: "4px",
                border: "solid 1px #fff",
                fontFamily: "NotoSansCJKkr",
                fontSize: "12px",
                fontWeight: "bold",
                lineHeight: "1.5",
                letterSpacing: "-0.3px",
                color: "#fff",
              }}
            >
              로그아웃
            </Button>
          </div>
        </div>
      </div>}
      {window.location.pathname.indexOf("/admin/policy") === -1 && <div
        className="row"
        style={{
          width: "100%",
          margin: "0",
          backgroundColor: "#FFD8D9",
          color: "white",
          fontSize: "15px",
          fontWeight: "bold",
          padding: "0 20px",
          paddingRight: "0",
        }}
      >
        <div className="col-11" style={{ paddingTop: "16px" }}>
          {headerMenu.map((v, i) => {
            const key = v.path.split("/")[1];
            const route = routes[key][0];
            if (userAccess === "/admin") {
              return (
                <a
                  key={i}
                  href={`${userAccess}${v.path}`}
                  style={{
                    padding: "10px",
                    fontFamily: "NotoSansCJKkr",
                    fontSize: "16px",
                    fontWeight: "500",
                    lineHeight: "1.5",
                    letterSpacing: "-0.4px",
                    fontWeight: "bold",
                    color:
                      window.location.href.indexOf(`${userAccess}/${key}`) > -1
                        ? "#c99dca"
                        : "#979797",
                    marginRight: "40px",
                    paddingBottom: "8px",
                    borderBottom:
                      window.location.href.indexOf(`${userAccess}/${key}`) > -1
                        ? "solid 4px #fff"
                        : "",
                  }}
                >
                  {v.name}
                </a>
              );
            } else if (accessUser === "store") {
              if (!route.onlyHeadquater) {
                if (v.name === "계정관리") {
                  v.path = "/accounts/identifier";
                }
                if (v.name === "앱 매출통계") {
                  v.path = "/statistics/branch";
                }
                if (v.name === "제품관리") {
                  v.path = "/products/manage/count";
                }
                return (
                  <a
                    key={i}
                    href={`${userAccess}${v.path}`}
                    style={{
                      padding: "10px",
                      fontFamily: "NotoSansCJKkr",
                      fontSize: "16px",
                      fontWeight: "500",
                      lineHeight: "1.5",
                      letterSpacing: "-0.4px",
                      fontWeight: "bold",
                      color:
                        window.location.href.indexOf(`${userAccess}/${key}`) >
                        -1
                          ? "#c99dca"
                          : "#979797",
                      marginRight: "40px",
                      paddingBottom: "8px",
                      borderBottom:
                        window.location.href.indexOf(`${userAccess}/${key}`) >
                        -1
                          ? "solid 4px #fff"
                          : "",
                    }}
                  >
                    {v.name}
                  </a>
                );
              }
            } else if (accessUser === "staff" && v.name !== "뷰티스트 관리") {
              if (!route.onlyHeadquater) {
                if (v.name === "계정관리") {
                  v.path = "/accounts/identifier";
                }
                if (v.name === "앱 매출통계") {
                  v.path = "/statistics/branch";
                }
                if (v.name === "제품관리") {
                  v.path = "/products/manage/count";
                }
                return (
                  <a
                    key={i}
                    href={`${userAccess}${v.path}`}
                    style={{
                      padding: "10px",
                      fontFamily: "NotoSansCJKkr",
                      fontSize: "16px",
                      fontWeight: "500",
                      lineHeight: "1.5",
                      letterSpacing: "-0.4px",
                      fontWeight: "bold",
                      color:
                        window.location.href.indexOf(`${userAccess}/${key}`) >
                        -1
                          ? "#c99dca"
                          : "#979797",
                      marginRight: "40px",
                      paddingBottom: "8px",
                      borderBottom:
                        window.location.href.indexOf(`${userAccess}/${key}`) >
                        -1
                          ? "solid 4px #fff"
                          : "",
                    }}
                  >
                    {v.name}
                  </a>
                );
              }
            }
          })}
        </div>
        <div className="col-1" style={{ width: "100%", height: "56px" }}></div>
      </div>}
    </>
  );
}

export default Header;
