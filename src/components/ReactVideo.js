import { useEffect, useRef } from "react";
import videojs from "video.js";
import "video.js/dist/video-js.css";
import "../views/css/VideoPlayer.scss";

const videoJSOptions = {
  controls: true,
  autoplay: false,
  muted: false,
  width: 720,
  height: 480,
};

const VideoPlayer = (props) => {
  const { videoSrc } = props;
  const playerRef = useRef();

  useEffect(() => {
    const player = videojs(playerRef.current, videoJSOptions, () =>
      player.src({ src: videoSrc, type: "video/webm" })
    );
    return () => {
      player.dispose();
    };
  }, []);

  return (
    <div data-vjs-player>
      <video
        ref={playerRef}
        className="video-js vjs-default-skin mb-4"
        playsInline
      />
    </div>
  );
};

export default VideoPlayer;
