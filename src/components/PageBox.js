import { useEffect, useState } from "react";

const PageBox = props => {
  const { pageList } = props;
  const [constructorHasRun, setConstructorHasRun] = useState(false);
  const [pages, setPages] = useState([]);
  const [selected, setSelected] = useState(1);

  useEffect(() => props.currentPage(selected));

  const constructor = () => {
    if (!constructorHasRun) {
      if (pageList) {
        for (let i = 1; i <= pageList; i++) {
          setPages(pages => pages.concat(i));
        }
      }
      setConstructorHasRun(true);
    }
  };
  constructor();

  const goToPrevPage = e => {
    e.preventDefault();
    if (selected <= 1) {
      return;
    }
    setSelected(selected - 1);
  };

  const goToNextPage = e => {
    e.preventDefault();
    if (selected >= Math.max(...pages)) {
      return;
    }
    setSelected(selected + 1);
  };

  return (
    <div className="pageBox">
      <a href="" className="prev" onClick={goToPrevPage}>
        <img src="/img/icon_prev_page.png" />
      </a>
      {pages.map((v, i) => {
        return (
          <a
            key={i}
            href=""
            className={`number ${selected === v ? "on" : null}`}
            onClick={e => {
              e.preventDefault();
              setSelected(v);
            }}
          >
            {v}
          </a>
        );
      })}
      <a href="" className="next" onClick={goToNextPage}>
        <img src="/img/icon_next_page.png" />
      </a>
    </div>
  );
};

export default PageBox;
