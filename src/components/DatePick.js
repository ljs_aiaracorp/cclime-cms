import { Form, Col, Row } from "react-bootstrap";
import { text } from "views/css/style";

let times = [
  "00:00",
  "01:00",
  "02:00",
  "03:00",
  "04:00",
  "05:00",
  "06:00",
  "07:00",
  "08:00",
  "09:00",
  "10:00",
  "11:00",
  "12:00",
  "13:00",
  "14:00",
  "15:00",
  "16:00",
  "17:00",
  "18:00",
  "19:00",
  "20:00",
  "21:00",
  "22:00",
  "23:00",
];

const DatePick = (props) => {
  const {
    readOnly,
    name,
    startDate,
    startTime,
    endDate,
    endTime,
    onChangeStartDate,
    onChangeEndDate,
    onChangeStartTime,
    onChangeEndTime,
    dateIsReadOnly,
  } = props;

  if (props.minTime) {
    times = times.filter(v => {
      const vn = Number(v.replace(":", ""));
      const min = Number(props.minTime.replace(":", ""));
      return vn >= min;
    });
  }
  if (props.maxTime) {
    times = times.filter(v => {
      const vn = Number(v.replace(":", ""));
      const max = Number(props.maxTime.replace(":", ""));
      return vn <= max;
    });
  }

  return (
    <Form.Group as={Row}>
      {text(name)}
      {props.dateOnly ? (
        <Col sm={3}>
          <Form.Control
            type="date"
            placeholder={name}
            value={startDate}
            onChange={e => {
              onChangeStartDate(e.target.value);
            }}
            readOnly={readOnly ?? dateIsReadOnly ?? false}
          />
        </Col>
      ) : (props.useTimeInput ? (
          dateIsReadOnly ? (
            <>
              <Col sm={2}>
                <Form.Control
                  type="date"
                  placeholder={name}
                  value={startDate}
                  readOnly={true}
                />
              </Col>
              <Col sm={2}>
                <Form.Control
                  type="time"
                  placeholder={name}
                  value={startTime}
                  onChange={e => {
                    const time = Number(e.target.value.replace(":", ""));
                    if (props.minTime) {
                      const min = Number(props.minTime.replace(":", ""));
                      if (time < min) {
                        onChangeStartTime(props.minTime);
                        return;
                      }
                    }
                    if (props.maxTime) {
                      const max = Number(props.maxTime.replace(":", ""));
                      if (time > max) {
                        onChangeStartTime(props.maxTime);
                        return;
                      }
                    }
                    onChangeStartTime(e.target.value);
                  }}
                />
              </Col>
            </>
          ) : (
            <Col sm={3}>
              <Form.Control
                type="datetime-local"
                placeholder={name}
                value={startDate + "T" + startTime}
                onChange={e => {
                  const arr = e.target.value.split("T");
                  onChangeStartDate(arr[0]);
                  const time = Number(arr[1].replace(":", ""));
                  if (props.minTime) {
                    const min = Number(props.minTime.replace(":", ""));
                    if (time < min) {
                      onChangeStartTime(props.minTime);
                      return;
                    }
                  }
                  if (props.maxTime) {
                    const max = Number(props.maxTime.replace(":", ""));
                    if (time > max) {
                      onChangeStartTime(props.maxTime);
                      return;
                    }
                  }
                  onChangeStartTime(arr[1]);
                }}
                readOnly={readOnly ?? false}
              />
            </Col>
          )
      ) : (
        <>
          <Col sm={2}>
            <Form.Control
              type="date"
              placeholder={name}
              value={startDate}
              onChange={e => onChangeStartDate(e.target.value)}
              readOnly={readOnly ?? dateIsReadOnly ?? false}
            />
          </Col>
          <Col sm={2}>
            <Form.Control
              as="select"
              value={startTime}
              onChange={(e) => onChangeStartTime(e.target.value)}
              readOnly={readOnly ?? false}
            >
              {times.map((v) => {
                return <option key={v}>{v}</option>;
              })}
            </Form.Control>
          </Col>
        </>
      ))}
      <Col sm={1} style={{ fontSize: "24px", textAlign: "center" }}>
        ~
      </Col>
      {props.dateOnly ? (
        <Col sm={3}>
          <Form.Control
            type="date"
            placeholder="종료 시간"
            value={endDate}
            onChange={e => {
              onChangeEndDate(e.target.value);
            }}
            readOnly={readOnly ?? dateIsReadOnly ?? false}
          />
        </Col>
      ) : (props.useTimeInput ? (
          dateIsReadOnly ? (
            <>
              <Col sm={2}>
                <Form.Control
                  type="date"
                  placeholder={name}
                  value={endDate}
                  readOnly={true}
                />
              </Col>
              <Col sm={2}>
                <Form.Control
                  type="time"
                  placeholder={name}
                  value={endTime}
                  onChange={e => {
                    const time = Number(e.target.value.replace(":", ""));
                    if (props.minTime) {
                      const min = Number(props.minTime.replace(":", ""));
                      if (time < min) {
                        onChangeEndTime(props.minTime);
                        return;
                      }
                    }
                    if (props.maxTime) {
                      const max = Number(props.maxTime.replace(":", ""));
                      if (time > max) {
                        onChangeEndTime(props.maxTime);
                        return;
                      }
                    }
                    onChangeEndTime(e.target.value);
                  }}
                />
              </Col>
            </>
          ) : (
            <Col sm={3}>
              <Form.Control
                type="datetime-local"
                placeholder="종료 시간"
                value={endDate + "T" + endTime}
                onChange={e => {
                  const arr = e.target.value.split("T");
                  onChangeEndDate(arr[0]);
                  const time = Number(arr[1].replace(":", ""));
                  if (props.minTime) {
                    const min = Number(props.minTime.replace(":", ""));
                    if (time < min) {
                      onChangeEndTime(props.minTime);
                      return;
                    }
                  }
                  if (props.maxTime) {
                    const max = Number(props.maxTime.replace(":", ""));
                    if (time > max) {
                      onChangeEndTime(props.maxTime);
                      return;
                    }
                  }
                  onChangeEndTime(arr[1]);
                }}
                readOnly={readOnly ?? false}
              />
            </Col>
          )
      ) : (
        <>
          <Col sm={2}>
            <Form.Control
              type="date"
              placeholder="종료 시간"
              value={endDate}
              onChange={e => onChangeEndDate(e.target.value)}
              readOnly={readOnly ?? dateIsReadOnly ?? false}
            />
          </Col>
          <Col sm={2}>
            <Form.Control
              as="select"
              value={endTime}
              onChange={(e) => onChangeEndTime(e.target.value)}
              readOnly={readOnly ?? false}
            >
              {times.map((v) => {
                return <option key={v}>{v}</option>;
              })}
            </Form.Control>
          </Col>
        </>
      ))}
    </Form.Group>
  );
};

export default DatePick;
