import ReactPaginate from "react-paginate";

export function pageFilter(index, selectedPage) {
  if (index + 1 >= selectedPage * 10 - 9 && index + 1 <= selectedPage * 10) {
    return true;
  } else {
    return false;
  }
}

const Pagination = (props) => {
  const { count, forcePage } = props;
  const selectedPage = ({ selected }) => props.selected(selected + 1);
  return (
    <ReactPaginate
      pageCount={Math.ceil(count / 10)}
      forcePage={forcePage}
      pageRangeDisplayed={6}
      marginPagesDisplayed={1}
      breakLabel={"..."}
      previousLabel={"<"}
      nextLabel={">"}
      onPageChange={selectedPage}
      containerClassName={`pagination ${props.center ? "" : "float-right"}`}
      activeClassName="active"
      pageLinkClassName="page-link"
      breakLinkClassName="page-link"
      nextLinkClassName="page-link"
      previousLinkClassName="page-link"
      pageClassName="page-item"
      breakClassName="page-item"
      nextClassName="page-item"
      previousClassName="page-item"
    />
  );
};

export default Pagination;
