import React from "react";
import { useSelector } from "react-redux";
import { useLocation, withRouter } from "react-router-dom";

import { Nav } from "react-bootstrap";

function Sidebar({ color, routes, history, navHeight }) {
  if (!routes) {
    history.goBack();
    return <></>;
  }
  const admin = useSelector((state) => state.login.admin);
  const accessUser = admin ? admin.data.role : null; // Login User role

  let userAccess = null;
  if (accessUser === "main") {
    userAccess = "/admin";
  } else {
    userAccess = "/staff";
  }

  const location = useLocation();
  const activeRoute = (routeName) => {
    let pathname = location.pathname;
    if (routeName.indexOf && routeName.indexOf(":") > -1) {
      const temp = routeName.split(":")[0];
      return pathname.indexOf(temp) > -1 ? "active" : "deactive";
    }
    return pathname === routeName ? "active" : "deactive";
  };
  const checkOpen = (v) => {
    let existActive = false;
    for (const sub of v.sub) {
      const active = activeRoute(userAccess + sub.path);
      existActive = active === "active";
      if (existActive) {
        return true;
      }
    }
    return false;
  };
  if (window.location.pathname.indexOf("/admin/policy") > -1) {
    return <></>;
  }
  return (
    <div
      className="sidebar"
      style={
        navHeight
          ? {
              top: `${navHeight}px`,
              // maxHeight: `calc(100% - ${navHeight}px)`,
              zIndex: 10,
              width: "193px",
              overflow: "hidden",
            }
          : {}
      }
    >
      <div className="sidebar-wrapper">
        <Nav>
          {routes.map((prop, key) => {
            if (!prop.redirect) {
              if (accessUser === "staff" && prop.name === "뷰티스트 관리")
                return;
              return (
                <li
                  className={
                    prop.upgrade
                      ? "active active-pro"
                      : activeRoute(userAccess + prop.path)
                  }
                  key={key}
                >
                  <Nav.Link
                    onClick={(e) => {
                      if (prop.sub) {
                        if (window.$(e.target).hasClass("nav-link")) {
                          window.$(e.target).toggleClass("open");
                        } else {
                          if (
                            window.$(e.target).parent().hasClass("nav-link")
                          ) {
                            window.$(e.target).parent().toggleClass("open");
                          } else {
                            if (
                              window
                                .$(e.target)
                                .parent()
                                .parent()
                                .hasClass("nav-link")
                            ) {
                              window
                                .$(e.target)
                                .parent()
                                .parent()
                                .toggleClass("open");
                            }
                          }
                        }
                      } else if (prop.layout) {
                        history.push(userAccess + prop.path);
                      }
                    }}
                    className={`nav-link ${
                      prop.sub && checkOpen(prop) ? "open" : ""
                    }`}
                    style={{
                      paddingTop: "0",
                      paddingBottom: "0",
                      margin: "0",
                      borderRadius: "0",
                    }}
                  >
                    {prop.name}
                  </Nav.Link>
                  {prop.sub &&
                    prop.sub
                      .filter((v) => v.name)
                      .map((v, i) => {
                        if (
                          v.name !== "지점 계정 관리" &&
                          v.name !== "고객 사용내역 조회" &&
                          v.name !== "전체 매출" &&
                          v.name !== "카테고리 관리" &&
                          v.name !== "상품 관리" &&
                          v.name !== "배너 관리" &&
                          v.name !== "쿠폰리스트" &&
                          accessUser === "store"
                        ) {
                          if (v.name === "뷰티스트 정산 관리") {
                            v.path = "/beautists/adjustment/branch/1";
                          }
                          return (
                            <Nav.Link
                              key={i}
                              onClick={(e) => {
                                if (v.layout) {
                                  if (v.name === "뷰티스트 정산 관리") {
                                    window.location.replace(
                                      userAccess + v.path
                                    );
                                    return;
                                  }
                                  history.push(userAccess + v.path);
                                }
                              }}
                              className={`nav-link ${activeRoute(
                                userAccess + v.path
                              )}`}
                              style={{
                                paddingTop: "0",
                                paddingBottom: "0",
                                fontWeight: "normal",
                                margin: "0",
                                borderRadius: "0",
                              }}
                            >
                              {v.name}
                            </Nav.Link>
                          );
                        } else if (
                          accessUser === "main" &&
                          v.name !== "지점별 재고관리"
                        ) {
                          return (
                            <Nav.Link
                              key={i}
                              onClick={(e) => {
                                if (v.layout) {
                                  history.push(userAccess + v.path);
                                }
                              }}
                              className={`nav-link ${activeRoute(
                                userAccess + v.path
                              )}`}
                              style={{
                                paddingTop: "0",
                                paddingBottom: "0",
                                fontWeight: "normal",
                                margin: "0",
                                borderRadius: "0",
                              }}
                            >
                              {v.name}
                            </Nav.Link>
                          );
                        } else if (
                          v.name !== "지점 계정 관리" &&
                          v.name !== "고객 사용내역 조회" &&
                          v.name !== "전체 매출" &&
                          v.name !== "뷰티스트 명단" &&
                          v.name !== "뷰티스트 정산 관리" &&
                          v.name !== "카테고리 관리" &&
                          v.name !== "상품 관리" &&
                          v.name !== "배너 관리" &&
                          v.name !== "쿠폰리스트" &&
                          accessUser === "staff"
                        ) {
                          return (
                            <Nav.Link
                              key={i}
                              onClick={(e) => {
                                if (v.layout) {
                                  history.push(userAccess + v.path);
                                }
                              }}
                              className={`nav-link ${activeRoute(
                                userAccess + v.path
                              )}`}
                              style={{
                                paddingTop: "0",
                                paddingBottom: "0",
                                fontWeight: "normal",
                                margin: "0",
                                borderRadius: "0",
                              }}
                            >
                              {v.name}
                            </Nav.Link>
                          );
                        }
                      })}
                </li>
              );
            }
            return null;
          })}
        </Nav>
      </div>
    </div>
  );
}

export default withRouter(Sidebar);
