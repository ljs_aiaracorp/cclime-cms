import { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";
import { getToday } from "../Formatter/DateFormatter";
import moment from "moment";

const Datepicker = props => {
  const [constructorHasRun, setConstructorHasRun] = useState(false);
  const [selectedDate, setSelectedDate] = useState(getToday());
  useEffect(() => {
    const constructor = () => {
      if (!constructorHasRun) {
        setConstructorHasRun(true);
        window.$(function () {
          window.$.datepicker.setDefaults({
            dateFormat: "yy. mm. dd", //Input Display Format 변경
            showOtherMonths: true, //빈 공간에 현재월의 앞뒤월의 날짜를 표시
            showMonthAfterYear: true, //년도 먼저 나오고, 뒤에 월 표시
            changeYear: true, //콤보박스에서 년 선택 가능
            changeMonth: true, //콤보박스에서 월 선택 가능
            showOn: "both", //button:버튼을 표시하고,버튼을 눌러야만 달력 표시 ^ both:버튼을 표시하고,버튼을 누르거나 input을 클릭하면 달력 표시
            buttonImage: "/img/icon_calendar.png", //버튼 이미지 경로
            buttonImageOnly: true, //기본 버튼의 회색 부분을 없애고, 이미지만 보이게 함
            buttonText: "선택", //버튼에 마우스 갖다 댔을 때 표시되는 텍스트
            yearSuffix: "년", //달력의 년도 부분 뒤에 붙는 텍스트
            monthNamesShort: [
              "1",
              "2",
              "3",
              "4",
              "5",
              "6",
              "7",
              "8",
              "9",
              "10",
              "11",
              "12",
            ], //달력의 월 부분 텍스트
            monthNames: [
              "1월",
              "2월",
              "3월",
              "4월",
              "5월",
              "6월",
              "7월",
              "8월",
              "9월",
              "10월",
              "11월",
              "12월",
            ], //달력의 월 부분 Tooltip 텍스트
            dayNamesMin: ["일", "월", "화", "수", "목", "금", "토"], //달력의 요일 부분 텍스트
            dayNames: [
              "일요일",
              "월요일",
              "화요일",
              "수요일",
              "목요일",
              "금요일",
              "토요일",
            ], //달력의 요일 부분 Tooltip 텍스트
            minDate: props.minDate ? calculateDates(props.minDate) : "-1M", //최소 선택일자(-1D:하루전, -1M:한달전, -1Y:일년전)
            maxDate: props.maxDate ? calculateDates(props.maxDate) : "+1M", //최대 선택일자(+1D:하루후, -1M:한달후, -1Y:일년후)
          });

          //input을 datepicker로 선언
          window.$("#datepicker").datepicker({
            onSelect: dateText => {
              setSelectedDate(dateText);
              if (props.onChangeDate) {
                props.onChangeDate(date.format("YYYY-MM-DD"));
              }
            },
          });
          window.$("#datepicker").datepicker("setDate", "today");
        });
        if (props.currentDate) {
          setSelectedDate(props.currentDate);
        }
      }
    };
    constructor();
  });

  const calculateDates = dateStr => {
    const today = moment().startOf('day');
    const date = moment(dateStr);
    return date.diff(today, "day") + "D";
  };

  return (
    <div className="dateBox">
      <a href="" className="prevBtn" onClick={e => {
        e.preventDefault();
        const date = moment(selectedDate);
        date.subtract(1, 'd');
        const minDate = moment(props.minDate);
        if (date.isBefore(minDate)) {
          return;
        }
        setSelectedDate(date.format("YYYY-MM-DD"));
        if (props.onChangeDate) {
          props.onChangeDate(date.format("YYYY-MM-DD"));
        }
      }}>
        <img src="/img/icon_prev_date.png" />
      </a>
      <div className="datepicker">
        <input type="text" id="datepicker" readOnly value={moment(selectedDate).format("YYYY. MM. DD")} />
      </div>
      <a href="" className="nextBtn" onClick={e => {
        e.preventDefault();
        const date = moment(selectedDate);
        date.add(1, 'd');
        const maxDate = moment(props.maxDate);
        if (date.isAfter(maxDate)) {
          return;
        }
        setSelectedDate(date.format("YYYY-MM-DD"));
        if (props.onChangeDate) {
          props.onChangeDate(date.format("YYYY-MM-DD"));
        }
      }}>
        <img src="/img/icon_next_date.png" />
      </a>
    </div>
  );
};

export default withRouter(Datepicker);
