import { useEffect, useState } from "react";
import React from "react";
import { Modal, Button, Form, Table } from "react-bootstrap";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";
import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";

export const UserListPopup = ({ onclose, onClickUser }) => {
  const user = axiosUrlFunction("store/userList");
  const axiosconfig = {
    headers: {
      Authorization: `Bearer ${user.token}`,
      "Content-Type": `application/json`,
    },
  };

  const [userList, setUserList] = useState([]);

  const [searchTarget, setSearchTarget] = useState("name");
  const [searchKeyword, setSearchKeyword] = useState("");

  const getUserList = async () => {
    const params = [];

    if (searchKeyword !== "" && searchKeyword !== "") {
      params.push(`type=${searchTarget}`);
      params.push(`word=${searchKeyword}`);
    }
    const paramsStr = params.length > 0 ? `?${params.join("&")}` : "";
    axios
      .get(`${user.apiUrl}${paramsStr}`, axiosconfig)
      .then((res) => {
        setUserList(res.data.data);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getUserList();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else if (err.response.status === 400) {
          alert("검색내용을 입력해 주세요.");
          return;
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  return (
    <Modal show={true} onHide={onclose} size="lg">
      <Modal.Header closeButton>
        <Modal.Title>
          <h3 style={{ margin: 0 }}>고객 검색</h3>
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <div style={{ textAlign: "center" }}>
          <select
            onChange={(e) => {
              setSearchTarget(e.target.value);
              setSearchKeyword("");
            }}
            className="form-control"
            style={{
              display: "inline-block",
              width: "120px",
              verticalAlign: "middle",
            }}
          >
            <option value="name">고객명</option>
            <option value="phone">핸드폰번호</option>
            <option value="id">고객번호</option>
          </select>
          {searchTarget === "phone" ? (
            <Form.Control
              type="tel"
              placeholder="검색"
              value={searchKeyword}
              onChange={(e) => {
                setSearchKeyword(e.target.value);
              }}
              onKeyUp={(e) => {
                if (e.keyCode === 13) {
                  getUserList();
                }
              }}
              maxLength={13}
              style={{
                display: "inline-block",
                width: "300px",
                verticalAlign: "middle",
                marginLeft: "10px",
                marginRight: "10px",
              }}
            />
          ) : (
            <Form.Control
              type="text"
              placeholder="검색"
              value={searchKeyword}
              onChange={(e) => setSearchKeyword(e.target.value)}
              onKeyUp={(e) => {
                if (e.keyCode === 13) {
                  getUserList();
                }
              }}
              maxLength={50}
              style={{
                display: "inline-block",
                width: "300px",
                verticalAlign: "middle",
                marginLeft: "10px",
                marginRight: "10px",
              }}
            />
          )}
          <Button
            variant="light"
            onClick={(e) => {
              getUserList();
            }}
            style={{
              display: "inline-block",
              border: "solid 1px #E3E3E3",
              color: "#565656",
              verticalAlign: "middle",
            }}
            className="search-btn"
          >
            <FontAwesomeIcon icon={faSearch} />
          </Button>
        </div>
        <div style={{ fontSize: "20px" }}>검색된 고객리스트</div>
        <div
          style={{
            minHeight: "300px",
            maxHeight: "70vh",
            overflow: "auto",
            fontSize: "16px",
          }}
        >
          <div className="table-scroll2">
            <Table className="table-hover table-scroll2__table mt-0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>고객명</th>
                  <th>고객번호</th>
                  <th>휴대폰번호</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {userList &&
                  userList.map((user, idx) => {
                    return (
                      <tr key={idx}>
                        <td>{idx + 1}</td>
                        <td>{user.user_name}</td>
                        <td>{user.user_id}</td>
                        <td>{user.phone}</td>
                        <td>
                          <button
                            style={{
                              backgroundColor: "white",
                              padding: "6px",
                              width: "50px",
                            }}
                            onClick={() => onClickUser(user)}
                          >
                            선택
                          </button>
                        </td>
                      </tr>
                    );
                  })}
              </tbody>
            </Table>
          </div>
        </div>
      </Modal.Body>

      <Modal.Footer></Modal.Footer>
    </Modal>
  );
};
