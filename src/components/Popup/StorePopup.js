import React from "react";
import { Modal, Button, Row, Col, Form, Card, Table } from "react-bootstrap";
import PropTypes from "prop-types";
import styled from "styled-components";

// 지점선택 팝업창
export const StorePopup = ({
  className,
  onClose,
  onSave,
  closable,
  visible,
  children,
  test,
  makeonClick,
}) => {
  const isAddButton = onSave ? true : false;
  return (
    <>
      <ModalOverlay visible={visible} />
      <ModalWrapper className={className} tabIndex="-1" visible={visible}>
        <ModalInner tabIndex="0" className="modal-inner">
          {closable && (
            <div className="addUserPopup__Modal">
              <span style={{ fontSize: "25px" }}>지점 추가</span>
              <div
                className="addUserPouup__Modal-content"
                style={{
                  width: "310px",
                  height: "300px",
                  overflow: "auto",
                }}
              >
                <br />
                {children}
              </div>
              <div className="text-center modal-footer">
                <br />
                {isAddButton && (
                  <button className="btn btn-primary" onClick={onSave}>
                    추가
                  </button>
                )}
                <button className="btn btn-primary" onClick={onClose}>
                  닫기
                </button>
              </div>
            </div>
          )}
        </ModalInner>
      </ModalWrapper>
    </>
  );
};

StorePopup.defaultProps = {
  closable: true,
  maskClosable: true,
  visible: false,
};

StorePopup.propTypes = {
  visible: PropTypes.bool,
};

const ModalWrapper = styled.div`
  box-sizing: border-box;
  display: ${(props) => (props.visible ? "block" : "none")};
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 1000;
  overflow: auto;
  outline: 0;
`;

const ModalOverlay = styled.div`
  box-sizing: border-box;
  display: ${(props) => (props.visible ? "block" : "none")};
  position: fixed;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  background-color: rgba(0, 0, 0, 0.6);
  z-index: 999;
`;

const ModalInner = styled.div`
  box-sizing: border-box;
  position: relative;
  box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.5);
  background-color: #fff;
  border-radius: 10px;
  width: 360px;
  max-width: 480px;
  top: 50%;
  transform: translateY(-50%);
  margin: 0 auto;
  padding: 40px 20px;
`;

export const GradeModal1 = ({ onclose, children }) => {
  return (
    <Modal show={true} onHide={onclose} size="lg">
      <Modal.Header closeButton>
        <Modal.Title>
          <b>등급선택</b>
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>{children}</Modal.Body>

      <Modal.Footer></Modal.Footer>
    </Modal>
  );
};
