import { useEffect, useState } from "react";
import React from "react";
import { Modal, Button, Form, Table } from "react-bootstrap";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";
import axios from "axios";

export const ProgramListPopup = ({ onclose, onClickProgram }) => {
  const user = axiosUrlFunction("programList", "프로그램");
  const axiosconfig = {
    headers: {
      Authorization: `Bearer ${user.token}`,
      "Content-Type": `application/json`,
    },
  };

  const [page, setPage] = useState(1);
  const [size, setSize] = useState(10);
  const [total, setTotal] = useState(0);
  const [programList, setProgramList] = useState([]);
  const [allProgramList, setAllProgramList] = useState([]);

  const getBeautysList = async () => {
    if (allProgramList.length > 0) {
      setProgramList(allProgramList.concat().splice((page - 1) * size, size));
      return;
    }
    const categories = (
      await axios.get(
        `${user.apiUrl.replace(
          "admin/programList",
          "categoryList"
        )}?type=program`,
        axiosconfig
      )
    ).data.data;
    let allList = [];
    await Promise.all(
      categories.map(async (category) => {
        axios
          .get(`${user.apiUrl}?category_id=${category.categoryId}`, axiosconfig)
          .then((res) => {
            allList = allList.concat(res.data.data);
            setTotal(allList.length);
            setProgramList(allList.concat().splice((page - 1) * size, size));
            setAllProgramList(allList);
          })
          .catch((err) => {
            if (err.response.status === 401) {
              if (err.response.data.error === "Unauthorized") {
                // 토큰 재발급
                const isCheck = tokenCheck();
                isCheck.then((res) => {
                  if (!res) return;
                  else getBeautysList();
                });
              } else {
                alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
              }
            } else {
              alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
            }
          });
      })
    );
  };

  useEffect(() => {
    getBeautysList();
  }, []);

  return (
    <Modal show={true} onHide={onclose} size="sl">
      <Modal.Header closeButton>
        <Modal.Title>
          <b>프로그램 선택</b>
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <div
          style={{
            minHeight: "300px",
            maxHeight: "70vh",
            overflow: "auto",
            fontSize: "16px",
          }}
        >
          <div className="table-scroll2">
            <Table className="table-hover table-scroll2__table mt-0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>프로그램/선불권 명</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {programList &&
                  programList.map((program, idx) => {
                    return (
                      <tr key={idx}>
                        <td>{idx + 1}</td>
                        <td>{program.programKo}</td>
                        <td>
                          <Button onClick={() => onClickProgram(program)}>
                            선택
                          </Button>
                        </td>
                      </tr>
                    );
                  })}
              </tbody>
            </Table>
          </div>
        </div>
      </Modal.Body>

      <Modal.Footer></Modal.Footer>
    </Modal>
  );
};
