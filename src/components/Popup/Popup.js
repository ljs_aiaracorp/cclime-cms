import { forwardRef, useState, useImperativeHandle } from "react";

export const CustomPopup = forwardRef((props, ref) => {
  const [show, setShow] = useState(false);
  const { width, title, element } = props;

  useImperativeHandle(ref, () => ({
    toggleMenu() {
      setShow(!show);
    },
  }));

  const handleClick = (e) => {
    e.preventDefault();
    setShow(!show);
  };

  return (
    <>
      {show ? (
        <div className="layer_popup listPopup">
          <div className="popup_main" style={{ width: `${width}px` }}>
            <a href="" className="closeBtn" onClick={handleClick}>
              <img src="/img/icon_close.png" />
            </a>
            <div className="popup_title">{title}</div>
            {element}
          </div>
        </div>
      ) : (
        <></>
      )}
    </>
  );
});

export const CustomPopup2 = forwardRef((props, ref) => {
  const [show, setShow] = useState(false);
  const { style, title, element } = props;

  useImperativeHandle(ref, () => ({
    toggleMenu() {
      setShow(!show);
    },
  }));

  const handleClick = (e) => {
    e.preventDefault();
    setShow(!show);
  };

  return (
    <>
      {show ? (
        <div className="listPopup" style={{ display: "block" }}>
          <div style={style}>
            <div className="popup_title" style={{ fontSize: "16px" }}>{title}</div>
            {element}
          </div>
        </div>
      ) : (
        <></>
      )}
    </>
  );
});
