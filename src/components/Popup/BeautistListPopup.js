import { useEffect, useState } from "react";
import React from "react";
import { Modal, Button, Form, Table } from "react-bootstrap";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";
import axios from "axios";
import Pagination from "components/Pagination";

export const BeautistListPopup = ({ onclose, onClickBeautist, onStoreId }) => {
  const user = axiosUrlFunction("beautystList");
  const axiosconfig = {
    headers: {
      Authorization: `Bearer ${user.token}`,
      "Content-Type": `application/json`,
    },
  };
  const [beautystList, setBeautystList] = useState([]);

  const getBeautysList = async () => {
    const strStoreId = onStoreId !== "0" ? `?store_id=${onStoreId}` : "";

    axios
      .get(`${user.apiUrl}${strStoreId}`, axiosconfig)
      .then((res) => {
        setBeautystList(res.data.data);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getBeautysList();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  useEffect(() => {
    getBeautysList();
  }, []);

  return (
    <Modal show={true} onHide={onclose} size="sl">
      <Modal.Header closeButton>
        <Modal.Title>
          <b>뷰티스트 선택</b>
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <div
          style={{
            minHeight: "300px",
            maxHeight: "70vh",
            overflow: "auto",
            fontSize: "16px",
          }}
        >
          <div className="table-scroll2">
            <Table className="table-hover table-scroll2__table mt-0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>뷰티스트 명</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {beautystList &&
                  beautystList.map((beautist, idx) => {
                    return (
                      <tr key={idx}>
                        <td>{idx + 1}</td>
                        <td>{beautist.name}</td>
                        <td>
                          <Button onClick={() => onClickBeautist(beautist)}>
                            선택
                          </Button>
                        </td>
                      </tr>
                    );
                  })}
              </tbody>
            </Table>
          </div>
        </div>
      </Modal.Body>

      <Modal.Footer></Modal.Footer>
    </Modal>
  );
};
