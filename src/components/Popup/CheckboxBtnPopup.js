import React from "react";
import { Modal } from "react-bootstrap";

export const CheckboxBtnPopup = ({ onClose, onConfirm, onOk, children }) => {
  return (
    <Modal show={true} onHide={onClose}>
      <Modal.Header></Modal.Header>

      <Modal.Body className="text-center">
        <span>{children}</span>
      </Modal.Body>

      <Modal.Footer className="text-center" style={{ display: "block" }}>
        <button
          variant="primary"
          onClick={() => {
            onClose();
            if (onOk) onOk();
          }}
          style={{
            width: "70px",
            backgroundColor: "white",
            fontWeight: "bold",
          }}
        >
          확인
        </button>
        {onConfirm && (
          <button
            variant="primary"
            onClick={onClose}
            style={{
              width: "70px",
              backgroundColor: "white",
              fontWeight: "bold",
            }}
          >
            취소
          </button>
        )}
      </Modal.Footer>
    </Modal>
  );
};
