import { Modal, Row, Col, Card } from "react-bootstrap";

export const HelpPopup = (props) => {
  return (
    <Modal show={true} onHide={props.onClickClose} size="lg">
      <Modal.Header closeButton>
        <Modal.Title>
          <b>도움말</b>
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Row>
          <Col md="12">
            <Card className="card-plain table-plain-bg">
              <Card.Body className="table-full-width table-responsive px-0">
                <div className="help-description">
                  {props.onContent.map((content, i) => {
                    return (
                      <p key={i} style={{ fontSize: "18px" }}>
                        {i + 1}. {content}
                      </p>
                    );
                  })}
                </div>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Modal.Body>
    </Modal>
  );
};
