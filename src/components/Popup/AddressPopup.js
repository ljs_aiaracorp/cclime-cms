import REACT from "react";
import DaumPostCode from "react-daum-postcode";

let fullAddr = null;
const AddressPopup = ({ onmakeclose }) => {
  const handleComplete = (data) => {
    let fullAddress = data.address;
    let extraAddress = "";
    if (data.addressType === "R") {
      if (data.bname !== "") {
        extraAddress += data.bname;
      }
      if (data.buildingName !== "") {
        extraAddress +=
          extraAddress !== "" ? `, ${data.buildingName}` : data.buildingName;
      }
      fullAddress += extraAddress !== "" ? ` (${extraAddress})` : "";
    }
    //fullAddress -> 전체 주소반환
    if (fullAddress) {
      fullAddr = fullAddress;
    }
  };
  return (
    <div>
      <button
        className="float-right"
        style={{
          marginBottom: "10px",
          backgroundColor: "white",
          borderRadius: "25%",
        }}
        onClick={() => onmakeclose("")}
      >
        닫기
      </button>
      <DaumPostCode
        onClose={() => onmakeclose(fullAddr)}
        onComplete={handleComplete}
      />
    </div>
  );
};

export default AddressPopup;
