import { useState, useEffect } from "react";
import { Modal, Button, Row, Col, Form, Card, Table } from "react-bootstrap";
import Loading from "components/Loading";
import axios from "axios";
import { axiosUrlFunction, tokenCheck } from "utils/AxiosUrl";

export const BeautystPopup = (props) => {
  const [list, setList] = useState([
    {
      name: "",
      beautyst_id: "",
      checked: false,
    },
  ]);

  const getBeauty = async () => {
    const beautyUrl = axiosUrlFunction("beautystList");
    axios
      .get(
        `${beautyUrl.apiUrl}${
          beautyUrl.accessor === "main" ? `?store_id=${props.onStoreId}` : ""
        }`,
        {
          headers: {
            Authorization: `Bearer ${beautyUrl.token}`,
            "Content-Type": `application/json`,
          },
        }
      )
      .then((res) => {
        let putData = [];
        res.data.data.map((v) => {
          putData.push({
            name: v.name,
            beautyst_id: v.beautyst_id,
            checked: false,
          });
        });
        setList(putData);
      })
      .catch((err) => {
        if (err.response.status === 401) {
          if (err.response.data.error === "Unauthorized") {
            // 토큰 재발급
            const isCheck = tokenCheck();
            isCheck.then((res) => {
              if (!res) return;
              else getBeauty();
            });
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        }
      });
  };

  const checkboxEventHandler = (event) => {
    const { value, checked } = event.target;

    const data = [];
    list.map((check) => {
      data.push(check);
    });

    if (checked) {
      for (let i = 0; i < data.length; i++) {
        if (data[i].beautyst_id === parseInt(value)) {
          data[i].checked = true;
          setList(data);
        } else {
          data[i].checked = false;
        }
      }
    } else {
      for (let i = 0; i < data.length; i++) {
        if (data[i].beautyst_id === parseInt(value)) {
          data[i].checked = false;
          setList(data);
          return;
        }
      }
    }
  };

  const sendList = () => {
    let data = {
      name: "",
      beautyst_id: "",
    };
    list.map((v) => {
      if (v.checked) {
        data.name = v.name;
        data.beautyst_id = v.beautyst_id;
      }
    });
    return data;
  };

  useEffect(() => {
    getBeauty();
  }, []);

  return (
    <Modal show={true} onHide={props.onClickClose} size="lg">
      <Modal.Header closeButton>
        <Modal.Title>
          <b>뷰티스트 선택</b>
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Row>
          <Col md="12">
            <div className="table-scroll2">
              <Table className="table-hover table-scroll2__table mt-0">
                <thead>
                  <tr>
                    <th></th>
                    <th>NO</th>
                    <th>뷰티스트명</th>
                  </tr>
                </thead>
                <tbody>
                  {list !== null && list !== undefined ? (
                    list.map((v, i) => {
                      if (v.beautyst_id !== "") {
                        return (
                          <tr key={i}>
                            <td>
                              {" "}
                              <input
                                id={i + "cheese"}
                                className="cursor"
                                type="checkbox"
                                value={v.beautyst_id}
                                checked={v.checked}
                                onChange={(e) => {
                                  checkboxEventHandler(e);
                                }}
                              />
                            </td>
                            <td>
                              <label htmlFor={i + "cheese"} className="cursor">
                                {i + 1}
                              </label>
                            </td>
                            <td>
                              <label htmlFor={i + "cheese"} className="cursor">
                                {v.name}
                              </label>
                            </td>
                          </tr>
                        );
                      }
                    })
                  ) : (
                    <tr>
                      <td colSpan={3} style={{ textAlign: "center" }}>
                        <Loading />
                      </td>
                    </tr>
                  )}
                </tbody>
              </Table>
            </div>
          </Col>
        </Row>
      </Modal.Body>

      <Modal.Footer className="text-center" style={{ display: "block" }}>
        <Button variant="primary" onClick={() => props.onSaveEvent(sendList())}>
          선택
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
