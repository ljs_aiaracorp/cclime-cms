import React, { useEffect, useState } from "react";
import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import styled from "styled-components";
import { EditorState } from "draft-js";
import { convertFromHTML, convertToHTML } from 'draft-convert';
import axios from "axios";
import { authHeaderMultipart } from "services/AuthHeader";

const MyBlock = styled.div`
  .wrapper-class {
    width: 100%;
  }
  .editor {
    height: ${props => props.height} !important;
    border: 1px solid #f1f1f1 !important;
    padding: 5px !important;
    border-radius: 2px !important;
  }
  p {
    line-height: 0.1em;
  }
`;

const fromHTML = convertFromHTML({
  htmlToEntity: (nodeName, node, createEntity) => {
    if (nodeName === 'img') {
      const entityConfig = {};
      entityConfig.src = node.getAttribute ? node.getAttribute('src') || node.src : node.src;
      entityConfig.alt = node.alt;
      entityConfig.height = node.style.height;
      entityConfig.width = node.style.width;

      return createEntity(
        'IMAGE',
        'MUTABLE',
        entityConfig,
      );
    }
  },
  htmlToStyle: (nodeName, node, currentStyle) => {
    if (nodeName === 'ins') {
      currentStyle = currentStyle.add('UNDERLINE');
    }
    if (node.style.backgroundColor) {
      currentStyle = currentStyle.add('bgcolor-' + node.style.backgroundColor);
    }
    if (node.style.color) {
      currentStyle = currentStyle.add('color-' + node.style.color);
    }
    if (node.style.fontSize) {
      currentStyle = currentStyle.add('fontsize-' + node.style.fontSize.replace('px', ''));
    }
    return currentStyle;
  },
});
const toHTML = convertToHTML({
  styleToHTML: (style) => {
    const domStyle = {};
    let domStyleAdded = false;
    if (style.startsWith('bgcolor-')) {
      domStyle.backgroundColor = style.replace('bgcolor-', '');
      domStyleAdded = true;
    }
    if (style.startsWith('color-')) {
      domStyle.color = style.replace('color-', '');
      domStyleAdded = true;
    }
    if (style.startsWith('fontsize-')) {
      domStyle.fontSize = style.replace('fontsize-', '') + 'px';
      domStyleAdded = true;
    }
    if (domStyleAdded) {
      return <span style={domStyle} />;
    }
  },
  entityToHTML: (entity, originalText) => {
    if (entity.type === 'IMAGE') {
      return <img src={entity.data.src} width={entity.data.width} height={entity.data.height} alt={entity.data.alt} />;
    }
  },
});

const TextEditor = (props) => {
  const [editorState, setEditorState] = useState(props.value ? EditorState.createWithContent(
    fromHTML(props.value)
  ) : EditorState.createEmpty());

  const onEditorStateChange = (editorState) => {
    // editorState에 값 설정
    setEditorState(editorState);
    const editorToHtml = toHTML(editorState.getCurrentContent());
    if (props.value !== editorToHtml) {
      props.returnText(editorToHtml);
    }
  };

  useEffect(() => {
    if (editorState) {
      const editorToHtml = toHTML(editorState.getCurrentContent());
      if (props.value !== editorToHtml) {
        setEditorState(props.value ? EditorState.createWithContent(
          fromHTML(props.value)
        ) : EditorState.createEmpty());
      }
    }
  }, [props.value]);

  return (
    <MyBlock height={props.height ?? "350px"}>
      <Editor
        // 에디터와 툴바 모두에 적용되는 클래스
        wrapperClassName="wrapper-class"
        // 에디터 주변에 적용된 클래스
        editorClassName="editor"
        // 툴바 주위에 적용된 클래스
        toolbarClassName="toolbar-class"
        // 툴바 설정
        toolbar={{
          // inDropdown: 해당 항목과 관련된 항목을 드롭다운으로 나타낼것인지
          list: { inDropdown: true },
          textAlign: { inDropdown: true },
          link: { inDropdown: true },
          history: { inDropdown: false },
          // image: {
          //   uploadCallback: (file) => {
          //     return new Promise(
          //       (resolve, reject) => {
          //         const formData = new FormData();
          //         formData.append('mode', 'editor');
          //         formData.append('file', file);
          //         axios
          //           .post(`${process.env.REACT_APP_API_URL}/commons/upload`, formData, authHeaderMultipart)
          //           .then(res => {
          //             resolve({ data: { link: res.data.file.file } });
          //           })
          //           .catch(err => reject(err));
          //       }
          //     );
          //   },
          //   previewImage: true,
          //   alt: { present: true, mandatory: false },
          // },
        }}
        placeholder={props.placeholder ?? "내용을 작성해주세요."}
        // 한국어 설정
        localization={{
          locale: "ko",
        }}
        // 초기값 설정
        editorState={editorState}
        // 에디터의 값이 변경될 때마다 onEditorStateChange 호출
        onEditorStateChange={onEditorStateChange}
      />
    </MyBlock>
  );
};

export default TextEditor;
