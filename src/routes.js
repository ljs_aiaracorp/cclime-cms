import { UserList } from "views/Users/UserList";
import { AddUser } from "views/Users/AddUser";
import { EditUser } from "views/Users/EditUser";
import { UserUsageList } from "views/Users/UserUsageList";
import { UserUsageHistory } from "views/Users/UserUsageHistory";
import { AllList } from "views/Statistics/AllList";
import { ListByBranch } from "views/Statistics/ListByBranch";
import { CancelPayment } from "views/Statistics/CancelPayment";
import { Reservations } from "views/Usages/Reservations";
import { Reservate } from "views/Usages/Reservate";
import { EditReservation } from "views/Usages/EditReservation";
import { UseReservation } from "views/Usages/UseReservation";
import { ManageChart } from "views/Usages/ManageChart";
import { ViewDiary } from "views/Usages/ViewDiary";
import { CouponList } from "views/Usages/CouponList";
import { AddCoupon } from "views/Usages/AddCoupon";
import { CouponPay } from "views/Usages/CouponPay";
import { EditCoupon } from "views/Usages/EditCoupon";
import { SendPush } from "views/Alarm/SendPush";
import { PushHistory } from "views/Alarm/PushHistory";
import { AutoPush } from "views/Alarm/AutoPush";
import { SendKakaotalk } from "views/Alarm/SendKakaotalk";
import { KakaotalkHistory } from "views/Alarm/KakaotalkHistory";
import { Event } from "views/Page/Event";
import { AddEvent } from "views/Page/AddEvent";
import { EditEvent } from "views/Page/EditEvent";
import { Notices as PageNotices } from "views/Page/Notices";
import { AddNotice as PageAddNotice } from "views/Page/AddNotice";
import { EditNotice as PageEditNotice } from "views/Page/EditNotice";
import { FAQ } from "views/Page/FAQ";
import { AddFAQ } from "views/Page/AddFAQ";
import { EditFAQ } from "views/Page/EditFAQ";
import { Report } from "views/Page/Report";
import { AnswerReport } from "views/Page/AnswerReport";
import { Programs } from "views/Menu/Programs";
import { Categories as MenuCategories } from "views/Menu/Categories";
import { Tickets } from "views/Menu/Tickets";
import { AddTicket } from "views/Menu/AddTicket";
import { EditTicket } from "views/Menu/EditTicket";
import { Variables } from "views/Menu/Variables";
import { Branchs } from "views/Branchs/Branchs";
import { AddBranch } from "views/Branchs/AddBranch";
import { EditBranch } from "views/Branchs/EditBranch";
import { BranchAdjustment } from "views/Branchs/BranchAdjustment";
import { Beautists } from "views/Beautists/Beautists";
import { AddBeautist } from "views/Beautists/AddBeautist";
import { EditBeautist } from "views/Beautists/EditBeautist";
import { BeautistAdjustment } from "views/Beautists/BeautistAdjustment";
import { BeautistAdjustmentByBranch } from "views/Beautists/BeautistAdjustmentByBranch";
import { Account } from "views/Account/Account";
import { Identifier } from "views/Account/Identifier";
import { Password } from "views/Account/Password";
import { Notices } from "views/Notices/Notices";
import { AddNotice } from "views/Notices/AddNotice";
import { EditNotice } from "views/Notices/EditNotice";
import { ViewNotice } from "views/Notices/ViewNotice";
import { QNA } from "views/Notices/QNA";
import { AddQNA } from "views/Notices/AddQNA";
import { EditQNA } from "views/Notices/EditQNA";
import { ViewQNA } from "views/Notices/ViewQNA";
import { AnswerQNA } from "views/Notices/AnswerQNA";
import { EditAnswersQNA } from "views/Notices/EditAnswersQNA";
import { Categories } from "views/Products/Categories";
import { Products } from "views/Products/Products";
import { AddProduct } from "views/Products/AddProduct";
import { EditProduct } from "views/Products/EditProduct";
import { Banners } from "views/Products/Banners";
import { AddBanner } from "views/Products/AddBanner";
import { EditBanner } from "views/Products/EditBanner";
import { ManageCount } from "views/Products/ManageCount";
import { Privacy } from "views/Policy/Privacy";

export const headerMenu = [
  { path: "/users/list", name: "고객정보" },
  { path: "/statistics/all", name: "앱 매출통계" },
  { path: "/usages/reservations/list", name: "예약/사용처리" },
  { path: "/alarms/send-push", name: "알림전송" },
  { path: "/pages/events/list", name: "페이지 관리" },
  { path: "/menus/programs", name: "메뉴관리" },
  { path: "/branchs/list", name: "지점관리" },
  { path: "/beautists/list", name: "뷰티스트 관리" },
  { path: "/accounts/list", name: "계정관리" },
  { path: "/notices/list", name: "공지/안내 관리" },
  { path: "/products/categories/list", name: "제품관리" },
];

const dashboardRoutes = {
  users: [
    {
      name: "고객정보",
      onlyHeadquater: false,
      sub: [
        {
          path: "/users/list",
          name: "고객명단",
          icon: "nc-icon nc-simple-delete",
          component: UserList,
          layout: "/admin",
        },
        {
          path: "/users/add",
          component: AddUser,
          layout: "/admin",
        },
        {
          path: "/users/edit/:id",
          component: EditUser,
          layout: "/admin",
        },
        {
          path: "/users/usages",
          name: "고객 사용내역 조회",
          icon: "nc-icon nc-simple-delete",
          component: UserUsageList,
          layout: "/admin",
        },
        {
          path: "/users/history/:id",
          component: UserUsageHistory,
          layout: "/admin",
        },
      ],
    },
  ],
  statistics: [
    {
      name: "앱 매출통계",
      onlyHeadquater: false,
      sub: [
        {
          path: "/statistics/all",
          name: "전체 매출",
          icon: "nc-icon nc-simple-delete",
          component: AllList,
          layout: "/admin",
        },
        {
          path: "/statistics/branch",
          name: "지점 매출",
          icon: "nc-icon nc-simple-delete",
          component: ListByBranch,
          layout: "/admin",
        },
        {
          path: "/statistics/cancel/:id",
          component: CancelPayment,
          layout: "/admin",
        },
      ],
    },
  ],
  usages: [
    {
      name: "예약/사용처리",
      onlyHeadquater: false,
      sub: [
        {
          path: "/usages/reservations/list",
          name: "예약현황",
          icon: "nc-icon nc-simple-delete",
          component: Reservations,
          layout: "/admin",
        },
        {
          path: "/usages/reservate/:id",
          component: Reservate,
          layout: "/admin",
        },
        {
          path: "/usages/edit-reservation",
          name: "예약조회/수정",
          icon: "nc-icon nc-simple-delete",
          component: EditReservation,
          layout: "/admin",
        },
        {
          path: "/usages/use-reservation",
          name: "사용처리",
          icon: "nc-icon nc-simple-delete",
          component: UseReservation,
          layout: "/admin",
        },
        // {
        //   path: "/usages/manage-chart",
        //   name: "관리차트",
        //   icon: "nc-icon nc-simple-delete",
        //   component: ManageChart,
        //   layout: "/admin",
        // },
        {
          path: "/usages/view-diary/:id",
          component: ViewDiary,
          layout: "/admin",
        },
        {
          path: "/usages/coupon/list",
          name: "쿠폰리스트",
          icon: "nc-icon nc-simple-delete",
          component: CouponList,
          layout: "/admin",
        },
        {
          path: "/usages/coupon/add",
          component: AddCoupon,
          layout: "/admin",
        },
        {
          path: "/usages/coupon/pay/:id",
          component: CouponPay,
          layout: "/admin",
        },
        {
          path: "/usages/coupon/edit/:id",
          component: EditCoupon,
          layout: "/admin",
        },
      ],
    },
  ],
  alarms: [
    {
      name: "알림전송",
      onlyHeadquater: true,
      sub: [
        {
          path: "/alarms/send-push",
          name: "푸시알림 전송",
          icon: "nc-icon nc-simple-delete",
          component: SendPush,
          layout: "/admin",
        },
        {
          path: "/alarms/push-history",
          name: "푸시알림 내역",
          icon: "nc-icon nc-simple-delete",
          component: PushHistory,
          layout: "/admin",
        },
        {
          path: "/alarms/auto-push",
          name: "자동푸시 설정",
          icon: "nc-icon nc-simple-delete",
          component: AutoPush,
          layout: "/admin",
        },
        {
          path: "/alarms/send-kakaotalk",
          name: "카카오톡 전송",
          icon: "nc-icon nc-simple-delete",
          component: SendKakaotalk,
          layout: "/admin",
        },
        {
          path: "/alarms/kakaotalk-history",
          name: "카카오톡 내역",
          icon: "nc-icon nc-simple-delete",
          component: KakaotalkHistory,
          layout: "/admin",
        },
      ],
    },
  ],
  pages: [
    {
      name: "페이지 관리",
      onlyHeadquater: true,
      sub: [
        {
          path: "/pages/events/list",
          name: "이벤트",
          icon: "nc-icon nc-simple-delete",
          component: Event,
          layout: "/admin",
        },
        {
          path: "/pages/events/add",
          component: AddEvent,
          layout: "/admin",
        },
        {
          path: "/pages/events/edit/:id",
          component: EditEvent,
          layout: "/admin",
        },
        {
          path: "/pages/notices/list",
          name: "공지사항",
          icon: "nc-icon nc-simple-delete",
          component: PageNotices,
          layout: "/admin",
        },
        {
          path: "/pages/notices/add",
          component: PageAddNotice,
          layout: "/admin",
        },
        {
          path: "/pages/notices/edit/:id",
          component: PageEditNotice,
          layout: "/admin",
        },
        {
          path: "/pages/faq/list",
          name: "자주묻는질문",
          icon: "nc-icon nc-simple-delete",
          component: FAQ,
          layout: "/admin",
        },
        {
          path: "/pages/faq/add",
          component: AddFAQ,
          layout: "/admin",
        },
        {
          path: "/pages/faq/edit/:id",
          component: EditFAQ,
          layout: "/admin",
        },
        {
          path: "/pages/reports/list",
          name: "불편신고",
          icon: "nc-icon nc-simple-delete",
          component: Report,
          layout: "/admin",
        },
        {
          path: "/pages/reports/answer/:id",
          component: AnswerReport,
          layout: "/admin",
        },
      ],
    },
  ],
  menus: [
    {
      name: "메뉴관리",
      onlyHeadquater: true,
      sub: [
        {
          path: "/menus/programs",
          name: "프로그램",
          icon: "nc-icon nc-simple-delete",
          component: Programs,
          layout: "/admin",
        },
        {
          path: "/menus/categories",
          component: MenuCategories,
          layout: "/admin",
        },
        {
          path: "/menus/tickets/list",
          name: "선불권",
          icon: "nc-icon nc-simple-delete",
          component: Tickets,
          layout: "/admin",
        },
        {
          path: "/menus/tickets/add",
          component: AddTicket,
          layout: "/admin",
        },
        {
          path: "/menus/tickets/edit/:id",
          component: EditTicket,
          layout: "/admin",
        },
        {
          path: "/menus/variables",
          name: "제어 변수 관리",
          icon: "nc-icon nc-simple-delete",
          component: Variables,
          layout: "/admin",
        },
      ],
    },
  ],
  branchs: [
    {
      name: "지점관리",
      onlyHeadquater: true,
      sub: [
        {
          path: "/branchs/list",
          name: "지점리스트",
          icon: "nc-icon nc-simple-delete",
          component: Branchs,
          layout: "/admin",
        },
        {
          path: "/branchs/add",
          component: AddBranch,
          layout: "/admin",
        },
        {
          path: "/branchs/edit/:id",
          component: EditBranch,
          layout: "/admin",
        },
        {
          path: "/branchs/adjustment",
          name: "지점별 정산 관리",
          icon: "nc-icon nc-simple-delete",
          component: BranchAdjustment,
          layout: "/admin",
        },
      ],
    },
  ],
  beautists: [
    {
      name: "뷰티스트 관리",
      onlyHeadquater: false,
      sub: [
        {
          path: "/beautists/list",
          name: "뷰티스트 명단",
          icon: "nc-icon nc-simple-delete",
          component: Beautists,
          layout: "/admin",
        },
        {
          path: "/beautists/add",
          component: AddBeautist,
          layout: "/admin",
        },
        {
          path: "/beautists/edit/:id",
          component: EditBeautist,
          layout: "/admin",
        },
        {
          path: "/beautists/adjustment/list",
          name: "뷰티스트 정산 관리",
          icon: "nc-icon nc-simple-delete",
          component: BeautistAdjustment,
          layout: "/admin",
        },
        {
          path: "/beautists/adjustment/branch/:id",
          component: BeautistAdjustmentByBranch,
          layout: "/admin",
        },
      ],
    },
  ],
  accounts: [
    {
      name: "계정관리",
      onlyHeadquater: false,
      sub: [
        {
          path: "/accounts/list",
          name: "지점 계정 관리",
          icon: "nc-icon nc-simple-delete",
          component: Account,
          layout: "/admin",
        },
        {
          path: "/accounts/identifier",
          name: "아이디 변경",
          icon: "nc-icon nc-simple-delete",
          component: Identifier,
          layout: "/admin",
        },
        {
          path: "/accounts/password",
          name: "비밀번호 변경",
          icon: "nc-icon nc-simple-delete",
          component: Password,
          layout: "/admin",
        },
      ],
    },
  ],
  notices: [
    {
      name: "공지/안내 관리",
      onlyHeadquater: false,
      sub: [
        {
          path: "/notices/list",
          name: "공지사항",
          icon: "nc-icon nc-simple-delete",
          component: Notices,
          layout: "/admin",
        },
        {
          path: "/notices/add",
          component: AddNotice,
          layout: "/admin",
        },
        {
          path: "/notices/edit/:id",
          component: EditNotice,
          layout: "/admin",
        },
        {
          path: "/notices/view/:id",
          component: ViewNotice,
          layout: "/admin",
        },
        {
          path: "/notices/qna/list",
          name: "Q&A",
          icon: "nc-icon nc-simple-delete",
          component: QNA,
          layout: "/admin",
        },
        {
          path: "/notices/qna/add",
          component: AddQNA,
          layout: "/admin",
        },
        {
          path: "/notices/qna/edit/:id",
          component: EditQNA,
          layout: "/admin",
        },
        {
          path: "/notices/qna/view/:id",
          component: ViewQNA,
          layout: "/admin",
        },
        {
          path: "/notices/qna/answer/:id",
          component: AnswerQNA,
          layout: "/admin",
        },
        {
          path: "/notices/qna/answerEdit/:id",
          component: EditAnswersQNA,
          layout: "/admin",
        },
      ],
    },
  ],
  products: [
    {
      name: "제품관리",
      onlyHeadquater: false,
      sub: [
        {
          path: "/products/categories/list",
          name: "카테고리 관리",
          icon: "nc-icon nc-simple-delete",
          component: Categories,
          layout: "/admin",
        },
        {
          path: "/products/list",
          name: "상품 관리",
          icon: "nc-icon nc-simple-delete",
          component: Products,
          layout: "/admin",
        },
        {
          path: "/products/add/:id",
          component: AddProduct,
          layout: "/admin",
        },
        {
          path: "/products/edit/:id",
          component: EditProduct,
          layout: "/admin",
        },
        {
          path: "/products/banners/list",
          name: "배너 관리",
          icon: "nc-icon nc-simple-delete",
          component: Banners,
          layout: "/admin",
        },
        {
          path: "/products/banners/add",
          component: AddBanner,
          layout: "/admin",
        },
        {
          path: "/products/banners/edit/:id",
          component: EditBanner,
          layout: "/admin",
        },
        {
          path: "/products/manage/count",
          name: "지점별 재고관리",
          icon: "nc-icon nc-simple-delete",
          component: ManageCount,
          layout: "/admin",
        },
      ],
    },
  ],
  policy: [
    {
      name: "약관",
      onlyHeadquater: false,
      sub: [
        {
          path: "/policy/privacy",
          name: "개인정보 처리방침",
          component: Privacy,
          layout: "/admin",
        },
      ],
    },
  ],
};

export default dashboardRoutes;
