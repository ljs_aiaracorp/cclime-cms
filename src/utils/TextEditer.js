import React, { useMemo, useRef } from "react";
import axios from "axios";
import ReactQuill, { Quill } from "react-quill";
import "react-quill/dist/quill.snow.css";
import ImageResize from "quill-image-resize";

export const TextEditer = ({ value, onChange }) => {
  const AlignStyle = Quill.import("attributors/style/align");
  Quill.register("modules/ImageResize", ImageResize);
  Quill.register(AlignStyle, true);

  const quillRef = useRef();

  // Image Url get
  const imageHandler = () => {
    const input = document.createElement("input");

    input.setAttribute("type", "file");
    input.setAttribute("accept", "image/*");
    input.click();

    input.addEventListener("change", async () => {
      const file = input.files[0];

      const formData = new FormData();
      formData.append("file", file);

      axios
        .post("/api/images", formData)
        .then(async (res) => {
          const IMG_URL = res.data.data;

          const editor = quillRef.current.getEditor();
          const range = editor.getSelection();
          editor.insertEmbed(range.index, "image", IMG_URL);
        })
        .catch((err) => {
          if (err.response.status === 401) {
            if (err.response.data.error === "Unauthorized") {
              // 토큰 재발급
              const isCheck = tokenCheck();
              isCheck.then((res) => {
                if (!res) return;
                else imageHandler();
              });
            } else {
              alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
            }
          } else {
            alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          }
        });
    });
  };

  const modules = useMemo(
    () => ({
      toolbar: {
        container: [
          [{ header: [1, 2, 3, 4, 5, false] }],
          ["bold", "italic", "underline", "strike", "blockquote"],
          [{ color: [] }, { background: [] }],
          [{ list: "ordered" }, { list: "bullet" }],
          [
            { align: "" },
            { align: "center" },
            { align: "right" },
            { align: "justify" },
          ],
          ["link", "image"],
          ["clean"],
        ],
        handlers: {
          image: imageHandler,
        },
      },
      ImageResize: { modules: ["Resize"] },
    }),
    []
  );

  const formats = [
    "header",
    "bold",
    "italic",
    "underline",
    "strike",
    "blockquote",
    "size",
    "color",
    "list",
    "bullet",
    "indent",
    "link",
    "image",
    "align",
  ];

  return (
    <ReactQuill
      ref={quillRef}
      className="ReactQuill-Fontcolor-black"
      style={{ height: "600px" }}
      theme="snow"
      modules={modules}
      formats={formats}
      value={value || ""}
      onChange={(content, delta, source, editor) => onChange(editor.getHTML())}
    />
  );
};
