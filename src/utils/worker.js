const onmessage = function (e) {
  if (e.data[1]) return;
  if (e.data[0] === 'initLrane') {
    // window.Lrane.init({
    //   apiKEY: process.env.REACT_APP_LRANE_API_KEY, // Required value
    // });
    postMessage('initLrane');
  }
  if (e.data[0] === 'initCrane') {
    // window.Crane.init({
    //   apiKEY: process.env.REACT_APP_CRANE_API_KEY,//'ScIw6l1lWN9Gg7pivdSmY5yGKVwFVHKW8TvlqMBC',
    //   baseURL: 'https://wsdk.urbanbase.com/crane_js/sample',
    //   cfURL: window.location.origin,
    //   pathData: {
    //     units: 'units',
    //     assets: 'assets',
    //     posts: 'posts',
    //     materials: 'materials',
    //   },
    // });
    postMessage('initCrane');
  }
};

let code = 'onmessage=' + onmessage.toString();

const blob = new Blob([code], { type: "application/javascript" });
const worker_script = URL.createObjectURL(blob);

export default worker_script;
