export const isValidDate = (dateStr) => {
  const date = new Date(dateStr);
  return !isNaN(date.getTime());
};

export const isValidUrl = (urlStr) => {
  const pattern = new RegExp(
    "^(https?:\\/\\/)?" + // protocol
      "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
      "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
      "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
      "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
      "(\\#[-a-z\\d_]*)?$",
    "i"
  ); // fragment locator
  return !!pattern.test(urlStr);
};

export const isValidEmail = (emailStr) => {
  const pattern =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return pattern.test(String(emailStr).toLowerCase());
};

export const isValidMobile = (mobileStr) => {
  // const pattern = /^(?:(010\d{4})|(01[1|6|7|8|9]\d{3,4}))(\d{4})$/;
  const pattern = /^01(?:0|1|[6-9])-(?:\d{3}|\d{4})-\d{4}$/;
  return pattern.test(String(mobileStr).toLowerCase());
};

export const isValidPhone = (phoneStr) => {
  const pattern = /^(0(2|3[1-3]|4[1-4]|5[1-5]|6[1-4]|70|80))(\d{3,4})(\d{4})$/;
  return pattern.test(String(phoneStr).toLowerCase());
};

export const isValidMobileOrPhone = (str) => {
  return isValidMobile(str) || isValidPhone(str);
};

export const isBusinessNumber = (strBusiness) => {
  const pattern = /^(\d{3})-(\d{2})-(\d{5})$/;
  return pattern.test(String(strBusiness).toLowerCase());
};

export const isValidId = (str) => {
  const pattern = /^[A-Za-z0-9+]*$/;
  return pattern.test(String(str).toLowerCase());
};
