import axios from "axios";

// userInform param Setter
let userRecode = {
  apiUrl: "",
  accessPath: "",
  store_name: "",
  user_id: "",
  token: "",
  accessor: "",
};

let loginUser = null;
let accessUser = null;
let accessPathName = null;
let accessor = null;
let userToken = null;
let userRefreshToken = null;
let userStoreName = null;

const getUser = () => {
  loginUser = JSON.parse(localStorage.getItem("admin")); // Login User Check
  accessUser = loginUser ? loginUser.data.role : null; // Login User role
  accessPathName = accessUser === "main" ? "/admin" : "/staff"; // user Path
  accessor = accessUser; // user accessor check
  userToken = loginUser ? loginUser.data.token.accessToken : null; // user Token
  userRefreshToken = loginUser ? loginUser.data.token.refreshToken : null; // user refresh Token
  userStoreName = loginUser ? loginUser.data.store_name : null; // user Store Name
};

// Token Refresh function
export const tokenCheck = async () => {
  const data = {
    accessToken: userToken,
    refreshToken: userRefreshToken,
  };
  const res = await axios
    .post(`/api/auth/adminReissue`, data, {
      headers: { "Content-Type": `application/json` },
    })
    .then((res) => {
      getUser();
      loginUser.data.token.accessToken = res.data.accessToken;
      loginUser.data.token.refreshToken = res.data.refreshToken;
      loginUser.data.token.accessTokenExpiresIn = res.data.accessTokenExpiresIn;
      userToken = res.data.accessToken;
      userRefreshToken = res.data.refreshToken;
      localStorage.setItem("admin", JSON.stringify(loginUser));
      return true;
    })
    .catch((err) => {
      if (err.response.status === 401) {
        if (err.response.data.error === "UNAUTHORIZED") {
          alert("로그인 정보가 만료되었습니다.\n다시 로그인해 주세요.");
          localStorage.removeItem("admin");
          window.location.href = "/Login";
          return false;
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          return false;
        }
      } else if (err.response.status === 400) {
        if (err.response.data.code === 5) {
          alert("로그인 정보가 만료되었습니다.\n다시 로그인해 주세요.");
          localStorage.removeItem("admin");
          window.location.href = "/Login";
          return false;
        } else {
          alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
          return false;
        }
      } else {
        alert("시스템오류가 발생하였습니다. \n관리자에게 문의하세요.");
        return false;
      }
    });
  return res;
};

export const axiosUrlFunction = (url, search = null) => {
  getUser();
  if (search === "검색") {
    if (url === "storeList" || url === "couponList" || url === "grade/coupon") {
      userRecode.apiUrl = `/api/admin/${url}`;
    } else {
      // admin or staff url
      userRecode.apiUrl = `/api${accessPathName}/${url}`;
    }
  } else {
    // 통합 api url
    userRecode.apiUrl = `/api/staff/${url}`;
  }

  if (search === "프로그램") {
    if (accessor === "main") {
      userRecode.apiUrl = `/api/admin/${url}`;
    } else if (accessor === "store") {
      userRecode.apiUrl = `/api/store/${url}`;
    }
  }
  if (search === "뷰티스트") {
    if (accessor === "main") {
      userRecode.apiUrl = `/api/admin/${url}`;
    } else if (accessor === "store") {
      userRecode.apiUrl = `/api/store/${url}`;
    }
  }
  if (search === "알림") {
    userRecode.apiUrl = `/api/admin/${url}`;
  }
  userRecode.accessPath = accessPathName;
  userRecode.store_name = userStoreName;
  userRecode.token = userToken;
  userRecode.accessor = accessor;
  userRecode.user_id = loginUser.data.id;
  return userRecode;
};
