import axios from "axios";
import moment from "moment";

// 비즈뿌리오 인증 토큰 리턴
export const getBizppurioToken = async () => {
  const tokenInfo = JSON.parse(localStorage.getItem("bizppurioToken"));
  if (tokenInfo) {
    // 발급받은 토큰이 있고
    const expired = Number(tokenInfo.expired);
    const current = Number(moment().format("YYYYMMDDHHmmSS"));
    if (expired > current) {
      // 토큰이 만료되지 않은 경우 저장된 토큰값 리턴
      return `${tokenInfo.type} ${tokenInfo.accesstoken}`;
    }
  }
  // 발급받은 토큰이 없거나 토큰이 만료된 경우 인증 토큰 발급 api 호출
  try {
    const response = await axios.post(
      `/ppurio/v1/token`,
      {},
      {
        headers: {
          Authorization: `Basic ${window.btoa(
            `${process.env.REACT_APP_PPURIO_ID}:${process.env.REACT_APP_PPURIO_PW}`
          )}`,
          "Content-Type": "application/json; charset=utf-8",
        },
      }
    );
    // 토큰 발급 성공시 토큰 데이터 저장 및 토큰값 리턴
    localStorage.setItem("bizppurioToken", JSON.stringify(response.data));
    return `${response.data.type} ${response.data.accesstoken}`;
  } catch (e) {
    // 토큰 발급 실패시 에러 메세지 출력 및 null 리턴
    console.error(e);
    alert(
      `비즈뿌리오 인증 토큰 발급에 실패했습니다.\n에러 코드: ${e.response.data.code}\n에러 메세지: ${e.response.data.description}`
    );
    return null;
  }
};

// 알림톡 전송
export const sendMessage = async (token, body) => {
  try {
    // api 호출 및 promise 리턴
    return await axios.post(
      `/ppurio/v3/message`,
      {
        ...body,
        account: process.env.REACT_APP_PPURIO_ID,
        type: "ft",
        from: "025124868",
      },
      {
        headers: {
          Authorization: token,
          "Content-Type": "application/json; charset=utf-8",
        },
      }
    );
  } catch (e) {
    // 전송 실패시 에러 메세지 출력 및 null 리턴
    console.error(e);
    alert(
      `메세지 전송에 실패했습니다.\n에러 코드: ${e.response.data.code}\n에러 메세지: ${e.response.data.description}`
    );
    return null;
  }
};
