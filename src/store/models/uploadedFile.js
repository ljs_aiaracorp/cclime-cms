import { Form, Col, Row } from "react-bootstrap";
import { text } from "../../views/css/style";

class UploadedFile {
  name = "";
  id = "";
  accept = "";
  file = null;
  url = "";
  sizeText = "";

  constructor(name, id, accept, url, sizeText = '') {
    this.name = name;
    this.id = id;
    this.accept = accept;
    this.url = url;
    this.sizeText = sizeText;
  }

  getFileNameFromUrl = () => {
    if (Array.isArray(this.url)) {
      for (const u of this.url) {
        const arr = u.split('/');
        return decodeURI(arr.pop());
      }
      return "업로드";
    }
    const arr = this.url.split('/');
    return decodeURI(arr.pop());
  };

  render = (onUpload) => {
    return (
      <Form.Group as={Row} className="mb-2">
        {text(this.name)}
        <Col sm={2}>
          <Form.File id={this.id} label={this.file ? this.file.name : (this.url ? this.getFileNameFromUrl() : "업로드")} accept={this.accept} custom onChange={(e) => {
            if (e.target.files.length === 0) {
              return;
            }
            if (onUpload) {
              onUpload(e);
            }
            $("#" + this.id + " + label").text(this.file[0] ? this.file[0].name : this.file.name);
          }} />
        </Col>
        {this.sizeText && <Col sm={3} className="pt-2">
          {this.sizeText}
        </Col>}
      </Form.Group>
    );
  };
};

export default UploadedFile;