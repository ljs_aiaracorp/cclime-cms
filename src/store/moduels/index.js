import { combineReducers } from "redux";
import { login } from "./reducer";

export const rootReducer = combineReducers({ login });
// 다른 리듀서를 만들게 되면 여기에 넣어줌
