const admin = JSON.parse(localStorage.getItem("admin"));

export const login_initialState = admin
  ? { isLoggedIn: true, admin }
  : { isLoggedIn: false, user: null };
