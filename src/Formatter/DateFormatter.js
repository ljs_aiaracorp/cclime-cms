export function getFormatDate(date) {
  const dateSplitBy = date.split("-");
  const year = dateSplitBy[0];
  const month = dateSplitBy[1];
  const daySplit = dateSplitBy[2].split("T");
  const day = daySplit[0];
  return year + ". " + month + ". " + day;
  // yyyy. mm. dd 로 변환해줌
}

export function getToday() {
  const date = new Date();
  const year = date.getFullYear();
  const month = ("0" + (1 + date.getMonth())).slice(-2);
  const day = ("0" + date.getDate()).slice(-2);

  return year + ". " + month + ". " + day;
}
// 오늘 날짜를 yyyy-MM-dd 형태로 리턴해줌
