import axios from "axios";

// Redux-thunk 미들웨어를 통한 API 통신 비동기 작업으로 로그인 및 로그아웃 처리가 가능합니다.

const login = (send_body) => {
  // return axios.post(`${process.env.REACT_APP_API_URL}/admins/login`, send_body).then((response) => {
  //   if (response.data.accessToken) {
  //     const {
  //       accessToken,
  //       accessTokenCurrentAt,
  //       accessTokenExpireAt,
  //       refreshToken,
  //       refreshTokenExpireAt,
  //     } = response.data;
  //     localStorage.setItem(
  //       "admin",
  //       JSON.stringify({
  //         accessToken,
  //         accessTokenCurrentAt,
  //         accessTokenExpireAt,
  //         refreshToken,
  //         refreshTokenExpireAt,
  //       })
  //     );
  //   } else {
  //     alert("액세스 토큰이 존재하지 않습니다!");
  //   }
  //   return response.data;
  // });
};

const logout = () => {
  if (localStorage.getItem("admin")) {
    localStorage.removeItem("admin");
    alert("로그아웃 되었습니다!");
  }
  window.location.href = "/Login";
};

export default {
  login,
  logout,
};
