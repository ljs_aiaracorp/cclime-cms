// 매 요청 시 액세스토큰을 헤더로 담을때 사용함
const admin = localStorage.getItem("admin")
  ? JSON.parse(localStorage.getItem("admin"))
  : null;

export const authHeader = admin
  ? {
      // headers: { Authorization: `Bearer ${admin.accessToken}` },
    }
  : null;

export const authHeaderMultipart = admin
  ? {
      headers: {
        // Authorization: `Bearer ${admin.accessToken}`,
        'Content-Type': 'multipart/form-data'
      },
    }
  : null;

  export const authHeaderJson = admin
  ? {
      headers: {
        // Authorization: `Bearer ${admin.accessToken}`,
        'Content-Type': 'application/json'
      },
    }
  : null;

export const SendHeader = admin
  ? {
    // Authorization: `Bearer ${admin.accessToken}`
  }
  : null;

// export const authHeader = () => {
//   const admin = JSON.parse(localStorage.getItem("admin"));

//   if (admin && admin.accessToken) {
//     return {
//       Authorization: "Bearer " + admin.accessToken,
//       withCredentials: true,
//     };
//   } else {
//     return {};
//   }
// };
