export const RESERVATION =
  "예약 취소 기간 : 앱에서 예약을 취소할 수 있는 기간을 변경할 수 있습니다.\n\
예약 변경 기간 : 앱에서 예약을 변경할 수 있는 기간을 변경할 수 있습니다.\n\
예약 가능 기간: 앱에서 예약을 진행할 수 있는 기간을 변경할 수 있습니다.\n\
알립니다 pop-up : 앱에 알립니다 pop-up 내용을 변경할 수 있습니다.";

export const MEMBER_SHIP =
  "베이직 : 앱에 표시되는 베이직 등급 안내 문구를 수정할 수 있습니다.\n\
골드 : 앱에 표시되는 골드 등급 안내 문구를 수정할 수 있습니다.\n\
플래티넘 : 앱에 표시되는 플래티넘 등급 안내 문구를 수정할 수 있습니다.\n\
에메랄드 : 앱에 표시되는 에메랄드 등급 안내 문구를 수정할 수 있습니다.\n\
사파이어 : 앱에 표시되는 사파이어 등급 안내 문구를 수정할 수 있습니다.\n\
다이아몬드 : 앱에 표시되는 다이아몬드 등급 안내 문구를 수정할 수 있습니다.\n\
회원등급 유의사항 문구 수정 : 앱 회원 등급 유의사항 안내 문구를 수정할 수 있습니다.";

export const BUSINESS_INFO =
  "앱의 사이드메뉴에 나오는 사업자정보 내용을 수정할 수 있습니다.";

export const TOS =
  "개인정보 처리방침 : 앱 회원가입시 필요한 개인정보 처리방침 내용을 수정할 수 있습니다.\n\
이용약관 : 앱 이용약관 내용을 수정할 수 있습니다.\n\
회원약관 : 앱 회원가입이 필요한 회원약관 내용을 수정할 수 있습니다.\n\
환불규정 : 앱 결제취소시 환불규정 내용을 수정할 수 있습니다.\n\
개인정보 활용 약관 : 앱의 개인정보 활용 약관 내용을 수정할 수 있습니다.";

export const ADDITIONAL_INFO0 =
  "앱 회원가입시 관심 있는 부가정보 설문 내용을 변경할 수 있습니다.\n\
우선순위 : 질문이 표시되는 순서를 설정할 수 있습니다. 우선순위의 숫자가 낮을수록 질문이 먼저 표시됩니다.\n\
부가정보 내용 : 설문 내용을 설정할 수 있습니다.";

export const ADDITIONAL_INFO3 =
  "앱 회원가입 시 끌리메 이용경로 설문 내용을 변경할 수 있습니다.\n\
우선순위 : 질문이 표시되는 순서를 설정할 수 있습니다. 우선순위의 숫자가 낮을수록 질문이 먼저 표시됩니다.\n\
부가정보 내용 : 설문 내용을 설정할 수 있습니다.";
