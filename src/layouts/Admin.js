import React from "react";
import { useSelector } from "react-redux";
import { useLocation, Route, Switch } from "react-router-dom";

import AdminNavbar from "components/Navbars/AdminNavbar";
import Sidebar from "components/Sidebar/Sidebar";
import routes from "routes.js";

function Admin() {
  const loginUser = useSelector((state) => state.login.admin); // Login User Check
  const accessUser = loginUser ? loginUser.data.role : null; // Login User role

  const [color, setColor] = React.useState("black");
  const location = useLocation();
  const mainPanel = React.useRef(null);
  const getRoutes = (routes) => {
    return routes.map((prop, key) => {
      if (accessUser === "main") {
        if (prop.path !== "/products/manage/count") {
          return (
            <Route
              path={prop.layout + prop.path}
              render={(props) => <prop.component {...props} />}
              key={key}
            />
          );
        }
      } else if (accessUser === "store") {
        if (
          prop.path !== "/accounts/list" &&
          prop.path !== "/users/usages" &&
          prop.path !== "/statistics/all" &&
          prop.path !== "/beautists/adjustment/list" &&
          prop.path !== "/products/categories/list" &&
          prop.path !== "/products/list" &&
          prop.path !== "/products/add/:id" &&
          prop.path !== "/usages/coupon/list"
        ) {
          return (
            <Route
              path={"/staff" + prop.path}
              render={(props) => <prop.component {...props} />}
              key={key}
            />
          );
        }
      } else if (accessUser === "staff") {
        if (
          prop.path !== "/accounts/list" &&
          prop.path !== "/users/usages" &&
          prop.path !== "/statistics/all" &&
          prop.path !== "/beautists/list" &&
          prop.path !== "/beautists/adjustment/list" &&
          prop.path !== "/products/categories/list" &&
          prop.path !== "/products/list" &&
          prop.path !== "/products/add/:id" &&
          prop.path !== "/usages/coupon/list"
        ) {
          return (
            <Route
              path={"/staff" + prop.path}
              render={(props) => <prop.component {...props} />}
              key={key}
            />
          );
        }
      } else {
        if (prop.path.indexOf('/policy') > -1) {
          return (
            <Route
              path={"/admin" + prop.path}
              render={(props) => <prop.component {...props} />}
              key={key}
            />
          );
        }
      }
    });
  };
  React.useEffect(() => {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    mainPanel.current.scrollTop = 0;
    if (
      window.innerWidth < 993 &&
      document.documentElement.className.indexOf("nav-open") !== -1
    ) {
      document.documentElement.classList.toggle("nav-open");
      var element = document.getElementById("bodyClick");
      element.parentNode.removeChild(element);
    }
  }, [location]);

  const filterRoutes = () => {
    const key = window.location.pathname.split("/")[2];
    return routes[key];
  };

  const getAllRoutes = () => {
    let list = [];
    for (const key in routes) {
      for (const menu of routes[key]) {
        if (accessUser === "main") {
          if (menu.sub) {
            list = list.concat(menu.sub);
          }
        } else {
          if (!menu.onlyHeadquater) {
            list = list.concat(menu.sub);
          }
        }
      }
    }
    return list;
  };

  return (
    <>
      <div className="wrapper">
        <div>
          <AdminNavbar />
        </div>
        <div style={{ display: "flex" }}>
          <Sidebar color={color} routes={filterRoutes()} navHeight={124} />
          <div
            className="main-panel"
            ref={mainPanel}
            style={{
              width: "100%",
              marginLeft: window.location.pathname.indexOf("/admin/policy") === -1 ? "193px" : "0px",
              backgroundColor: "transparent",
              float: "unset",
            }}
          >
            <div className="content">
              <Switch>{getRoutes(getAllRoutes(routes))}</Switch>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Admin;
