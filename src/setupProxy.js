const { createProxyMiddleware } = require("http-proxy-middleware");

module.exports = function (app) {
  app.use(
    createProxyMiddleware("/api", {
      target: process.env.REACT_APP_API,
      changeOrigin: true,
    })
  );
  app.use(
    createProxyMiddleware("/ppurio", {
      target: process.env.REACT_APP_PPURIO,
      changeOrigin: true,
      secure: false,
      pathRewrite: {
        "^/ppurio": "",
      },
    })
  );
};
